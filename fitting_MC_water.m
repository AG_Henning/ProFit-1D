function fitting_MC_water(filenameWater, waterPath, TE, TR, outputPath)

nuclues = '1H';
%read in the *.RAW LCModel file

%% load fit specific settings
pathName = 'DF data path';
pathBasisSets = pathToDataFolder(pathName, {'Basis_sets'});

fitSettingsFileName = 'Basis_sets\final_T2_met_MM_paper/FitSettings_sLASER_3Iter_T2_no_emg_sLASER_7ppm_moiety_UF_basis_TE24';
load([pathBasisSets, fitSettingsFileName], 'data', 'fitsettings');

%retrieve the water FID
[waterRef, bandwidth, scanFrequency, ~, ~] = ImportLCModelBasis(waterPath, filenameWater, false, nuclues);
waterBasisFileName = 'Basis_sets\final_T2_met_MM_paper/sLASER_7ppm_Water_TE24_basis.mat';
load([pathBasisSets, waterBasisFileName], 'basis');
water_basis = basis;

% 1D spectral data set
data.spectral_dimension = 1;

% add additional data set specific information and settings
data.dt         = [0, 1/bandwidth];
data.bw         = [0 bandwidth];

data.f0         = scanFrequency;      % Hz/ppm
data.te         = TE * 1e-3;
data.pts        = size(waterRef);
data.total_ppm  = data.bw ./ data.f0;
data.pts_ppm    = data.pts ./ data.total_ppm;
data.tr         = TR;
data.center_ppm = 4.7;
data.phasePivot_ppm = data.center_ppm; % in ppm

% set initial vector of time and frequency domain axis
data.t2 = (0:data.pts(2) - 1) * data.dt(2);
data.w1 = linspace(-data.bw(1)/2, data.bw(1)/2 - data.bw(1)/data.pts(1), data.pts(1));
data.w2 = linspace(-data.bw(2)/2, data.bw(2)/2 - data.bw(2)/data.pts(2), data.pts(2));
data.ppm2 = linspace(-data.total_ppm(2)/2, data.total_ppm(2)/2-data.total_ppm(2)/data.pts(2), data.pts(2)) + data.center_ppm;

% setup shift and tilting matrices
data.shift_t2_mx   = ones(data.pts(1),1)*data.t2;
data.lb_t1_mx      = data.te * ones(1, data.pts(2));

data.phase_1_f2_mx = data.ppm2 - data.phasePivot_ppm;

% automatic noise determination
[data.noise_var, data.noise_std] = calcNoiseLevel1D(waterRef, data);
% zero filling
[fid, fitsettings, data] = zero_fill(waterRef, fitsettings, data, data.zero_fill_factor);

%determine truncation point based on data
[data.truncPoint, data.decayPoint] = getTruncationPoint(waterRef);
data.truncPointMasked = getTruncationPoint(waterRef);
data = getFidWeighting(data, 'trunc');

%% fit the water reference
[waterRef, concH2O] = fitWater(waterRef, water_basis, data);
save([outputPath, filenameWater, '_profit.mat'], 'concH2O');