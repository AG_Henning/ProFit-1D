function fullFileName = createBasisSetFromLCModelFiles()

plotBasis = true;

%% add metabolites
pathName = 'DF data path';
sampleCriteria = {'Basis_sets'};
[filePath] = pathToDataFolder(pathName, sampleCriteria);

metaboliteNames = {'Leu', ...
    'Asp', 'Cr',                   'Cr_CH2',               'GABA', 'Gln', 'Glu', ...
    'Glyc', 'GSH', 'Lac', 'mI', 'NAA_as', 'NAA_ac', 'NAAG', 'tCho_P',  'Scyllo', 'Tau'};
metaboliteFileNames = {'MMB_without_metabolite_TE24', ...
    'Asp', 'Cr_singlet_tot_3.028', 'Cr_singlet_tot_3.925', 'GABA', 'Gln', 'Glu',...
    'Glyc', 'GSH', 'Lac', 'mI', 'NAA_1',  'NAA_2',  'NAAG', 'tCho_PE', 'Scyllo', 'Tau'};    

filePathBasis = strcat(filePath,  'Basis_sets/final_T2_met_MM_paper/');
pathMM = 'sLASER_MM/';
pathMetabolites = 'sLASER_new_UF_7ppm/TE24/';

basisFileName = 'sLASER_7ppm_moiety_UF_basis_TE24';
fids = cell(1, length(metaboliteNames));

for indexMetabolite = 1:length(metaboliteNames)
    fileName = metaboliteFileNames{indexMetabolite};
    if strcmp(metaboliteNames{indexMetabolite},'Leu')
        filePathMetabolite = strcat(filePathBasis, pathMM);
    else
        filePathMetabolite = strcat(filePathBasis, pathMetabolites);
    end
    [fid, bandwidth, scanFrequency, metaboliteName, singletPresent] = ...
        ImportLCModelBasis(filePathMetabolite, fileName, plotBasis, '1H');
    if strcmp(metaboliteNames{indexMetabolite},'NAA_ac')
%         fid = fid * 3;
    end
    if strcmp(metaboliteNames{indexMetabolite},'Leu')
        fids{indexMetabolite}(1,:)= fid(1:4096).*1e8; % stupid change to make all metabolites 4096 long!
    else
        fids{indexMetabolite}(1,:)= fid;
    end
end

basis = basis_set;
basis.bw = [1, bandwidth];
basis.dt = 1 ./ basis.bw;
basis.f0 = scanFrequency*1e6;
basis.fid = fids;
basis.met = metaboliteNames;
basis.pts = size(fids{1});

fullFileName = [filePathBasis, basisFileName '.mat'];
save(fullFileName, 'basis')