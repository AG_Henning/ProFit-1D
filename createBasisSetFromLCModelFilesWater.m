function createBasisSetFromLCModelFilesWater()

plotBasis = true;

%% add metabolites
pathName = 'DF data path';
sampleCriteria = {'Basis_sets'};
[filePath] = pathToDataFolder(pathName, sampleCriteria);

metaboliteNames = {'H2O'};
metaboliteFileNames = {'Water'};    

filePathBasis = strcat(filePath,  'Basis_sets/final_T2_met_MM_paper/');
pathMetabolites = 'sLASER_4.66ppm/TE24/';

basisFileName = 'sLASER_7ppm_Water_TE24_basis';
fids = cell(1, length(metaboliteNames));

for indexMetabolite = 1:length(metaboliteNames)
    fileName = metaboliteFileNames{indexMetabolite};
    filePathMetabolite = strcat(filePathBasis, pathMetabolites);
    [fid, bandwidth, scanFrequency, metaboliteName, singletPresent] = ...
        ImportLCModelBasis(filePathMetabolite, fileName, plotBasis, '1H');
    fids{indexMetabolite}(1,:)= fid;
end

basis = basis_set;
basis.bw = [1, bandwidth];
basis.dt = 1 ./ basis.bw;
basis.f0 = scanFrequency*1e6;
basis.fid = fids;
basis.met = metaboliteNames;
basis.pts = size(fids{1});

save([filePathBasis, basisFileName '.mat'], 'basis')