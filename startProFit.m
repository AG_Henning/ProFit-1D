function startProFit(fileNameBasisSet)

% Adding the folder path to the ProFit library is clear by having the path
% of this file
currentPath = fileparts(mfilename('fullpath'));
addpath(genpath(currentPath))
% The MR_SpectroS library is however independent, hence, we need to set it
%additionally
if ~exist('ImportLCModelBasis', 'file')
    MR_SpectroS_path = uigetdir(currentPath, 'Please select the path where MR_SpectroS is in you file system');
    addpath(genpath(MR_SpectroS_path))
end

recreateBasisSet = questdlg('Create new basis set using the "createBasisSetFromLCModelFiles" function?', ...
	'ProFit-1D basis set', ...
	'Yes','No','No');
if (strcmp(recreateBasisSet,'Yes'))
    %create Basis set files
    % you should change/adapt the function to change the basis set.
    % Changing metabolite names leads also to a change in the Fitsettings
    fileNameBasisSet = createBasisSetFromLCModelFiles();
else
    if ~exist('fileNameBasisSet', 'var')
        fileNameBasisSet = [];
    end
end

fitSettingsFileName = createFitSettings_UI(fileNameBasisSet, currentPath);

if true
    [fileName,pathName]=uigetfile('*.mat', ...
        'Please select the water basis set .mat file.',...
        [currentPath '/sampleBasisSet/']);
    waterBasisFileName = [pathName fileName];
end

[filenameData,dataPath]=uigetfile('*.RAW', 'Please select preprocessed spectrum to fit',pwd);
[filenameWater,waterPath]=uigetfile('*.RAW', 'Please select preprocessed water reference to fit',pwd);

TE = 24; %ms
TR = 6000;%ms
refFreqPpm = -2.3;%ppm
prompt = {'Echo time (TE) [ms]:','Repetition time (TR) [ms]:', 'Reference Frequency acquisition compared to water [ppm]'};
dlgtitle = 'Spectral parameters';
dims = [1 35];
definput = {num2str(TE),num2str(TR), num2str(refFreqPpm)};
answer = inputdlg(prompt,dlgtitle,dims,definput);
TE = str2double(answer{1});
TR = str2double(answer{2});
refFreqPpm = str2double(answer{3});

outputPath = [uigetdir(dataPath, 'Please select the output path') '/'];

fittingProFit(filenameData, dataPath, filenameWater, waterPath, TE, TR, outputPath, refFreqPpm, fitSettingsFileName, waterBasisFileName);
plot_ProFit_Metabolites_Fit([filenameData, '_profit.mat'],outputPath, 'Met', true);
% sorry, concentrations are still not scaled with water.
end

function outputFile = createFitSettings_UI(fileNameBasisSet, currentPath)

if isempty(fileNameBasisSet)
    [fileName,pathName]=uigetfile('*.mat', ...
        'Please select basis set .mat file to create the fit settings.',...
        [currentPath '/sampleBasisSet/']);
    fileNameBasisSet = [pathName fileName];
else
    [pathName,fileName,~] = fileparts(fileNameBasisSet);
end
load(fileNameBasisSet, 'basis');

expression = 'basisset';
replace = 'fitSettings';
pathNameOutput = regexprep(pathName,expression,replace);

outputFile = [pathNameOutput 'FitSettings_semi_LASER_final_' fileName];
FitSettings_semi_LASER_final(basis, outputFile);
end