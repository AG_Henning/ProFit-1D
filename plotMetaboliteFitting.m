%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   PROFIT - Plot Metabolites and create pdf                              %
%                                                                         %
%   run code to plot metabolites and create a pdf                         %
%                                                                         %
%                                                                         %
%   Patrik Wyss (pawyss@biomed.ee.ethz.ch)                                %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotMetaboliteFitting

gammaData = 0;
opengl('software')

overrideExistingOnes=1; %If you want to override an existing PDF=1, else =0;
iterToRun=4:4;          % Specify the range of iterations for which
% pdfs are created, i.e. iterToRun=1:4 plots pdf
% for all 4 iterations


[pathToProfit,~,~]=fileparts(pwd);

addpath(genpath([pathToProfit '/profit3']));

[filenames, path] = uigetfile('*.mat',...               % Select files
    'Please select the .mat file to plot. ' ,pwd, 'MultiSelect','on');

numberOfFiles = size(filenames, 2);
%timing measurement setup
totalStartTime = tic;
fileStartTime = tic;
fileTimings = nan(1, numberOfFiles);
estimatedTotalDuration = Inf;


%handle single or multiple input files and call the profit batch process
if ischar(filenames)
    currentProfitFile=filenames;
    %concatenate the file to have it with full path
    currentProfitFileFullPath = [path currentProfitFile];
    
    display(['Start writing results to: ' currentProfitFile]);
    for iteration = iterToRun
        display(['iteration: ' num2str(iteration)]);
        %decide which plotting option to use (1D or 2D spectra)
        % load fitresult from file
        load(currentProfitFileFullPath)
%         calculateCorrelationMatrix(currentProfitFileFullPath,iteration, fitresult);
        if(size(fitresult{1}.fitted_spec, 1) > 1)
            plotMetaboliteFits2D(currentProfitFileFullPath,iteration, fitresult);
        else
            plotMetaboliteFits1D(currentProfitFileFullPath,iteration, fitresult, gammaData);
        end
    end
else
    disp(['You selected multiple files:' filenames])
    for fileCount = 1 : numberOfFiles
        if fileCount > 1
            previousFileTime             = toc(fileStartTime);
            fileStartTime                = tic;
            fileTimings(fileCount-1) = previousFileTime;
            estimatedTotalDuration       = mean(fileTimings(1:fileCount-1)) * (numberOfFiles-fileCount+1); % could use nanmean from statistics toolbox too
        end
        currentTimeElapsed = toc(totalStartTime);
        fprintf('Time elapsed so far:  %02d:%02d:%02d:%02d hh:mm:ss:hs\n', floor(currentTimeElapsed/3600), ...
            mod(floor(currentTimeElapsed/60),60), floor(mod(currentTimeElapsed,60)), round(100*mod(currentTimeElapsed,1)));
        fprintf('Estimated time to go: %02d:%02d:%02d:%02d hh:mm:ss:hs\n\n', floor(estimatedTotalDuration/3600), ...
            mod(floor(estimatedTotalDuration/60),60), floor(mod(estimatedTotalDuration,60)), round(100*mod(estimatedTotalDuration,1)));
        
        currentProfitFile=filenames{fileCount};
        %concatenate the file to have it with full path
        currentProfitFileFullPath = [path currentProfitFile];
        
        display(['Start writing results to: ' currentProfitFile]);
        for iteration = iterToRun
            display(['iteration: ' num2str(iteration)]);
            %decide which plotting option to use (1D or 2D spectra)
            % load fitresult from file
            load(currentProfitFileFullPath)
            
%             calculateCorrelationMatrix(currentProfitFileFullPath,iteration, fitresult);
            if(size(fitresult{1}.fitted_spec, 1) > 1)
                plotMetaboliteFits2D(currentProfitFileFullPath,iteration, fitresult);
            else
                plotMetaboliteFits1D(currentProfitFileFullPath,iteration, fitresult, gammaData);
            end
        end
    end
end

currentTimeElapsed = toc(totalStartTime);
fprintf('\n**************************************************************\n');
fprintf('Total time elapsed:  %02d:%02d:%02d:%02d hh:mm:ss:hs\n\n', floor(currentTimeElapsed/3600), ...
    mod(floor(currentTimeElapsed/60),60), floor(mod(currentTimeElapsed,60)), round(100*mod(currentTimeElapsed,1)));

display('Finished writting results.');