function FitSettings_sLASER_3Iter_prior_T2_no_emg(basis, outputFile)

%% initialize fit specific settings
data.quant_window_ppm = [ 0.0 0.6; ...
    0.0 4.1];

data.noise_window_ppm = [-5, -1];

data.zero_fill_factor    = 1;
data.debug               = false;
data.do_water_separation = false;
data.do_water_filtering  = true;


%% configure individual fit iterations
fitsettings = FitQueue(basis);

    function fititer = setBoundsEmLineshape(fititer, std_factor_T2, factor_T2app_true_T2)
        % Metabolite names and T2 times taken for starting point and overall upper and lower bounds for the em parameter.
        % These values are mostly from Murali-Manohar 2020 et al. 
        % However, the min T2 of 70 ms was chosen, plus maximal standard deviations were set to 25
        metabolites_T2  = {'Asp', 'Cr_CH2',   'Cr',       'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_ac',   'NAA_as',   'NAAG', 'tCho_P', 'Scyllo', 'Tau', 'Leu'};
        T2s             = [ 70,     82,         100,        85,    70,    85,     70,     75,    75,   90,    110,         100,        70,     90,       70,      70,   25]; %ms
%       upper variant was the used one, has slightly better results. Upper trace has cutoff T2 at 70, lower trace at 65
%       T2s             = [ 65,     82,         100,        75,    65,    85,     65,     75,    75,   90,    110,         100,        65,     90,       75,      75,   25]; %ms
        T2_std          = [ 12,     20,          26,        25,    20,    25,     10,     25,    25,   20,     30,          25,        20,     25,       25,      25,   10]; %ms
       
        for indexMet = 1:fititer.nr_mets
            if fititer.active_mets(indexMet) == 1
                currentMetabolite = fititer.basis.met{indexMet};
                if ~strcmp(currentMetabolite,'Leu') 
                    indexT2 = find(strcmp(metabolites_T2, currentMetabolite));
                    current_T2 = T2s(indexT2) * factor_T2app_true_T2;
                    current_T2_std = T2_std(indexT2);
                    current_em_starting = 1 / (pi * current_T2 * 1e-3);%from ms to s and then Hz
                    current_em_lb = 1 / (pi * (current_T2 + current_T2_std * std_factor_T2) * 1e-3);%from ms to s and then Hz
                    current_em_ub = 1 / (pi * (current_T2 - current_T2_std * std_factor_T2) * 1e-3);%from ms to s and then Hz
                    if current_em_ub > 30 %sanity check, we should not have lower T2 relaxation times than 10 ms
                        current_em_ub = 30; 
                    end
                    fititer.SetStartingValues({currentMetabolite}, {'em'}, current_em_starting);
                    fititer.SetLB({currentMetabolite}, {'em'}, current_em_lb);
                    fititer.SetUB({currentMetabolite}, {'em'}, current_em_ub);
                else
                    % don't set any bounds for Leu, since its em should be fixed to 0
                end
            end
        end
    end

%% set up first fitting iteration ************************************************************************
% ********************************************************************************************************

fititer = Iteration(basis);

fititer.ActivateMets({'NAA_ac', 'NAA_as', 'Cr', 'Cr_CH2', 'tCho_P', 'mI', 'Glu','Leu'});
fititer.ConnectMets({'name'}, {'conc',  'em', 'df2'});
fititer.ConnectMets({'global'}, {'gm', 'pc0', 'pc12', 'df2'});

fititer.ConnectMets({'Leu'}, {'gm'});

% fix parameters to certain values
fititer.FixMets({'Leu'}, {'gm'});
fititer.SetFixedValues({'Leu'}, {'gm'}, 0);%'Leu' is actually the MM spectrum
fititer.FixMets({'Leu'}, {'em'});
fititer.SetFixedValues({'Leu'}, {'em'}, 0);

% set up typical bounds for the first iteration
fititer.SetLB({'all'}, {'conc'}, 0);
fititer.SetLB({'all'}, {'pc0'}, -10);
fititer.SetLB({'all'}, {'pc12'}, -5);
fititer.SetLB({'all'}, {'df2'}, -5);
fititer.SetLB({'all'}, {'gm'}, 0);

fititer.SetUB({'all'}, {'pc0'}, 10);
fititer.SetUB({'all'}, {'pc12'}, 5);
fititer.SetUB({'all'}, {'df2'}, 5);
fititer.SetUB({'all'}, {'gm'}, 100);

% set starting values (prior knowledge)
fititer.SetStartingValues({'all'}, {'gm'}, 10);
fititer.SetStartingValues({'all'}, {'pc0'}, 0);
fititer.SetStartingValues({'all'}, {'pc12'}, 0);

std_factor_T2 = 0.01;
factor_T2app_true_T2_1 = 1.2;
fititer = setBoundsEmLineshape(fititer, std_factor_T2, factor_T2app_true_T2_1);

fititer.estimate_sp_baseline_stiffness = false;
fititer.use_sp_baseline     = false;
fititer.UpdateIteration;

% add the just configured iteration to the queue
fitsettings.AddIteration(fititer);

        
    function obj = sanityCheckCorrection(obj)
        mobj = ?Parameters;
        for par_cnt=1:length(mobj.Properties)
            current_par = mobj.Properties{par_cnt}.Name;
            mask_outer_of_upper_bounds = obj.start_values.(current_par) > obj.upper_bounds.(current_par);
            obj.start_values.(current_par)(mask_outer_of_upper_bounds) = obj.upper_bounds.(current_par)(mask_outer_of_upper_bounds);
            mask_outer_of_lower_bounds = obj.start_values.(current_par) < obj.lower_bounds.(current_par);
            obj.start_values.(current_par)(mask_outer_of_lower_bounds) = obj.lower_bounds.(current_par)(mask_outer_of_lower_bounds);
        end
    end

%% set up second fitting iteration ***********************************************************************
% ********************************************************************************************************
    function obj = Starting2(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
        for current_met = {'NAA_ac', 'NAA_as', 'Cr', 'Cr_CH2', 'tCho_P', 'mI', 'Glu', 'Leu'}
            currentMetString = current_met{:};
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                fitted_value = res.GetFittedValues({currentMetString}, {current_par});
                if length(fitted_value)>1
                    if all(fitted_value == fitted_value(1))
                        fitted_value = fitted_value(1);
                    else
                        error('Something does not seem to be right. Check what is going on');
                    end
                end
                obj.SetStartingValues({currentMetString}, {current_par}, fitted_value);
            end
        end
        
        referenceConcentration = res.GetFittedValues({'Cr'}, {'conc'});
        % adjust starting values of newly added metabolites
        for current_met = {'NAAG', 'GABA', 'Lac', 'Scyllo', 'Tau', 'Glyc','GSH', 'Asp', 'Gln'}
            currentMetString = current_met{:};
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                switch current_par
                    case 'conc'
                        switch currentMetString
                            case {'GABA', 'GSH', 'Asp', 'Gln'}
                                scale_factor = 0.2;
                            case {'Glyc'}
                                scale_factor = 0.1;
                            case {'Lac', 'Scyllo'}
                                scale_factor = 0.05;
                            case {'Tau'}
                                scale_factor = 0.15;
                            case {'NAAG'}
                                scale_factor = 0.25;
                        end
                        % the factor 0.9 comes from the fact that creatine is slightly overestimated in the first iteration
                        obj.SetStartingValues({currentMetString}, {current_par}, scale_factor*0.9*referenceConcentration);

                    otherwise
                        mean_par_values = mean(unique([res.GetFittedValues({'Cr'}, {current_par}), ...
                            res.GetFittedValues({'Cr_CH2'}, {current_par}), ...
                            res.GetFittedValues({'NAA_ac'}, {current_par}), ...
                            res.GetFittedValues({'tCho_P'}, {current_par})]));
                        obj.SetStartingValues({currentMetString}, {current_par}, mean_par_values);
                end
            end
        end
        % overwrite df2 starting value for Leu with previously fitted
        obj.full_start_values.df2(obj.met_idx('Leu')) = res.GetFittedValues({'Leu'}, {'df2'});
%         obj.SetStartingValues({'Leu'}, {'gm'}, 0); %as in previous setups
        

        std_factor_T2_ = 0.1;
        factor_T2app_true_T2 = 1.2;
        obj = setBoundsEmLineshape(obj, std_factor_T2_, factor_T2app_true_T2);
        obj.UpdateIteration;
        obj = sanityCheckCorrection(obj);
    end


    function obj = LB2(obj, res)
        
        mean_df2 = mean(unique([res.GetFittedValues({'Cr'}, {'df2'}), ...
            res.GetFittedValues({'Cr_CH2'}, {'df2'}), ...
            res.GetFittedValues({'NAA_ac'}, {'df2'}), ...
            res.GetFittedValues({'tCho_P'}, {'df2'})]));
        pc0_lb  = res.GetFittedValues({'Cr'}, {'pc0'}) - 2;%5
        pc12_lb  = res.GetFittedValues({'Cr'}, {'pc12'}) - 1.5;
        df2_lb  = mean_df2 - 5;%3
        gm_lb   = res.GetFittedValues({'Cr'}, {'gm'}) - 5;
        obj.SetLB({'all'}, {'pc0'}, pc0_lb);
        obj.SetLB({'all'}, {'pc12'}, pc12_lb);
        obj.SetLB({'all'}, {'df2'}, df2_lb);
        if gm_lb<0
            obj.SetLB({'all'}, {'gm'}, 0);
        else
            obj.SetLB({'all'}, {'gm'}, gm_lb);
        end
        %setup identical constraints as in the first iteration for all
        %metabolites, then restrict the bounds for the newly added metabolites
%         obj.SetLB({'Leu'}, {'gm'}, 0); 
        obj.UpdateIteration;
    end

    function obj = UB2(obj, res)
        
        %setup identical constraints as in the first iteration for all
        %metabolites, then restrict the bounds for the newly added metabolites 
        mean_df2 = mean(unique([res.GetFittedValues({'Cr'}, {'df2'}), ...
            res.GetFittedValues({'Cr_CH2'}, {'df2'}), ...
            res.GetFittedValues({'NAA_ac'}, {'df2'}), ...
            res.GetFittedValues({'tCho_P'}, {'df2'})]));
        pc0_ub  = res.GetFittedValues({'Cr'}, {'pc0'}) + 2;%5
        pc12_ub  = res.GetFittedValues({'Cr'}, {'pc12'}) + 1.5;
        df2_ub  = mean_df2 + 5;%3
        gm_ub   = res.GetFittedValues({'Cr'}, {'gm'}) + 5;
        obj.SetUB({'all'}, {'df2'}, df2_ub);
        obj.SetUB({'all'}, {'pc0'}, pc0_ub);
        obj.SetUB({'all'}, {'pc12'}, pc12_ub);
        obj.SetUB({'all'}, {'gm'}, gm_ub);
        % Set upper bound of the Leu Gaussian to the previously fitted Gaussian filter found for the metabolites.
        % The assumption is, that the acquired MMB should already have some
        % Gaussian broadening due to the data acquisition process.
%         obj.SetUB({'Leu'}, {'gm'}, gm_ub/4);
        obj.UpdateIteration;
    end

fititer = Iteration(basis);

fititer.ActivateMets({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P', 'mI', 'Glu', 'Gln', 'NAAG', 'GABA', 'NAA_as', 'Lac', ...
    'Scyllo','Tau','Glyc','GSH', 'Asp', 'Leu'});

fititer.ConnectMets({'name'}, {'conc', 'em', 'df2'});
fititer.ConnectMets({'global'}, {'gm', 'pc0', 'pc12'});

fititer.ConnectMets({'Leu'}, {'em'});
fititer.ConnectMets({'Leu'}, {'gm'});

% fix parameters to certain values
fititer.FixMets({'Leu'}, {'gm'});
fititer.SetFixedValues({'Leu'}, {'gm'}, 0);
fititer.FixMets({'Leu'}, {'em'});
fititer.SetFixedValues({'Leu'}, {'em'}, 0);

% set up typical bounds for the first iteration
fititer.SetLB({'all'}, {'conc'}, 0);

% define update function handles
fititer.UpdateLowerBounds    = @LB2;
fititer.UpdateUpperBounds    = @UB2;
fititer.UpdateStartingValues = @Starting2;
fititer.estimate_sp_baseline_stiffness = false;
fititer.use_sp_baseline     = false;

fititer.UpdateIteration;

% add the just configured iteration to the queue
fitsettings.AddIteration(fititer);

%% set up third fitting iteration ************************************************************************
% ********************************************************************************************************

    function obj = Starting3(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
        for par_cnt=1:length(mobj.Properties)
            current_par = mobj.Properties{par_cnt}.Name;
            mask = res.full_mask.(current_par).full(any(res.full_mask.(current_par).full,2), :);
            for fit_cnt=1:length(res.fitted_values.(current_par))
                fitted_value = res.fitted_values.(current_par)(fit_cnt);
                % the starting values are upated after the bounds so if some copied starting value
                % is outside the currently set bounds e.g. for df2 change that stating value to a value
                % within allowed values. This works just for df2 now. for more general case more conflicts
                % have to be considered
                if strcmp(current_par, 'df2')
                    if ((fitted_value>obj.upper_bounds.(current_par)(fit_cnt)) || ...
                            (fitted_value<obj.lower_bounds.(current_par)(fit_cnt)))
                        fitted_value = mean(unique([res.GetFittedValues({'Cr'}, {current_par}), ...
                            res.GetFittedValues({'Cr_CH2'}, {current_par}), ...
                            res.GetFittedValues({'NAA_ac'}, {current_par}), ...
                            res.GetFittedValues({'tCho_P'}, {current_par})]));
                    end
                end
                
                obj.full_start_values.(current_par)(mask(fit_cnt,:)) = fitted_value;
            end
        end
        % overwrite df2 starting value for Leu with previously fitted
        obj.full_start_values.df2(obj.met_idx('Leu')) = res.GetFittedValues({'Leu'}, {'df2'});
        obj.SetStartingValues({'Leu'}, {'gm'}, res.GetFittedValues({'Leu'}, {'gm'})); %use previous value      

        std_factor_T2_ = 0.05;
        factor_T2app_true_T2 = 1.2;
        obj = setBoundsEmLineshape(obj, std_factor_T2_, factor_T2app_true_T2);
        obj.UpdateIteration;
        obj = sanityCheckCorrection(obj);
    end

    function obj = LB3(obj, res)       
        mean_df2 = mean(unique([res.GetFittedValues({'Cr'}, {'df2'}), ...
            res.GetFittedValues({'Cr_CH2'}, {'df2'}), ...
            res.GetFittedValues({'NAA_ac'}, {'df2'}), ...
            res.GetFittedValues({'tCho_P'}, {'df2'})]));
        pc0_lb  = res.GetFittedValues({'Cr'}, {'pc0'}) - 15;
        pc12_lb  = res.GetFittedValues({'Cr'}, {'pc12'}) - 15;
        df2_lb  = mean_df2 - 3;
        gm_lb   = res.GetFittedValues({'Cr'}, {'gm'}) - 2;
        obj.SetLB({'all'}, {'pc0'}, pc0_lb);
        obj.SetLB({'all'}, {'pc12'}, pc12_lb);
        obj.SetLB({'all'}, {'df2'}, df2_lb);
        obj.SetLB({'Leu'}, {'df2'}, res.GetFittedValues({'Leu'}, {'df2'}) - 0.2);%1.27
        if gm_lb<0
            obj.SetLB({'all'}, {'gm'}, 0);
        else
            obj.SetLB({'all'}, {'gm'}, gm_lb);
        end
        %setup identical constraints as in the first iteration for all
        %metabolites, then restrict the bounds for the newly added metabolites
        obj.SetLB({'Leu'}, {'gm'}, 0); 
        obj.UpdateIteration;
    end

    function obj = UB3(obj, res)
        %setup identical constraints as in the first iteration for all
        %metabolites, then restrict the bounds for the newly added metabolites 
        mean_df2 = mean(unique([res.GetFittedValues({'Cr'}, {'df2'}), ...
            res.GetFittedValues({'Cr_CH2'}, {'df2'}), ...
            res.GetFittedValues({'NAA_ac'}, {'df2'}), ...
            res.GetFittedValues({'tCho_P'}, {'df2'})]));
        pc0_ub  = res.GetFittedValues({'Cr'}, {'pc0'}) + 15;
        pc12_ub  = res.GetFittedValues({'Cr'}, {'pc12'}) + 15;
        df2_ub  = mean_df2 + 3;
        gm_ub   = res.GetFittedValues({'Cr'}, {'gm'}) + 2;
        obj.SetUB({'all'}, {'df2'}, df2_ub);
        obj.SetUB({'Leu'}, {'df2'}, res.GetFittedValues({'Leu'}, {'df2'}) + 0.2);%1.27
        obj.SetUB({'all'}, {'pc0'}, pc0_ub);
        obj.SetUB({'all'}, {'pc12'}, pc12_ub);
        obj.SetUB({'all'}, {'gm'}, gm_ub);
        % Set upper bound of the Leu Gaussian to the previously fitted Gaussian filter found for the metabolites.
        % The assumption is, that the acquired MMB should already have some
        % Gaussian broadening due to the data acquisition process.
        obj.SetUB({'Leu'}, {'gm'}, 5);
        obj.UpdateIteration;
    end

fititer = Iteration(basis);

fititer.ActivateMets({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P', 'mI', 'Glu', 'Gln', 'NAAG', 'GABA', 'NAA_as', 'Lac', ...
    'Scyllo','Tau','Glyc','GSH', 'Asp', 'Leu'});

fititer.ConnectMets({'name'}, {'conc', 'em', 'df2'});
fititer.ConnectMets({'global'}, {'gm', 'pc0', 'pc12'});

fititer.ConnectMets({'Leu'}, {'em'}); 
fititer.ConnectMets({'Leu'}, {'gm'}); 

% fix parameters to certain values
fititer.FixMets({'Leu'}, {'em'}); 
fititer.SetFixedValues({'Leu'}, {'em'}, 0); 
% fititer.FixMets({'Leu'}, {'gm'}); 
% fititer.SetFixedValues({'Leu'}, {'gm'}, 0); 

% set up typical bounds for the first iteration
fititer.SetLB({'all'}, {'conc'}, 0);

% define update function handles
fititer.UpdateLowerBounds    = @LB3;
fititer.UpdateUpperBounds    = @UB3;
fititer.UpdateStartingValues = @Starting3;

fititer.estimate_sp_baseline_stiffness = true;
fititer.use_sp_baseline     = true;
fititer.spbase_knot_spacing = [0.25 0.25];
fititer.spbase_lambda       = 0e-1*[1 1];

fititer.UpdateIteration;

% add the just configured iteration to the queue
fitsettings.AddIteration(fititer);%% set up fourth fitting iteration ************************************************************************
% ********************************************************************************************************

    function obj = Starting4(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
        for par_cnt=1:length(mobj.Properties)
            current_par = mobj.Properties{par_cnt}.Name;
            mask = res.full_mask.(current_par).full(any(res.full_mask.(current_par).full,2), :);
            for fit_cnt=1:length(res.fitted_values.(current_par))
                fitted_value = res.fitted_values.(current_par)(fit_cnt);
                % the starting values are upated after the bounds so if some copied starting value
                % is outside the currently set bounds e.g. for df2 change that stating value to a value
                % within allowed values. This works just for df2 now. for more general case more conflicts
                % have to be considered
                if strcmp(current_par, 'df2')
                    if ((fitted_value>obj.upper_bounds.(current_par)(fit_cnt)) || ...
                            (fitted_value<obj.lower_bounds.(current_par)(fit_cnt)))
                        fitted_value = mean(unique([res.GetFittedValues({'Cr'}, {current_par}), ...
                            res.GetFittedValues({'Cr_CH2'}, {current_par}), ...
                            res.GetFittedValues({'NAA_ac'}, {current_par}), ...
                            res.GetFittedValues({'tCho_P'}, {current_par})]));
                    end
                end
                
                if strcmp(current_par, 'gm')
                    fitted_value = res.fitted_values.(current_par)(fit_cnt)-2;
                end
                obj.full_start_values.(current_par)(mask(fit_cnt,:)) = fitted_value;
            end
        end
        % overwrite df2 starting value for Leu with previously fitted
        obj.full_start_values.df2(obj.met_idx('Leu')) = res.GetFittedValues({'Leu'}, {'df2'});
        obj.SetStartingValues({'Leu'}, {'gm'}, res.GetFittedValues({'Leu'}, {'gm'})-2); %use previous value
        
        std_factor_T2_ = 1;
        factor_T2app_true_T2 = 1.2;
        obj = setBoundsEmLineshape(obj, std_factor_T2_, factor_T2app_true_T2);
        obj.UpdateIteration;
        obj = sanityCheckCorrection(obj);
    end

    function obj = LB4(obj, res)
        
        fitted_pc0  = res.GetFittedValues({'Cr'}, {'pc0'});
        fitted_pc12  = res.GetFittedValues({'Cr'}, {'pc12'});
        fitted_gm   = res.GetFittedValues({'Cr'}, {'gm'});
        gm_lb   = fitted_gm - 5;
        pc0_lb  = fitted_pc0 - 0.1;
        pc12_lb  = fitted_pc12 - 0.1;
        obj.SetLB({'all'}, {'pc0'}, pc0_lb);
        obj.SetLB({'all'}, {'pc12'}, pc12_lb);
        if gm_lb<0
            obj.SetLB({'all'}, {'gm'}, 0);
        else
            obj.SetLB({'all'}, {'gm'}, gm_lb);
        end
        obj.SetLB({'Leu'}, {'df2'}, res.GetFittedValues({'Leu'}, {'df2'}) - 0.2);%1.27
        
        mask = res.full_mask.df2.full(any(res.full_mask.df2.full,2), :);
        for fit_cnt=1:length(res.fitted_values.df2)
            lb_df2 = res.fitted_values.df2(fit_cnt) - 0.2;%1.27
            obj.full_lower_bounds.df2(mask(fit_cnt,:)) = lb_df2;
        end
       
        obj.SetLB({'Leu'}, {'gm'}, 0); 
        obj.UpdateIteration;
    end

    function obj = UB4(obj, res)
        
        fitted_pc0  = res.GetFittedValues({'Cr'}, {'pc0'});
        fitted_pc12  = res.GetFittedValues({'Cr'}, {'pc12'});
        fitted_gm   = res.GetFittedValues({'Cr'}, {'gm'});
        gm_ub   = fitted_gm + 5;
        pc0_ub  = fitted_pc0 + 0.1;
        pc12_ub  = fitted_pc12 + 0.1;
        obj.SetUB({'all'}, {'pc0'}, pc0_ub);
        obj.SetUB({'all'}, {'pc12'}, pc12_ub);
        obj.SetUB({'all'}, {'gm'}, gm_ub);
        obj.SetUB({'Leu'}, {'df2'}, res.GetFittedValues({'Leu'}, {'df2'}) + 0.2);%1.27
        
        mask = res.full_mask.df2.full(any(res.full_mask.df2.full,2), :);
        for fit_cnt=1:length(res.fitted_values.df2)
            ub_df2 = res.fitted_values.df2(fit_cnt) + 0.2;%1.27
            obj.full_upper_bounds.df2(mask(fit_cnt,:)) = ub_df2;
        end
        
        % Set upper bound of the Leu Gaussian to the previously fitted Gaussian filter found for the metabolites.
        % The assumption is, that the acquired MMB should already have some
        % Gaussian broadening due to the data acquisition process.
        obj.SetUB({'Leu'}, {'gm'}, 5);
        obj.UpdateIteration;
    end

fititer = Iteration(basis);

fititer.ActivateMets({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P', 'mI', 'Glu', 'Gln', 'NAAG', 'GABA', 'NAA_as', 'Lac', ...
    'Scyllo','Tau','Glyc','GSH', 'Asp', 'Leu'});

fititer.ConnectMets({'name'}, {'conc', 'em', 'df2'});
fititer.ConnectMets({'global'}, {'gm', 'pc0', 'pc12'});

fititer.ConnectMets({'Leu'}, {'em'}); 
fititer.ConnectMets({'Leu'}, {'gm'}); 

% fix parameters to certain values
fititer.FixMets({'Leu'}, {'em'}); 
fititer.SetFixedValues({'Leu'}, {'em'}, 0); 
% fititer.FixMets({'Leu'}, {'gm'}); 
% fititer.SetFixedValues({'Leu'}, {'gm'}, 0); 

% set up typical bounds for the first iteration
fititer.SetLB({'all'}, {'conc'}, 0);

% define update function handles
fititer.UpdateLowerBounds    = @LB4;
fititer.UpdateUpperBounds    = @UB4;
fititer.UpdateStartingValues = @Starting4;

fititer.estimate_sp_baseline_stiffness = false;
fititer.use_sp_baseline     = true;
% fititer.spbase_knot_spacing = [0.25 0.25];%should be read from previous iteration
% fititer.spbase_lambda       = 0e-1*[1 1];%should be read from previous iteration

fititer.UpdateIteration;

% add the just configured iteration to the queue
fitsettings.AddIteration(fititer);

%% save protocol file
save(outputFile, 'fitsettings', 'data');


end
