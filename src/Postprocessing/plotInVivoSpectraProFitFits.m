function plotInVivoSpectraProFitFits()

%% path to the export of the spectra
exportFolder = {'1 Normal recon', '2 Removed averages'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolder);
pathBaseExportFiles = pathBaseExportFiles(1:end-1);
pathBaseExportFiles = [pathBaseExportFiles ' - Weighted Cost Fun R3 0.25scale m_15 trunc\'];
dataExportPathBase1 = strcat(pathBaseExportFiles, exportFolder{1}, '\');
dataExportPathBase2 = strcat(pathBaseExportFiles, exportFolder{2}, '\');
plotPath = [dataExportPathBase1, 'Plots\BaselinePlots\'];

%%

subjects = 1:11;

suffixes = {...
    '_32ave_1', '_16ave_2', '_16ave_3';...
    };
titles = {{'Reference spectra', 'NEX = 96'}, {'Subset spectra','NEX = 64'}, ...
    {'Subset spectra','NEX = 32'}, {'Subset spectra','NEX = 32'}};

metaboliteNames = {'Asp'   'Cr'     'Cr_CH2'  'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'Lac' 'mI' 'NAA_as'   'NAA_ac'    'NAAG' 'Scyllo' 'Tau' 'tCho_P'};
metaboliteLabels = {'Asp';'tCr(CH_3)';'tCr(CH_2)';'GABA';'Glu';'Gln';'GSH';'Gly';'Lac';'mI';'NAA(CH_2)';'NAA(CH_3)'; 'NAAG';'sI';'Tau';'tCho+';};

displayBaseline = false;

xLimits = [0.5 4.097];
xLimitsText = xLimits(1) -0.03;
numberOfMet = length(metaboliteNames);

%plotting offsets
offsetMetabolite = 0.1;
offsetResidual = offsetMetabolite * (numberOfMet+1);
offsetBaseline = offsetResidual + offsetMetabolite;

FontSize = 14;
LineWidth = 1.5;

truncOn = true;
if truncOn
    truncSuffix = '';
else
    truncSuffix = '_truncOff';
end

iteration = 4 ;

subplotGridX = 2;
subplotGridY = 2;
nGrid = subplotGridX * subplotGridY;

subplotNames = {'A', 'B', 'C', 'D'};

for indexSubject = subjects
    for indexAverage = 1:4
        if mod(indexAverage-1,nGrid)==0
            %         figure;
            figure('units','normalized','outerposition',[0,0,1,1]); %Works on the Dell screen portrait
        end
        
        %% create the figure
        indexSubplot = mod(indexAverage-1, nGrid)+1;
        hs = subplot(subplotGridX,subplotGridY,indexSubplot);
        coordinates = hs.Position;
        
        %% load ProFit Fit
        if indexAverage == 1
            dataExportPath = [dataExportPathBase1, 'Subj_', num2str(indexSubject), '\TE24\'];
            suffix = '';
        else
            dataExportPath = [dataExportPathBase2, 'Subj_', num2str(indexSubject), '\TE24\'];
            suffix = suffixes{indexAverage-1};
        end
        fileName = ['Subj_', num2str(indexSubject), '_TE24', suffix, truncSuffix, '_profit.mat'];
        
        load(strcat(dataExportPath,fileName),'fitresult', 'data');
        
        %% process ProFit data
        currentFitresult = fitresult{1,iteration};
        %spline baseline
        if ~isempty(currentFitresult.splines_baseline)
            splines_ed_vec = currentFitresult.splines_ed_vec;
            splines_optim_model_vec = currentFitresult.splines_optim_model_vec;
            splines_optim_idx = currentFitresult.splines_optim_idx;
        else
            splines_ed_vec = [];
            splines_optim_model_vec = [];
            splines_optim_idx = 0;
        end
        
        % get mean values for pc0, pc1 and df1, df2
        % values which should be applied on spectra and not the basis set
        pc0 = zeros(1, currentFitresult.nr_act_mets);
        pc12 = zeros(1, currentFitresult.nr_act_mets);
        df2 = zeros(1, currentFitresult.nr_act_mets);
        for met_cnt=1:currentFitresult.nr_act_mets
            pc0(met_cnt) = currentFitresult.fitted_values.pc0(currentFitresult.sub_mask.pc0.full(:,met_cnt));
            pc12(met_cnt) = currentFitresult.fitted_values.pc12(currentFitresult.sub_mask.pc12.full(:,met_cnt));
            df2(met_cnt) = currentFitresult.fitted_values.df2(currentFitresult.sub_mask.df2.full(:,met_cnt));
        end
        %ppm axis
        ppm = currentFitresult.common_data.data.fitroi.ppm2;
        
        %phaseData
        pData = currentFitresult.processed_spec;
        %fit data
        fData = currentFitresult.fitted_spec;
        %residual
        rData = currentFitresult.residual_spec;
        %spline baseline
        if ~isempty(currentFitresult.splines_baseline)
            spBB = currentFitresult.splines_baseline.basis_matrix;
            concSpline = currentFitresult.splines_baseline.coeff;
            bData = (spBB * concSpline(1:size(spBB,2)))';
        else
            bData = zeros(size(currentFitresult.residual_spec));
        end
        %Macromolecular baseline
        indexMMB   = find(strcmp(currentFitresult.met(currentFitresult.active_mets), 'Leu'));
        fit_spec_metabolite = ...
            reshape(currentFitresult.basis_matrix(:,currentFitresult.sub_mask.conc.full(:,indexMMB)),...
            [currentFitresult.common_data.data.fitroi.pts(1) ...
            currentFitresult.common_data.data.fitroi.pts(2)]);
        fit_conc_metabolite = currentFitresult.fitted_values.conc(currentFitresult.sub_mask.conc.full(:,indexMMB)');
        mmData= fit_spec_metabolite * fit_conc_metabolite;
        
        %scaling calculation
        scale = real(max(pData));
        
        %plotting
        hold on
        if displayBaseline
            p = plot(ppm, real(pData) ./ scale, ...
                ppm,real(fData) ./ scale, ...
                ppm,real(mmData) ./ scale - 0.03, ...
                ppm,real(bData) ./ scale - offsetBaseline, ...
                ppm,real(rData) ./ scale - offsetResidual);
            text(xLimitsText,-0.03,'MM spect.', 'FontSize', FontSize);
        else
            p = plot(ppm, real((pData - bData)) ./ scale, ...
                ppm,real((fData - bData)) ./ scale, ...
                ppm,real(mmData) ./ scale - 0.03, ...
                ppm,real(bData) ./ scale - offsetBaseline, ...
                ppm,real(rData) ./ scale - offsetResidual);
            text(xLimitsText,-0.03,'MM spect.', 'FontSize', FontSize);
        end
        
        text(xLimitsText,0.05,'Data + Fit', 'FontSize', FontSize);
        text(xLimitsText,-offsetResidual,'Residual', 'FontSize', FontSize);
        text(xLimitsText,-offsetBaseline,'Baseline', 'FontSize', FontSize);
        for plots = 1:length(p)
            set(p(plots),'LineWidth',LineWidth);
        end
        
        for indexMetabolite = 1:length(metaboliteNames)
            %evaluate each metabolite
            indexMet   = find(strcmp(currentFitresult.met(currentFitresult.active_mets), metaboliteNames{indexMetabolite}));
            
            fit_spec_metabolite = ...
                reshape(currentFitresult.basis_matrix(:,currentFitresult.sub_mask.conc.full(:,indexMet)),...
                [currentFitresult.common_data.data.fitroi.pts(1) ...
                currentFitresult.common_data.data.fitroi.pts(2)]);
            fit_conc_metabolite = currentFitresult.fitted_values.conc(currentFitresult.sub_mask.conc.full(:,indexMet)');
            metaboliteSpectrum = fit_spec_metabolite * fit_conc_metabolite;
            
            pMetabolite = plot(ppm, real(metaboliteSpectrum ./ scale) - indexMetabolite * offsetMetabolite);
            text(xLimitsText, indexMetabolite * -offsetMetabolite,metaboliteLabels{indexMetabolite}, 'FontSize', FontSize);
            set(pMetabolite,'LineWidth',LineWidth);
        end
        
        xlim(xLimits);
        xlabel('\delta (ppm)');
        ylim([-offsetBaseline-offsetMetabolite*2 1.1]);
        %     ylim([-0.08 0.1]);
        set(gca,'xDir','reverse')
        set(gca,'ytick',[]);
        title(titles{indexAverage}, 'HorizontalAlignment','right')
        set(gca,'fontsize',FontSize);
        
        
        %% mAIC
        annotation('rectangle',[coordinates(1)+coordinates(3)*0.61 coordinates(2)+coordinates(4)*0.73 ...
            .37./subplotGridX .28./subplotGridY],'FaceColor', 'w', 'FaceAlpha',0, 'Color',[0 0.4 0.1])
        
        axes('Position',[coordinates(1)+coordinates(3)*0.75 coordinates(2)+coordinates(4)*0.81 ...
            .2./subplotGridX .2./subplotGridY])
        box on;
        plot(splines_ed_vec, splines_optim_model_vec,'Color',[0. 0.5 0.2]);
        hold on
        if ~isempty(splines_ed_vec(splines_optim_idx))
            xl = xline(splines_ed_vec(splines_optim_idx),'-.', {['ED: ' sprintf('%.f',splines_ed_vec(splines_optim_idx))],...
                ['mAIC: ', sprintf('%.1f',splines_optim_model_vec(splines_optim_idx))]}, ...
                'HandleVisibility','off', 'LineWidth', 2);
            xl.LabelVerticalAlignment = 'top';
            xl.LabelHorizontalAlignment = 'center';
            xl.FontSize = 9;
        end
        set(gca,'FontSize',10)
        set(gca,'Xscale','log');
        %      set(gca,'Color',backgroundInlayColor)
        xlabel('Baseline ED / ppm');
        ylabel('mAIC');
        ylim([min(splines_optim_model_vec)-0.03 max(splines_optim_model_vec)+0.03])
        title('ProFit Baseline Smoothness');
        
        annotation('textbox',[coordinates(1)+coordinates(3)*0.02-0.03 coordinates(2)+coordinates(4)*0.50 ...
            .35./subplotGridX .4./subplotGridY],'String',subplotNames{indexSubplot},'FontSize', 16, 'FontWeight', 'bold','LineStyle','none')
        if mod(indexAverage,nGrid)==0
            %         print('-dtiff','-r600',[plotPath, 'Baseline_Fit_',num2str(indexParam),'.tif'])
            %         savefig(gcf,[plotPath, 'Baseline_Fit_',num2str(indexParam),'.fig'], 'compact');
        end
    end
end
end
