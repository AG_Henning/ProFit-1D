function calculateCorrelationMatrix(currentProfitFileFullPath,iteration, fitresult)

%Initialise metabolite title struct
% metaboliteTitle={...
%     'cre','naa','tcho','gln','glu','mi','baseline',...
%     '','','','','','','','','','','','',;
%     'cre','naa','tcho','glc','gsh','asp','gaba','gln','glu','gly','lac',...
%     'mi','naag','pe','tau','scy','asc','ace','baseline';
%     'cre','naa','tcho','glc','gsh','asp','gaba','gln','glu','gly','lac',...
%     'mi','naag','pe','tau','scy','asc','ace','baseline';
%     'cre','naa','tcho','glc','gsh','asp','gaba','gln','glu','gly','lac',...
%     'mi','naag','pe','tau','scy','asc','ace','baseline'};

metaboliteTitle = { ...
    'naa', 'cr', 'gpc', 'pch','mi', 'glu', ...
    '','','','','','','','','','','','','';
    'naa', 'cr', 'pcr', 'gpc', 'cho', 'pch', 'naag', 'mi', 'scy', 'gaba', ...
    'gln', 'glu', 'gly', 'glc', 'gsh', 'asp', 'lac','pe', 'tau';
    'naa', 'cr', 'pcr', 'gpc', 'cho', 'pch', 'naag', 'mi', 'scy', 'gaba', ...
    'gln', 'glu', 'gly', 'glc', 'gsh', 'asp', 'lac','pe', 'tau';
    'naa', 'cr', 'pcr', 'gpc', 'cho', 'pch', 'naag', 'mi', 'scy', 'gaba', ...
    'gln', 'glu', 'gly', 'glc', 'gsh', 'asp', 'lac','pe', 'tau'};

currentFitresult = fitresult{iteration};
basis_matrix = currentFitresult .basis_matrix;
numberOfMetabolites = size(basis_matrix,2);
noise_var = currentFitresult .noise_var;

%calculate Fisher matrix
F = real(basis_matrix' * basis_matrix) / noise_var;

F_inv = inv(F);

correlationMatrix = zeros(size(F));

for n = 1:size(F,1)
    for m = 1:size(F,2)
        correlationMatrix(m,n) = F_inv(m,n) / ( sqrt(F_inv(m,m) * F_inv(n,n)));
    end
end
%%
fitted_crlb = zeros(numberOfMetabolites,1);
for actualMetabolite=1:numberOfMetabolites
    fitted_crlb(actualMetabolite) = currentFitresult.crlb(actualMetabolite);
end

%%

[pathname,filename_curr,ext] = fileparts(currentProfitFileFullPath);
outputFile = [filename_curr '_corr_mx'];
outputFileFull = [pathname '\' outputFile '.mat'];
currentTitle = metaboliteTitle{iteration};

save(outputFileFull, 'currentTitle', 'correlationMatrix', 'fitted_crlb');


end