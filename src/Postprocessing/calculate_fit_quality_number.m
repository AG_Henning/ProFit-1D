function FQN = calculate_fit_quality_number(residual_spectrum, ppmVector, spectrum, ppmVectorFull, fitted_roi, noise_roi)
if ~exist('noise_roi','var')
    noise_roi = [-5 -1];
end
if ~exist('fitted_roi','var')
    fitted_roi = [0.6 4.1];
end
%get masks for the spectrum
ppmMask_fit_residual = (ppmVector > fitted_roi(1)) & (ppmVector < fitted_roi(2));
ppmMask_noise  = (ppmVectorFull > noise_roi(1))  & (ppmVectorFull < noise_roi(2));

%extract the residual ranges to compare
fit_residual = real(residual_spectrum(ppmMask_fit_residual));
noise        = real(spectrum(ppmMask_noise));

%subtract a linear fit from the residual ranges
fitobject = fit((0:length(fit_residual)-1)',fit_residual','poly1'); %fit a linear curve to noise
y         = feval(fitobject,(0:length(fit_residual)-1)');   % substract poly1 from noise
fit_residual     = fit_residual - y';% now the noise does not have linear component

fitobject = fit((0:length(noise)-1)',noise','poly1'); %fit a linear curve to noise
y         = feval(fitobject,(0:length(noise)-1)');   % substract poly1 from noise
noise     = noise - y';% now the noise does not have linear component

%calculate the noise variance
fit_residual_variance = var(fit_residual);
noise_variance        = var(noise);

display(['Should be equal: std(noise): ', num2str(std(noise)), ' vs. rms(noise): ', num2str(rms(noise))]);

% FQN > 1  underfitting
% FQN ~= 1 no structured noise in residual left
% FQN < 1  overfitting
FQN = fit_residual_variance / noise_variance;

display(['Should be ~1: FQN: ', num2str(FQN)]);
end