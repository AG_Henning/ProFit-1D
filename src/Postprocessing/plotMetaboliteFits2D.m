function[fitted]=plotMetaboliteFits2D(profitFitFile,iteration, fitresult)
%%% [fitted]=plotMetaboliteFits2D(profitFitFile,iteration, fitresult) returns the
%%% struct fitted for the provided profitFitFile (.mat) and the selected
%%% iteration rounds
%%%
%%% Usage: [fitted]=plotMetaboliteFits2D('X:/data/profit.mat',[1:4],fitresult)
%%% to include the file X:/data/profit.mat and go through iteration 1 to 4
%%% For only 4th iteration results, select
%%% [fitted]=plotMetaboliteFits2D('X:/data/profit.mat',[4],fitresult)
%%%
%%% To be able to write the pdfs, the export_fig code from Yair Altman is
%%% needed. Also, ghostscript has to be installed.


% Last Update: 06.08.2015, PW (patrik@biomed.ee.ethz.ch)
%
% Remarks:
%
% active metabolites
% iteration 1:  1(cre), 2(naa),  3(tcho), 4(gln), 5(glu),  6(mi), 7(baseline)
% iteration 2:  1(cre), 2(naa),  3(tcho), 4(glc), 5(gsh),  6(asp), 7(gaba),
%               8(gln), 9(glu), 10(gly), 11(lac), 12(mi), 13(naag),
%               14(pe), 15(tau),16(scy), 17(asc), 18(ace),19(baseline)
% iteration 3:  1(cre), 2(naa),  3(tcho), 4(glc), 5(gsh),  6(asp), 7(gaba),
%               8(gln), 9(glu), 10(gly), 11(lac), 12(mi), 13(naag),
%               14(pe), 15(tau),16(scy), 17(asc), 18(ace),19(baseline)
% iteration 4:  1(cre), 2(naa),  3(tcho), 4(glc), 5(gsh),  6(asp), 7(gaba),
%               8(gln), 9(glu), 10(gly), 11(lac), 12(mi), 13(naag),
%               14(pe), 15(tau),16(scy), 17(asc), 18(ace),19(baseline)
%
% Fit Region of interest is a 25 x 242 large matrix
% fitresult{1,4}.common_data.data.fitroi.pts= [25 242]
% covering a ppm range of [-0.5088:0.4306] in indirect
% (fitresult{4}.common_data.data.fitroi.ppm1) and a ppm range
% of [0.4954:4.1802] in direct frequency direction
% (fitresult{4}.common_data.data.fitroi.ppm2)
% Since fit region of interest does not change with iteration steps, we can
% use the fitresult{1,iteration} settings of the iteration-th iteration.

% Prepare environment and delete previously created files:
close all;
[filePath,fileName,~]=fileparts(profitFitFile);
fullPathFileName = [filePath '/' fileName];
exportFilePath = [fullPathFileName '_2DFit_iteration' num2str(iteration) '.pdf'];
if exist(exportFilePath ,'file') == 2
    delete(exportFilePath)
end

%Initialise metabolite title struct
metaboliteTitle={...
    'cre','naa','tcho','gln','glu','mi','baseline',...
    '','','','','','','','','','','','',;
    'cre','naa','tcho','glc','gsh','asp','gaba','gln','glu','gly','lac',...
    'mi','naag','pe','tau','scy','asc','ace','baseline';
    'cre','naa','tcho','glc','gsh','asp','gaba','gln','glu','gly','lac',...
    'mi','naag','pe','tau','scy','asc','ace','baseline';
    'cre','naa','tcho','glc','gsh','asp','gaba','gln','glu','gly','lac',...
    'mi','naag','pe','tau','scy','asc','ace','baseline'};

% metaboliteTitle = { ...
%     'naa', 'cr', 'gpc', 'pch','mi', 'glu', ...
%     '','','','','','','','','','','','','';
%     'naa', 'cr', 'pcr', 'gpc', 'cho', 'pch', 'naag', 'mi', 'scy', 'gaba', ...
%     'gln', 'glu', 'gly', 'glc', 'gsh', 'asp', 'lac','pe', 'tau';
%     'naa', 'cr', 'pcr', 'gpc', 'cho', 'pch', 'naag', 'mi', 'scy', 'gaba', ...
%     'gln', 'glu', 'gly', 'glc', 'gsh', 'asp', 'lac','pe', 'tau';
%     'naa', 'cr', 'pcr', 'gpc', 'cho', 'pch', 'naag', 'mi', 'scy', 'gaba', ...
%     'gln', 'glu', 'gly', 'glc', 'gsh', 'asp', 'lac','pe', 'tau'};

% Display images high resolution: scaling factor higherResolutionFactor
higherResolutionFactor = [4 4];

% If higherResolutionFactor is larger than one, adjust the axis to cover finer
% resolution -> from [25 242] to [25*higherResolutionFactor(1) 242*higherResolutionFactor(2)]
% f1 is indirect and f2 is direct frequency axis
yaxis = interp1(1:fitresult{1,iteration}.common_data.data.fitroi.pts(1), ...
    fitresult{1,iteration}.common_data.data.fitroi.ppm1, ...
    linspace(1, fitresult{1,iteration}.common_data.data.fitroi.pts(1), ...
    higherResolutionFactor(1) * fitresult{1,iteration}.common_data.data.fitroi.pts(1)));
xaxis = interp1(1:fitresult{1,iteration}.common_data.data.fitroi.pts(2), ...
    fitresult{1,iteration}.common_data.data.fitroi.ppm2, ...
    linspace(1, fitresult{1,iteration}.common_data.data.fitroi.pts(2), ...
    higherResolutionFactor(2) * fitresult{1,iteration}.common_data.data.fitroi.pts(2)));

% Prepare meshgrid parameter to be able to interp2 the fit
% from 25x242 to 25*higherResolutionFactor(1)x242*higherResolutionFactor(2)
% Original grid
[ogrid_x, ogrid_y] = meshgrid(1:fitresult{1,iteration}.common_data.data.fitroi.pts(2), ...
    1:fitresult{1,iteration}.common_data.data.fitroi.pts(1));
% High resolution grid
[ngrid_x, ngrid_y] = meshgrid(...
    linspace(1, fitresult{1,iteration}.common_data.data.fitroi.pts(2), ...
    higherResolutionFactor(2) * fitresult{1,iteration}.common_data.data.fitroi.pts(2)), ...
    linspace(1, fitresult{1,iteration}.common_data.data.fitroi.pts(1), ...
    higherResolutionFactor(1) * fitresult{1,iteration}.common_data.data.fitroi.pts(1)));

% Initialise metabolite and concentration struct and plot all
% metabolite fits for all iteration steps in individual figures
fitted_metabolites={};
fitted_concentrations={};

currentFitresult=fitresult{1,iteration};

% Total 2D Spectrum at the beginning
figureHandle(iteration,20).handle=...
    figure((iteration-1)*25+20);
subplots={'spec', 'fit_spec', 'res_spec'};
for pp = 1:length(subplots)
    subplot(length(subplots), 1, pp);
%     figure(pp);
    twoDplot_real = real(eval(['currentFitresult.' ...
        currentFitresult.subplottypes(subplots{pp})]));
    twoDplot = interp2(ogrid_x, ogrid_y, twoDplot_real, ngrid_x, ngrid_y);
%     imagesc(xaxis, yaxis, twoDplot);
    md = plot2d_log(xaxis, yaxis, twoDplot);
    if(pp == 1)
%         md = md * 0.3;
        axisValue = [-md md];
        caxis(axisValue);
    else
        caxis(axisValue);
    end
    ax = gca;
%     load('BlackJet','mycmap');
%     colormap(ax, mycmap);
    set(gca, 'XDir', 'reverse');
    xlabel('[ppm]');ylabel('[ppm]');
end
export_fig(exportFilePath, '-append', figureHandle(iteration,20).handle);
close(figureHandle(iteration,20).handle);

% Put all numbers in a table and save it on the second page of the file
figureHandle(iteration,21).handle=figure((iteration-1)*25+21);
set(figureHandle(iteration,21).handle,...
    'Position',[350 400 890 600],'Color',[1 1 1]);
crlb_print =currentFitresult.crlb;
crlb_print(abs(crlb_print)>=1000) = 999.9999;
active_met_names = currentFitresult.met(currentFitresult.active_mets);
dataOfTable={};
columnname =  {'met','conc', '/Cr', '<html>crlb<br />[%]</html>', ...
    '<html>T2<br />[ms]</html>','<html>em<br />[Hz]</html>',...
    '<html>gm<br />[Hz]</html>', '<html>em_g<br />[Hz]</html>', ...
    '<html>pc0<br />[deg]</html>', '<html>df1<br />[Hz]</html>', ...
    '<html>df2<br />[Hz]</html>', '<html>pc11<br />[deg/ppm]</html>', ...
    '<html>pc12<br />[deg/ppm]</html>'};
columnformat={'char',[],[],[],[],[],[],[],[],[],[],[],[]};
for met_cnt=1:currentFitresult.nr_act_mets
    dataOfTable=[dataOfTable;{...
        active_met_names{met_cnt},...
        num2str(currentFitresult.fitted_values.conc(...
        currentFitresult.sub_mask.conc.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.conc(...
        currentFitresult.sub_mask.conc.full(:,met_cnt)')/...
        currentFitresult.fitted_values.conc(1),'%6.4f'),...
        num2str(crlb_print(...
        currentFitresult.sub_mask.conc.full(:,met_cnt)'),'%6.4f'),...
        num2str(1/(pi*currentFitresult.fitted_values.em(...
        currentFitresult.sub_mask.em.full(:,met_cnt)')),'%6.4f'),...
        num2str(currentFitresult.fitted_values.em(...
        currentFitresult.sub_mask.em.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.gm(...
        currentFitresult.sub_mask.gm.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.em_g(...
        currentFitresult.sub_mask.em_g.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.pc0(...
        currentFitresult.sub_mask.pc0.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.df1(...
        currentFitresult.sub_mask.df1.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.df2(...
        currentFitresult.sub_mask.df2.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.pc11(...
        currentFitresult.sub_mask.pc11.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.pc12(...
        currentFitresult.sub_mask.pc12.full(:,met_cnt)'),'%6.4f')...
        }];
end;
tableProfit(iteration) = uitable(...%'Units','normalized',...%
    'Position',[20 20 850 580],'Data', dataOfTable,...
    'ColumnName', columnname,'ColumnFormat',columnformat,...
    'RowName',[],'ColumnWidth',{65});
export_fig(exportFilePath, '-append', figureHandle(iteration,21).handle);
close(figureHandle(iteration,21).handle);


fitted_spec = currentFitresult.fitted_spec;
twoDFittedSpec = interp2(ogrid_x, ogrid_y, real(fitted_spec), ngrid_x, ngrid_y);
    
% Run for all metabolites in the actual iteration
for actualMetabolite=1:size(currentFitresult.basis_matrix,2)
    % Prepare and calculate values
    figureHandle(iteration,actualMetabolite).handle=...
        figure((iteration-1)*25+actualMetabolite);
    fitted_metabolites{iteration,actualMetabolite}=...
        reshape(currentFitresult.basis_matrix(:,actualMetabolite),...
        [fitresult{1,iteration}.common_data.data.fitroi.pts(1) ...
        fitresult{1,iteration}.common_data.data.fitroi.pts(2)]);
    fitted_concentrations{iteration,actualMetabolite}=...
        currentFitresult.fitted_values.conc(actualMetabolite);
    fitted_crlb{iteration,actualMetabolite}=...
        currentFitresult.crlb(actualMetabolite);
    twoDSpectrumFit{iteration,actualMetabolite} = ...
        interp2(ogrid_x, ogrid_y, ...
        real(fitted_metabolites{iteration,actualMetabolite} * fitted_concentrations{iteration,actualMetabolite}), ...
        ngrid_x, ngrid_y);
    
    % Plot figures, save them to file and close figure
    subplot(2,1,1);
    imagesc(xaxis, yaxis, ...
        twoDSpectrumFit{iteration,actualMetabolite});

%     plot2d_log(xaxis, yaxis, twoDSpectrumFit{iteration,actualMetabolite});
    set(gca,'XDir','reverse');
    xlabel('[ppm]'); ylabel('[ppm]');
    title([char(metaboliteTitle(iteration,actualMetabolite)) ...
        ' with CRLB(%) = ' ...
        num2str(fitted_crlb{iteration,actualMetabolite})]);
    subplot(2,1,2);
    hold on
    plot(xaxis, mean(twoDSpectrumFit{iteration,actualMetabolite},1));
    plot(xaxis, mean(twoDFittedSpec,1));
    hold off
    set(gca,'XDir','reverse');
    xlabel('[ppm]');ylabel('[a.u.]');
    axis([xaxis(1) xaxis(end) 0 Inf]);
    
    % Export acutal figure to pdf file with function export_fig from
    % Woodford and Altman (available on MathWorks Exchange)
    export_fig(exportFilePath, '-append', figureHandle(iteration,actualMetabolite).handle);
    close(figureHandle(iteration,actualMetabolite).handle)
end % End for loop metabolites

if(iteration == 4)
	figureHandle(iteration,25).handle= figure(100);
	spBB = currentFitresult.splines_baseline.basis_matrix;
	concSpline = currentFitresult.splines_baseline.coeff;
	splineBaseline = spBB * concSpline(1:size(spBB,2));
	fitted_spec = currentFitresult.fitted_spec;
	splineBaselineMatrix = reshape(splineBaseline,size(fitted_spec));
	twoDBaselineFit = ...
        interp2(ogrid_x, ogrid_y, real(splineBaselineMatrix), ngrid_x, ngrid_y);
	twoDFittedSpec = ...
        interp2(ogrid_x, ogrid_y, real(fitted_spec), ngrid_x, ngrid_y);
	% Plot figures, save them to file and close figure
    subplot(2,1,1);
    imagesc(xaxis, yaxis, twoDBaselineFit);

%     plot2d_log(xaxis, yaxis, twoDSpectrumFit{iteration,actualMetabolite});
    set(gca,'XDir','reverse');
    xlabel('[ppm]'); ylabel('[ppm]');
    title('Baseline');
    subplot(2,1,2);
    hold on
    plot(xaxis, mean(twoDBaselineFit,1));
    plot(xaxis, mean(twoDFittedSpec,1));
    hold off
    set(gca,'XDir','reverse');
    xlabel('[ppm]');ylabel('[a.u.]');
    axis([xaxis(1) xaxis(end) 0 Inf]);
    
    % Export acutal figure to pdf file with function export_fig from
    % Woodford and Altman (available on MathWorks Exchange)
    export_fig(exportFilePath, '-append', figureHandle(iteration,25).handle);
    close(figureHandle(iteration,25).handle)
end

% Return also results in the struct fitted
fitted=struct('fittedMetabolites',fitted_metabolites,'fitted_concentration',...
    fitted_concentrations,'fitted_crlb',fitted_crlb,...
    'spectrumFit',twoDSpectrumFit);