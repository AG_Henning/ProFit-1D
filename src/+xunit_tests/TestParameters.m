classdef TestParameters < TestCase
    
    properties
        
        testobj;
        pars;
    end
    
    methods
        
        function this = TestParameters(name)
            
            this = this@TestCase(name);
        end
        
        function this = setUp(this)
            
            this.testobj = Parameters;
            this.pars    = {'conc'; 'em'; 'em_g'; 'gm'; 'pc0'; 'pc1'; 'df1'; 'df2'; 't1'};
        end
        
        function this = TestSubParsExist(this)
            % Check if all parameters that are needed and specified in the
            % setup methods get actually initialized
            
            mhobj = ?handle;
            method_list = cellfun(@(meth) meth.Name, mhobj.Methods, 'UniformOutput', false);
            prop_list   = cellfun(@(prop) prop.Name, mhobj.Properties, 'UniformOutput', false);
            
            custom_methods = {'Parameters'; 'Fill'};
            custom_props   = this.pars;
            
            method_list = sort(vertcat(method_list, custom_methods));
            prop_list   = sort(vertcat(prop_list, custom_props));
            
            mobj = metaclass(this.testobj);
            % get all availbale methods and properties and check if the
            % expected ones arme among them
            % version for R2010a
            available_methods = sort(cellfun(@(x) x.Name, mobj.Methods, 'UniformOutput', false));
            available_props   = sort(cellfun(@(x) x.Name, mobj.Properties, 'UniformOutput', false));

           % now search and check
           assertEqual(available_methods, method_list);
           assertEqual(available_props, prop_list);
        end
        
        function this = TestFillSubPars(this)
            % use the Fill method to set values in the sub parameters
                        
            target_out = 100 * ones(1,10);
            target_in  = target_out;
            
            assertExceptionThrown(@() this.testobj.Fill('all', target_in), 'Parameters:Fill:WrongInput');
            assertExceptionThrown(@() this.testobj.Fill([1 2 3 4], target_in), 'Parameters:Fill:WrongInput');
            assertExceptionThrown(@() this.testobj.Fill({'konc'}, target_in), 'Parameters:Fill:NonExistingField');

            % TEST 1: Fill all subparameters with the same data
            this.testobj.Fill({'all'}, target_in);
            
            mobj = ?Parameters;
            
            for subpar_cnt=1:length(mobj.Properties)
                current_subpar = mobj.Properties{subpar_cnt}.Name;
                
                assertEqual(this.testobj.(current_subpar), target_out);
            end
            
            % TEST 2: to fill only one parameter with new data
            for subpar_cnt=1:length(mobj.Properties)
                current_subpar = mobj.Properties{subpar_cnt}.Name;
                
                target_in = subpar_cnt * ones(1,10) + 199;
                target_out = target_in;
                
                this.testobj.Fill({current_subpar}, target_in);
                
                for cnt=1:length(mobj.Properties)
                    tmp_subpar = mobj.Properties{cnt}.Name;
                    
                    if (cnt==subpar_cnt)
                        assertEqual(this.testobj.(tmp_subpar), target_out);
                    else
                        assertFalse(all(this.testobj.(tmp_subpar)==target_out));
                    end
                end
            end
            
            % TEST 3: to fill 3 parameters with new data
            target_in = subpar_cnt * ones(1,10) + 299;
            target_out = target_in;
            current_subpar = {'em', 'gm', 'pc0'};
            this.testobj.Fill(current_subpar, target_in);
            
            for cnt=1:length(mobj.Properties)
                tmp_subpar = mobj.Properties{cnt}.Name;
                
                if ismember(tmp_subpar, current_subpar)
                    assertEqual(this.testobj.(tmp_subpar), target_out);
                else
                    assertFalse(all(this.testobj.(tmp_subpar)==target_out));
                end
            end
        end
    end
end