function this = TestUpdateIterationStartValuesCalcFixedMets(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.ConnectMets({'Cho1', 'Cho2'}, {'conc', 'df2', 'df1'});
    this.testobj.ConnectMets({'all'}, {'gm', 'em_g'});
    this.testobj.FixMets({'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;
    
    default_target_out_conc     = [1 1 1];
    default_target_out_df12     = [0 0 0 0];
    default_target_out_gmem_g   = 0;
    default_target_out_t1       = [Inf Inf Inf Inf Inf];
    default_target_out_rest     = [0 0 0 0 0];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            target_out = default_target_out_conc;
        elseif strcmp(current_par, 't1')
            target_out = default_target_out_t1;
        elseif (strcmp(current_par, 'df2') || strcmp(current_par, 'df1'))
            target_out = default_target_out_df12;
        elseif (strcmp(current_par, 'gm') || strcmp(current_par, 'em_g'))
            target_out = default_target_out_gmem_g;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.start_values.(current_par), target_out);
    end

    % reset all met connections and free all mets
    this.testobj.ConnectMets({'Naa1'},{'all'});
    this.testobj.ConnectMets({'Cr1'},{'all'});
    this.testobj.ConnectMets({'Cr2'},{'all'});
    this.testobj.ConnectMets({'Cho1'},{'all'});
    this.testobj.ConnectMets({'Cho2'},{'all'});
    this.testobj.FreeMets({'all'}, {'all'});
    this.testobj.UpdateIteration;
    
    this.testobj.SetStartingValues({'Cr1'}, {'em', 'conc'}, 2);
    this.testobj.SetStartingValues({'Cr1'}, {'gm', 'df2'}, 3.2);
    this.testobj.ConnectMets({'Cho1', 'Cho2'}, {'conc', 'df2', 'df1'});
    this.testobj.ConnectMets({'all'}, {'em_g'});
    this.testobj.FixMets({'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;
    
    default_target_out_conc = [1 2 1];
    default_target_out_em   = [0 2 0 0 0];
    default_target_out_em_g = 0;
    default_target_out_df2  = [0 3.2 0 0];
    default_target_out_df1  = [0 0 0 0];
    default_target_out_gm   = [0 3.2 0 0 0];
    default_target_out_t1   = [Inf Inf Inf Inf Inf];
    default_target_out_rest = [0 0 0 0 0];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            target_out = default_target_out_conc;
        elseif strcmp(current_par, 't1')
            target_out = default_target_out_t1;
        elseif strcmp(current_par, 'gm')
            target_out = default_target_out_gm;
        elseif strcmp(current_par, 'em')
            target_out = default_target_out_em;
        elseif strcmp(current_par, 'em_g')
            target_out = default_target_out_em_g;
        elseif strcmp(current_par, 'df1')
            target_out = default_target_out_df1;
        elseif strcmp(current_par, 'df2')
            target_out = default_target_out_df2;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.start_values.(current_par), target_out);
    end
end