function this = TestSetLinPriorsValueExceptions(this)
    
    assertExceptionThrown(@() this.testobj.SetLinPriors({'Cr1', 'Cr2'}, {'conc'}, 3), 'Iteration:CreatePriorsMx:InconsArgs');
    assertExceptionThrown(@() this.testobj.SetLinPriors({'Cr1', 'Cr2'}, {'conc'}, [3 3 2 1]), 'Iteration:CreatePriorsMx:InconsArgs');
    assertExceptionThrown(@() this.testobj.SetLinPriors({'Cr1', 'Cr2'}, {'conc'}, 'all'), 'Iteration:CreatePriorsMx:WrongType');
    assertExceptionThrown(@() this.testobj.SetLinPriors({'Cr1', 'Cr2'}, {'conc'}, logical([1 0])), 'Iteration:CreatePriorsMx:WrongType');
end