function this = TestUpdateNeededAfterSetFixedValues(this)

    assertFalse(this.testobj.needUpdate);
    this.testobj.SetFixedValues([1 2], {'em_g'}, Inf);
    assertTrue(this.testobj.needUpdate);
end
