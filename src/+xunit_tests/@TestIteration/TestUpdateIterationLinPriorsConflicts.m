function this = TestUpdateIterationLinPriorsConflicts(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.ConnectMets({'Cho1', 'Cho2'}, {'conc'});
    this.testobj.ConnectMets({'Naa1', 'Cr1'}, {'conc'});
    this.testobj.FixMets({'Cr2'}, {'conc'});
    this.testobj.SetLinPriors({'Naa1', 'Cr1'}, {'conc'}, [-2 5]);
    assertExceptionThrown(@() this.testobj.UpdateIteration, 'Iteration:UpdateIteration:LinPriorConflict');
end