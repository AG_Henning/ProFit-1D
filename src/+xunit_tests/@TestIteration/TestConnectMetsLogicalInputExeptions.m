function this = TestConnectMetsLogicalInputExeptions(this)
    % in this test different aspects of connecting multiple metabolite
    % parameters are checked.

    % provoke execptions    
    assertExceptionThrown(@() this.testobj.ConnectMets([], {'gm'}), 'Iteration:ConnectMets:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.ConnectMets(logical([0 0 0 0 1 0 0 0 0 0]), 1), 'Iteration:ConnectMets:WrongType');
    assertExceptionThrown(@() this.testobj.ConnectMets(false(1,10), 'sds'), 'Iteration:ConnectMets:NoMetsSelect');
    assertExceptionThrown(@() this.testobj.ConnectMets(logical([1 0 0 0 0 0 0 0 0 0 0 1]), {'conc'}), 'Iteration:ConnectMets:IndexExceeds');
    assertExceptionThrown(@() this.testobj.ConnectMets(logical([0 0 1 1 0 0 0 0 0 0]), {'ultimate_desaster'}), 'Iteration:ConnectMets:WrongArgument');
end
