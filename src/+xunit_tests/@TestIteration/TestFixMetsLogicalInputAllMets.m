function this = TestFixMetsLogicalInputAllMets(this)

    this.testobj.FixMets(true(1,this.testobj.nr_mets), {'pc1', 't1'});
    
    target_out = true(1,this.testobj.nr_mets);
    
    mobj = metaclass(this.testobj.fixed_mets);
    for par_cnt = 1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'pc1', 't1'})
            assertEqual(this.testobj.fixed_mets.(current_par), target_out);
        else
            assertEqual(this.testobj.fixed_mets.(current_par), false(1, this.testobj.nr_mets));
        end
    end
end