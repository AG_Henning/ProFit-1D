function this = TestUpdateIterationLBConflicts(this)

    % check if the lower of two lower bounds is chosen in case of conflict
    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.ConnectMets({'Cr1', 'Cr2'}, {'conc'});
    this.testobj.SetLB({'Cr1'}, {'conc'}, 2);
    this.testobj.UpdateIteration;
    
    default_target_out_conc = [-Inf -Inf -Inf -Inf];
    default_target_out_rest = [-Inf -Inf -Inf -Inf -Inf];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
           target_out = default_target_out_conc; 
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.lower_bounds.(current_par), target_out);
    end
    
    % reset connection & starting value
    this.testobj.ConnectMets({'Cr1'}, {'conc'});
    this.testobj.SetLB({'Cr1'}, {'conc'}, -Inf);
    this.testobj.SetLB({'Cr2'}, {'conc'}, -Inf);
    this.testobj.UpdateIteration;
    
    this.testobj.SetLB({'Cr1'}, {'conc'}, 0);
    this.testobj.ConnectMets({'Cr1', 'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;
    
    default_target_out_conc = [-Inf -Inf -Inf -Inf];
    default_target_out_rest = [-Inf -Inf -Inf -Inf -Inf];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
           target_out = default_target_out_conc; 
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.lower_bounds.(current_par), target_out);
    end
end
