function this = TestSetUB(this)

    this.testobj.SetUB({'baseline', 'Glu'}, {'em', 'gm', 'em_g'}, 8);
    this.testobj.SetUB({'Naa1'}, {'em', 'gm', 'em_g'}, 10);
    
    target_out = [10 nan nan nan 8 nan nan nan nan 8];
    
    mobj = metaclass(this.testobj.full_upper_bounds);
    for par_cnt = 1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'em', 'gm', 'em_g'})
            assertEqual(this.testobj.full_upper_bounds.(current_par), target_out);
        else
            assertEqual(this.testobj.full_upper_bounds.(current_par), nan(1, this.testobj.nr_mets));
        end
    end
end