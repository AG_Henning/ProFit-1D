function this = TestConnectMetsGlobalTextInput(this)
    % in this test different aspects of connecting multiple metabolite
    % parameters are checked.
    
    this.testobj.ConnectMets({'global'}, {'gm', 'pc0'});
    target_mx = true(10);
    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'gm', 'pc0'})
            assertEqual(this.testobj.met_connections.(current_par), target_mx);
        else
            assertEqual(this.testobj.met_connections.(current_par), logical(eye(10)));
        end
    end
end
    