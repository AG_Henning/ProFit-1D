function this = TestUpdateNeededAfterSetStartingValues(this)

    assertFalse(this.testobj.needUpdate);
    this.testobj.SetStartingValues([1 2], {'em_g'}, 50);
    assertTrue(this.testobj.needUpdate);
end