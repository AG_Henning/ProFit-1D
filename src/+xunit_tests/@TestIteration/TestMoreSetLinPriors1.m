function this = TestMoreSetLinPriors1(this)

    % The name Naa2 is present to times in the metabolite list so two lines
    % have to be created
    this.testobj.SetLinPriors({'Naa2', 'Glu', 'Gln', 'baseline'}, {'conc', 'em'}, [-1 2 -3 4 ]);
    
    target_out = [0 -1 0 0 2 0 -3 0 0 4; ...
                  0 0 0 0 2 0 -3 0 -1 4];
    
    mobj = metaclass(this.testobj.full_lin_priors);
    for par_cnt = 1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'conc', 'em'})
            assertEqual(this.testobj.full_lin_priors.(current_par), target_out);
        else
            assertEqual(this.testobj.full_lin_priors.(current_par), []);
        end
    end
end