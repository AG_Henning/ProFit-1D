function this = TestIdxStd(this)
    % check if the idx property gets setup correctly taking into account
    % the specified linkage between different parameters of different
    % metabolite signals
    
    this.testobj.ConnectMets({'Cr1', 'Cr2'}, {'conc', 'df2'});
    this.testobj.UpdateIteration();
    
    target_out = [1 2 3 3 4 5 6 7 8 9];
    
    mobj = metaclass(this.testobj.idx);
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'conc', 'df2'})
            assertEqual(this.testobj.idx.(current_par), target_out);
        else
            assertEqual(this.testobj.idx.(current_par), 1:this.testobj.nr_mets);
        end
    end
end
