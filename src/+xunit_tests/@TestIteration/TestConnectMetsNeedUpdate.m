function this = TestConnectMetsNeedUpdate(this)
    % check if new metabolite connections are made the object will be
    % marked to need an update
    
    assertFalse(this.testobj.needUpdate);
    this.testobj.ConnectMets([1 2], {'em_g'});
    assertTrue(this.testobj.needUpdate);
end
