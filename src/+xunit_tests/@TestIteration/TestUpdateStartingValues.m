function this = TestUpdateStartingValues(this)

    % check default behaviour
    obj = this.testobj.UpdateStartingValues();
    assertEqual(this.testobj, obj);
   
    % check custom updating function
    this.testobj.UpdateStartingValues = @static_update_startvalues;
    obj = this.testobj.UpdateStartingValues(this.testobj,[]);

    mobj = ?Parameters;
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, 'conc')
            assertEqual(obj.full_start_values.(current_par), 666*ones(1,10));
        else
            assertEqual(obj.full_start_values.(current_par), this.testobj.full_start_values.(current_par));
        end
    end
end