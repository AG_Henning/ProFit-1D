function this = TestIdxIndividual(this)
    % check if the idx property gets setup correctly taking into account
    % the specified linkage between different parameters of different
    % metabolite signals
    
    this.testobj.ConnectMets({'global'}, {'all'});
    this.testobj.ConnectMets({'individual'}, {'em'; 'df2'});
    this.testobj.UpdateIteration();
    
    target_out = 1:this.testobj.nr_mets;
    
    mobj = metaclass(this.testobj.idx);
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'em', 'df2'})
            assertEqual(this.testobj.idx.(current_par), target_out);
        else
            assertEqual(this.testobj.idx.(current_par), ones(1,this.testobj.nr_mets));
        end
    end
end
