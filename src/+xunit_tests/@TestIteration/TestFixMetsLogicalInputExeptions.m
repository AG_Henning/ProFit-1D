function this = TestFixMetsLogicalInputExeptions(this)

    assertExceptionThrown(@() this.testobj.FixMets([], {'gm'}), 'Iteration:MetValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.FixMets(logical([1 1 0 0 0 0 0 0 0 0 1 0 1 0 1]), {'gm'}), 'Iteration:MetValidator:IndexExceeds');
    assertExceptionThrown(@() this.testobj.FixMets(false(1,10), {'gm'}), 'Iteration:MetValidator:NoMetsSelect');
end