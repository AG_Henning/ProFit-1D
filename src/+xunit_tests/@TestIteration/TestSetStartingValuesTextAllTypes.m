function this = TestSetStartingValuesTextAllTypes(this)
    
    this.testobj.SetStartingValues({'Cr1', 'Cr2', 'baseline'}, {'all'}, 10);
    target_out1 = [1 1 10 10 1 1 1 1 1 10];
    target_out2 = [0 0 10 10 0 0 0 0 0 10];
    target_out3 = [Inf Inf 10 10 Inf Inf Inf Inf Inf 10];
        
    mobj = ?Parameters;
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, 'conc')
            assertEqual(this.testobj.full_start_values.(current_par), target_out1);
        elseif ismember(current_par, 't1')
            assertEqual(this.testobj.full_start_values.(current_par), target_out3);
        else
            assertEqual(this.testobj.full_start_values.(current_par), target_out2);
        end
    end
end
