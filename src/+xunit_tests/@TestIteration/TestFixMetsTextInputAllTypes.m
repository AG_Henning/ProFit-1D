function this = TestFixMetsTextInputAllTypes(this)

    this.testobj.FixMets({'Naa1', 'baseline'}, {'all'});
    
    target_out = logical([1 0 0 0 0 0 0 0 0 1]);
    
    mobj = metaclass(this.testobj.fixed_mets);
    for par_cnt = 1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.fixed_mets.(current_par), target_out);
    end
end