function this = TestUpdateIterationLinPriorsCalcFixedMets(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.ConnectMets({'Cho1', 'Cho2'}, {'conc', 'df2', 'df1'});
    this.testobj.ConnectMets({'all'}, {'gm', 'em_g'});
    this.testobj.FixMets({'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;

    target_out = [];

    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.lin_priors.(current_par), target_out);
    end

    % reset all met connections and free all mets
    this.testobj.ConnectMets({'Naa1'},{'all'});
    this.testobj.ConnectMets({'Cr1'},{'all'});
    this.testobj.ConnectMets({'Cr2'},{'all'});
    this.testobj.ConnectMets({'Cho1'},{'all'});
    this.testobj.ConnectMets({'Cho2'},{'all'});
    this.testobj.FreeMets({'all'}, {'all'});
    this.testobj.UpdateIteration;

    this.testobj.ConnectMets({'Cho1', 'Cho2'}, {'conc', 'df2', 'df1'});
    this.testobj.ConnectMets({'all'}, {'em_g'});
    this.testobj.FixMets({'Cr2'}, {'conc'});
    this.testobj.SetLinPriors({'Cr1', 'Cho2'}, {'em', 'conc'}, [-1 1]);
    this.testobj.SetLinPriors({'Naa1', 'Cr1'}, {'gm', 'df2'}, [1 -3]);
    this.testobj.UpdateIteration;

    default_target_out_conc = [0 -1 1];
    default_target_out_em   = [0 -1 0 0 1];
    default_target_out_em_g = [];
    default_target_out_df2  = [1 -3 0 0];
    default_target_out_df1  = [];
    default_target_out_gm   = [1 -3 0 0 0];
    default_target_out_rest = [];

    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            target_out = default_target_out_conc;
        elseif strcmp(current_par, 'gm')
            target_out = default_target_out_gm;
        elseif strcmp(current_par, 'em')
            target_out = default_target_out_em;
        elseif strcmp(current_par, 'em_g')
            target_out = default_target_out_em_g;
        elseif strcmp(current_par, 'df1')
            target_out = default_target_out_df1;
        elseif strcmp(current_par, 'df2')
            target_out = default_target_out_df2;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.lin_priors.(current_par), target_out);
    end
    
    % reset all met connections and free all mets
    this.testobj.ConnectMets({'Naa1'},{'all'});
    this.testobj.ConnectMets({'Cr1'},{'all'});
    this.testobj.ConnectMets({'Cr2'},{'all'});
    this.testobj.ConnectMets({'Cho1'},{'all'});
    this.testobj.ConnectMets({'Cho2'},{'all'});
    this.testobj.FreeMets({'all'}, {'all'});
    this.testobj.SetLinPriors({'all'}, {'all'}, []);
    this.testobj.UpdateIteration;

    this.testobj.ConnectMets({'Cho1', 'Cho2', 'Cr1'}, {'conc'});
    this.testobj.FixMets({'Cr2'}, {'conc'});
    this.testobj.SetLinPriors({'Naa1', 'Cho2'}, {'conc'}, [-2 5]);
    this.testobj.UpdateIteration;

    default_target_out_conc = [-2 5];
    default_target_out_rest = [];

    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            target_out = default_target_out_conc;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.lin_priors.(current_par), target_out);
    end
    
    % reset all met connections and free all mets
    this.testobj.ConnectMets({'Naa1'},{'all'});
    this.testobj.ConnectMets({'Cr1'},{'all'});
    this.testobj.ConnectMets({'Cr2'},{'all'});
    this.testobj.ConnectMets({'Cho1'},{'all'});
    this.testobj.ConnectMets({'Cho2'},{'all'});
    this.testobj.FreeMets({'all'}, {'all'});
    this.testobj.SetLinPriors({'all'}, {'all'}, []);
    this.testobj.UpdateIteration;

    this.testobj.ConnectMets({'Cho1', 'Cho2'}, {'conc'});
    this.testobj.ConnectMets({'Naa1', 'Cr1'}, {'conc'});
    this.testobj.FixMets({'Cr2'}, {'conc'});
    this.testobj.SetLinPriors({'Naa1', 'Cho2'}, {'conc'}, [-2 5]);
    this.testobj.UpdateIteration;

    default_target_out_conc = [-2 5];
    default_target_out_rest = [];

    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            target_out = default_target_out_conc;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.lin_priors.(current_par), target_out);
    end
end
