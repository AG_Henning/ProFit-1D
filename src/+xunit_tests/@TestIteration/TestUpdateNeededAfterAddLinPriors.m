function this = TestUpdateNeededAfterAddLinPriors(this)

    assertFalse(this.testobj.needUpdate);
    this.testobj.AddLinPriors([1 2], {'em_g'}, [-1 1]);
    assertTrue(this.testobj.needUpdate);
end