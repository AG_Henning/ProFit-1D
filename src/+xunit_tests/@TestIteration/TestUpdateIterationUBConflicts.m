function this = TestUpdateIterationUBConflicts(this)

    % check if the upper of two upper bounds is chosen in case of conflict
    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.ConnectMets({'Cr1', 'Cr2'}, {'conc'});
    this.testobj.SetUB({'Cr1'}, {'conc'}, 2);
    this.testobj.UpdateIteration;
    
    default_target_out_conc = [Inf Inf Inf Inf];
    default_target_out_rest = [Inf Inf Inf Inf Inf];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
           target_out = default_target_out_conc; 
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.upper_bounds.(current_par), target_out);
    end
    
    % reset connection & starting value
    this.testobj.ConnectMets({'Cr1'}, {'conc'});
    this.testobj.SetUB({'Cr1'}, {'conc'}, Inf);
    this.testobj.SetUB({'Cr2'}, {'conc'}, Inf);
    this.testobj.UpdateIteration;
    
    this.testobj.SetUB({'Cr1'}, {'conc'}, 0);
    this.testobj.SetUB({'Cr2'}, {'conc'}, 10);
    this.testobj.ConnectMets({'Cr1', 'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;
    
    default_target_out_conc = [Inf 10 Inf Inf];
    default_target_out_rest = [Inf Inf Inf Inf Inf];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
           target_out = default_target_out_conc; 
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.upper_bounds.(current_par), target_out);
    end
end