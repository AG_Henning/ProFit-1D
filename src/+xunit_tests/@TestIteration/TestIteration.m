classdef TestIteration < TestCase
    
    properties
        
        testobj;
        basis;
    end
    
    methods
        
        function this = TestIteration(name)
            
            this = this@TestCase(name);
        end
        
        function this = setUp(this)
            
            this.basis.met = {'Naa1', 'Naa2', 'Cr1', 'Cr2', 'Glu', 'Cho1', 'Gln', 'Cho2', 'Naa2', 'baseline'};
            this.testobj   = Iteration(this.basis);
        end
        
        this = TestAfterCreation(this);
        this = TestInitializedValues(this);
        this = TestActivateMets(this);
        this = TestDeactivateMets(this);
        % connecting metabolites tests
        this = TestConnectMetsTextInput1(this);
        this = TestConnectMetsGlobalTextInput(this);
        this = TestConnectMetsIndividualTextInput(this);
        this = TestConnectMetsNameTextInput(this);
        this = TestUnConnectMetsTextInput(this);
        this = TestConnectMetsTextInputExeptions(this);
        this = TestConnectMetsDoubleInput(this);
        this = TestUnConnectMetsDoubleInput(this);
        this = TestConnectMetsDoubleInputExeptions(this);
        this = TestConnectMetsLogicalInput(this);
        this = TestUnConnectMetsLogicalInput(this);
        this = TestConnectMetsLogicalInputExeptions(this);
        this = TestConnectMetsNeedUpdate(this);
        this = TestConnectMetsFillNeedUpdate(this);
        this = TestConnectMetsTextInputAllTypes(this);
        this = TestConnectMetsDoubleInputAllTypes(this);
        this = TestConnectMetsLogicalInputAllTypes(this);
        this = TestConnectMetsGlobalInputAllTypes(this);
        this = TestConnectMetsIndividualInputAllTypes(this);
        this = TestConnectMetsNameInputAllTypes(this);
        
        % test the index creations from metabolite connection data
        this = TestIdxStd(this);
        this = TestIdxGlobal(this);
        this = TestIdxIndividual(this);
        this = TestIdxName(this);
        
        % tests for fixing metabolites parameters
        this = TestFixMetsTextInputMets(this);
        this = TestFixMetsTextInputAllMets(this);
        this = TestFixMetsTextInputAllTypes(this);
        this = TestFixMetsDoubleInputMets(this);
        this = TestFixMetsDoubleInputAllMets(this);
        this = TestFixMetsDoubleInputAllTypes(this);
        this = TestFixMetsLogicalInputMets(this);
        this = TestFixMetsLogicalInputAllMets(this);
        this = TestFixMetsLogicalInputAllTypes(this);
        this = TestFixMetsTextInputExeptions(this);
        this = TestFixMetsDoubleInputExeptions(this);
        this = TestFixMetsLogicalInputExeptions(this);
        this = TestFixMetsInputTypeExeptions(this);
        this = TestFreeMets(this);

        % test for starting values to set
        this = TestSetStartingValuesText(this);
        this = TestSetStartingValuesTextAllMets(this);
        this = TestSetStartingValuesTextAllTypes(this);
        this = TestSetStartingValuesDouble(this);
        this = TestSetStartingValuesLogical(this);
        this = TestSetStartingValuesTextExpections(this);
        this = TestSetStartingValuesDoubelExpections(this);
        this = TestSetStartingValuesLogicalExpections(this);
        this = TestSetStartingValuesTypeExpections(this);
        this = TestSetStartingValuesValuesExpections(this);
        
        % test for setting lower bounds (only basic test needed)
        this = TestSetLB(this);
        this = TestSetUB(this);
        this = TestSetFixedValues(this);
        
        this = TestSetLinPriors(this);
        this = TestSetLinPriorsDouble(this);
        this = TestSetLinPriorsLogical(this);
        this = TestSetLinPriorsValueExceptions(this);
        this = TestMoreSetLinPriors1(this);
        this = TestAddLinPriors(this);
        this = TestAddLinPriorsDouble(this);
        this = TestAddLinPriorsLogical(this);
        
        % test more update needed triggers
        this = TestUpdateNeededAfterSetStartingValues(this);
        this = TestUpdateNeededAfterSetFixMets(this);
        this = TestUpdateNeededAfterSetLB(this);
        this = TestUpdateNeededAfterSetUB(this);
        this = TestUpdateNeededAfterSetLinPriors(this);
        this = TestUpdateNeededAfterAddLinPriors(this);
        this = TestUpdateNeededAfterSetFixedValues(this);
        
        this = TestMetaboliteHashTable(this);
        
        % Tests that check correct behaviour of different aspects of
        % UpdateIteration
        this = TestUpdateIterationFullMaskCalc(this);
        this = TestUpdateIterationFullMaskCalcFixedMets(this);
        this = TestUpdateIterationSubMaskCalc(this);
        this = TestUpdateIterationSubMaskCalcFixedMets(this);
        this = TestUpdateIterationNrMetsCalc(this);
        this = TestUpdateIterationNrMetsCalcFixedMets(this);
        this = TestUpdateIterationStartValuesCalc(this);
        this = TestUpdateIterationStartValuesCalcFixedMets(this);
        this = TestUpdateIterationStartValuesConflicts(this);
        this = TestUpdateIterationFixedValuesCalc(this);
        this = TestUpdateIterationLBCalc(this);
        this = TestUpdateIterationLBCalcFixedMets(this);
        this = TestUpdateIterationLBConflicts(this);
        this = TestUpdateIterationUBCalc(this);
        this = TestUpdateIterationUBCalcFixedMets(this);
        this = TestUpdateIterationUBConflicts(this);
        this = TestUpdateIterationLinPriorsCalc(this);
        this = TestUpdateIterationLinPriorsCalcFixedMets(this);
        this = TestUpdateIterationLinPriorsConflicts(this);
        
        % Tests to handle the update procedure during different iterations
        this = TestUpdateStartingValues(this);
        this = TestUpdateLB(this);
        this = TestUpdateUB(this);
        
    end
end