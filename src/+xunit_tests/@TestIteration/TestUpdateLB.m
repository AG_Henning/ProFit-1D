function this = TestUpdateLB(this)

    % check default behaviour
    obj = this.testobj.UpdateLowerBounds();
    assertEqual(this.testobj, obj);
   
    % check custom updating function
    this.testobj.UpdateLowerBounds = @static_update_lb;
    obj = this.testobj.UpdateLowerBounds(this.testobj,[]);

    mobj = ?Parameters;
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, 'conc')
            assertEqual(obj.full_lower_bounds.(current_par), zeros(1,10));
        else
            assertEqual(obj.full_lower_bounds.(current_par), this.testobj.full_lower_bounds.(current_par));
        end
    end
end