function this = TestSetStartingValuesLogicalExpections(this)

    assertExceptionThrown(@() this.testobj.SetStartingValues([], {'gm'}, 10), 'Iteration:MetValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.SetStartingValues(logical([1 1 0 0 0 0 0 0 0 0 1 0 1 0 1]), {'gm'}, 10), 'Iteration:MetValidator:IndexExceeds');
    assertExceptionThrown(@() this.testobj.SetStartingValues(false(1,10), {'gm'}, 10), 'Iteration:MetValidator:NoMetsSelect');
end