function this = TestConnectMetsTextInputExeptions(this)
    % in this test different aspects of connecting multiple metabolite
    % parameters are checked.

    % provoke execptions    
    assertExceptionThrown(@() this.testobj.ConnectMets({}, {'gm'}), 'Iteration:ConnectMets:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.ConnectMets('sdf', 'sds'), 'Iteration:ConnectMets:WrongType');
    assertExceptionThrown(@() this.testobj.ConnectMets({'Naa1', 'Naa5'}, {'conc'}), 'Iteration:ConnectMets:NoKey');
    assertExceptionThrown(@() this.testobj.ConnectMets({'Cr1', 'Cr2'}, {'ultimate_desaster'}), 'Iteration:ConnectMets:WrongArgument');
    assertExceptionThrown(@() this.testobj.ConnectMets({'Glu', 'all'}, {'gm'}), 'Iteration:ConnectMets:TooManyArguments');
    assertExceptionThrown(@() this.testobj.ConnectMets({'global', 'all'}, {'gm'}), 'Iteration:ConnectMets:TooManyArguments');
end
