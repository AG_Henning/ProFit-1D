function this = TestUpdateIterationNrMetsCalcFixedMets(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.FixMets({'Cr1', 'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            assertEqual(this.testobj.nr.(current_par).full, 5);
            assertEqual(this.testobj.nr.(current_par).free, 3);
            assertEqual(this.testobj.nr.(current_par).fixed, 2);            
        else
            assertEqual(this.testobj.nr.(current_par).full, 5);
            assertEqual(this.testobj.nr.(current_par).free, 5);
            assertEqual(this.testobj.nr.(current_par).fixed, 0);
        end
    end
end