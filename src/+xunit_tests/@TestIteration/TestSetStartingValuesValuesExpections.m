function this = TestSetStartingValuesValuesExpections(this)

    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, {'conc'}, []), 'Iteration:ValuesValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, {'conc'}, [10 13]), 'Iteration:ValuesValidator:TooMuchInput');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, {'conc'}, 'ddaas'), 'Iteration:ValuesValidator:WrongType');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, {'conc'}, {10}), 'Iteration:ValuesValidator:WrongType');
end