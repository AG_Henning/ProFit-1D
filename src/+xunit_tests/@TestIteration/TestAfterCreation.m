function this = TestAfterCreation(this)

    % in this test it is checked if some neccessary methods and
    % properies are present. It is not checked if any unknown
    % methods or properties are implemented too.

    % fill cellstr with the names of methods and properties that
    % should be availbale
    mhobj = ?handle;
    method_list = cellfun(@(meth) meth.Name, mhobj.Methods, 'UniformOutput', false);
    prop_list   = cellfun(@(prop) prop.Name, mhobj.Properties, 'UniformOutput', false);

    custom_methods = {'Iteration'; 'SetupHashTables'; 'ActivateMets'; ...
                      'UpdateIteration'; 'MarkForUpdate',; 'CreateListeners'; ...
                      'DeactivateMets'; 'ActiveMetsOnOff'; 'ConnectMets'; 'InitValues'; ...
                      'FixMets'; 'FreeMets'; 'MetInputValidator'; ...
                      'TypeInputValidator'; 'SetStartingValues'; 'SetInternalParameters'; ...
                      'ValueInputValidator'; 'SetLB'; 'SetUB'; 'SetFixedValues'; 'SetLinPriors'; ...
                      'AddLinPriors'; 'CreateLinPriorMatrix'; 'ResolveConncetionsLinPriorMatrix'};
    custom_props   = {'basis'; 'nr_mets'; 'met_idx'; 'met_boolidx'; 'active_mets'; ...
                      'nr_act_mets'; 'needUpdate'; 'hUpdates'; 'met_connections'; 'idx'; ...
                      'fixed_mets'; 'full_start_values'; 'full_lower_bounds'; 'full_upper_bounds'; ...
                      'full_fixed_values'; 'full_lin_priors'; 'fixed_values'; 'start_values'; ...
                      'upper_bounds'; 'lower_bounds'; 'lin_priors'; 'full_mask' ; 'sub_mask'; ...
                      'nr'; 'mapping_mx'; 'UpdateStartingValues'; 'UpdateLowerBounds'; 'UpdateUpperBounds'; ...
                      'use_sp_baseline'; 'use_sp_lineshape'};

    method_list = sort(vertcat(method_list, custom_methods));
    prop_list   = sort(vertcat(prop_list, custom_props));

    mobj = metaclass(this.testobj);

    % get all availbale methods and properties and check if the
    % expected ones arme among them

    % version for R2010a
    available_methods = sort(cellfun(@(x) x.Name, mobj.Methods, 'UniformOutput', false));
    available_props   = sort(cellfun(@(x) x.Name, mobj.Properties, 'UniformOutput', false));

    %            % This syntax is only working in Matlab 2011b so this is version
    %            % dependent (careful)
    %            available_methods = arrayfun(@(x) x.Name, mobj.MethodList, 'UniformOutput', false);
    %            available_props   = arrayfun(@(x) x.Name, mobj.PropertyList, 'UniformOutput', false);

    % now search and check
    assertEqual(available_methods, method_list);
    assertEqual(available_props, prop_list);
end