function this = TestActivateMets(this)
    % Activate a set of metabolites in 3 different ways and check
    % if the correct ones are activated and if related object
    % properties are updated

    assertTrue(length(this.testobj.active_mets)==10);

    this.testobj.ActivateMets({'Naa2', 'Cr2', 'Glu', 'baseline'});
    assertEqual(this.testobj.active_mets, logical([0 1 0 1 1 0 0 0 1 1]));
    assertEqual(this.testobj.nr_act_mets, 5);
    assertTrue(this.testobj.needUpdate);
    this.testobj.DeactivateMets({'all'});   % to start fresh for the next test
    this.testobj.UpdateIteration();         % needs to be done to reset the needUpdate property

    this.testobj.ActivateMets({'all'});
    assertEqual(this.testobj.active_mets, logical([1 1 1 1 1 1 1 1 1 1]));
    assertEqual(this.testobj.nr_act_mets, 10);
    assertTrue(this.testobj.needUpdate);
    this.testobj.DeactivateMets({'all'});   % to start fresh for the next test
    this.testobj.UpdateIteration();         % needs to be done to reset the needUpdate property

    this.testobj.ActivateMets([2 5 7 3 9]);
    assertEqual(this.testobj.active_mets, logical([0 1 1 0 1 0 1 0 1 0]));
    assertEqual(this.testobj.nr_act_mets, 5);
    assertTrue(this.testobj.needUpdate);
    this.testobj.DeactivateMets({'all'});   % to start fresh for the next test
    this.testobj.UpdateIteration();         % needs to be done to reset the needUpdate property

    this.testobj.ActivateMets(logical([1 0 1 0 1 1 0 1 1 0]));
    assertEqual(this.testobj.active_mets, logical([1 0 1 0 1 1 0 1 1 0]));
    assertEqual(this.testobj.nr_act_mets, 6);
    assertTrue(this.testobj.needUpdate);
    this.testobj.DeactivateMets({'all'});   % to start fresh for the next test
    this.testobj.UpdateIteration();         % needs to be done to reset the needUpdate property

    % Test some wrong inputs
    assertExceptionThrown(@() this.testobj.ActivateMets('wronginput'), 'Iteration:MetsOnOff:WrongInputFormat');
    assertExceptionThrown(@() this.testobj.ActivateMets({'Naa2', 'nonExisting', 'Glu'}), 'Iteration:MetsOnOff:WrongMet');
    assertExceptionThrown(@() this.testobj.ActivateMets([1 2 3 4 11]), 'Iteration:MetsOnOff:IndexExceeds');
    assertExceptionThrown(@() this.testobj.ActivateMets([0 1 2 3 4 9]), 'Iteration:MetsOnOff:IndexExceeds');
    assertExceptionThrown(@() this.testobj.ActivateMets(logical([0 1 1 0 0 1])), 'Iteration:MetsOnOff:IndexExceeds');
    assertExceptionThrown(@() this.testobj.ActivateMets(logical([0 1 1 0 0 1 0 0 1 1 0 1 1 0 1])), 'Iteration:MetsOnOff:IndexExceeds');
end