function this = TestUpdateNeededAfterSetUB(this)

    assertFalse(this.testobj.needUpdate);
    this.testobj.SetUB([1 2], {'em_g'}, 9);
    assertTrue(this.testobj.needUpdate);
end