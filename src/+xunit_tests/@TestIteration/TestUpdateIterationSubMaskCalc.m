function this = TestUpdateIterationSubMaskCalc(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.UpdateIteration;
    
    target_out = logical([1 0 0 0 0; ...
                          0 1 0 0 0; ...
                          0 0 1 0 0; ...
                          0 0 0 1 0; ...
                          0 0 0 0 1]);    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.sub_mask.(current_par).full, target_out);
        assertEqual(this.testobj.sub_mask.(current_par).free, target_out);
        assertEqual(this.testobj.sub_mask.(current_par).fixed, false(5));
    end
end