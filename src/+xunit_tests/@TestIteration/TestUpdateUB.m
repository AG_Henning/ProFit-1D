function this = TestUpdateUB(this)

    % check default behaviour
    obj = this.testobj.UpdateLowerBounds();
    assertEqual(this.testobj, obj);
   
    % check custom updating function
    this.testobj.UpdateUpperBounds = @static_update_ub;
    obj = this.testobj.UpdateUpperBounds(this.testobj,[]);

    mobj = ?Parameters;
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, 'conc')
            assertEqual(obj.full_upper_bounds.(current_par), 100*ones(1,10));
        else
            assertEqual(obj.full_upper_bounds.(current_par), this.testobj.full_upper_bounds.(current_par));
        end
    end
end