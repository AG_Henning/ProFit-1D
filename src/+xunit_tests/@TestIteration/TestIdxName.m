function this = TestIdxName(this)
    % check if the idx property gets setup correctly taking into account
    % the specified linkage between different parameters of different
    % metabolite signals
    
    this.testobj.ConnectMets({'name'}, {'df1'; 'pc1'});
    this.testobj.UpdateIteration();
    
    target_out = [1 2 3 4 5 6 7 8 2 9];
                     
    mobj = metaclass(this.testobj.idx);
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'df1', 'pc1'})
            assertEqual(this.testobj.idx.(current_par), target_out);
        else
            assertEqual(this.testobj.idx.(current_par), 1:this.testobj.nr_mets);
        end
    end
end
