function this = TestIdxGlobal(this)
    % check if the idx property gets setup correctly taking into account
    % the specified linkage between different parameters of different
    % metabolite signals
    
    this.testobj.ConnectMets({'global'}, {'gm'; 'em_g'});
    this.testobj.UpdateIteration();
    
    target_out = [1 1 1 1 1 1 1 1 1 1];
    
    mobj = metaclass(this.testobj.idx);
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'gm', 'em_g'})
            assertEqual(this.testobj.idx.(current_par), target_out);
        else
            assertEqual(this.testobj.idx.(current_par), 1:this.testobj.nr_mets);
        end
    end
end
