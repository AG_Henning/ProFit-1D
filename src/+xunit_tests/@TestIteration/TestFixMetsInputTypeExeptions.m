function this = TestFixMetsInputTypeExeptions(this)

    assertExceptionThrown(@() this.testobj.FixMets({'Cr1', 'Cr2'}, {'ultimate_desaster'}), 'Iteration:TypeValidator:NoKey');
    assertExceptionThrown(@() this.testobj.FixMets({'Cr1', 'Cr2'}, {}), 'Iteration:TypeValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.FixMets({'Cr1', 'Cr2'}, []), 'Iteration:TypeValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.FixMets({'Cr1', 'Cr2'}, 'all'), 'Iteration:TypeValidator:WrongType');
    assertExceptionThrown(@() this.testobj.FixMets({'Cr1', 'Cr2'}, [1 2 3]), 'Iteration:TypeValidator:WrongType');
    assertExceptionThrown(@() this.testobj.FixMets({'Cr1', 'Cr2'}, {'gm', 'mmpf'}), 'Iteration:TypeValidator:NoKey');
end