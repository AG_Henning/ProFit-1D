function this = TestUpdateIterationSubMaskCalcFixedMets(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.FixMets({'Cr1', 'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;
    
    target_std = logical(eye(5));
                           
    target_cfree = logical([1 0 0 0 0; ...
                            0 0 0 0 0; ...
                            0 0 0 0 0; ...
                            0 0 0 1 0; ...
                            0 0 0 0 1]);
                        
    target_cfixed = logical([0 0 0 0 0; ...
                             0 1 0 0 0; ...
                             0 0 1 0 0; ...
                             0 0 0 0 0; ...
                             0 0 0 0 0]);

    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            assertEqual(this.testobj.sub_mask.(current_par).full, target_std);
            assertEqual(this.testobj.sub_mask.(current_par).free, target_cfree);
            assertEqual(this.testobj.sub_mask.(current_par).fixed, target_cfixed);            
        else
            assertEqual(this.testobj.sub_mask.(current_par).full, target_std);
            assertEqual(this.testobj.sub_mask.(current_par).free, target_std);
            assertEqual(this.testobj.sub_mask.(current_par).fixed, false(5));
        end
    end
end