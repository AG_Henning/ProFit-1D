function this = TestUpdateIterationStartValuesConflicts(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.ConnectMets({'Cr1', 'Cr2'}, {'conc'});
    this.testobj.SetStartingValues({'Cr1'}, {'conc'}, 2);
    this.testobj.UpdateIteration;
    
    default_target_out_conc = [1 1.5 1 1];
    default_target_out_t1   = [Inf Inf Inf Inf Inf];
    default_target_out_rest = [0 0 0 0 0];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
           target_out = default_target_out_conc; 
        elseif strcmp(current_par, 't1')
           target_out = default_target_out_t1; 
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.start_values.(current_par), target_out);
    end
    
    % reset connection & starting value
    this.testobj.ConnectMets({'Cr1'}, {'conc'});
    this.testobj.SetStartingValues({'Cr1'}, {'conc'}, 1);
    this.testobj.SetStartingValues({'Cr2'}, {'conc'}, 1);
    this.testobj.UpdateIteration;
    
    this.testobj.SetStartingValues({'Cr1'}, {'conc'}, 2);
    this.testobj.ConnectMets({'Cr1', 'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;
    
    default_target_out_conc = [1 1.5 1 1];
    default_target_out_t1   = [Inf Inf Inf Inf Inf];
    default_target_out_rest = [0 0 0 0 0];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
           target_out = default_target_out_conc; 
        elseif strcmp(current_par, 't1')
           target_out = default_target_out_t1; 
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.start_values.(current_par), target_out);
    end
end