function this = TestFixMetsTextInputMets(this)

    this.testobj.FixMets({'baseline', 'Glu'}, {'em', 'gm', 'em_g', 'pc0', 'pc1'});
    
    target_out = logical([0 0 0 0 1 0 0 0 0 1]);
    
    mobj = metaclass(this.testobj.fixed_mets);
    for par_cnt = 1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'em', 'gm', 'em_g', 'pc0', 'pc1'})
            assertEqual(this.testobj.fixed_mets.(current_par), target_out);
        else
            assertEqual(this.testobj.fixed_mets.(current_par), false(1, this.testobj.nr_mets));
        end
    end
end