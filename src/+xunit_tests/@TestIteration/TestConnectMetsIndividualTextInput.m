function this = TestConnectMetsIndividualTextInput(this)
    % in this test different aspects of connecting multiple metabolite
    % parameters are checked.
    
    this.testobj.ConnectMets({'global'}, {'em'});    % perturbate from initial values first
    this.testobj.ConnectMets({'individual'}, {'em'});
    target_mx = logical(eye(10));
    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'em')
            assertEqual(this.testobj.met_connections.(current_par), target_mx);
        else
            assertEqual(this.testobj.met_connections.(current_par), logical(eye(10)));
        end
    end
end
