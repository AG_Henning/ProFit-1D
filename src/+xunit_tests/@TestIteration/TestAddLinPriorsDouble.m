function this = TestAddLinPriorsDouble(this)

    this.testobj.AddLinPriors([1 5], {'conc'}, [-1 1]); % means concentration naa > glu

    target_out = [-1 0 0 0 1 0 0 0 0 0];

    mobj = metaclass(this.testobj.full_lin_priors);
    for par_cnt = 1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'conc'})
            assertEqual(this.testobj.full_lin_priors.(current_par), target_out);
        else
            assertEqual(this.testobj.full_lin_priors.(current_par), []);
        end
    end
end