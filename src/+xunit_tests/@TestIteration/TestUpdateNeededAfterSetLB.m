function this = TestUpdateNeededAfterSetLB(this)

    assertFalse(this.testobj.needUpdate);
    this.testobj.SetLB([1 2], {'em_g'}, 5);
    assertTrue(this.testobj.needUpdate);
end