function this = TestMetaboliteHashTable(this)

    % during this test it is checked if the the hash table
    % containing the indices associated with the basis signal names
    % from the basis set object is setup properly

    % check hash table containing double indices
    assertEqual(double(this.testobj.met_idx.Count), 9);
    assertEqual(this.testobj.met_idx('Naa1'), 1);
    assertEqual(this.testobj.met_idx('Naa2'), [2 9]);
    assertEqual(this.testobj.met_idx('Cr1'), 3);
    assertEqual(this.testobj.met_idx('Cr2'), 4);
    assertEqual(this.testobj.met_idx('Glu'), 5);
    assertEqual(this.testobj.met_idx('Cho1'), 6);
    assertEqual(this.testobj.met_idx('Gln'), 7);
    assertEqual(this.testobj.met_idx('Cho2'), 8);
    assertEqual(this.testobj.met_idx('baseline'), 10);

    % check hash table containing logical indices
    assertEqual(double(this.testobj.met_boolidx.Count), 9);
    assertEqual(this.testobj.met_boolidx('Naa1'),     logical([1 0 0 0 0 0 0 0 0 0]));
    assertEqual(this.testobj.met_boolidx('Naa2'),     logical([0 1 0 0 0 0 0 0 1 0]));
    assertEqual(this.testobj.met_boolidx('Cr1'),      logical([0 0 1 0 0 0 0 0 0 0]));
    assertEqual(this.testobj.met_boolidx('Cr2'),      logical([0 0 0 1 0 0 0 0 0 0]));
    assertEqual(this.testobj.met_boolidx('Glu'),      logical([0 0 0 0 1 0 0 0 0 0]));
    assertEqual(this.testobj.met_boolidx('Cho1'),     logical([0 0 0 0 0 1 0 0 0 0]));
    assertEqual(this.testobj.met_boolidx('Gln'),      logical([0 0 0 0 0 0 1 0 0 0]));
    assertEqual(this.testobj.met_boolidx('Cho2'),     logical([0 0 0 0 0 0 0 1 0 0]));
    assertEqual(this.testobj.met_boolidx('baseline'), logical([0 0 0 0 0 0 0 0 0 1]));
end