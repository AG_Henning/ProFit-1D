function this = TestUpdateIterationFixedValuesCalc(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2','Naa2'});
    this.testobj.UpdateIteration;

    target_out = nan(1,7);
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.fixed_values.(current_par), target_out);
    end
    
    this.testobj.SetFixedValues({'Cho1', 'Naa2'}, {'conc'}, 10);
    this.testobj.UpdateIteration;
    
    default_target_out_conc   = [nan 10 nan nan 10 nan 10];
    default_target_out_rest   = nan(1,7);
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            target_out = default_target_out_conc;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.fixed_values.(current_par), target_out);
    end
end