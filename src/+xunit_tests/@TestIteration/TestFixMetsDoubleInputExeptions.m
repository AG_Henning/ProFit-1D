function this = TestFixMetsDoubleInputExeptions(this)

    assertExceptionThrown(@() this.testobj.FixMets([], {'gm'}), 'Iteration:MetValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.FixMets([1 2 15], {'gm'}), 'Iteration:MetValidator:IndexExceeds');
    assertExceptionThrown(@() this.testobj.FixMets([0 1 2], {'gm'}), 'Iteration:MetValidator:IndexExceeds');
    assertExceptionThrown(@() this.testobj.FixMets({1, 2}, {'gm'}), 'Iteration:MetValidator:WrongType');
    assertExceptionThrown(@() this.testobj.FixMets({4, 'all'}, {'gm'}), 'Iteration:MetValidator:WrongType');
    assertExceptionThrown(@() this.testobj.FixMets({'Cr1', 'all'}, {'gm'}), 'Iteration:MetValidator:TooManyArguments');
end