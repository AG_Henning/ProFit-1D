function this = TestSetStartingValuesDoubelExpections(this)

    assertExceptionThrown(@() this.testobj.SetStartingValues([], {'gm'}, 10), 'Iteration:MetValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.SetStartingValues([1 2 15], {'gm'}, 10), 'Iteration:MetValidator:IndexExceeds');
    assertExceptionThrown(@() this.testobj.SetStartingValues([0 1 2], {'gm'}, 10), 'Iteration:MetValidator:IndexExceeds');
    assertExceptionThrown(@() this.testobj.SetStartingValues({1, 2}, {'gm'}, 10), 'Iteration:MetValidator:WrongType');
    assertExceptionThrown(@() this.testobj.SetStartingValues({4, 'all'}, {'gm'}, 10), 'Iteration:MetValidator:WrongType');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'all'}, {'gm'}, 10), 'Iteration:MetValidator:TooManyArguments');
end