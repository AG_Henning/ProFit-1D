function this = TestUpdateIterationLBCalc(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.UpdateIteration;

    target_out = [-Inf -Inf -Inf -Inf -Inf];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.lower_bounds.(current_par), target_out);
    end
    
    this.testobj.SetLB({'Cho1'}, {'em', 'conc'}, 0);
    this.testobj.SetLB({'Cr1'}, {'gm', 'df2'}, 3.2);
    this.testobj.UpdateIteration;
    
    default_target_out_concem   = [-Inf -Inf -Inf 0 -Inf];
    default_target_out_df2gm    = [-Inf 3.2 -Inf -Inf -Inf];
    default_target_out_rest     = [-Inf -Inf -Inf -Inf -Inf];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if (strcmp(current_par, 'conc') || strcmp(current_par, 'em'))
            target_out = default_target_out_concem;
        elseif (strcmp(current_par, 'gm') || strcmp(current_par, 'df2'))
            target_out = default_target_out_df2gm;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.lower_bounds.(current_par), target_out);
    end    
end