function this = TestFixMetsTextInputExeptions(this)

    assertExceptionThrown(@() this.testobj.FixMets({}, {'gm'}), 'Iteration:MetValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.FixMets('Naa1', {'gm'}), 'Iteration:MetValidator:WrongType');
    assertExceptionThrown(@() this.testobj.FixMets({'Cr1', 'abc'}, {'gm'}), 'Iteration:MetValidator:NoKey');
    assertExceptionThrown(@() this.testobj.FixMets({'Glu', 2}, {'gm'}), 'Iteration:MetValidator:WrongType');
    assertExceptionThrown(@() this.testobj.FixMets({'Glu', 'all'}, {'gm'}), 'Iteration:MetValidator:TooManyArguments');
end