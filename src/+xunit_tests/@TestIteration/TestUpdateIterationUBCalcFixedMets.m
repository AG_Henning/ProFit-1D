function this = TestUpdateIterationUBCalcFixedMets(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.ConnectMets({'Cho1', 'Cho2'}, {'conc', 'df2', 'df1'});
    this.testobj.ConnectMets({'all'}, {'gm', 'em_g'});
    this.testobj.FixMets({'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;

    default_target_out_conc     = [Inf Inf Inf];
    default_target_out_df12     = [Inf Inf Inf Inf];
    default_target_out_gmem_g   = Inf;
    default_target_out_rest     = [Inf Inf Inf Inf Inf];

    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            target_out = default_target_out_conc;
        elseif (strcmp(current_par, 'df2') || strcmp(current_par, 'df1'))
            target_out = default_target_out_df12;
        elseif (strcmp(current_par, 'gm') || strcmp(current_par, 'em_g'))
            target_out = default_target_out_gmem_g;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.upper_bounds.(current_par), target_out);
    end

    % reset all met connections and free all mets
    this.testobj.ConnectMets({'Naa1'},{'all'});
    this.testobj.ConnectMets({'Cr1'},{'all'});
    this.testobj.ConnectMets({'Cr2'},{'all'});
    this.testobj.ConnectMets({'Cho1'},{'all'});
    this.testobj.ConnectMets({'Cho2'},{'all'});
    this.testobj.FreeMets({'all'}, {'all'});
    this.testobj.UpdateIteration;

    this.testobj.SetUB({'Cr1'}, {'em', 'conc'}, 0);
    this.testobj.SetUB({'Cr1'}, {'gm', 'df2'}, 3.2);
    this.testobj.ConnectMets({'Cho1', 'Cho2'}, {'conc', 'df2', 'df1'});
    this.testobj.ConnectMets({'all'}, {'em_g'});
    this.testobj.FixMets({'Cr2'}, {'conc'});
    this.testobj.UpdateIteration;

    default_target_out_conc = [Inf 0 Inf];
    default_target_out_em   = [Inf 0 Inf Inf Inf];
    default_target_out_em_g = Inf;
    default_target_out_df2  = [Inf 3.2 Inf Inf];
    default_target_out_df1  = [Inf Inf Inf Inf];
    default_target_out_gm   = [Inf 3.2 Inf Inf Inf];
    default_target_out_rest = [Inf Inf Inf Inf Inf];

    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if strcmp(current_par, 'conc')
            target_out = default_target_out_conc;
        elseif strcmp(current_par, 'gm')
            target_out = default_target_out_gm;
        elseif strcmp(current_par, 'em')
            target_out = default_target_out_em;
        elseif strcmp(current_par, 'em_g')
            target_out = default_target_out_em_g;
        elseif strcmp(current_par, 'df1')
            target_out = default_target_out_df1;
        elseif strcmp(current_par, 'df2')
            target_out = default_target_out_df2;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.upper_bounds.(current_par), target_out);
    end
end