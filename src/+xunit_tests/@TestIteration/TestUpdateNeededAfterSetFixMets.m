function this = TestUpdateNeededAfterSetFixMets(this)

    assertFalse(this.testobj.needUpdate);
    this.testobj.FixMets([1 2], {'em_g'});
    assertTrue(this.testobj.needUpdate);
end