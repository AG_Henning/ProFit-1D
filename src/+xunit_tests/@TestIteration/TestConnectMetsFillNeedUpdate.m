function this = TestConnectMetsFillNeedUpdate(this)
    % check if new metabolite connections are made the object will be
    % marked to need an update
    
    assertFalse(this.testobj.needUpdate);
    this.testobj.met_connections.Fill({'em_g'}, 1:10);
    assertTrue(this.testobj.needUpdate);    
end
