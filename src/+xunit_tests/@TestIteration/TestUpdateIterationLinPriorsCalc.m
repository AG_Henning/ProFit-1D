function this = TestUpdateIterationLinPriorsCalc(this)

    this.testobj.ActivateMets({'Naa1', 'Cr1', 'Cr2', 'Cho1', 'Cho2'});
    this.testobj.UpdateIteration;

    target_out = [];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.lin_priors.(current_par), target_out);
    end
    
    this.testobj.SetLinPriors({'Cr1', 'Cr2'}, {'em'}, [-1 1]);
    this.testobj.SetLinPriors({'Naa1','Cho1'}, {'conc', 'df2'}, [1 2]);
    this.testobj.UpdateIteration;
    
    default_target_out_em   = [0 -1 1 0 0];
    default_target_out_conc = [1 0 0 2 0];
    default_target_out_rest = [];
    
    mobj = ?Parameters;    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if (strcmp(current_par, 'conc') || strcmp(current_par, 'df2'))
            target_out = default_target_out_conc;
        elseif strcmp(current_par, 'em')
            target_out = default_target_out_em;
        else
            target_out = default_target_out_rest;
        end
        assertEqual(this.testobj.lin_priors.(current_par), target_out);
    end
end
