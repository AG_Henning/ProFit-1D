function this = TestConnectMetsDoubleInputExeptions(this)
    % in this test different aspects of connecting multiple metabolite
    % parameters are checked.

    % provoke execptions    
    assertExceptionThrown(@() this.testobj.ConnectMets([], {'gm'}), 'Iteration:ConnectMets:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.ConnectMets(5, 1), 'Iteration:ConnectMets:WrongType');
    assertExceptionThrown(@() this.testobj.ConnectMets(0, 'sds'), 'Iteration:ConnectMets:IndexExceeds');
    assertExceptionThrown(@() this.testobj.ConnectMets([1 12], {'conc'}), 'Iteration:ConnectMets:IndexExceeds');
    assertExceptionThrown(@() this.testobj.ConnectMets([3 4], {'ultimate_desaster'}), 'Iteration:ConnectMets:WrongArgument');
end
