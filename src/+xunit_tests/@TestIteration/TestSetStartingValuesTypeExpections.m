function this = TestSetStartingValuesTypeExpections(this)

    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, {'ultimate_desaster'}, 10), 'Iteration:TypeValidator:NoKey');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, {}, 10), 'Iteration:TypeValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, [], 10), 'Iteration:TypeValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, 'all', 10), 'Iteration:TypeValidator:WrongType');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, [1 2 3], 10), 'Iteration:TypeValidator:WrongType');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'Cr2'}, {'gm', 'mmpf'}, 10), 'Iteration:TypeValidator:NoKey');
end