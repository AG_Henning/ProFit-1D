function this = TestCorrectNrMets(this)
            
    % check if the number of signals in the basis set is correctly
    % detected
    assertEqual(this.testobj.nr_mets, 10);
end