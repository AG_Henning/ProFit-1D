function this  = TestInitializedValues(this)
    % check if the porperties have been initialized to the correct values
    
    mobj = ?Parameters;
    submobj = ?Collection;
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.met_connections.(current_par), logical(eye(this.testobj.nr_mets)));
    end

    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.idx.(current_par), 1:this.testobj.nr_mets);
    end
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.fixed_mets.(current_par), false(1,this.testobj.nr_mets));
    end
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, 'conc')
            assertEqual(this.testobj.full_start_values.(current_par), ones(1,this.testobj.nr_mets));
        elseif ismember(current_par, 't1')
            assertEqual(this.testobj.full_start_values.(current_par), Inf(1,this.testobj.nr_mets));
        else
            assertEqual(this.testobj.full_start_values.(current_par), zeros(1,this.testobj.nr_mets));
        end
    end
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.full_lower_bounds.(current_par), nan(1,this.testobj.nr_mets));
    end
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.full_upper_bounds.(current_par), nan(1,this.testobj.nr_mets));
    end
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.full_fixed_values.(current_par), nan(1,this.testobj.nr_mets));
    end
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.full_lin_priors.(current_par), []);
    end
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        for subpar_cnt = 1:length(submobj.Properties)
            current_subpar = submobj.Properties{subpar_cnt}.Name;
            assertEqual(this.testobj.full_mask.(current_par).(current_subpar), false(this.testobj.nr_mets));
        end
    end
    
    assertFalse(this.testobj.needUpdate);
    assertFalse(this.testobj.use_sp_baseline);
    assertFalse(this.testobj.use_sp_lineshape);
end