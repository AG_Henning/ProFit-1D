function this = TestSetStartingValuesDouble(this)

    this.testobj.SetStartingValues([3 4], {'conc'}, 10);
    target_out = [1 1 10 10 1 1 1 1 1 1];
        
    mobj = ?Parameters;
    
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, 'conc')
            assertEqual(this.testobj.full_start_values.(current_par), target_out);
        elseif ismember(current_par, 't1')
            assertEqual(this.testobj.full_start_values.(current_par), Inf(1,this.testobj.nr_mets));
        else
            assertEqual(this.testobj.full_start_values.(current_par), zeros(1,this.testobj.nr_mets));
        end
    end
end