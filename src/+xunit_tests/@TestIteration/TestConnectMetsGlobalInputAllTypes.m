function this = TestConnectMetsGlobalInputAllTypes(this)
    % in this test different aspects of connecting multiple metabolite
    % parameters are checked.
    
    this.testobj.ConnectMets({'global'}, {'all'});
    target_mx = true(this.testobj.nr_mets);
    
    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        assertEqual(this.testobj.met_connections.(current_par), target_mx);
    end
end