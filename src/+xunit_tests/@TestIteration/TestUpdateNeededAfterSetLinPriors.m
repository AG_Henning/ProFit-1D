function this = TestUpdateNeededAfterSetLinPriors(this)

    assertFalse(this.testobj.needUpdate);
    this.testobj.SetLinPriors([1 2], {'em_g'}, [-1 1]);
    assertTrue(this.testobj.needUpdate);
end