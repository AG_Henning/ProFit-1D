function this = TestSetLB(this)

    this.testobj.SetLB({'baseline', 'Glu'}, {'em', 'gm', 'em_g'}, 0);
    
    target_out = [nan nan nan nan 0 nan nan nan nan 0];
    
    mobj = metaclass(this.testobj.full_lower_bounds);
    for par_cnt = 1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        if ismember(current_par, {'em', 'gm', 'em_g'})
            assertEqual(this.testobj.full_lower_bounds.(current_par), target_out);
        else
            assertEqual(this.testobj.full_lower_bounds.(current_par), nan(1, this.testobj.nr_mets));
        end
    end
end