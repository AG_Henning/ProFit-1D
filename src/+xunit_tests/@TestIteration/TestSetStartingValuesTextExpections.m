function this = TestSetStartingValuesTextExpections(this)

    assertExceptionThrown(@() this.testobj.SetStartingValues({}, {'gm'}, 10), 'Iteration:MetValidator:NotEnoughInput');
    assertExceptionThrown(@() this.testobj.SetStartingValues('Naa1', {'gm'}, 10), 'Iteration:MetValidator:WrongType');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Cr1', 'abc'}, {'gm'}, 10), 'Iteration:MetValidator:NoKey');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Glu', 2}, {'gm'}, 10), 'Iteration:MetValidator:WrongType');
    assertExceptionThrown(@() this.testobj.SetStartingValues({'Glu', 'all'}, {'gm'}, 10), 'Iteration:MetValidator:TooManyArguments');
end