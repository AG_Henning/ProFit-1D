classdef TestCommonResultData < TestCase
    
    properties
        
        testobj;
        pars;
    end
    
    methods
        
        function this = TestCommonResultData(name)
            
            this = this@TestCase(name);
        end
        
        function this = setUp(this)
            
            this.testobj = CommonResultData;
            this.pars    = {'spec'; 'data'; 'fid'};
        end
        
        function this = TestPropsExist(this)
            % Check if all parameters that are needed and specified in the
            % setup methods get actually initialized
            
            mhobj = ?handle;
            method_list = cellfun(@(meth) meth.Name, mhobj.Methods, 'UniformOutput', false);
            prop_list   = cellfun(@(prop) prop.Name, mhobj.Properties, 'UniformOutput', false);
            
            custom_methods = {'CommonResultData'};
            custom_props   = this.pars;
            
            method_list = sort(vertcat(method_list, custom_methods));
            prop_list   = sort(vertcat(prop_list, custom_props));
            
            mobj = metaclass(this.testobj);
            % get all availbale methods and properties and check if the
            % expected ones arme among them
            % version for R2010a
            available_methods = sort(cellfun(@(x) x.Name, mobj.Methods, 'UniformOutput', false));
            available_props   = sort(cellfun(@(x) x.Name, mobj.Properties, 'UniformOutput', false));

           % now search and check
           assertEqual(available_methods, method_list);
           assertEqual(available_props, prop_list);
        end
    end
end