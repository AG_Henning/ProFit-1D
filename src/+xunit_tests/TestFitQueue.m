classdef TestFitQueue < TestCase
   
    properties
       
        testobj;

        % mock objects (at the moment only simple matrices
        basis;
        iter;
        iter2;
    end
    
    methods
        
        function this = TestFitQueue(name)
           
            this = this@TestCase(name);
            this.basis = rand(5);
            this.iter.basis   = this.basis;
            this.iter.conf    = rand(1,10);
            this.iter2.basis  = this.basis;
            this.iter2.conf   = rand(1,8);
        end
        
        function this = setUp(this)
           
            % initialize FitQueue object to be testes but only with
            % arbitrary matrix representing a basis object
            this.testobj = FitQueue(this.basis);
        end
        
        function this = TestAfterCreation(this)
           % in this test it is checked if some neccessary methods and
           % properies are present. It is not checked if any unknown
           % methods or properties are implemented too.
           
           % fill cellstr with the names of methods and properties that
           % should be availbale
           mhobj = ?handle;
           method_list = cellfun(@(meth) meth.Name, mhobj.Methods, 'UniformOutput', false);
           prop_list   = cellfun(@(prop) prop.Name, mhobj.Properties, 'UniformOutput', false);
           
           custom_methods = {'AddIteration'; 'FitQueue'};
           custom_props   = {'basis'; 'nr_iter'; 'seq'};
           
           method_list = sort(vertcat(method_list, custom_methods));
           prop_list   = sort(vertcat(prop_list, custom_props));
           
           mobj = metaclass(this.testobj);
           
           % get all availbale methods and properties and check if the
           % expected ones arme among them
           
           % version for R2010a
           available_methods = sort(cellfun(@(x) x.Name, mobj.Methods, 'UniformOutput', false));
           available_props   = sort(cellfun(@(x) x.Name, mobj.Properties, 'UniformOutput', false));
           
%            % This syntax is only working in Matlab 2011b so this is version
%            % dependent (careful)
%            available_methods = arrayfun(@(x) x.Name, mobj.MethodList, 'UniformOutput', false);
%            available_props   = arrayfun(@(x) x.Name, mobj.PropertyList, 'UniformOutput', false);
           
           % now search and check
           assertEqual(available_methods, method_list);
           assertEqual(available_props, prop_list);
        end
        
        function this = TestInitValues(this)
           
            % check if the initial values are set correcty
            assertEqual(this.testobj.basis, this.basis);
            assertTrue(isempty(this.testobj.seq));
            assertEqual(this.testobj.nr_iter, 0);
        end
        
        function this = TestAddIteration(this)
            
            % check if new iteration objects are correctly appended to the
            % end of the queue and if the counter is updated accordingly
           this.testobj.AddIteration(this.iter);
           
           assertEqual(this.testobj.seq{end}, this.iter);
           assertEqual(this.testobj.nr_iter, 1);
           
           this.testobj.AddIteration(this.iter2);
           
           assertEqual(this.testobj.seq{end}, this.iter2);
           assertEqual(this.testobj.nr_iter, 2);
           
           % check if different basis set lead to error
           this.iter2.basis = rand(9);
           assertExceptionThrown(@() this.testobj.AddIteration(this.iter2), 'FitQueue:DiffBasis');
        end
    end
    
end