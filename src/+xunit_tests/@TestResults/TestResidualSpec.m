function this = TestResidualSpec(this)

   this.testobj.fitted_spec = 6:10;
   assertEqual(this.testobj.residual_spec, [-5 -5 -5 -5 -5]);
end