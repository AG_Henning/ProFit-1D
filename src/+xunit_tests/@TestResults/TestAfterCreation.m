function this = TestAfterCreation(this)

    % in this test it is checked if some neccessary methods and
    % properies are present. It is not checked if any unknown
    % methods or properties are implemented too.

    % fill cellstr with the names of methods and properties that
    % should be availbale
    mhobj = ?handle;
    method_list = cellfun(@(meth) meth.Name, mhobj.Methods, 'UniformOutput', false);
    prop_list   = cellfun(@(prop) prop.Name, mhobj.Properties, 'UniformOutput', false);

    custom_methods = {'Results'; 'GetFittedValues'; 'Plot'; 'PrintTable'};
    custom_props   = {'fitted_values'; 'basis_matrix'; 'fitted_spec'; 'splines_baseline'; ...
                      'spline_lineshape'; 'global_lineshape'; 'active_mets'; 'fixed_mets'; ...
                      'fixed_values'; 'start_values'; 'upper_bounds'; 'lower_bounds'; 'lin_priors'; ...
                      'idx'; 'full_mask'; 'sub_mask'; 'nr'; 'full_fixed_values'; 'full_start_values'; ...
                      'full_upper_bounds'; 'full_lower_bounds'; 'full_lin_priors'; 'mapping_mx'; ...
                      'nr_mets'; 'common_data'; 'optim_exit'; 'optim_output'; 'optim_lambda'; ...
                      'optim_x'; 'met_idx'; 'met_boolidx'; 'met'; 'crlb'; 'subplottypes'; 'residual_fid'; ...
                      'residual_spec'; 'fitted_fid'};

    method_list = sort(vertcat(method_list, custom_methods));
    prop_list   = sort(vertcat(prop_list, custom_props));

    mobj = metaclass(this.testobj);

    % get all availbale methods and properties and check if the
    % expected ones arme among them

    % version for R2010a
    available_methods = sort(cellfun(@(x) x.Name, mobj.Methods, 'UniformOutput', false));
    available_props   = sort(cellfun(@(x) x.Name, mobj.Properties, 'UniformOutput', false));

    %            % This syntax is only working in Matlab 2011b so this is version
    %            % dependent (careful)
    %            available_methods = arrayfun(@(x) x.Name, mobj.MethodList, 'UniformOutput', false);
    %            available_props   = arrayfun(@(x) x.Name, mobj.PropertyList, 'UniformOutput', false);

    % now search and check
    assertEqual(available_methods, method_list);
    assertEqual(available_props, prop_list);
end