classdef TestResults < TestCase
    
    properties
        
        testobj;
        basis;
        iteration;
        common_data;
    end
    
    methods
        
        function this = TestResults(name)
            
            this = this@TestCase(name);
        end
        
        function this = setUp(this)
            
            this.basis.met   = {'Naa1'; 'Naa2'; 'Cr1'; 'Cr2'; 'Glu'; 'Cho1'; 'Gln'; 'Cho2'; 'Naa2'; 'baseline'};
            this.iteration   = Iteration(this.basis);
            this.common_data = CommonResultData;
            this.common_data.spec = 1:5;
            this.common_data.data.f0 = 127e6; 
            this.testobj     = Results(this.iteration, this.common_data);
        end
        
        this = TestAfterCreation(this);
        this = TestResidualSpec(this);
        
        this  = TestGetFittedValuesTextInputMets(this);
        this  = TestGetFittedValuesLogicalInput(this);
        this  = TestGetFittedValuesDoubleInput(this);
    
        this  = TestGetLBTextInputMets(this);
        this  = TestGetLBLogicalInput(this);
        this  = TestGetLBDoubleInput(this);

        this  = TestGetUBTextInputMets(this);
        this  = TestGetUBLogicalInput(this);
        this  = TestGetUBDoubleInput(this);
    end
end