classdef TestParameterCollection < TestCase
    
    properties
        
        testobj;
        collobj;
    end
    
    methods
        
        function this = TestParameterCollection(name)
            
            this = this@TestCase(name);
        end
        
        function this = setUp(this)
            
            this.testobj = ParameterCollection();
        end
        
        function this = TestSubParsExist(this)
            % Check if all parameters that are needed and specified in the
            % setup methods get actually initialized
            
            mhobj = ?handle;
            method_list = cellfun(@(meth) meth.Name, mhobj.Methods, 'UniformOutput', false);
            prop_list   = cellfun(@(prop) prop.Name, mhobj.Properties, 'UniformOutput', false);
            
            custom_methods = {'ParameterCollection'; 'Fill'};
            custom_props   = {'conc'; 'em'; 'em_g'; 'gm'; 'pc0'; 'pc1'; 'df1'; 'df2'; 't1'};
            
            method_list = sort(vertcat(method_list, custom_methods));
            prop_list   = sort(vertcat(prop_list, custom_props));
            
            mobj = metaclass(this.testobj);
            % get all availbale methods and properties and check if the
            % expected ones arme among them
            % version for R2010a
            available_methods = sort(cellfun(@(x) x.Name, mobj.Methods, 'UniformOutput', false));
            available_props   = sort(cellfun(@(x) x.Name, mobj.Properties, 'UniformOutput', false));

           % now search and check
           assertEqual(available_methods, method_list);
           assertEqual(available_props, prop_list);
        end
        
        function this = TestSubParsContent(this)
            % Check if the data were input correctly into the sub parameter
            % fields
            
            mobj = metaclass(this.testobj);
            
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                
                assertTrue(strcmp(class(this.testobj.(current_par)), 'Collection'));
            end
        end
        
        function this = TestFillSubParsTest1(this)
            % use the Fill method to set values in the sub parameters
            
            target_out = 100 * ones(5);
            target_in  = target_out;
            
            this.testobj.Fill({'all'}, {'all'}, target_in);
            
            mobj = ?Parameters;
            submobj = ?Collection;
            
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                for subpar_cnt=1:length(submobj.Properties)
                    current_subpar = submobj.Properties{subpar_cnt}.Name;
                        assertEqual(this.testobj.(current_par).(current_subpar), target_in);
                end
            end
        end
        
        function this = TestFillSubParsTest2(this)
            % use the Fill method to set values in the sub parameters
            
            target_out = 100 * ones(5);
            target_in  = target_out;
            
            this.testobj.Fill({'conc'}, {'full'}, target_in);
            
            mobj = ?Parameters;
            submobj = ?Collection;
            
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                for subpar_cnt=1:length(submobj.Properties)
                    current_subpar = submobj.Properties{subpar_cnt}.Name;
                    if (strcmp(current_par, 'conc') && strcmp(current_subpar, 'full'))
                        assertEqual(this.testobj.(current_par).(current_subpar), target_in);
                    else
                        assertEqual(this.testobj.(current_par).(current_subpar), []);
                    end
                end
            end
        end

        function this = TestFillSubParsTest3(this)
            % use the Fill method to set values in the sub parameters
            
            target_out = 100 * ones(5);
            target_in  = target_out;
            
            this.testobj.Fill({'conc', 'em'}, {'full', 'fixed'}, target_in);
            
            mobj = ?Parameters;
            submobj = ?Collection;
            
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                for subpar_cnt=1:length(submobj.Properties)
                    current_subpar = submobj.Properties{subpar_cnt}.Name;
                    if (ismember(current_par, {'conc', 'em'}) && ismember(current_subpar, {'full', 'fixed'}))
                        assertEqual(this.testobj.(current_par).(current_subpar), target_in);
                    else
                        assertEqual(this.testobj.(current_par).(current_subpar), []);
                    end
                end
            end
        end

        function this = TestExceptionsFillSubPars(this)

            target_in = rand(5);
            assertExceptionThrown(@() this.testobj.Fill('all', {'free'}, target_in), 'ParCollection:Fill:WrongType');
            assertExceptionThrown(@() this.testobj.Fill({'all'}, 'free', target_in), 'ParCollection:Fill:WrongSubType');
            assertExceptionThrown(@() this.testobj.Fill([1 2 3 4], {'fixed'}, target_in), 'ParCollection:Fill:WrongType');
            assertExceptionThrown(@() this.testobj.Fill({'conc'}, [1 2 3 4], target_in), 'ParCollection:Fill:WrongSubType');
            assertExceptionThrown(@() this.testobj.Fill({'konc'}, {'full'}, target_in), 'ParCollection:Fill:NonExistingField');
            assertExceptionThrown(@() this.testobj.Fill({'em'}, {'fuller'}, target_in), 'ParCollection:Fill:NonExistingField');
        end
    end
end