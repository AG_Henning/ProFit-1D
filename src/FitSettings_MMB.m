function FitSettings_MMB(basis, outputFile)

    %% initialize fit specific settings
    data.quant_window_ppm = [ 0.0 0.5; ...
                              0.0 9.0];
    
    data.noise_window_ppm = [-3,  0; ...
                              9, 12.5];

    data.zero_fill_factor    = [2 1];
    data.debug               = false;
    data.do_water_separation = false;
    data.do_water_filtering  = true;
    
    
    %% configure individual fit iterations
    fitsettings = FitQueue(basis);

	%% set up first fitting iteration ************************************************************************
    % ******************************************************************************************************** 
    
    fititer = Iteration(basis);
    fititer.ActivateMets({'Cr(CH2)', 'NAA', 'ILE', 'GLU', 'PRO', 'LYS'});
    fititer.ConnectMets({'name'}, {'conc',  'em', 'df2'});
%     fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 't1'});
    fititer.ConnectMets({'Cr(CH2)', 'NAA'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'});
    fititer.ConnectMets({'ILE', 'GLU', 'PRO', 'LYS'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'});
    
	% fix parameters to certain values
    fititer.FixMets({'all'}, {'t1'});
    fititer.SetFixedValues({'all'}, {'t1'}, Inf);
    fititer.FixMets({'all'}, {'df1'});
    fititer.SetFixedValues({'all'}, {'df1'}, 0);
    fititer.FixMets({'all'}, {'pc0'});
    fititer.SetFixedValues({'all'}, {'pc0'}, 0);
    fititer.FixMets({'all'}, {'pc11'});
    fititer.SetFixedValues({'all'}, {'pc11'}, 0);
    fititer.FixMets({'all'}, {'pc12'});
    fititer.SetFixedValues({'all'}, {'pc12'}, 0);
    
    % set up typical bounds for the first iteration
    fititer.SetLB({'all'}, {'conc'}, 0);
    fititer.SetLB({'all'}, {'em'}, 0);
    fititer.SetLB({'all'}, {'df2'}, -10);
    fititer.SetLB({'all'}, {'gm', 'em_g'}, 0);
    
    fititer.SetUB({'all'}, {'em_g'}, 1);
    fititer.SetUB({'all'}, {'gm'}, 1);
    fititer.SetUB({'all'}, {'em'}, 1);
    fititer.SetUB({'Cr(CH2)', 'NAA'}, {'em_g'}, 10);
    fititer.SetUB({'Cr(CH2)', 'NAA'}, {'gm'}, 100);
    fititer.SetUB({'Cr(CH2)', 'NAA'}, {'em'}, 30);
    fititer.SetUB({'all'}, {'df2'}, 10);
    
	% set starting values (prior knowledge)
	fititer.SetStartingValues({'all'}, {'em'}, 0);
	fititer.SetStartingValues({'all'}, {'em_g'}, 0);
	fititer.SetStartingValues({'all'}, {'gm'}, 0);
	fititer.SetStartingValues({'Cr(CH2)', 'NAA'}, {'em'}, 6);
	fititer.SetStartingValues({'Cr(CH2)', 'NAA'}, {'em_g'}, 6);
	fititer.SetStartingValues({'Cr(CH2)', 'NAA'}, {'gm'}, 10);

    fititer.UpdateIteration;
    
    % add the just configured iteration to the queue
    fitsettings.AddIteration(fititer);

	%% set up second fitting iteration ***********************************************************************
    % ******************************************************************************************************** 
    function obj = Starting2(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
        for current_met = {'Cr(CH2)', 'NAA', 'ILE', 'GLU', 'PRO', 'LYS'}
            currentMetString = current_met{:};
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
				fitted_value = res.GetFittedValues({currentMetString}, {current_par});
				if length(fitted_value)>1
					if all(fitted_value == fitted_value(1))
						fitted_value = fitted_value(1);
					else
						error('Something does not seem to be right. Check what is going on');
					end
				end
                obj.SetStartingValues({currentMetString}, {current_par}, fitted_value);
            end
        end
        
        % adjust starting values of newly added metabolites
        for current_met = {'ALA','ARG','ASN','ASP','CYS','GLN','GLY','HIS','LEU','MET','PHE','SER','THR','TRP','TYR','VAL'}
            currentMetString = current_met{:};
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                switch current_par
                    case 'conc'
                        scale_factor = 0.5;
						% the factor 0.9 comes from the fact that creatine is slightly overestimated in the first iteration
                        obj.SetStartingValues({currentMetString}, {current_par}, scale_factor*0.9*res.GetFittedValues({'Cr(CH2)'}, {current_par}));
					case 'em'
					    fitted_AA_em = [res.GetFittedValues({'ILE'}, {'em'}), ...
                            res.GetFittedValues({'GLU'}, {'em'}), ...
                            res.GetFittedValues({'LYS'}, {'em'})];
						fitted_AA_mean_t2 = mean(1./(pi.*fitted_AA_em));
					    fitted_AA_mean_em = 1/(pi*fitted_AA_mean_t2);
                        obj.SetStartingValues({currentMetString}, {current_par}, fitted_AA_mean_em);
                    otherwise
						mean_par_values = mean(unique([res.GetFittedValues({'ILE'}, {current_par}), ...
													   res.GetFittedValues({'GLU'}, {current_par}), ...
													   res.GetFittedValues({'LYS'}, {current_par})]));
                        obj.SetStartingValues({currentMetString}, {current_par}, mean_par_values);
                end
            end
        end

        obj.UpdateIteration;
    end

    function obj = LB2(obj, res)
        
	   mean_df2_met = mean(unique([res.GetFittedValues({'Cr(CH2)'}, {'df2'}), ...
	  						   res.GetFittedValues({'NAA'}, {'df2'})]));
	   mean_df2_AA = mean(unique([res.GetFittedValues({'ILE'}, {'df2'}), ...
							   res.GetFittedValues({'GLU'}, {'df2'}), ...
							   res.GetFittedValues({'LYS'}, {'df2'})]));
	   df2_lb_AA  = mean_df2_AA - 3;
	   df2_lb_met  = mean_df2_met - 3;
	   gm_lb_met   = res.GetFittedValues({'Cr(CH2)'}, {'gm'}) - 1;
	   em_g_lb_met = res.GetFittedValues({'Cr(CH2)'}, {'em_g'}) - 1;
	   em_lb_met = res.GetFittedValues({'Cr(CH2)'}, {'em_g'}) - 1;
	   gm_lb_AA   = res.GetFittedValues({'GLU'}, {'gm'}) - 1;
	   em_g_lb_AA = res.GetFittedValues({'GLU'}, {'em_g'}) - 1;
       if gm_lb_met < 0
           gm_lb_met = 0;
       end
       if gm_lb_AA < 0
           gm_lb_AA = 0;
       end
       if em_g_lb_met < 0
           em_g_lb_met = 0;
       end
       if em_g_lb_AA < 0
           em_g_lb_AA = 0;
       end
       if em_lb_met < 0
           em_lb_met = 0;
       end
       obj.SetLB({'all'}, {'df2'}, df2_lb_AA);
       obj.SetLB({'all'}, {'gm'}, gm_lb_AA);
       obj.SetLB({'all'}, {'em_g'}, em_g_lb_AA);
       obj.SetLB({'Cr(CH2)', 'NAA'}, {'df2'}, df2_lb_met);
       obj.SetLB({'Cr(CH2)', 'NAA'}, {'gm'}, gm_lb_met);
       obj.SetLB({'Cr(CH2)', 'NAA'}, {'em_g'}, em_g_lb_met);
       obj.SetLB({'Cr(CH2)', 'NAA'}, {'em'}, em_lb_met);
	   % setup restricted em bounds in the first iteration where all mets are present
       fitted_AA_em = [res.GetFittedValues({'ILE'}, {'em'}), ...
           res.GetFittedValues({'GLU'}, {'em'}), ...
           res.GetFittedValues({'LYS'}, {'em'})];
	   AA_lb_t2 = mean(1./(pi.*fitted_AA_em)) + 20e-3;  % has to be plus because increasing t2 in ms yields lower Hz broadening
	   AA_lb_em = 1/(pi*AA_lb_t2);
       if AA_lb_em < 0
           AA_lb_em = 0;
       end
	   for current_met = {'ALA','ARG','ASN','ASP','CYS','GLU','GLN','GLY','HIS','ILE','LEU','LYS','MET','PHE','PRO','SER','THR','TRP','TYR','VAL'}
           currentMetString = current_met{:};
		   obj.SetLB({currentMetString}, {'em'}, AA_lb_em);
	   end
       obj.UpdateIteration;
    end

    function obj = UB2(obj, res)

	   mean_df2_met = mean(unique([res.GetFittedValues({'Cr(CH2)'}, {'df2'}), ...
	  						   res.GetFittedValues({'NAA'}, {'df2'})]));
	   mean_df2_AA = mean(unique([res.GetFittedValues({'ILE'}, {'df2'}), ...
							   res.GetFittedValues({'GLU'}, {'df2'}), ...
							   res.GetFittedValues({'LYS'}, {'df2'})]));
	   df2_ub_AA  = mean_df2_AA + 3;
	   df2_ub_met  = mean_df2_met + 3;
	   gm_ub_met   = res.GetFittedValues({'Cr(CH2)'}, {'gm'}) + 1;
	   em_g_ub_met = res.GetFittedValues({'Cr(CH2)'}, {'em_g'}) + 1;
	   em_ub_met = res.GetFittedValues({'Cr(CH2)'}, {'em'}) + 1;
	   gm_ub_AA   = res.GetFittedValues({'GLU'}, {'gm'}) + 1;
	   em_g_ub_AA = res.GetFittedValues({'GLU'}, {'em_g'}) + 1;
       obj.SetUB({'all'}, {'df2'}, df2_ub_AA);
       obj.SetUB({'all'}, {'gm'}, gm_ub_AA);
       obj.SetUB({'all'}, {'em_g'}, em_g_ub_AA);
       obj.SetUB({'Cr(CH2)', 'NAA'}, {'df2'}, df2_ub_met);
       obj.SetUB({'Cr(CH2)', 'NAA'}, {'gm'}, gm_ub_met);
       obj.SetUB({'Cr(CH2)', 'NAA'}, {'em_g'}, em_g_ub_met);
       obj.SetUB({'Cr(CH2)', 'NAA'}, {'em'}, em_ub_met);
	   % setup restricted em bounds in the first iteration where all mets are present
       fitted_AA_em = [res.GetFittedValues({'ILE'}, {'em'}), ...
           res.GetFittedValues({'GLU'}, {'em'}), ...
           res.GetFittedValues({'LYS'}, {'em'})];
	   AA_ub_t2 = mean(1./(pi.*fitted_AA_em)) - 30e-3;  % has to be plus because decreasing t2 in ms yields higher Hz broadening
	   AA_ub_em = 1/(pi*AA_ub_t2);
	   for current_met = {'ALA','ARG','ASN','ASP','CYS','GLN','GLY','HIS','LEU','MET','PHE','PRO','SER','THR','TRP','TYR','VAL'}
           current_metString = current_met{:};
		   obj.SetUB({current_metString}, {'em'}, AA_ub_em);
	   end
	   obj.UpdateIteration;
    end

    fititer = Iteration(basis);
    
        

    fititer.ActivateMets({'ALA','ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS',...
        'MET','PHE','PRO','SER','THR','TRP','TYR','VAL','Cr(CH2)','NAA'});

    fititer.ConnectMets({'name'}, {'conc', 'em', 'df2'});
%     fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 't1'});
    fititer.ConnectMets({'Cr(CH2)', 'NAA'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'});
    fititer.ConnectMets({'ALA','ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS',...
        'MET','PHE','PRO','SER','THR','TRP','TYR','VAL'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'});

	% use the same em dependent on cre for all small metabolites
	fititer.ConnectMets({'ALA','ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS',...
        'MET','PHE','PRO','SER','THR','TRP','TYR','VAL'}, {'em'});
	fititer.ConnectMets({'ALA','ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS',...
        'MET','PHE','PRO','SER','THR','TRP','TYR','VAL'}, {'df2'});
                    
	% fix parameters to certain values
    fititer.FixMets({'all'}, {'t1'});
    fititer.SetFixedValues({'all'}, {'t1'}, Inf);
    fititer.FixMets({'all'}, {'df1'});
    fititer.SetFixedValues({'all'}, {'df1'}, 0);
    fititer.FixMets({'all'}, {'pc0'});
    fititer.SetFixedValues({'all'}, {'pc0'}, 0);
    fititer.FixMets({'all'}, {'pc11'});
    fititer.SetFixedValues({'all'}, {'pc11'}, 0);
    fititer.FixMets({'all'}, {'pc12'});
    fititer.SetFixedValues({'all'}, {'pc12'}, 0);
    
    % set up typical bounds for the first iteration
    fititer.SetLB({'all'}, {'conc'}, 0);

    % define update function handles
    fititer.UpdateStartingValues = @Starting2;
    fititer.UpdateLowerBounds    = @LB2;
    fititer.UpdateUpperBounds    = @UB2;

    fititer.UpdateIteration;
    
    % add the just configured iteration to the queue
    fitsettings.AddIteration(fititer);

	%% set up third fitting iteration ************************************************************************
    % ******************************************************************************************************** 
    
    function obj = Starting3(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
		for par_cnt=1:length(mobj.Properties)
			current_par = mobj.Properties{par_cnt}.Name;
			mask = res.full_mask.(current_par).full(any(res.full_mask.(current_par).full,2), :);
			for fit_cnt=1:length(res.fitted_values.(current_par))
				fitted_value = res.fitted_values.(current_par)(fit_cnt);
				% the starting values are upated after the bounds so if some copied starting value
				% is outside the currently set bounds e.g. for df2 change that stating value to a value
				% within allowed values. This works just for df2 now. for more general case more conflicts 
				% have to be considered
				if strcmp(current_par, 'df2')
                       if ((fitted_value>obj.upper_bounds.(current_par)(fit_cnt)) || ...
                           (fitted_value<obj.lower_bounds.(current_par)(fit_cnt)))
							fitted_value = mean(unique([res.GetFittedValues({'Cr(CH2)'}, {current_par}), ...
														res.GetFittedValues({'NAA'}, {current_par}), ...
														res.GetFittedValues({'ILE'}, {current_par}), ...
														res.GetFittedValues({'GLU'}, {current_par})]));
                       end
				end
				obj.full_start_values.(current_par)(mask(fit_cnt,:)) = fitted_value;
			end
		end
		% overwrite df2 & df1 starting value for LYS with previously fitted
		obj.full_start_values.df2(obj.met_idx('LYS')) = res.GetFittedValues({'LYS'}, {'df2'}); 
        obj.UpdateIteration;
    end

    function obj = LB3(obj, res)
       
        
	   mean_df2_met = mean(unique([res.GetFittedValues({'Cr(CH2)'}, {'df2'}), ...
	  						   res.GetFittedValues({'NAA'}, {'df2'})]));
	   mean_df2_AA = mean(unique([res.GetFittedValues({'ILE'}, {'df2'}), ...
							   res.GetFittedValues({'GLU'}, {'df2'}), ...
							   res.GetFittedValues({'LYS'}, {'df2'})]));
	   df2_lb_AA  = mean_df2_AA - 3;
	   df2_lb_met  = mean_df2_met - 3;
	   gm_lb_met   = res.GetFittedValues({'Cr(CH2)'}, {'gm'}) - 1;
	   em_g_lb_met = res.GetFittedValues({'Cr(CH2)'}, {'em_g'}) - 1;
	   em_lb_met = res.GetFittedValues({'Cr(CH2)'}, {'em_g'}) - 1;
	   gm_lb_AA   = res.GetFittedValues({'GLU'}, {'gm'}) - 1;
	   em_g_lb_AA = res.GetFittedValues({'GLU'}, {'em_g'}) - 1;
       if gm_lb_met < 0
           gm_lb_met = 0;
       end
       if gm_lb_AA < 0
           gm_lb_AA = 0;
       end
       if em_g_lb_met < 0
           em_g_lb_met = 0;
       end
       if em_g_lb_AA < 0
           em_g_lb_AA = 0;
       end
       if em_lb_met < 0
           em_lb_met = 0;
       end
       obj.SetLB({'all'}, {'df2'}, df2_lb_AA);
       obj.SetLB({'all'}, {'gm'}, gm_lb_AA);
       obj.SetLB({'all'}, {'em_g'}, em_g_lb_AA);
       obj.SetLB({'Cr(CH2)', 'NAA'}, {'df2'}, df2_lb_met);
       obj.SetLB({'Cr(CH2)', 'NAA'}, {'gm'}, gm_lb_met);
       obj.SetLB({'Cr(CH2)', 'NAA'}, {'em_g'}, em_g_lb_met);
       obj.SetLB({'Cr(CH2)', 'NAA'}, {'em'}, em_lb_met);

%        %setup em lower bounds based on res
% 	   mask = res.full_mask.em.full(any(res.full_mask.em.full,2), :);
%        for fit_cnt=1:length(res.fitted_values.em)
% 	       fitted_value = res.fitted_values.em(fit_cnt);
% 		   lb_t2 = 1/(pi*fitted_value) + 20e-3;
% 		   lb_em = 1/(pi*lb_t2);
%            if lb_em < 0 %avoid non physical behaviour
%                lb_em = 0;
%            end
% 	       obj.full_lower_bounds.em(mask(fit_cnt,:)) = lb_em;
%        end
	   % setup restricted em bounds in the first iteration where all mets are present
       fitted_AA_em = [res.GetFittedValues({'ILE'}, {'em'}), ...
           res.GetFittedValues({'GLU'}, {'em'}), ...
           res.GetFittedValues({'LYS'}, {'em'})];
	   AA_lb_t2 = mean(1./(pi.*fitted_AA_em)) + 20e-3;  % has to be plus because increasing t2 in ms yields lower Hz broadening
	   AA_lb_em = 1/(pi*AA_lb_t2);
       if AA_lb_em < 0
           AA_lb_em = 0;
       end
	   for current_met = {'ALA','ARG','ASN','ASP','CYS','GLU','GLN','GLY','HIS','ILE','LEU','LYS','MET','PHE','PRO','SER','THR','TRP','TYR','VAL'}
           currentMetString = current_met{:};
		   obj.SetLB({currentMetString}, {'em'}, AA_lb_em);
	   end
       obj.UpdateIteration;
    end

    function obj = UB3(obj, res)
         

	   mean_df2_met = mean(unique([res.GetFittedValues({'Cr(CH2)'}, {'df2'}), ...
	  						   res.GetFittedValues({'NAA'}, {'df2'})]));
	   mean_df2_AA = mean(unique([res.GetFittedValues({'ILE'}, {'df2'}), ...
							   res.GetFittedValues({'GLU'}, {'df2'}), ...
							   res.GetFittedValues({'LYS'}, {'df2'})]));
	   df2_ub_AA  = mean_df2_AA + 3;
	   df2_ub_met  = mean_df2_met + 3;
	   gm_ub_met   = res.GetFittedValues({'Cr(CH2)'}, {'gm'}) + 1;
	   em_g_ub_met = res.GetFittedValues({'Cr(CH2)'}, {'em_g'}) + 1;
	   em_ub_met = res.GetFittedValues({'Cr(CH2)'}, {'em'}) + 1;
	   gm_ub_AA   = res.GetFittedValues({'GLU'}, {'gm'}) + 1;
	   em_g_ub_AA = res.GetFittedValues({'GLU'}, {'em_g'}) + 1;
       obj.SetUB({'all'}, {'df2'}, df2_ub_AA);
       obj.SetUB({'all'}, {'gm'}, gm_ub_AA);
       obj.SetUB({'all'}, {'em_g'}, em_g_ub_AA);
       obj.SetUB({'Cr(CH2)', 'NAA'}, {'df2'}, df2_ub_met);
       obj.SetUB({'Cr(CH2)', 'NAA'}, {'gm'}, gm_ub_met);
       obj.SetUB({'Cr(CH2)', 'NAA'}, {'em_g'}, em_g_ub_met);
       obj.SetUB({'Cr(CH2)', 'NAA'}, {'em'}, em_ub_met);
       
       %setup em upper bounds based on res
% 	   mask = res.full_mask.em.full(any(res.full_mask.em.full,2), :);
% 	   for fit_cnt=1:length(res.fitted_values.em)
% 	       fitted_value = res.fitted_values.em(fit_cnt);
% 		   ub_t2 = 1/(pi*fitted_value) - 20e-3;
% 		   ub_em = 1/(pi*ub_t2);
% 	       obj.full_upper_bounds.em(mask(fit_cnt,:)) = ub_em;
% 	   end
       

	   % setup restricted em bounds in the first iteration where all mets are present
       fitted_AA_em = [res.GetFittedValues({'ILE'}, {'em'}), ...
           res.GetFittedValues({'GLU'}, {'em'}), ...
           res.GetFittedValues({'LYS'}, {'em'})];
	   AA_ub_t2 = mean(1./(pi.*fitted_AA_em)) - 20e-3;  % has to be plus because decreasing t2 in ms yields higher Hz broadening
	   AA_ub_em = 1/(pi*AA_ub_t2);
	   for current_met = {'ALA','ARG','ASN','ASP','CYS','GLN','GLY','HIS','LEU','MET','PHE','PRO','SER','THR','TRP','TYR','VAL'}
           current_metString = current_met{:};
		   obj.SetUB({current_metString}, {'em'}, AA_ub_em);
	   end
	   obj.UpdateIteration;
    end

    fititer = Iteration(basis);
    
    fititer.ActivateMets({'ALA','ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS',...
        'MET','PHE','PRO','SER','THR','TRP','TYR','VAL','Cr(CH2)','NAA'});
    
    fititer.ConnectMets({'name'}, {'conc', 'em', 'df2'});
%     fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 't1'});
    fititer.ConnectMets({'Cr(CH2)', 'NAA'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'});
    fititer.ConnectMets({'ALA','ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS',...
        'MET','PHE','PRO','SER','THR','TRP','TYR','VAL'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'});
        
	% fix parameters to certain values
    fititer.FixMets({'all'}, {'t1'});
    fititer.SetFixedValues({'all'}, {'t1'}, Inf);
    fititer.FixMets({'all'}, {'df1'});
    fititer.SetFixedValues({'all'}, {'df1'}, 0);
    fititer.FixMets({'all'}, {'pc0'});
    fititer.SetFixedValues({'all'}, {'pc0'}, 0);
    fititer.FixMets({'all'}, {'pc11'});
    fititer.SetFixedValues({'all'}, {'pc11'}, 0);
    fititer.FixMets({'all'}, {'pc12'});
    fititer.SetFixedValues({'all'}, {'pc12'}, 0);

    % set up typical bounds for the first iteration
    fititer.SetLB({'all'}, {'conc'}, 0);
    
    % define update function handles
    fititer.UpdateStartingValues = @Starting3;
    fititer.UpdateLowerBounds    = @LB3;
    fititer.UpdateUpperBounds    = @UB3;

    fititer.UpdateIteration;
    
    % add the just configured iteration to the queue
    fitsettings.AddIteration(fititer);
    %% set up forth fitting iteration ************************************************************************
    % ******************************************************************************************************** 
    
    function obj = Starting4(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
		for par_cnt=1:length(mobj.Properties)
			current_par = mobj.Properties{par_cnt}.Name;
			mask = res.full_mask.(current_par).full(any(res.full_mask.(current_par).full,2), :);
			for fit_cnt=1:length(res.fitted_values.(current_par))
				fitted_value = res.fitted_values.(current_par)(fit_cnt);
				obj.full_start_values.(current_par)(mask(fit_cnt,:)) = fitted_value;
			end
		end
		obj.SetStartingValues({'all'}, {'em_g', 'gm'}, 0);	
        obj.UpdateIteration;
    end

    function obj = LB4(obj, res)

		% keep the lower bounds from the previous iteration
		obj.full_lower_bounds.pc0 = res.full_lower_bounds.pc0;
		obj.full_lower_bounds.df2 = res.full_lower_bounds.df2;
		obj.full_lower_bounds.em = res.full_lower_bounds.em;
	    obj.SetLB({'all'}, {'gm'}, 0);
	    obj.SetLB({'all'}, {'em_g'}, 0); 
	 	obj.UpdateIteration;
    end

    function obj = UB4(obj, res)

		% keep the upper bounds from the previous iteration
		obj.full_upper_bounds.pc0 = res.full_upper_bounds.pc0;
		obj.full_upper_bounds.df2 = res.full_upper_bounds.df2;
		obj.full_upper_bounds.em = res.full_upper_bounds.em;
		obj.SetUB({'all'}, {'gm'}, 1);
	    obj.SetUB({'all'}, {'em_g'}, 1); 
	 	obj.UpdateIteration;
    end

    fititer = Iteration(basis);
    
    fititer.ActivateMets({'ALA','ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS',...
        'MET','PHE','PRO','SER','THR','TRP','TYR','VAL','Cr(CH2)','NAA'});
                      
    fititer.ConnectMets({'name'}, {'conc'});
%     fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 't1'});
    fititer.ConnectMets({'Cr(CH2)', 'NAA'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'});
    fititer.ConnectMets({'ALA','ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS',...
        'MET','PHE','PRO','SER','THR','TRP','TYR','VAL'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'});
    	        
	% fix parameters to certain values
    fititer.FixMets({'all'}, {'t1'});
    fititer.SetFixedValues({'all'}, {'t1'}, Inf);
    fititer.FixMets({'all'}, {'df1'});
    fititer.SetFixedValues({'all'}, {'df1'}, 0);
    fititer.FixMets({'all'}, {'pc0'});
    fititer.SetFixedValues({'all'}, {'pc0'}, 0);
    fititer.FixMets({'all'}, {'pc11'});
    fititer.SetFixedValues({'all'}, {'pc11'}, 0);
    fititer.FixMets({'all'}, {'pc12'});
    fititer.SetFixedValues({'all'}, {'pc12'}, 0);

    % set up typical bounds for the first iteration
    fititer.SetLB({'all'}, {'conc'}, 0);
    	
	% define update function handles
    %TODO change back? if we add spline lineshape
%     fititer.UpdateStartingValues = @Starting4;
%     fititer.UpdateLowerBounds    = @LB4;
%     fititer.UpdateUpperBounds    = @UB4;
    fititer.UpdateStartingValues = @Starting3;
    fititer.UpdateLowerBounds    = @LB3;
    fititer.UpdateUpperBounds    = @UB3;

    % turn on nonparametric spline baseline part 
    fititer.use_sp_baseline     = true;
    fititer.spbase_knot_spacing = [2.0 2.0];
    fititer.spbase_lambda       = 0e-1*[1 1];
    
    % also turn on spline line shape model 
    fititer.use_sp_lineshape = false;
    
    fititer.UpdateIteration;
    
    % add the just configured iteration to the queue
    fitsettings.AddIteration(fititer);
    %% save protocol file
    save(outputFile, 'fitsettings', 'data');
    
    
 end
