function createFitSettings()
%defines to adapt at call ---------------------
use_fitting_challenge_read_data = 0;
%----------------------------------------------

[fileName,pathName]=uigetfile('*.mat', ...
    'Please select .mat file to create the settings.',...
    pwd);

load([pathName fileName]);

expression = 'basisset';
replace = 'fitSettings';
pathNameOutput = regexprep(pathName,expression,replace);

%create the fitsettings based on whether basis is 1D or 2D
if (basis.pts(1) > 1)
    outputFile = [pathNameOutput 'FinalFitSettings_004+_' fileName];
    FinalFitSettings_004(basis, outputFile);
%     outputFile = [pathNameOutput 'FitSettingsJSemiLaser_4Iter_' fileName];
%     FitSettingsJSemiLaser_4Iter(basis, outputFile);
%     outputFile = [pathNameOutput 'FitSettingsJSemiLaser_NoTotal_4Iter_' fileName];
%     FitSettingsJSemiLaser_NoTotal_4Iter(basis, outputFile);
else
    if (use_fitting_challenge_read_data == 1)
        outputFile = [pathNameOutput 'FitSettingsRead4Iter_' fileName];
        FitSettingsPress4IterFittingChallenge(basis, outputFile); 
    else
%         outputFile = [pathNameOutput 'FitSettingsPress4Iter_' fileName];
%         FitSettingsPress4Iter(basis, outputFile);

%         outputFile = [pathNameOutput 'FitSettings_sLASER_3Iter_' fileName];
%         FitSettings_sLASER_3Iter(basis, outputFile);

%         outputFile = [pathNameOutput 'FitSettings_MMB_' fileName];
%         FitSettings_MMB(basis, outputFile);

%         outputFile = [pathNameOutput 'FitSettings_sLASER_3Iter_T2_' fileName];
%         FitSettings_sLASER_3Iter_prior_T2(basis, outputFile);
        
%         outputFile = [pathNameOutput 'FitSettings_sLASER_3Iter_T2_baseline2nd_' fileName];
%         FitSettings_sLASER_3Iter_prior_T2_baseline2nd(basis, outputFile);
        
        outputFile = [pathNameOutput 'FitSettings_sLASER_3Iter_T2_no_emg_' fileName];
        FitSettings_sLASER_3Iter_prior_T2_no_emg(basis, outputFile);
    end
end

end