classdef TestLsqNonLin  < ProfitTestBase
    %TESTLSQNONLIN tests the lsqnonlin Matlab function for the CalcCostFun 
    %script for the correctness of the results. 
    %The correctness of the results is proved by using sample data from
    %successful runs within the entire Profit 2.0 quantification.
    
    properties
        %none given
    end
    
    methods
        %Constructor of the class
        %param numOfIteration specifies the number of repetitions, which
        %shall be used for testing
        %param vectorOfTestCases gives a list of the testcases on which the
        %test shall be run
        function obj = TestLsqNonLin(numOfIteration, vectorOfTestCases)
            obj@ProfitTestBase(numOfIteration, vectorOfTestCases);
        end
    end
    
    methods(Test)
        %Test methods
        function testRealSolution(obj)
            %TESTREALSOLUTION checks if the recorded test data is matched
            %by the execution call of the lsqnonlin with CalcCostFun method. 
            %Output variables are checked based on a relative tolerance
            %(keep in mind that we use floating point operations.)
            
            %read in the needed variables
            path=[obj.pathToTestData 'LsqNonLinData/Test' num2str(obj.testNumber_) '/'];
            
            %setting the global damping variables
            global g_damping;
            
            %read in input arguments
            load([path 'input.mat']);
                        
            %for cycle used for timing measurements (set it to 1:obj.numberOfIterations_ for
            %example)
            for iteration = 1: obj.numberOfIterations_
                %retrieving the output variables from the current
                %lsqnonlin function.
                data.metaboliteFilter =  abs(data.lb_t1_mx + data.shift_t2_mx);
                data.shifted_lb_mx_abs = abs(data.shifted_lb_mx);
                data.shifted_lb_mx_squared = data.shifted_lb_mx.^2;
                [x_real, resnorm_real, residual_real, exitflag_real, output_real, lambda_real, jacobian_real]= ...
                    lsqnonlin(@(x) CalcCostFun(x, fid, cdres.spec, fs.seq{iter}, basis, data, undamped_fid, spbase, spls), x0, lb, ub, options);
                g_damping_real = g_damping;
                undamped_fid_real = undamped_fid;
            end
            
            %read in expected output variables
            load([path 'output.mat']);            
            
            %verifying if the returned values of the lsqlin function match the
            %expected values.
            tolObj = matlab.unittest.constraints.AbsoluteTolerance(eps) | matlab.unittest.constraints.RelativeTolerance(10^(-7));
            obj.verifyEqual(x_real, x, 'RelTol', 10^(-10), 'Difference between actual x and expected x exceeds relative tolerance');
            obj.verifyThat(resnorm_real, matlab.unittest.constraints.IsEqualTo(resnorm, 'Within', tolObj), 'Difference between actual resnorm and expected resnorm exceeds tolerance');
            obj.verifyThat(residual_real, matlab.unittest.constraints.IsEqualTo(residual, 'Within', tolObj), 'Difference between actual residual and expected residual exceeds tolerance');
            obj.verifyThat(output_real, matlab.unittest.constraints.IsEqualTo(output, 'Within', tolObj), 'Difference between actual output and expected output exceeds tolerance');
            obj.verifyThat(lambda_real, matlab.unittest.constraints.IsEqualTo(lambda, 'Within', tolObj), 'Difference between actual lambda and expected lambda exceeds tolerance');
            obj.verifyThat(jacobian_real, matlab.unittest.constraints.IsEqualTo(jacobian, 'Within', tolObj), 'Difference between actual jacobian and expected jacobian exceeds tolerance');
            
            obj.verifyEqual(exitflag_real, exitflag, 'Difference between actual exitflag and expected exitflag exceeds relative tolerance');
            obj.verifyEqual(g_damping_real, g_damping, 'RelTol', 10^(-8), 'Difference between actual g_damping_before and expected g_damping_after exceeds relative tolerance');
            obj.verifyEqual(undamped_fid_real, undamped_fid, 'RelTol', 10^(-8), 'Difference between actual undamped_fid_before and expected g_damping_after exceeds relative tolerance');
        end
    end
end

