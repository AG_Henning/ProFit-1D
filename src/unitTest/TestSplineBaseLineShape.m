classdef TestSplineBaseLineShape  < ProfitTestBase
    %TESTSPLINEBASELINESHAPE tests the get_spline_basis_ls script for the correctness of
    %the results. 
    %The correctness of the results is proved by using sample data from
    %successful runs within the entire Profit 2.0 quantification.
    
    properties
        %iteration number for which the test cases were produced
        %(fs.seq{iter}
        fixedFitSettingsIteration_ = 4;
    end
    
    methods
        %Constructor of the class
        %param numOfIteration specifies the number of repetitions, which
        %shall be used for testing
        %param vectorOfTestCases gives a list of the testcases on which the
        %test shall be run
        function this = TestSplineBaseLineShape(numOfIteration, vectorOfTestCases)
            this@ProfitTestBase(numOfIteration, vectorOfTestCases);
        end
    end
    
    methods(Test)
        %Test methods
        function testRealSolution(this)
            %TESTREALSOLUTION checks if the recorded test data is matched
            %by the execution call of the get_spline_basis method. 
            %Output variables are checked based on a relative tolerance
            %(keep in mind that we use floating point operations.)
            
            %read in the needed variables
            path=[this.pathToTestData 'SplineBasisLineShapeData/Test' num2str(this.testNumber_) '/'];
            
            %read in input arguments
            load([path 'data.mat']);
            load([path 'fitSettings.mat']);
                        
            %read in expected output variables
            load([path 'spls.mat']);
                
            %for cycle used for timing measurements (set it to 1:this.numberOfIterations_ for
            %example)
            for iteration = 1: this.numberOfIterations_
                %retrieving the output variables from the current
                %CalcCostFun function.
                [spls_real.BB, spls_real.P, spls_real.knots]  = get_spline_basis_ls(fs.seq{this.fixedFitSettingsIteration_}, data);
            end
            
            %verifying if the returned values of the CalcCostFun match the
            %expected values.
            this.verifyEqual(spls_real.BB, spls.BB, 'RelTol', 10^(-10), 'Difference between actual spls.BB and expected spls.BB exceeds relative tolerance');
            this.verifyEqual(spls_real.P, spls.P, 'RelTol', 10^(-10), 'Difference between actual spls.P and expected spls.P exceeds relative tolerance');
            this.verifyEqual(spls_real.knots{1}, spls.knotsx);
            this.verifyEqual(spls_real.knots{2}, spls.knotsy);
          end
    end
end

