%% test CalcCostFun
testCase = TestCalcCostFun(1, 9:10); 
% run(testCase, 'testRealSolution');
run(testCase, 'testRealSolutionSuite');

%% test SplineBase
testCase = TestSplineBase(3,1:6);
%run(testCase, 'testRealSolution');
run(testCase, 'testRealSolutionSuite');

%% test SplineBaseLineShape
testCase = TestSplineBaseLineShape(3,1:6);
%run(testCase, 'testRealSolution');
run(testCase, 'testRealSolutionSuite');

%% test LsqNonLin
testCase = TestLsqNonLin(1, [4]); 
%run(testCase, 'testRealSolution');
run(testCase, 'testRealSolutionSuite');

%% test ProFit
testCase = TestProfit(1, 5:5);
run(testCase, 'testRealSolutionSuite');
