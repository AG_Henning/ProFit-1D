function plotCompareBasisSet2D(basis1, basis2)

% Prepare environment and delete previously created files:
close all;

plotImage = 1;

%create the correct path to the results folder of profit
currentFilePath = mfilename('fullpath');
[currentPath, ~, ~] = fileparts(currentFilePath);
pathToResults = '/../../../../results/' ;
pathToResults = [currentPath pathToResults];
if exist(pathToResults , 'dir') == 0
    warning('Folder %s does not exist. Creating it.', pathToResults);
    mkdir(pathToResults);
end  

fileNameOutput = [pathToResults 'CompareBasisSets.pdf'];
if exist(fileNameOutput ,'file')==2
    delete(fileNameOutput);
end

ptsXDim = basis1.pts(2);
ptsYDim = basis1.pts(1);
xaxis = 1 : ptsXDim;
yaxis = 1 : ptsYDim;


% Run for all metabolites in the actual iteration
for actualMetabolite=1:size(basis1.fid,2)
    % Prepare and calculate values
    figureHandle(actualMetabolite).handle=...
        figure(actualMetabolite);
    spectrum1 = fft(basis1.fid{actualMetabolite}, [], 2);
    spectrum2 = fft(basis2.fid{actualMetabolite}, [], 2);
    spectrumDiff = fft(spectrum1, [], 2) - fft(spectrum2, [], 2);
    
    title(char(basis1.met(actualMetabolite)));
    % Plot figures, save them to file and close figure
    subplot(3,1,1);
    if (plotImage == 1)
        imagesc(xaxis, yaxis, real(fft(spectrum1, [], 1)));
    else
        plot(xaxis, real(spectrum1));
    end
    set(gca,'XDir','reverse');
    xlabel('[ppm]');ylabel('[a.u.]');
    axis([xaxis(1) xaxis(end) 0 Inf]);    
    title(char(basis1.met(actualMetabolite)));
    
    subplot(3,1,2);
    if (plotImage == 1)
        imagesc(xaxis, yaxis, real(fft(spectrum2, [], 1)));
    else
        plot(xaxis, real(spectrum2));
    end
    set(gca,'XDir','reverse');
    xlabel('[ppm]');ylabel('[a.u.]');
    axis([xaxis(1) xaxis(end) 0 Inf]);
    title(char(basis2.met(actualMetabolite)));
    
    subplot(3,1,3);
    if (plotImage == 1)
        imagesc(xaxis, yaxis, real(fft(spectrumDiff, [], 1)));
    else
        plot(xaxis, real(spectrumDiff));
    end
    set(gca,'XDir','reverse');
    xlabel('[ppm]');ylabel('[a.u.]');
    axis([xaxis(1) xaxis(end) 0 Inf]);
    title(char(basis1.met(actualMetabolite)));
    
    % Export acutal figure to pdf file with function export_fig from
    % Woodford and Altman (available on MathWorks Exchange)
    export_fig(fileNameOutput,...
        '-append', figureHandle(actualMetabolite).handle);
    close(figureHandle(actualMetabolite).handle)
end % End for loop metabolites

display(['Results written to: ' fileNameOutput]);