function [equal] = compareFitSettings(fitSettings1, fitSettings2)

%assume that the fitSettings are equal
equal = true;

%create nice printing function
tf_words = {'false','true'};
bool2char = @(x) tf_words{x+1};

for iter = 1 : 4
    
    %assume that the iteration of fitSettings are equal
    equalIter = true;
    
    equalActiveMets = eq(fitSettings1.seq{iter}.active_mets, fitSettings2.seq{iter}.active_mets);
    equalIter = equalIter && (sum(equalActiveMets)/size(equalActiveMets,2)==1);
    %TODO UpdateStartingValues
    %TODO UpdateLowerBounds
    %TODO UpdateUpperBounds
    equalUseSpBaseline = eq(fitSettings1.seq{iter}.use_sp_baseline, fitSettings2.seq{iter}.use_sp_baseline);
    equalIter = equalIter && (sum(equalUseSpBaseline)/size(equalUseSpBaseline,2)==1);
    equalUseSpLineshape = eq(fitSettings1.seq{iter}.use_sp_lineshape, fitSettings2.seq{iter}.use_sp_lineshape);
    equalIter = equalIter && (sum(equalUseSpLineshape)/size(equalUseSpLineshape,2)==1);
    equalFullApplySpLineshape = eq(fitSettings1.seq{iter}.full_apply_sp_lineshape, fitSettings2.seq{iter}.full_apply_sp_lineshape);
    equalIter = equalIter && (sum(equalFullApplySpLineshape)/size(equalFullApplySpLineshape,2)==1);
    equalApplySpLineshape = eq(fitSettings1.seq{iter}.apply_sp_lineshape, fitSettings2.seq{iter}.apply_sp_lineshape);
    equalIter = equalIter && (sum(equalApplySpLineshape)/size(equalApplySpLineshape,2)==1);
    % spbase_knot_spacing
    equalSpbaseKnotSpacing = isequal(fitSettings1.seq{iter}.spbase_knot_spacing, fitSettings2.seq{iter}.spbase_knot_spacing);
    equalIter = equalIter && (sum(equalSpbaseKnotSpacing)/size(equalSpbaseKnotSpacing,2)==1);
    % spbase_knot_spacing
    equalSpbaseLambda = isequal(fitSettings1.seq{iter}.spbase_lambda, fitSettings2.seq{iter}.spbase_lambda);
    equalIter = equalIter && (sum(equalSpbaseLambda)/size(equalSpbaseLambda,2)==1);
    %nr_act_mets
    equalNrActMets = eq(fitSettings1.seq{iter}.nr_act_mets, fitSettings2.seq{iter}.nr_act_mets);
    equalIter = equalIter && (sum(equalNrActMets)/size(equalNrActMets,2)==1);
    %TODO basis
    equalMetIdx = isequal(fitSettings1.seq{iter}.met_idx, fitSettings2.seq{iter}.met_idx);
    equalIter = equalIter && equalMetIdx;
    equalMetBoolIdx = isequal(fitSettings1.seq{iter}.met_boolidx, fitSettings2.seq{iter}.met_boolidx);
    equalIter = equalIter && equalMetBoolIdx;
    equalNrMets = eq(fitSettings1.seq{iter}.nr_mets, fitSettings2.seq{iter}.nr_mets);
    equalIter = equalIter && (sum(equalNrMets)/size(equalNrMets,2)==1);
    
    %% met_connections 35x35
    %conc
    equalMetConnectionsConc = eq(fitSettings1.seq{iter}.met_connections.conc, fitSettings2.seq{iter}.met_connections.conc);
    equalIter = equalIter && (sum(equalMetConnectionsConc(:))/numel(equalMetConnectionsConc)==1);
    %em
    equalMetConnectionsEm = eq(fitSettings1.seq{iter}.met_connections.em, fitSettings2.seq{iter}.met_connections.em);
    equalIter = equalIter && (sum(equalMetConnectionsEm(:))/numel(equalMetConnectionsEm)==1);
    %em_g
    equalMetConnectionsEmg = eq(fitSettings1.seq{iter}.met_connections.em_g, fitSettings2.seq{iter}.met_connections.em_g);
    equalIter = equalIter && (sum(equalMetConnectionsEmg(:))/numel(equalMetConnectionsEmg)==1);
    %gm
    equalMetConnectionsGm = eq(fitSettings1.seq{iter}.met_connections.gm, fitSettings2.seq{iter}.met_connections.gm);
    equalIter = equalIter && (sum(equalMetConnectionsGm(:))/numel(equalMetConnectionsGm)==1);
    %pc0
    equalMetConnectionsPc0 = eq(fitSettings1.seq{iter}.met_connections.pc0, fitSettings2.seq{iter}.met_connections.pc0);
    equalIter = equalIter && (sum(equalMetConnectionsPc0(:))/numel(equalMetConnectionsPc0)==1);
    %pc11
    equalMetConnectionsPc11 = eq(fitSettings1.seq{iter}.met_connections.pc11, fitSettings2.seq{iter}.met_connections.pc11);
    equalIter = equalIter && (sum(equalMetConnectionsPc11(:))/numel(equalMetConnectionsPc11)==1);
    %pc12
    equalMetConnectionsPc12 = eq(fitSettings1.seq{iter}.met_connections.pc12, fitSettings2.seq{iter}.met_connections.pc12);
    equalIter = equalIter && (sum(equalMetConnectionsPc12(:))/numel(equalMetConnectionsPc12)==1);
    %df1
    equalMetConnectionsDf1 = eq(fitSettings1.seq{iter}.met_connections.df1, fitSettings2.seq{iter}.met_connections.df1);
    equalIter = equalIter && (sum(equalMetConnectionsDf1(:))/numel(equalMetConnectionsDf1)==1);
    %df2
    equalMetConnectionsDf2 = eq(fitSettings1.seq{iter}.met_connections.df2, fitSettings2.seq{iter}.met_connections.df2);
    equalIter = equalIter && (sum(equalMetConnectionsDf2(:))/numel(equalMetConnectionsDf2)==1);
    equalMetConnectionsT1 = eq(fitSettings1.seq{iter}.met_connections.t1, fitSettings2.seq{iter}.met_connections.t1);
    equalIter = equalIter && (sum(equalMetConnectionsT1(:))/numel(equalMetConnectionsT1)==1);
    %%
    % idx 1x35
    equalIdx = isequal(fitSettings1.seq{iter}.idx, fitSettings2.seq{iter}.idx);
    equalIter = equalIter && equalIdx;
    
    % fixed_mets 1x35
    equalFixedMets = isequal(fitSettings1.seq{iter}.fixed_mets, fitSettings2.seq{iter}.fixed_mets);
    equalIter = equalIter && equalFixedMets;
    
    % full_mask
    equalFullMask = isequal(fitSettings1.seq{iter}.full_mask, fitSettings2.seq{iter}.full_mask);
    equalIter = equalIter && equalFullMask;
    
    % sub_mask
    equalSubMask = isequal(fitSettings1.seq{iter}.sub_mask, fitSettings2.seq{iter}.sub_mask);
    equalIter = equalIter && equalSubMask;
    
    % nr
    equalNr = isequal(fitSettings1.seq{iter}.nr, fitSettings2.seq{iter}.nr);
    equalIter = equalIter && equalNr;
    
    % mapping_mx 7x35
    equalMappingMx = isequal(fitSettings1.seq{iter}.mapping_mx, fitSettings2.seq{iter}.mapping_mx);
    equalIter = equalIter && equalMappingMx;
    
    % full_start_values 1x35
    equalFullStartValues = isequal(fitSettings1.seq{iter}.full_start_values, fitSettings2.seq{iter}.full_start_values);
    equalIter = equalIter && equalFullStartValues;
    
    % full_lower_bounds 1x35
    equalFullUpperBounds = isequaln(fitSettings1.seq{iter}.full_lower_bounds, fitSettings2.seq{iter}.full_lower_bounds);
    equalIter = equalIter && equalFullUpperBounds;

    % full_upper_bounds 1x35
    equalFullUpperBounds = isequaln(fitSettings1.seq{iter}.full_upper_bounds, fitSettings2.seq{iter}.full_upper_bounds);
    equalIter = equalIter && equalFullUpperBounds;
    
    % full_fixed_values 1x35
    equalFullFixedValues = isequaln(fitSettings1.seq{iter}.full_fixed_values, fitSettings2.seq{iter}.full_fixed_values);
    equalIter = equalIter && equalFullFixedValues;
    
    % full_lin_priors []
    equalFullLinPriors = isequal(fitSettings1.seq{iter}.full_lin_priors, fitSettings2.seq{iter}.full_lin_priors);
    equalIter = equalIter && equalFullLinPriors;
    
    % full_lin_priors []
    equalUserFullLinPriors = isequal(fitSettings1.seq{iter}.user_full_lin_priors, fitSettings2.seq{iter}.user_full_lin_priors);
    equalIter = equalIter && equalUserFullLinPriors;
    
   % fixed_values 1x13
    equalFixedValues = isequaln(fitSettings1.seq{iter}.fixed_values, fitSettings2.seq{iter}.fixed_values);
    equalIter = equalIter && equalFixedValues;
    
    % start_values custom
    equalStartValues = isequal(fitSettings1.seq{iter}.start_values, fitSettings2.seq{iter}.start_values);
    equalIter = equalIter && equalStartValues;
    
    % upper_bounds custom
    equalUpperBounds = isequal(fitSettings1.seq{iter}.upper_bounds, fitSettings2.seq{iter}.upper_bounds);
    equalIter = equalIter && equalUpperBounds;
    
    % lower_bounds custom
    equalLowerBounds = isequal(fitSettings1.seq{iter}.lower_bounds, fitSettings2.seq{iter}.lower_bounds);
    equalIter = equalIter && equalLowerBounds;
    
    % lin_priors custom
    equalLinPriors = isequal(fitSettings1.seq{iter}.lin_priors, fitSettings2.seq{iter}.lin_priors);
    equalIter = equalIter && equalLinPriors;
    
    % hUpdates cells! Check
    equalHUpdates = isequal(fitSettings1.seq{iter}.hUpdates, fitSettings2.seq{iter}.hUpdates);
    equalIter = equalIter && equalHUpdates;
    
    % needUpdates
    equalNeedUpdates = eq(fitSettings1.seq{iter}.needUpdate, fitSettings2.seq{iter}.needUpdate);
    equalIter = equalIter && equalNeedUpdates;
    
    display(['Iteration: ' num2str(iter) ' equal: ' bool2char(equalIter)]);
    
    equal = equal && equalIter;
end


end
