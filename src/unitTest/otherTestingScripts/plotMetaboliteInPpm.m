function [] = plotMetaboliteInPpm(basis, fitroi, tilt_matrix)

exportFilePath = 'PlotMetaboliteInPpmResult2.pdf';
if exist(exportFilePath ,'file') == 2
    delete(exportFilePath)
end
% Display images high resolution: scaling factor higherResolutionFactor
higherResolutionFactor = [4 4];

% If higherResolutionFactor is larger than one, adjust the axis to cover finer
% resolution -> from [25 242] to [25*higherResolutionFactor(1) 242*higherResolutionFactor(2)]
% f1 is indirect and f2 is direct frequency axis
yaxis = interp1(1:fitroi.pts(1), fitroi.ppm1, ...
    linspace(1, fitroi.pts(1), higherResolutionFactor(1) * fitroi.pts(1)));
xaxis = interp1(1:fitroi.pts(2), fitroi.ppm2, ...
    linspace(1, fitroi.pts(2), higherResolutionFactor(2) * fitroi.pts(2)));
% Prepare meshgrid parameter to be able to interp2 the fit
% from 25x242 to 25*higherResolutionFactor(1)x242*higherResolutionFactor(2)
% Original grid
[ogrid_x, ogrid_y] = meshgrid(1:fitroi.pts(2), 1:fitroi.pts(1));
% High resolution grid
[ngrid_x, ngrid_y] = meshgrid(...
    linspace(1, fitroi.pts(2), higherResolutionFactor(2) * fitroi.pts(2)), ...
    linspace(1, fitroi.pts(1), higherResolutionFactor(1) * fitroi.pts(1)));

% Define the region of interest (ROI) of the spectrum and perfoming the
% computations in the following only on this region.
spectrum_roi = [fitroi.bounds(1,1) fitroi.bounds(1,2); fitroi.bounds(2,1) fitroi.bounds(2,2)];

% Run for all metabolites in the actual iteration
for metabolite = 1 : length(basis.fid)
    % Prepare and calculate values
    figureHandle = figure(metabolite);
    
    % get tilted version of basis spectrum
	basis_spec = fft(fft(basis.fid{metabolite},[],2).*tilt_matrix,[],1);
    % cut it to the size of the quantification window
    basis_spec = basis_spec(spectrum_roi(1,1):spectrum_roi(2,1), ...
                            spectrum_roi(1,2):spectrum_roi(2,2));
                        
    fitted_metabolites = reshape(basis_spec, [fitroi.pts(1) fitroi.pts(2)]);
    twoDSpectrumFit = interp2(ogrid_x, ogrid_y, real(fitted_metabolites), ngrid_x, ngrid_y);
    
    % Plot figures, save them to file and close figure
    subplot(2,1,1);
    imagesc(xaxis, yaxis, twoDSpectrumFit);
    set(gca,'XDir','reverse');
    xlabel('[ppm]'); ylabel('[ppm]');
    title(char(basis.met{metabolite}));
    subplot(2,1,2);
    plot(xaxis, mean(twoDSpectrumFit, 1));
    set(gca,'XDir','reverse');
    xlabel('[ppm]');ylabel('[a.u.]');
    axis([xaxis(1) xaxis(end) 0 Inf]);
    
    % Export acutal figure to pdf file with function export_fig from
    % Woodford and Altman (available on MathWorks Exchange)
    export_fig(exportFilePath, '-append', figureHandle);
    close(figureHandle)
end % End for loop metabolites

end