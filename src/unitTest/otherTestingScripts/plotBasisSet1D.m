function plotBasisSet1D(basis)

% Prepare environment and delete previously created files:
close all;

%create the correct path to the results folder of profit
currentFilePath = mfilename('fullpath');
[currentPath, ~, ~] = fileparts(currentFilePath);
pathToResults = '/../../../../results/' ;
pathToResults = [currentPath pathToResults];
if exist(pathToResults , 'dir') == 0
    warning('Folder %s does not exist. Creating it.', pathToResults);
    mkdir(pathToResults);
end    

fileNameOutput = [pathToResults 'FittingChallengeBasisSet1D_PRESS.pdf'];
if exist(fileNameOutput ,'file')==2
    delete(fileNameOutput);
end


xaxis = 1 : basis.pts(2);
% Run for all metabolites in the actual iteration
for actualMetabolite = 1 : size(basis.met,2)
    % Prepare and calculate values
    figureHandle(actualMetabolite).handle=...
        figure(actualMetabolite);
    spectrum = real(fftshift(fft(basis.fid{actualMetabolite})));
    
    % Plot figures, save them to file and close figure
    plot(xaxis, spectrum);
    title(char(basis.met(actualMetabolite)));
    set(gca,'XDir','reverse');
    xlabel('[ppm]');ylabel('[a.u.]');
    axis([xaxis(1) xaxis(end) 0 Inf]);
    
    % Export acutal figure to pdf file with function export_fig from
    % Woodford and Altman (available on MathWorks Exchange)
    export_fig(fileNameOutput,...
        '-append', figureHandle(actualMetabolite).handle);
    close(figureHandle(actualMetabolite).handle)
end % End for loop metabolites

display(['Results written to: ' fileNameOutput]);