function plotBasisSet2D(basis)

% Prepare environment and delete previously created files:
close all;

plotImage = 1;
doFftShift = 1;

%create the correct path to the results folder of profit
currentFilePath = mfilename('fullpath');
[currentPath, ~, ~] = fileparts(currentFilePath);
pathToResults = '/../../../../results/' ;
pathToResults = [currentPath pathToResults];
if exist(pathToResults , 'dir') == 0
    warning('Folder %s does not exist. Creating it.', pathToResults);
    mkdir(pathToResults);
end  

fileNameOutput = [pathToResults 'BasisSet2D_JPRESS.pdf'];
if exist(fileNameOutput ,'file')==2
    delete(fileNameOutput);
end


ptsXDim = basis.pts(2);
ptsYDim = basis.pts(1);
xaxis = 1 : ptsXDim;
yaxis = 1 : ptsYDim;

% Run for all metabolites in the actual iteration
for actualMetabolite = 1 : size(basis.fid,2)
    % Prepare and calculate values
    figureHandle(actualMetabolite).handle=...
        figure(actualMetabolite);
    spectrum = (fft(basis.fid{actualMetabolite}, [], 2));
    if (doFftShift == 1)
        spectrum = fftshift(spectrum);
    end
    % Plot figures, save them to file and close figure
    if (plotImage == 1)
        imagesc(xaxis, yaxis, real(fft(spectrum, [], 1)));
    else
        plot(xaxis, real(spectrum));
    end
    title(char(basis.met(actualMetabolite)));
    set(gca,'XDir','reverse');
    xlabel('[ppm]');ylabel('[a.u.]');
    axis([xaxis(1) xaxis(end) 0 Inf]);
    
    % Export acutal figure to pdf file with function export_fig from
    % Woodford and Altman (available on MathWorks Exchange)
    export_fig(fileNameOutput,...
        '-append', figureHandle(actualMetabolite).handle);
    close(figureHandle(actualMetabolite).handle)
end % End for loop metabolites

display(['Results written to: ' fileNameOutput]);