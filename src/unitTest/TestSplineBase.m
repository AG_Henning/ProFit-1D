classdef TestSplineBase  < ProfitTestBase
    %TESTSPLINEBASE tests the get_spline_basis script for the correctness of
    %the results. 
    %The correctness of the results is proved by using sample data from
    %successful runs within the entire Profit 2.0 quantification.
    
    properties
        %iteration number for which the test cases were produced
        %(fs.seq{iter}
        fixedFitSettingsIteration_ = 4;
    end
    
    methods
        %Constructor of the class
        %param numOfIteration specifies the number of repetitions, which
        %shall be used for testing
        %param vectorOfTestCases gives a list of the testcases on which the
        %test shall be run
        function this = TestSplineBase(numOfIteration, vectorOfTestCases)
            this@ProfitTestBase(numOfIteration, vectorOfTestCases);
        end
    end
    
    methods(Test)
        %Test methods
        function testRealSolution(this)
            %TESTREALSOLUTION checks if the recorded test data is matched
            %by the execution call of the get_spline_basis method. 
            %Output variables are checked based on a relative tolerance
            %(keep in mind that we use floating point operations.)
            
            %read in the needed variables
            path=[this.pathToTestData 'SplineBasisData/Test' num2str(this.testNumber_) '/'];
            
            %read in input arguments
            load([path 'data.mat']);
            load([path 'fitSettings.mat']);
                        
            %read in expected output variables
            load([path 'spbase.mat']);
                
            %for cycle used for timing measurements (set it to 1:this.numberOfIterations_ for
            %example)
            for iteration = 1: this.numberOfIterations_
                %retrieving the output variables from the current
                %CalcCostFun function.
                [spbase_real.BB, spbase_real.P] = get_spline_basis(fs.seq{this.fixedFitSettingsIteration_}, data);
                
                %TODO this try is a horrible failure in terms of Matlab class
                %inheritance. Take a decision to eliminate this completely
                %or not
%                 splineBase = ProfitSplineBaseLine(fs.seq{this.fixedFitSettingsIteration_}, data);
%                 spbase_real.BB = splineBase.getBSpline();
            end
            
            %verifying if the returned values of the CalcCostFun match the
            %expected values.
            this.verifyEqual(spbase_real.BB, spbase.BB, 'RelTol', 10^(-10), 'Difference between actual spbase.BB and expected spbase.BB exceeds relative tolerance');
            this.verifyEqual(spbase_real.P, spbase.P, 'RelTol', 10^(-10), 'Difference between actual spbase.P and expected spbase.P exceeds relative tolerance');
          end
    end
end

