classdef TestProfit < ProfitTestBase
    %TESTCALCCOSTFUN tests the CalcCostFun script for the correctness of
    %the results. 
    %The correctness of the results is proved by using sample data from
    %successful runs within the entire Profit 2.0 quantification.
    
    properties
        %none given
    end
    
    methods
        %Constructor of the class
        %param numOfIteration specifies the number of repetitions, which
        %shall be used for testing
        %param vectorOfTestCases gives a list of the testcases on which the
        %test shall be run
        function obj = TestProfit(numOfIteration, vectorOfTestCases)
            obj@ProfitTestBase(numOfIteration, vectorOfTestCases);
        end     
        
        
        function saveParameters(obj, parameter, parameterName, filepath)
            paramProperties = {'conc' 'em', 'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'};
            for paramProperty =  paramProperties
                newParameterName = [parameterName, '__', paramProperty{1}];
                eval([newParameterName '=parameter.' paramProperty{1} ';']);
                save(filepath, newParameterName, '-append');
            end
        end
        
        function saveParametersCollection(obj, parameter, parameterName, filepath)
            paramProperties = {'conc' 'em', 'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 'df2', 't1'};
            paramCollections = {'full' 'free' 'fixed'};
            for paramProperty =  paramProperties
                for paramCollection = paramCollections
                    newParameterName = [parameterName, '__', paramProperty{1}, '__', paramCollection{1}];
                    eval([newParameterName '=parameter.' paramProperty{1} '.' paramCollection{1} ';']);
                    save(filepath, newParameterName, '-append');
                end
            end
        end
        
        function saveBasis(obj, basis, filepath)
            basisProperties = {'f0', 'dt', 'bw', 'pts', 'met', 'fid'};
            for basisProperty =  basisProperties
                newParameterName = ['basis__', basisProperty{1}];
                eval([newParameterName '=basis.' basisProperty{1} ';']);
                save(filepath, newParameterName, '-append');
            end
        end
        
        
        function saveFitRoi(obj, fitroi, filepath)
            fitroiProperties = {'bounds', 'mask', 'ppm1', 'ppm2', 'pts', 't1', 't2', 'w1', 'w2', 'tilt_mx', 'untilt_mx', 'bw', 'dt'};
            for fitroiProperty =  fitroiProperties
                newParameterName = ['fitroi__', fitroiProperty{1}];
                eval([newParameterName '=fitroi.' fitroiProperty{1} ';']);
                save(filepath, newParameterName, '-append');
            end
        end
        
                
        function saveFitObj(obj, fitobj, path, suffix)
            if ~exist('suffix', 'var')
                suffix = '';
            end
            filepath = [path 'fitobj_unpack' suffix '.mat'];
            %%
            active_mets = fitobj.active_mets;
            use_sp_baseline = fitobj.use_sp_baseline;
            use_sp_lineshape = fitobj.use_sp_lineshape;
            full_apply_sp_lineshape = fitobj.full_apply_sp_lineshape;
            apply_sp_lineshape = fitobj.apply_sp_lineshape;
            spbase_knot_spacing = fitobj.spbase_knot_spacing;
            spbase_lambda = fitobj.spbase_lambda;
            nr_act_mets = fitobj.nr_act_mets;
            met_idx = fitobj.met_idx;
            met_boolidx = fitobj.met_boolidx;
            nr_mets = fitobj.nr_mets;
            %
            hUpdates = fitobj.hUpdates;
            needUpdate = fitobj.needUpdate;
            
            variables = {'active_mets', 'use_sp_baseline', 'use_sp_lineshape', 'full_apply_sp_lineshape', 'apply_sp_lineshape', ...
                'spbase_knot_spacing', 'spbase_lambda', 'nr_act_mets', 'met_idx', 'met_boolidx', 'nr_mets', 'hUpdates', 'needUpdate'};
            save(filepath, variables{1})
            
            for variable = variables
                save(filepath, variable{1}, '-append')
            end
            
            obj.saveBasis(fitobj.basis, filepath);
            
            obj.saveParameters(fitobj.met_connections, 'met_connections', filepath);
            obj.saveParameters(fitobj.idx, 'idx', filepath);
            obj.saveParameters(fitobj.fixed_mets, 'fixed_mets', filepath);
            obj.saveParametersCollection(fitobj.full_mask, 'full_mask', filepath);
            obj.saveParametersCollection(fitobj.sub_mask, 'sub_mask', filepath);
            obj.saveParametersCollection(fitobj.nr, 'nr', filepath);
            obj.saveParameters(fitobj.mapping_mx, 'mapping_mx', filepath);
            obj.saveParameters(fitobj.full_start_values, 'full_start_values', filepath);
            obj.saveParameters(fitobj.full_lower_bounds, 'full_lower_bounds', filepath);
            obj.saveParameters(fitobj.full_upper_bounds, 'full_upper_bounds', filepath);
            obj.saveParameters(fitobj.full_fixed_values, 'full_fixed_values', filepath);
            obj.saveParameters(fitobj.full_lin_priors, 'full_lin_priors', filepath);
            obj.saveParameters(fitobj.user_full_lin_priors, 'user_full_lin_priors', filepath);
            obj.saveParameters(fitobj.fixed_values, 'fixed_values', filepath);
            obj.saveParameters(fitobj.start_values, 'start_values', filepath);
            obj.saveParameters(fitobj.upper_bounds, 'upper_bounds', filepath);
            obj.saveParameters(fitobj.lower_bounds, 'lower_bounds', filepath);
            obj.saveParameters(fitobj.lin_priors, 'lin_priors', filepath);
        end
        
        function saveData(obj, data, path)
            filepath = [path 'data_unpack.mat'];
            %%
            quant_window_ppm = data.quant_window_ppm;
            noise_window_ppm = data.noise_window_ppm;
            zero_fill_factor = data.zero_fill_factor;
            debug = data.debug;
            do_water_separation = data.do_water_separation;
            do_water_filtering = data.do_water_filtering;
            f0 = data.f0;
            dt = data.dt;
            bw = data.bw;
            te = data.te;
            pts = data.pts;
            total_ppm = data.total_ppm;
            pts_ppm = data.pts_ppm;
            tr = data.tr;
            center_ppm = data.center_ppm;
            t1 = data.t1;
            t2 = data.t2;
            w1 = data.w1;
            w2 = data.w2;
            ppm1 = data.ppm1;
            ppm2 = data.ppm2;
            shift_t1_mx = data.shift_t1_mx;
            shift_t2_mx = data.shift_t2_mx;
            lb_t1_mx = data.lb_t1_mx;
            shifted_lb_mx = data.shifted_lb_mx;
            tilt_mx = data.tilt_mx;
            untilt_mx = data.untilt_mx;
            phase_0_mx = data.phase_0_mx;
            phase_1_f1_mx = data.phase_1_f1_mx;
			data.phase_1_f2_mx = data.phase_1_f2_mx ./ data.total_ppm(2) * 2; %corrected for new version
            phase_1_f2_mx = data.phase_1_f2_mx;
            phase_1_f2_mx = data.phase_1_f2_mx;
            noise_var = data.noise_var;
            spectral_dimension = data.spectral_dimension;
            
            variables = {'quant_window_ppm', 'noise_window_ppm', 'zero_fill_factor', 'debug', 'do_water_separation', ...
                'do_water_filtering', 'f0', 'dt', 'bw', 'te', 'pts', 'total_ppm', 'pts_ppm', 'tr', 'center_ppm', 't1', ...
                't2', 'w1', 'w2', 'ppm1', 'ppm2', 'shift_t1_mx', 'shift_t2_mx', 'lb_t1_mx', 'shifted_lb_mx', 'tilt_mx', ...
                'untilt_mx', 'phase_0_mx', 'phase_1_f1_mx', 'phase_1_f2_mx', 'noise_var', 'spectral_dimension'};
            save(filepath, variables{1})

            for variable = variables
                save(filepath, variable{1}, '-append')
            end
            
            obj.saveFitRoi(data.fitroi, filepath);
        end
    end
    
    methods(Test)
        
        %Test methods
        function testRealSolution(obj)
            %TESTREALSOLUTION checks if the recorded test data is matched
            %by the execution call of the CalcCostFun method. 
            %Output variables are checked based on a relative tolerance
            %(keep in mind that we use floating point operations.)
            
            saveData =  false;
            saveMatlabOutput = false;
            %read in the needed variables
            path=[obj.pathToTestData 'ProfitData/Test' num2str(obj.testNumber_) '/'];
            
            %read in input arguments
            load([path 'fid.mat']);
            load([path 'fs.mat']);
            load([path 'data.mat']);
            
            %read in expected output variables
%             load([path 'res.mat']);
            
            try
                spectralDimension = data.spectral_dimension;
            catch
                spectralDimension  = 2;
                data.spectral_dimension = spectralDimension;
            end
            
            %%
            if saveData
                % no need to save Fid, since it is already in readable format
                nr_iterations =fs.nr_iter;
                save([path 'basis.mat'], 'nr_iterations') 
                obj.saveBasis(fs.basis, [path 'basis.mat'])
                for fitIter = 1: nr_iterations
                    obj.saveFitObj(fs.seq{fitIter}, path, num2str(fitIter))
                end
                obj.saveData(data, path)
            end
            %%
            %for cycle used for timing measurements (set it to 1:obj.numberOfIterations_ for
            %example)
            for iteration = 1: obj.numberOfIterations_
                               
                [res_real] = profit(fid, fs, data);
                
                if saveMatlabOutput
                    %read in expected output variables
                    res = res_real;
                    save([path 'res.mat'], 'res');
                end
            end
            
            %verifying if the returned values of the CalcCostFun match the
            %expected values.
%             tolObj1 = matlab.unittest.constraints.AbsoluteTolerance(1e-6) | matlab.unittest.constraints.RelativeTolerance(1e-8);
%             obj.verifyThat(F_real, matlab.unittest.constraints.IsEqualTo(F, 'Within', tolObj1), 'Difference between actual F and expected F exceeds relative tolerance');
%             tolObj2 = matlab.unittest.constraints.AbsoluteTolerance(1e-9) | matlab.unittest.constraints.RelativeTolerance(1e-7);
%             obj.verifyThat(conc_real, matlab.unittest.constraints.IsEqualTo(conc, 'Within', tolObj2), 'Difference between actual conc and expected conc exceeds tolerance');
%             obj.verifyEqual(basis_matrix_real, basis_matrix, 'RelTol', 10^(-8), 'Difference between actual basis_matrix and expected basis_matrix exceeds relative tolerance');
%             obj.verifyEqual(spBB_real, spBB, 'Difference between actual spBB and expected spBB exceeds relative tolerance');
%             obj.verifyEqual(fitted_spec_real, fitted_spec, 'RelTol', 1e-6, 'Difference between actual fitted_spec and expected fitted_spec exceeds relative tolerance');
%             obj.verifyEqual(fixed_basis_real, fixed_basis, 'Difference between actual fixed_basis and expected fixed_basis exceeds relative tolerance');
%             obj.verifyEqual(g_damping, g_damping_after, 'RelTol', 10^(-8), 'Difference between actual g_damping_after and expected g_damping_after exceeds relative tolerance');
        end

    end
end

