classdef (Abstract) ProfitTestBase < matlab.unittest.TestCase
    %PROFITTESTBASE provides a basic test environment for other testing classes used for the profit software
    
    properties
        %Number specifying which test case should be used (i.e. which test
        %set of recorded data.
        testNumber_ = 2;
        %Path variable storing the path for TestMethodSetup and
        %TestMethodTearDown methods
        originalPath_
        %number of repetitions for a test. Used to eventually create Run
        %and Time measurements
        numberOfIterations_ = 1;
        %a list of the testcases on which the test shall be run
        vectorOfTestCases_ = 1:1;
        %path to the test case data folder. Reinitialized to full path in
        %constructor
        pathToTestData = '/../../../unitTestData/';
    end
        
    methods 
        %Constructor of the class
        %param numOfIteration specifies the number of repetitions, which
        %shall be used for testing
        %param vectorOfTestCases gives a list of the testcases on which the
        %test shall be run
        function obj = ProfitTestBase(numOfIteration, vectorOfTestCases) 
            obj.numberOfIterations_ = numOfIteration;
            obj.vectorOfTestCases_ = vectorOfTestCases;
            currentFilePath = mfilename('fullpath');
            [currentPath, ~, ~] = fileparts(currentFilePath);
            obj.pathToTestData = [currentPath obj.pathToTestData];
        end
    end
    
    methods (TestMethodSetup)
        %Methods called when the TestCase is set up.
        function addCalcCostFunToPath(obj)
            %ADDCALCCOSTFUNTOPATH changes the path to execute the testCase,
            %while saving the original path.
            %Called when the TestCase is set up.
            obj.originalPath_ = path;
            addpath(fullfile(pwd,'..'));
        end
    end
    
    methods (TestMethodTeardown)
        %Methods called when the TestCase is teared down.
        function restorePath(obj)
            %RESTOREPATH restores the path from before the testing using
            %the originalPath variable
            path(obj.originalPath_);
        end
    end
    
    methods(Test , Abstract)
        %Test methods
        testRealSolution(obj)
    end
    methods(Test)
        function testRealSolutionSuite(obj)
            %TESTREALSOLUTIONSUITE test suite for the CalcCostFun,
            %executing the TESTREALSOLUTION test functions on all recorded
            %test data sets. 
            %See more at: TESTREALSOLUTION .
            for currentTestNumber = obj.vectorOfTestCases_
                obj.testNumber_ = currentTestNumber;
                testRealSolution(obj);
            end
        end
    end
end

