function val = GetFittedValues(this, mets, type)

    mets = mets{1};
    type = type{1};

    % no error checking done yet
    active_met_names = this.met(this.active_mets);
    idx = find(strcmp(active_met_names, mets));
    val = [];
    for idx_cnt=1:length(idx)
        val = horzcat(val, this.fitted_values.(type)(this.sub_mask.(type).full(:,idx(idx_cnt))'));
    end
end