classdef Results < handle

    properties (SetAccess=public)   % originally planned to be Write-private but for now not that important
        
        fitted_values;
        basis_matrix;
        fixed_basis;
        fitted_spec;
        processed_spec;
        splines_baseline;
        
        splines_lambda
        splines_ed_vec;
        splines_optim_model_vec;
        splines_optim_idx;
        
        spline_lineshape;
        global_lineshape;

        % copied a few parameters that should be copied from iteration
        % object
        active_mets;    % logical value specifing the position of the activly used metabolites from the basis set
        
        fixed_mets;     % class of logical vectors specifiying which parameters are fixed to certain values for which metabolites in the basis set
        fixed_values;   % the actual fixed value that will be used for a specific parameter of a specific metabolite
        
        start_values;
        upper_bounds;
        lower_bounds;
        lin_priors;
        
        met_idx;        % containing a hash table with basis signal names as keys and double indices as values
        met_boolidx;    % containing a hash table with basis signal names as keys and logical indices as values
        idx;            % containing the relationship between certain metabolites for each fittable parameter
            
        full_mask;      % contains a full matrix of fittable parameters vs. metabolites and their relationship with respect to the full available basis
        sub_mask;       % contains a reduced matrix of fittable parameters vs. metabolites and their relationship with respect to only the activated metabolites
        nr;             % information of number of parameters, how many are free and how many are fixed
       
        full_fixed_values;   % the actual fixed value that will be used for a specific parameter of a specific metabolite
        
        full_start_values;
        full_upper_bounds;
        full_lower_bounds;
        full_lin_priors;
        mapping_mx;
        nr_mets;
        nr_act_mets;
        
        met;           % names of the included metabolites in the basis set
        
        common_data;   % this holds data like measured spectrum or sequence parameters. To keep it static over all iteration result it is put into its own class
        
        optim_exit;
        optim_output;
        optim_lambda;
        optim_x;
        
        crlb;           % calculated crlb in percentage
        
        subplottypes;
                
        noise_var;
        
        fit_quality_number;
        fit_quality_number2;
        
        preprocessedValues;
    end
    
    properties (Dependent)
       
        residual_fid;
        residual_spec;
        fitted_fid;
        fitroi_fid;
		Q;				% quality measure
    end

    methods 
        
        function this = Results(iteration, resultdata)
           
            this.fitted_values = Parameters;
            
            this.active_mets        = iteration.active_mets;
            this.fixed_mets         = iteration.fixed_mets;
            this.fixed_values       = iteration.fixed_values;
            this.start_values       = iteration.start_values;
            this.upper_bounds       = iteration.upper_bounds;
            this.lower_bounds       = iteration.lower_bounds;
            this.lin_priors         = iteration.lin_priors;
            this.idx                = iteration.idx;
            this.full_mask          = iteration.full_mask;
            this.sub_mask           = iteration.sub_mask;
            this.nr                 = iteration.nr;
            this.full_start_values  = iteration.full_start_values;
            this.full_fixed_values  = iteration.full_fixed_values;
            this.full_upper_bounds  = iteration.full_upper_bounds;
            this.full_lower_bounds  = iteration.full_lower_bounds;
            this.full_lin_priors    = iteration.full_lin_priors;
            this.mapping_mx         = iteration.mapping_mx;
            this.nr_mets            = iteration.nr_mets;
            this.met                = iteration.basis.met;
            this.nr_act_mets        = iteration.nr_act_mets;
            
            this.met_idx            = iteration.met_idx; 
            this.met_boolidx        = iteration.met_boolidx;
            
            this.splines_lambda     = iteration.spbase_lambda;
            this.splines_ed_vec     = iteration.spbase_ed_vec;
            this.splines_optim_model_vec = iteration.spbase_optim_model_vec;
            this.splines_optim_idx  = iteration.spbase_optim_idx;
            
            this.common_data        = resultdata;
            
            this.subplottypes = containers.Map({'spec', 'fit_spec', 'res_spec', 'fid', 'fit_fid', 'res_fid'}, ...
                                              {'processed_spec', 'fitted_spec', 'residual_spec', 'fitroi_fid', 'fitted_fid', 'residual_fid'}); 
        end
        
        function value = get.residual_spec(this)
            
            value = this.processed_spec - this.fitted_spec;
        end
        
        function value = get.residual_fid(this)
            
            value = this.fitroi_fid - this.fitted_fid;
        end
                
        function value = get.fitted_fid(this)
            
            value = ifft(this.fitted_spec);
        end

        function value = get.fitroi_fid(this)
            
            value = ifft(this.processed_spec);
        end

		function value = get.Q(this)

			value = std(this.residual_spec(:))/sqrt(this.common_data.data.noise_var);
		end

        val  = GetFittedValues(obj, mets, type);
        hf   = Plot(obj, subplots, plottype, hires);
        PrintTable(this);
    end
    
    methods (Static)
        
    end

end
