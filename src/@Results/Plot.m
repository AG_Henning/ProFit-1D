function hf = Plot(this, subplots, plottype, hires)

    switch plottype
        case 'c'
            plot_mode = @contour;
        case 'i'
            plot_mode = @imagesc;
        otherwise
            error('Results:Plot:UnknownPlotType', 'Unknown plot type');
    end
    
    hires_factor = [4 4];
    
    if (hires)
        t1 = interp1(1:this.common_data.data.fitroi.pts(1), this.common_data.data.fitroi.t1 * 1e3, ...
                     linspace(1, this.common_data.data.fitroi.pts(1), hires_factor(1) * this.common_data.data.fitroi.pts(1)));
        t2 = interp1(1:this.common_data.data.fitroi.pts(2), this.common_data.data.fitroi.t2 * 1e3, ...
                     linspace(1, this.common_data.data.fitroi.pts(2), hires_factor(2) * this.common_data.data.fitroi.pts(2)));
        f1 = interp1(1:this.common_data.data.fitroi.pts(1), this.common_data.data.fitroi.ppm1, ...
                     linspace(1, this.common_data.data.fitroi.pts(1), hires_factor(1) * this.common_data.data.fitroi.pts(1)));
        f2 = interp1(1:this.common_data.data.fitroi.pts(2), this.common_data.data.fitroi.ppm2, ...
                     linspace(1, this.common_data.data.fitroi.pts(2), hires_factor(2) * this.common_data.data.fitroi.pts(2)));
        [ogrid_x, ogrid_y] = meshgrid(1:this.common_data.data.fitroi.pts(2), 1:this.common_data.data.fitroi.pts(1));
        [ngrid_x, ngrid_y] = meshgrid(linspace(1, this.common_data.data.fitroi.pts(2), hires_factor(2) * this.common_data.data.fitroi.pts(2)), ...
                                     linspace(1, this.common_data.data.fitroi.pts(1), hires_factor(1) * this.common_data.data.fitroi.pts(1)));
    else
        t1 = this.common_data.data.fitroi.t1 * 1e3;
        t2 = this.common_data.data.fitroi.t2 * 1e3;
        f1 = this.common_data.data.fitroi.ppm1;
        f2 = this.common_data.data.fitroi.ppm2;
    end
    
    xdir = 'normal';
    axislabel  = [];
    if all(ismember(subplots, this.subplottypes.keys))
        hf = figure;
        for pp = 1:length(subplots)
            subplot(length(subplots), 1, pp);
            if ~isempty(regexpi(subplots{pp}, 'fid'))
                xaxis = t2;
                yaxis = t1;
                axislabel  = '[ms]';
            elseif ~isempty(regexpi(subplots{pp}, 'spec'))
                xdir  = 'reverse';
                xaxis = f2;
                yaxis = f1;
                axislabel  = '[ppm]';
            end
            plot_content = real(eval(['this.' this.subplottypes(subplots{pp})]));
            if (hires)
               plot_content = interp2(ogrid_x, ogrid_y, plot_content, ngrid_x, ngrid_y); 
            end
            feval(plot_mode, xaxis, yaxis, plot_content);
            set(gca, 'XDir', xdir);
            xlabel(axislabel);
            ylabel(axislabel);
        end
    else
        error('Results:Plot:UnknownSubPlotType', 'Don''t know what to plot');
    end
end