close all;
clear all;
clc;

set_path = {'.'};
fititer_idx = 4;
fititer_mm_idx = 3;
conc_scaling = 1e4;		% scaling factor for display purposes
glu_idx = 9;
gln_idx = 8;
gaba_idx = 7;
glc_idx  = 4;

for folder_cnt = 1:length(set_path)
    current_path = set_path{folder_cnt};

	results_files = dirr(current_path, '_profit', 'name');
	results_files = arrayfun(@(xx) xx.name, results_files, 'UniformOutput', false);

	% save full conc data matrix in csv tables
	[fID, msg] = fopen(sprintf('%s/conc_data.csv', current_path), 'w');
	if ~isempty(msg)
		error('Couldn''t open conc data file for writing');
	end
	% save full ratios data matrix in csv tables
	[fID2, msg] = fopen(sprintf('%s/ratios_data.csv', current_path), 'w');
	if ~isempty(msg)
		error('Couldn''t open ratios data file for writing');
	end

	for file_cnt=1:size(results_files,1)
		load(sprintf('./%s/%s', current_path, results_files{file_cnt}));
		if (file_cnt==1)
			nr_conc = length(fitresult{4}.fitted_values.conc);
			% fit.conc = zeros(size(results_files,1), nr_conc + 1);
            fit.conc = zeros(size(results_files,1), nr_conc);
			fit.ratios = zeros(size(fit.conc));
			% construct active metabolite name list
			active_mets = fitresult{fititer_idx}.met(fitresult{fititer_idx}.active_mets);
			[~, met_unique_idx] = unique(active_mets);
			met_names = active_mets(sort(met_unique_idx));
			met_names = met_names(2:end);
			met_names{1} = 'cre';
			cellfun(@(x) fprintf(fID, '\t%s', x), met_names);
			fprintf(fID, '\n');
			cellfun(@(x) fprintf(fID2, '\t%s', x), met_names);
			fprintf(fID2, '\n');
		end
		% get fitted values and rescale them to the originally simulated
		% concentrations
% 		fit.conc(file_cnt,:) = [fitresult{fititer_idx}.fitted_values.conc fitresult{fititer_mm_idx}.fitted_values.conc(end)] ./ ...
% 								fitresult{fititer_idx}.common_data.data.fid_scaling;
        fit.conc(file_cnt,:) = [fitresult{fititer_idx}.fitted_values.conc] ./ ...
                                fitresult{fititer_idx}.common_data.data.fid_scaling;
		fit.ratios(file_cnt,:) = fit.conc(file_cnt,:)./fit.conc(file_cnt,1);
		fprintf(fID, ['%s' repmat('\t%f', [1 size(fit.conc,2)]) '\n'], results_files{file_cnt}, fit.conc(file_cnt,:) .* conc_scaling);
		fprintf(fID2, ['%s' repmat('\t%f', [1 size(fit.ratios,2)]) '\n'], results_files{file_cnt}, fit.ratios(file_cnt,:));
	end
	fclose(fID);
	fclose(fID2);
	
	% scale concentration just for display purposes 1e4
	fit.conc = fit.conc .* conc_scaling;

	% create correlation plots
	figure('Name', 'GABA/Glu');
	corr_gaba_glu =  corr(fit.conc(:,gaba_idx), fit.conc(:,glu_idx));
	scatter(fit.conc(:,gaba_idx), fit.conc(:,glu_idx));
	h = lsline;
	set(h(1), 'color', 'r');
	xlabel('GABA');
	ylabel('Glu');
	title(sprintf('correlation: %4.2f', corr_gaba_glu));

	figure('Name', 'Glu/Gln');
	corr_glu_gln =  corr(fit.conc(:,glu_idx), fit.conc(:,gln_idx));
	scatter(fit.conc(:,glu_idx), fit.conc(:,gln_idx));
	h = lsline;
	set(h(1), 'color', 'r');
	xlabel('Glu');
	ylabel('Gln');
	title(sprintf('correlation: %4.2f', corr_glu_gln));

	figure('Name', 'Glu/Glc');
	corr_glu_glc =  corr(fit.conc(:,glu_idx), fit.conc(:,glc_idx));
	scatter(fit.conc(:,glu_idx), fit.conc(:,glc_idx));
	h = lsline;
	set(h(1), 'color', 'r');
	xlabel('Glu');
	ylabel('Glc');
	title(sprintf('correlation: %4.2f', corr_glu_glc));

	figure('Name', 'Gln/Glc');
	corr_gln_glc =  corr(fit.conc(:,gln_idx), fit.conc(:,glc_idx));
	scatter(fit.conc(:,gln_idx), fit.conc(:,glc_idx));
	h = lsline;
	set(h(1), 'color', 'r');
	xlabel('Gln');
	ylabel('Glc');
	title(sprintf('correlation: %4.2f', corr_gln_glc));

    % create /Cr correlation plots
	figure('Name', 'GABA2Cr/Glu2Cr');
	corr_gaba_glu =  corr(fit.ratios(:,gaba_idx), fit.ratios(:,glu_idx));
	scatter(fit.ratios(:,gaba_idx), fit.ratios(:,glu_idx));
	h = lsline;
	set(h(1), 'color', 'r');
	xlabel('GABA/Cr');
	ylabel('Glu/cr');
	title(sprintf('correlation: %4.2f', corr_gaba_glu));

	figure('Name', 'Glu2Cr/Gln2Cr');
	corr_glu_gln =  corr(fit.ratios(:,glu_idx), fit.ratios(:,gln_idx));
	scatter(fit.ratios(:,glu_idx), fit.ratios(:,gln_idx));
	h = lsline;
	set(h(1), 'color', 'r');
	xlabel('Glu/Cr');
	ylabel('Gln/Cr');
	title(sprintf('correlation: %4.2f', corr_glu_gln));

	figure('Name', 'Glu2Cr/Glc2Cr');
	corr_glu_glc =  corr(fit.ratios(:,glu_idx), fit.ratios(:,glc_idx));
	scatter(fit.ratios(:,glu_idx), fit.ratios(:,glc_idx));
	h = lsline;
	set(h(1), 'color', 'r');
	xlabel('Glu/Cr');
	ylabel('Glc/Cr');
	title(sprintf('correlation: %4.2f', corr_glu_glc));

	figure('Name', 'Gln2Cr/Glc2Cr');
	corr_gln_glc =  corr(fit.ratios(:,gln_idx), fit.ratios(:,glc_idx));
	scatter(fit.ratios(:,gln_idx), fit.ratios(:,glc_idx));
	h = lsline;
	set(h(1), 'color', 'r');
	xlabel('Gln/Cr');
	ylabel('Glc/Cr');
	title(sprintf('correlation: %4.2f', corr_gln_glc));
    
	% save correlations files and write out table
	corr_mx = corrcoef(fit.conc);
	[fIDcorr, msg] = fopen(sprintf('%s/correlations.csv', current_path), 'w');
	if ~isempty(msg)
		error('Couldn''t open ratios data file for writing');
	end
	table_out{5} = sprintf('\n%100s\n%14s', 'metabolite correlations','');
	tmp = char(cellfun(@(x) sprintf('%14s', x), met_names, 'UniformOutput', false))';
	table_out{5} = horzcat(table_out{5}, reshape(tmp, [1 numel(tmp)]));
	table_out{5} = horzcat(table_out{5}, sprintf('\n'));
	cellfun(@(x) fprintf(fIDcorr, '\t%s', x), met_names);
	fprintf(fIDcorr, '\n');
	for c_cnt=1:size(fit.conc,2)
		fprintf(fIDcorr, ['%s' repmat('\t%f', [1 size(fit.conc,2)]) '\n'], met_names{c_cnt}, corr_mx(c_cnt,:));
		table_out{5} = horzcat(table_out{5}, sprintf(['%14s' repmat('%14f', [1 size(fit.conc,2)]) '\n'], met_names{c_cnt}, corr_mx(c_cnt,:)));
	end
	fclose(fIDcorr);
    
	% save /Cr correlations files and write out table
	corr_mx = corrcoef(fit.ratios);
	[fIDcorr, msg] = fopen(sprintf('%s/correlations_2Cr.csv', current_path), 'w');
	if ~isempty(msg)
		error('Couldn''t open ratios data file for writing');
	end
	table_out{6} = sprintf('\n%100s\n%14s', 'metabolite /Cr correlations','');
	tmp = char(cellfun(@(x) sprintf('%14s', x), met_names, 'UniformOutput', false))';
	table_out{6} = horzcat(table_out{6}, reshape(tmp, [1 numel(tmp)]));
	table_out{6} = horzcat(table_out{6}, sprintf('\n'));
	cellfun(@(x) fprintf(fIDcorr, '\t%s', x), met_names);
	fprintf(fIDcorr, '\n');
	for c_cnt=1:size(fit.conc,2)
		fprintf(fIDcorr, ['%s' repmat('\t%f', [1 size(fit.ratios,2)]) '\n'], met_names{c_cnt}, corr_mx(c_cnt,:));
		table_out{6} = horzcat(table_out{6}, sprintf(['%14s' repmat('%14f', [1 size(fit.ratios,2)]) '\n'], met_names{c_cnt}, corr_mx(c_cnt,:)));
	end
	fclose(fIDcorr);
    
	met_mean = zeros(1, size(fit.conc,2));
	met_var = zeros(size(met_mean));
	met_std = zeros(size(met_mean));
	met_cv = zeros(size(met_mean));
	met_median = zeros(size(met_mean));
	met_range = zeros(size(met_mean));
	met_min = zeros(size(met_mean));
	met_max = zeros(size(met_mean));
	met_iqr = zeros(size(met_mean));
	met_mad = zeros(size(met_mean));
	met_skewness = zeros(size(met_mean));
	met_kurtosis = zeros(size(met_mean));

	met_ratio_mean = zeros(size(met_mean));
	met_ratio_var = zeros(size(met_mean));
	met_ratio_std = zeros(size(met_mean));
	met_ratio_cv = zeros(size(met_mean));
	met_ratio_median = zeros(size(met_mean));
	met_ratio_range = zeros(size(met_mean));
	met_ratio_min = zeros(size(met_mean));
	met_ratio_max = zeros(size(met_mean));
	met_ratio_iqr = zeros(size(met_mean));
	met_ratio_mad = zeros(size(met_mean));
	met_ratio_skewness = zeros(size(met_mean));
	met_ratio_kurtosis = zeros(size(met_mean));

	group_mean_ratios = zeros(size(met_mean));

	% save all performance results in csv files
	[fID, msg] = fopen(sprintf('%s/fit_statistics_absolute.csv', current_path), 'w');
	if ~isempty(msg)
		error('Couldn''t open performance result file for writing');
	end
	[fID2, msg] = fopen(sprintf('%s/fit_statistics_ratios.csv', current_path), 'w');
	if ~isempty(msg)
		error('Couldn''t open performance result file for writing');
	end
	[fID3, msg] = fopen(sprintf('%s/fit_statistics_special_ratios.csv', current_path), 'w');
	if ~isempty(msg)
		error('Couldn''t open performance result file for writing');
	end
	
	for f_cnt = [fID fID2 fID3]
		fprintf(f_cnt, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n', 'Met', 'mean', 'std', 'cv [%%]', 'var', 'median', 'range', 'min', 'max', ...
						'iqr', 'mad', 'skewness', 'kurtosis');
	end

	table_out{1} = sprintf('\n%64s\n', 'absolute conc');
	table_out{2} = sprintf('\n%64s\n', 'ratios /Cr');
	table_out{3} = sprintf('\n%26s\n', 'group mean table');
	table_out{3} = horzcat(table_out{3}, sprintf('%s%14s%14s\n', 'Met', 'conc', '/Cr'));
	table_out{3} = horzcat(table_out{3}, sprintf('%s\n', repmat('-', [1,32])));
	table_out{4} = sprintf('\n%64s\n', 'special ratios');
	for t_cnt = [1 2 4]
		table_out{t_cnt} = horzcat(table_out{t_cnt}, sprintf('%s%14s%14s%14s%14s%14s%14s%14s%14s%14s%14s%14s%14s\n', 'Met', 'mean', 'std', ...
															 'cv [%%]', 'var', 'median', 'range', 'min', 'max', 'iqr', 'mad', 'skewness', 'kurtosis'));
		table_out{t_cnt} = horzcat(table_out{t_cnt}, sprintf('%s\n', repmat('-', [1,171])));
	end
	
	mean_cr = mean(fit.conc(:,1));
	for c_cnt=1:size(fit.conc,2)
		% calculate descriptive statistics on metabolite concentrations and ratios
		met_mean(c_cnt) = mean(fit.conc(:,c_cnt));
		met_std(c_cnt)  = std(fit.conc(:,c_cnt));
		met_var(c_cnt)  = var(fit.conc(:,c_cnt));
		met_cv(c_cnt)   = std(fit.conc(:,c_cnt)) ./ mean(fit.conc(:,c_cnt)) * 100;
		met_median(c_cnt)  = median(fit.conc(:,c_cnt));
		met_range(c_cnt)  = range(fit.conc(:,c_cnt));
		met_min(c_cnt)  = min(fit.conc(:,c_cnt));
		met_max(c_cnt)  = max(fit.conc(:,c_cnt));
		met_iqr(c_cnt) = iqr(fit.conc(:,c_cnt));
		met_mad(c_cnt) = mad(fit.conc(:,c_cnt));
		met_skewness(c_cnt) = skewness(fit.conc(:,c_cnt));
		met_kurtosis(c_cnt) = kurtosis(fit.conc(:,c_cnt));

		met_ratio_mean(c_cnt) = mean(fit.ratios(:,c_cnt));
		met_ratio_std(c_cnt)  = std(fit.ratios(:,c_cnt));
		met_ratio_var(c_cnt)  = var(fit.ratios(:,c_cnt));
		met_ratio_cv(c_cnt)   = std(fit.ratios(:,c_cnt)) ./ mean(fit.ratios(:,c_cnt)) * 100; 
		met_ratio_median(c_cnt)  = median(fit.ratios(:,c_cnt));
		met_ratio_range(c_cnt)  = range(fit.ratios(:,c_cnt));
		met_ratio_min(c_cnt)  = min(fit.ratios(:,c_cnt));
		met_ratio_max(c_cnt)  = max(fit.ratios(:,c_cnt));
		met_ratio_iqr(c_cnt) = iqr(fit.ratios(:,c_cnt));
		met_ratio_mad(c_cnt) = mad(fit.ratios(:,c_cnt));
		met_ratio_skewness(c_cnt) = skewness(fit.ratios(:,c_cnt));
		met_ratio_kurtosis(c_cnt) = kurtosis(fit.ratios(:,c_cnt));

		group_mean_ratios(c_cnt) = met_mean(c_cnt) / mean_cr;

		table_out{3} = horzcat(table_out{3}, sprintf('%s%s%9.4f%14.4f\n', met_names{c_cnt}, repmat(' ', [1 8-length(met_names{c_cnt})]), ...
													  met_mean(c_cnt), group_mean_ratios(c_cnt)));

		table_out{1} = horzcat(table_out{1}, sprintf('%s%s%9.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f\n', ...
					   met_names{c_cnt}, repmat(' ', [1 8-length(met_names{c_cnt})]), ...
					   met_mean(c_cnt), met_std(c_cnt), met_cv(c_cnt), met_var(c_cnt),...
					   met_median(c_cnt), met_range(c_cnt), met_min(c_cnt), met_max(c_cnt), ...	
					   met_iqr(c_cnt), met_mad(c_cnt), met_skewness(c_cnt), met_kurtosis(c_cnt)));

		table_out{2} = horzcat(table_out{2}, sprintf('%s%s%9.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f\n', ...
					   met_names{c_cnt}, repmat(' ', [1 8-length(met_names{c_cnt})]), ...
					   met_ratio_mean(c_cnt), met_ratio_std(c_cnt), met_ratio_cv(c_cnt), met_ratio_var(c_cnt),...
					   met_ratio_median(c_cnt), met_ratio_range(c_cnt), met_ratio_min(c_cnt), met_ratio_max(c_cnt), ...	
					   met_ratio_iqr(c_cnt), met_ratio_mad(c_cnt), met_ratio_skewness(c_cnt), met_ratio_kurtosis(c_cnt)));

		fprintf(fID, '%s\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n', ...
					  met_names{c_cnt}, ...
					  met_mean(c_cnt), met_std(c_cnt), met_cv(c_cnt), met_var(c_cnt),...
					  met_median(c_cnt), met_range(c_cnt), met_min(c_cnt), met_max(c_cnt), ...	
					  met_iqr(c_cnt), met_mad(c_cnt), met_skewness(c_cnt), met_kurtosis(c_cnt));

		fprintf(fID2, '%s\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n', ...
					   met_names{c_cnt}, ...
					   met_ratio_mean(c_cnt), met_ratio_std(c_cnt), met_ratio_cv(c_cnt), met_ratio_var(c_cnt),...
					   met_ratio_median(c_cnt), met_ratio_range(c_cnt), met_ratio_min(c_cnt), met_ratio_max(c_cnt), ...	
					   met_ratio_iqr(c_cnt), met_ratio_mad(c_cnt), met_ratio_skewness(c_cnt), met_ratio_kurtosis(c_cnt));

	end
	fprintf(fID2, '\ngroup ratios\n');
	fprintf(fID2, 'Met\tconc\tratios /Cr\n');
	for c_cnt=1:length(group_mean_ratios)
		fprintf(fID2, '%s\t%f\t%f\n', met_names{c_cnt}, met_mean(c_cnt), group_mean_ratios(c_cnt));
	end
	
	gln_glu_ratio  = fit.conc(:, gln_idx)./fit.conc(:,glu_idx);
	gaba_glu_ratio = fit.conc(:, gaba_idx)./fit.conc(:,glu_idx);
	glc_glu_ratio  = fit.conc(:, glc_idx)./fit.conc(:,glu_idx);
	glc_gln_ratio  = fit.conc(:, glc_idx)./fit.conc(:,gln_idx);

	fprintf(fID3, 'Gln/Glu\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n', ...
				   mean(gln_glu_ratio), std(gln_glu_ratio), std(gln_glu_ratio)./mean(gln_glu_ratio)*100, var(gln_glu_ratio),...
				   median(gln_glu_ratio), range(gln_glu_ratio), min(gln_glu_ratio), max(gln_glu_ratio), ...	
				   iqr(gln_glu_ratio), mad(gln_glu_ratio), skewness(gln_glu_ratio), kurtosis(gln_glu_ratio));
	fprintf(fID3, 'GABA/Glu\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n', ...
				   mean(gaba_glu_ratio), std(gaba_glu_ratio), std(gaba_glu_ratio)./mean(gaba_glu_ratio)*100, var(gaba_glu_ratio),...
				   median(gaba_glu_ratio), range(gaba_glu_ratio), min(gaba_glu_ratio), max(gaba_glu_ratio), ...	
				   iqr(gaba_glu_ratio), mad(gaba_glu_ratio), skewness(gaba_glu_ratio), kurtosis(gaba_glu_ratio));
	fprintf(fID3, 'Glc/Glu\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n', ...
				   mean(glc_glu_ratio), std(glc_glu_ratio), std(glc_glu_ratio)./mean(glc_glu_ratio)*100, var(glc_glu_ratio),...
				   median(glc_glu_ratio), range(glc_glu_ratio), min(glc_glu_ratio), max(glc_glu_ratio), ...	
				   iqr(glc_glu_ratio), mad(glc_glu_ratio), skewness(glc_glu_ratio), kurtosis(glc_glu_ratio));
	fprintf(fID3, 'Glc/Gln\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n', ...
				   mean(glc_gln_ratio), std(glc_gln_ratio), std(glc_gln_ratio)./mean(glc_gln_ratio)*100, var(glc_gln_ratio),...
				   median(glc_gln_ratio), range(glc_gln_ratio), min(glc_gln_ratio), max(glc_gln_ratio), ...	
				   iqr(glc_gln_ratio), mad(glc_gln_ratio), skewness(glc_gln_ratio), kurtosis(glc_gln_ratio));

	fclose(fID);
	fclose(fID2);
	fclose(fID3);
	table_out{4} = horzcat(table_out{4}, sprintf('Gln/Glu%10.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f\n', ...
				   mean(gln_glu_ratio), std(gln_glu_ratio), std(gln_glu_ratio)./mean(gln_glu_ratio)*100, var(gln_glu_ratio),...
				   median(gln_glu_ratio), range(gln_glu_ratio), min(gln_glu_ratio), max(gln_glu_ratio), ...	
				   iqr(gln_glu_ratio), mad(gln_glu_ratio), skewness(gln_glu_ratio), kurtosis(gln_glu_ratio)));
	table_out{4} = horzcat(table_out{4}, sprintf('GABA/Glu%9.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f\n', ...
				   mean(gaba_glu_ratio), std(gaba_glu_ratio), std(gaba_glu_ratio)./mean(gaba_glu_ratio)*100, var(gaba_glu_ratio),...
				   median(gaba_glu_ratio), range(gaba_glu_ratio), min(gaba_glu_ratio), max(gaba_glu_ratio), ...	
				   iqr(gaba_glu_ratio), mad(gaba_glu_ratio), skewness(gaba_glu_ratio), kurtosis(gaba_glu_ratio)));
	table_out{4} = horzcat(table_out{4}, sprintf('Glc/Glu%10.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f\n', ...
				   mean(glc_glu_ratio), std(glc_glu_ratio), std(glc_glu_ratio)./mean(glc_glu_ratio)*100, var(glc_glu_ratio),...
				   median(glc_glu_ratio), range(glc_glu_ratio), min(glc_glu_ratio), max(glc_glu_ratio), ...	
				   iqr(glc_glu_ratio), mad(glc_glu_ratio), skewness(glc_glu_ratio), kurtosis(glc_glu_ratio)));
	table_out{4} = horzcat(table_out{4}, sprintf('Glc/Gln%10.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f%14.4f\n', ...
				   mean(glc_gln_ratio), std(glc_gln_ratio), std(glc_gln_ratio)./mean(glc_gln_ratio)*100, var(glc_gln_ratio),...
				   median(glc_gln_ratio), range(glc_gln_ratio), min(glc_gln_ratio), max(glc_gln_ratio), ...	
				   iqr(glc_gln_ratio), mad(glc_gln_ratio), skewness(glc_gln_ratio), kurtosis(glc_gln_ratio)));

	cellfun(@(tt) fprintf(tt), table_out);
end
fprintf('\n');

