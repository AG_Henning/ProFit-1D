classdef ParameterCollection < Parameters    
% contains a further seperation of fit parameters according to their function within 
% a certain fit iteration
    
    methods
        
        function this = ParameterCollection()
            
            this.conc   = Collection;
            this.em     = Collection;
            this.gm     = Collection;
            this.pc0    = Collection;
            this.pc12   = Collection;
            this.df2    = Collection;
        end
        
        function this = Fill(this, type, subtype, content)
            
            if ~iscell(type)
                error('ParCollection:Fill:WrongType', 'The input of the Fill type needs to be a cell string');
            end
            
            if ~iscell(subtype)
                error('ParCollection:Fill:WrongSubType', 'The input of the Fill sub type needs to be a cell string');
            end
            
            if ((length(type)==1) && strcmpi(type, 'all'));

                mobj = ?Parameters;
                type = cellfun(@(prop) prop.Name, mobj.Properties, 'UniformOutput', false);
            end

            if ((length(subtype)==1) && strcmpi(subtype, 'all'));

                mobj = ?Collection;
                subtype = cellfun(@(prop) prop.Name, mobj.Properties, 'UniformOutput', false);
            end
            
            for type_cnt=1:length(type)
                
                for subtype_cnt=1:length(subtype)
                    
                    try
                        this.(type{type_cnt}).(subtype{subtype_cnt}) = content;
                    catch exception
                        error('ParCollection:Fill:NonExistingField', 'Trying to access non a existing field');
                    end
                end
            end
        end
    end    
end
