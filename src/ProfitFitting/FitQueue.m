classdef FitQueue < handle
    
    properties
        
        basis;      % holds basis set object
        seq;        % cell array of individual fit iteration objects
    end
        
    properties (Dependent)
       
        nr_iter;    % number of contained iterations
    end
   
    methods
        
        function this = FitQueue(basisobj)
            % initialize the object by linking a basis set to the Fit queue
            % object and initializing and empty cell array to hold ready
            % configured iteration objects.
            
            this.basis   = basisobj;
            this.seq     = {};
        end
        
        function this = AddIteration(this, iterobj)
            % add an iteration object to the queue for later processing.
            % Make sure the basis set associated with the iteration object
            % is the same as specified to construct the FitQueue object and
            % therefor ensure the basis set to be consistent for all fit
            % iteration collected here
            
            try
                if (iterobj.basis ~= this.basis)
                    error('FitQueue:DiffBasis', 'The basis set object has to be the same for all iteration within one FitQueue');
                end
            catch exception
                error('FitQueue:DiffBasis', 'The basis set object has to be the same for all iteration within one FitQueue');
            end
            
            this.seq = [this.seq {iterobj}];
        end
        
        function value = get.nr_iter(this)
            % keep the number of availbale iteration accessable
            
            value = length(this.seq);
        end
    end
end