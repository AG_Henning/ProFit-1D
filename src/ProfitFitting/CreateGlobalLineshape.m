function lsm = CreateGlobalLineshape(fidOrig, delta_fidOrig, spls, data)
    debug = false;
    % prepare fid only within fitroi for SNR reasons
    spec = fft(fft(fidOrig, [], 2) .* data.tilt_mx, [], 1);
    delta_spec = fft(fft(delta_fidOrig, [], 2) .* data.tilt_mx, [], 1);  

    measuredPoints = round(data.fitroi.pts./data.zero_fill_factor); % measured fid size prior to zerofilling
    spec = spec(data.fitroi.bounds(1,1):data.fitroi.bounds(2,1), ...
        data.fitroi.bounds(1,2):data.fitroi.bounds(2,2));
    delta_spec = delta_spec(data.fitroi.bounds(1,1):data.fitroi.bounds(2,1), ...
        data.fitroi.bounds(1,2):data.fitroi.bounds(2,2));
    spec(1:1, 1:273) = 0;
    delta_spec(1:1, 1:273) = 0;
    fid       = ifft(ifft(spec, [], 1) .* data.fitroi.untilt_mx, [], 2);
    delta_fid = ifft(ifft(delta_spec, [], 1) .* data.fitroi.untilt_mx, [], 2);
    
    switch data.spectral_dimension
        case 1
            deconv_ls = fid(1:1,1:measuredPoints(2)) ./ (delta_fid(1:1,1:measuredPoints(2)) + 0.01);
        case 2
            deconv_ls = fid(1:measuredPoints(1),1:measuredPoints(2)) ./ (delta_fid(1:measuredPoints(1),1:measuredPoints(2)) + 0.01);
    end
    deconv_ls(:,150:end) = 0;   % should also be controlled by the iteration object or calculated from data
    
    % prevent extreme spikes after deconvolution -> envelope shouldn't
    % exceed values much over 1 so just cut everything off that's higher or
    % lower
    if debug
        figure; imagesc(real(deconv_ls))
    end
    deconv_ls(deconv_ls >  1.5)  =  1.5;
    deconv_ls(deconv_ls < -1.5) = -1.5;
    if debug
        figure; imagesc(real(deconv_ls))
    end
    lambda = 2.5e0;%fitobj.spline_ls_lambda(1);

    madlinOptions = optimset('Display', 'off');
    
    %% process real part envelope
    % more sophistication in correcting artifacts in the lineshape model
    % prior to the surface fit. mainly baseline problems in frequency
    % domain

    % correct for baseline problems in frequency domain
    ftnr_knotsy = 8;
    ky = 4;
    switch data.spectral_dimension
        case 1
           gridDimension = size(fid(1:1,1:measuredPoints(2)),2);
        case 2
           gridDimension = size(fid(1:measuredPoints(1),1:measuredPoints(2)),2);
    end
    ftknotsy = augknt(linspace(1, gridDimension, ftnr_knotsy), ky);     % create knot sequence
    ftBy = spcol(ftknotsy, ky, linspace(1,gridDimension, gridDimension));
    ftspec = fftshift(fft(deconv_ls, [], 2),2).';
    corr_spec = zeros(size(deconv_ls));

    switch data.spectral_dimension
        case 1
            spec1d = ftspec(:,1);
            ftfit = madlin(ftBy, real(spec1d(:)), [], [], [], [], [], [], [], madlinOptions);
            ftbaseline = ftBy*ftfit;
            corr_spec(1,:) = (spec1d - ftbaseline).';
            figure; plot(real(spec1d(:))); hold on; plot(ftbaseline); plot(real(corr_spec))
        case 2
            for zz=1:measuredPoints(1)
                spec1d = ftspec(:,zz);
                % ftfit = ftBy \ real(spec1d(:));
                ftfit = madlin(ftBy, real(spec1d(:)), [], [], [], [], [], [], [], madlinOptions);  
                ftbaseline = ftBy*ftfit;
                corr_spec(zz,:) = (spec1d - ftbaseline).';
                %%
                if debug
                    xxx = filloutliers(real(spec1d(:)),'linear', 'movmedian', 50);
                    test = lsqlin(ftBy, xxx);
                    %                 figure; plot(ftfit-test); hold on; plot(ftfit);
                    testbaseline = ftBy * test;
                    figure; plot(real(spec1d(:))); hold on; plot(ftbaseline); plot(testbaseline)
                end
            end
    end
    
    % potential TODO: correct high frequency noise in FD
    
    deconv_ls = ifft(ifftshift(corr_spec,2), [], 2);
    deconv_ls(:, 150:end) = 0;
    
    ls_fit_r = madlin2([spls.BB; lambda*spls.P], [real(deconv_ls(:)); zeros(size(spls.P,1),1)],[],[],[],[],[],[],[],madlinOptions);
    if debug
        test_r = lsqlin([spls.BB; lambda*spls.P], [real(deconv_ls(:)); zeros(size(spls.P,1),1)]);
        test_diff = ls_fit_r - test_r;
        figure; plot(test_diff); hold on; plot(test_r)
    end
    %% process imaginary part envelope

    % correct for baseline problems in frequency domain
    switch data.spectral_dimension
        case 1
            spec1d = ftspec(:,1);
            % ftfit = ftBy \ real(spec1d(:));
            ftfit = madlin(ftBy, imag(spec1d(:)),[],[],[],[],[],[],[],madlinOptions);
            
            ftbaseline = ftBy*ftfit;
            corr_spec(1,:) = (spec1d - 1i*ftbaseline).';
            figure; plot(imag(spec1d(:))); hold on; plot(ftbaseline); plot(imag(corr_spec))
        case 2
            for zz=1:measuredPoints(1)
                spec1d = ftspec(:,zz);
                % ftfit = ftBy \ real(spec1d(:));
                ftfit = madlin(ftBy, imag(spec1d(:)),[],[],[],[],[],[],[],madlinOptions);
                
                ftbaseline = ftBy*ftfit;
                corr_spec(zz,:) = (spec1d - 1i*ftbaseline).';                
                %%
                if debug
                    test = lsqlin(ftBy, imag(spec1d(:)));
                    %                 figure; plot(ftfit-test); hold on; plot(ftfit);
                    testbaseline = ftBy * test;
                    figure; plot(imag(spec1d(:))); hold on; plot(ftbaseline); plot(testbaseline)
                end
            end
    end
    % correct high frequency noise in FD
    
    deconv_ls = ifft(ifftshift(corr_spec,2), [], 2);
    deconv_ls(:, 150:end) = 0;
     
    ls_fit_i = madlin2([spls.BB; lambda*spls.P], [imag(deconv_ls(:)); zeros(size(spls.P,1),1)],[],[],[],[],[],[],[],madlinOptions);

%     ls_fit_i = zeros(size(ls_fit_r));   % skip imaginary part for now
    if debug
        test_i = lsqlin([spls.BB; lambda*spls.P], [imag(deconv_ls(:)); zeros(size(spls.P,1),1)]);
        test_diff = ls_fit_i - test_i;
        figure; plot(test_diff); hold on; plot(test_i)
    end
    %% return complex lineshape model interpolated again to the orignal full matix size
    %TODO change to make adaptive for 1D and 2D
    switch data.spectral_dimension
        case 1
            gridDimension = round(data.fitroi.pts(2)./data.zero_fill_factor(2)); % measured fid size prior to zerofilling
            measuredPoints = round(data.pts(2)./data.zero_fill_factor(2)); % measured fid size prior to zerofilling
            BB = spcol(spls.knots{1}, 4, linspace(1, gridDimension, measuredPoints));
            %TODO check whether reshape made sense. Eliminated from 1D
            lsm = (BB * ls_fit_r)' + 1i * (BB * ls_fit_i)';
            normalizationMatrix = repmat(max(abs(lsm), [], 2), [1 size(lsm, 2)]);
            normalizationMatrix(normalizationMatrix <  0.0001)  =  0.0001; %avoid too small values in the normalization.
            lsm = lsm ./ normalizationMatrix;  % normalize damping for each line;
            lsm = [lsm ; zeros((data.zero_fill_factor(1)-1)*measuredPoints(1))];
%             lsm = lsm';
            %%
            phase_0 = angle(lsm(1))
            possiblePhases = linspace(0, 2 * pi * (1-1/360*2), 360*2);
            integralValuesReal = zeros(size(possiblePhases));
            integralValuesImag = zeros(size(possiblePhases));
            for indexPhase = 1:length(possiblePhases)
                possible_lsm = lsm .* exp(-1i * possiblePhases(indexPhase));
                integralValuesReal(indexPhase)=sum(real(fftshift(fft(possible_lsm(1,:)))));
                integralValuesImag(indexPhase)=sum(imag(fftshift(fft(possible_lsm(1,:)))));
            end
            figure; plot(possiblePhases/pi*180, integralValuesReal)
            [maxValue, maxIndex] = max(integralValuesReal);
            figure; plot(possiblePhases/pi*180, integralValuesImag)
            [minValue, minIndex] = min(abs(integralValuesImag));
            [combValue, combIndex] = max(integralValuesReal - (abs(integralValuesImag)));
            figure; plot(possiblePhases/pi*180, integralValuesReal - (abs(integralValuesImag)))
            phase_0 = possiblePhases(combIndex)
            figure; plot(real(fftshift(fft(lsm(1,:))))); hold on; plot(real(fftshift(fft(lsm(1,:).*exp(-1i * phase_0))))); phase_0/pi*180
            figure; plot(real(((lsm(1,:))))); hold on; plot(real(((lsm(1,:).*exp(-1i * phase_0)))));
            lsm = lsm .* exp(-1i * phase_0);
        case 2
            gridDimension = round(data.fitroi.pts ./ data.zero_fill_factor);
            measuredPoints = round(data.pts./data.zero_fill_factor); % measured fid size prior to zerofilling
            Bx = spcol(spls.knots{1}, 4, linspace(1, gridDimension(1), measuredPoints(1)));
            By = spcol(spls.knots{2}, 4, linspace(1, gridDimension(2), measuredPoints(2)));
            BB = kron(By,Bx);
            lsm = reshape(BB*ls_fit_r, [measuredPoints(1), measuredPoints(2)]) + 1i*reshape(BB*ls_fit_i, [measuredPoints(1), measuredPoints(2)]);
            if debug
                test_lsm = reshape(BB*test_r, [measuredPoints(1), measuredPoints(2)]) + 1i*reshape(BB*test_i, [measuredPoints(1), measuredPoints(2)]);
                figure; imagesc(real(lsm)); figure; imagesc(real(test_lsm));
            end
            lsm = lsm ./ repmat(max(abs(lsm), [], 2), [1 size(lsm, 2)]);  % normalize damping for each line;
            lsm = [lsm zeros(size(lsm,1), (data.zero_fill_factor(2)-1)*measuredPoints(2)); ...
                zeros((data.zero_fill_factor(1)-1)*measuredPoints(1), data.zero_fill_factor(2) * measuredPoints(2))];
    end
end
