function data = createSummedMaskFitRoi(spec,fiter,basis,data,em_x0, gm_x0, pc0_x0,pc12_x0,df2_x0)
plotMask = false;

ppmVector = data.fitroi.ppm2;

gm_Basis = 20+zeros(size(gm_x0));
summedMask = zeros(size(data.fitroi.ppm2,2),1);
data.summed_mask_fit_roi = summedMask;
use_sp_baseline_value = fiter.use_sp_baseline;
fiter.use_sp_baseline = false;
xBasis = [em_x0, gm_Basis, zeros(size(pc0_x0)), zeros(size(pc12_x0)), zeros(size(df2_x0))];
[F, conc, basis_matrix, spBB, fitted_spec, fixed_basis, processed_spec] = ...
    CalcCostFun(xBasis, spec, fiter, basis, data, []);

if plotMask == true
    LineWidth = 1.5;
    figure;
    
    set(gcf,'Units','normalized');
    hs = subplot(1,2,1);
    numberOfBasisSet = size(basis_matrix,2);
    coordinates = hs.Position;
    delete(hs)
    overlap = 0.1; % 10 percent
    divisionFactor = (numberOfBasisSet-1)*overlap+1;
    offsetsX = coordinates(3)/divisionFactor*overlap;
    offsetsY = coordinates(4)/divisionFactor*overlap;
    axesWidth = coordinates(3)/divisionFactor;
    axesHeigth = coordinates(4)/divisionFactor;
end
for metabolite = 1: size(basis_matrix,2)
    currentSpectrum = basis_matrix(:,metabolite);
    maxPeak = max(abs(currentSpectrum));
    mask = abs(currentSpectrum) > (0.25 *maxPeak);
    summedMask = summedMask + mask;
   
    if plotMask == true
        axes('Position',[coordinates(1)+offsetsX*(metabolite-1) coordinates(2)+offsetsY*(metabolite-1) ...
            axesWidth axesHeigth])
        box on;
        plot(ppmVector,real(currentSpectrum)./maxPeak,'LineWidth',LineWidth);
        hold on
        plot(ppmVector,real(mask),'Color', [0.85 0.33 0.1],'LineWidth',1);
        plotSetup(ppmVector, metabolite == 1)
    end
end

if plotMask == true
    title({'Metabolite weighting envelopes'})
    subplot(1,2,2);
    
    plot(ppmVector,summedMask,'Color', [0.85 0.33 0.1],'LineWidth',LineWidth);
    plotSetup(ppmVector, true)
    ylim([0, max(summedMask)+0.1])
    title('Residual weighting')
    ax = gca;
    ax.YAxisLocation= 'right';
    
    annotation('textbox',[0.485 0.5 0.1 0.1],'String','\Sigma', 'FontSize',14,'LineStyle','none');
    annotation('arrow',[0.47 0.56], [0.53 0.53]);
    %still did some manual adjustments. Easier in the figure
    %%
    figure(1001);
    subplot(2,7,8:14);
    hold on
    if size(basis,2)==5 %iteration 0 (preprocessing
        scaledSpec = real(processed_spec) ./ max(real(processed_spec)) * 5;
        plot(ppmVector,scaledSpec,'Color', [0.5 0.5 0.5],'LineWidth',LineWidth);
        plot(ppmVector,summedMask,'LineWidth',LineWidth+0.5);%,'Color', [0.85 0.33 0.1]
        plotSetup(ppmVector, true)
    else
        s = plot(ppmVector,summedMask,'LineWidth',LineWidth);%,'Color', [0.85 0.33 0.1]
        alpha(s,.5)
        plotSetup(ppmVector, true)
        legend('Sample spectrum', '{\rm\boldmath$weights$} of Iter. 0', '{\rm\boldmath$weights$} of Iter. 1', '{\rm\boldmath$weights$} of Iter. 2\&3\&4', 'Interpreter','latex')
    end
    set(gca,'fontsize',14);
    ylim([0, max(summedMask)+0.1])
    title('Residual weighting')
    if size(basis,2) == 17
        for iMetabolite = 1: size(basis,2)
            if (strcmp(fiter.basis.met{iMetabolite},'Glu'))
                subfigId = 1:2;
                metName = 'Glu';
            elseif (strcmp(fiter.basis.met{iMetabolite},'GSH'))
                subfigId = 3:4;
                metName = 'GSH';
            elseif (strcmp(fiter.basis.met{iMetabolite},'tCho_P'))
                subfigId = 6:7;
                metName = 'tCho+';
            else
                continue;
            end
            subplot(2,7,subfigId);
            
            currentSpectrum = basis_matrix(:,iMetabolite);
            maxPeak = max(abs(currentSpectrum));
            mask = abs(currentSpectrum) > (0.25 *maxPeak);

            plot(ppmVector,real(currentSpectrum)./maxPeak,'LineWidth',LineWidth);
            hold on
            plot(ppmVector,real(mask),'Color', [0.85 0.33 0.1],'LineWidth',1);
            plotSetup(ppmVector, true)
            title(metName)
        end
    end
%     ax = gca;
%     ax.YAxisLocation= 'right';
end
data.summed_mask_fit_roi = summedMask;
fiter.use_sp_baseline = use_sp_baseline_value;
end


function plotSetup(ppmVector, first)
FontSize = 12;
xlim([min(ppmVector), max(ppmVector)])
ylim([0, 1.05])
set(gca,'xDir','reverse')
set(gca,'fontsize',FontSize);
set(gca,'FontWeight','bold');
if first
    xlabel('\delta (ppm)')
%     ylabel('Signal (arb. u.)')
else
    set(gca,'ytick',[]);
    set(gca,'xtick',[]);
end
end