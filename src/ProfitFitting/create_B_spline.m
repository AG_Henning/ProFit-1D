function [ BSpline , splineOrder, knotsSequence] = create_B_spline( gridDimension, numberOfKnots, splineOrder )
%CREATE_B_SPLINE create a B_spline given the grid dimension and the number
%of knots

% create knot sequence
knotsSequence = augknt(linspace(1, gridDimension, numberOfKnots), splineOrder);
%generate spline
BSpline = sparse(spcol(knotsSequence, splineOrder, linspace(1,gridDimension, gridDimension)));
end

