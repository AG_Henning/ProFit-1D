function [F, conc, basis_matrix, spBB, fitted_spec, fixed_basis, processed_spec] = ...
    CalcCostFun(xMatrix, spec, fitobj, basis, data, spbase, lastIter)

if ~exist('lastIter','var')
    lastIter = false;
end

spBB = [];

offset = 1;
for current_par={'em', 'gm', 'pc0', 'pc12', 'df2'}

    current_par_string = current_par{:};
    
    full_nr     = fitobj.nr.(current_par_string).full;
    free_nr     = fitobj.nr.(current_par_string).free;
    free_idx    = any(fitobj.sub_mask.(current_par_string).free, 2)';
    fixed_idx   = any(fitobj.sub_mask.(current_par_string).fixed, 2)';
    fixed_value = zeros(1,full_nr-free_nr);
    tmp = fitobj.sub_mask.(current_par_string).fixed(fixed_idx,:);
    for tt = 1:full_nr-free_nr
        idx = find(tmp(tt,:)==true, 1, 'first');
        fixed_value(tt) = fitobj.fixed_values.(current_par_string)(idx);
    end
    % example processing that is hidden by the eval statements
    % em = nan(1, fitobj.nr.em.full);
    % em(free_idx)  = xMatrix(offset:offset+free_nr+1);
    % em(fixed_idx) = fixed_value;
    eval(sprintf('%s = nan(1, full_nr);', current_par_string));
    if any(free_idx)
        eval(sprintf('%s(free_idx) = xMatrix(offset:offset+free_nr-1);', current_par_string));
    end
    eval(sprintf('%s(fixed_idx) = fixed_value;', current_par_string));
    
    offset = offset + free_nr;
end

n_sig = fitobj.nr_act_mets;
n_met = fitobj.nr.conc.free;

% calculate global parameters and apply it to the spectra
pc0_global = mean(pc0);
pc12_global = mean(pc12);
df2_global = mean(df2);
pc0 = pc0 - pc0_global;
pc12 = pc12 - pc12_global;
df2 = df2 - df2_global;
% apply global parameters tot the spectra
fid = ifft(spec);
zero_order_phase_coeff_global = -1i*pc0_global*pi/180;
coeff_F2_degree_ppm_global = -1i*pc12_global*pi/180;
frequency_shift_F2_coeff_global = -1i*df2_global*2*pi;
fid = fid .* exp(...
    ... zero order phase correction ...
    zero_order_phase_coeff_global +  ...
    ... frequency shift in the F2 ...
    frequency_shift_F2_coeff_global * data.timeVect);
% filter the signal and truncate all the noise from where the signal has decayed
fid = fid .* data.fid_weights;

processed_spec = fft(fid);
% try to define the paramter unit as degree / ppm
processed_spec = processed_spec .* exp( ...
    ... applying the F2 corrections ...
    coeff_F2_degree_ppm_global * data.phase_1_f2_mx);

% cut out spectrum to the dimensions of the fitting roi
processed_spec = processed_spec(data.fitroi.bounds(1,1):data.fitroi.bounds(2,1), ...
            data.fitroi.bounds(1,2):data.fitroi.bounds(2,2));
basis_matrix = zeros(numel(processed_spec), n_met);
fixed_basis  = zeros(numel(processed_spec), 1);

for zz=1:n_sig
    
    basis_fid = basis{zz};
    
    %----- precalculate coefficients for the corrections and filters -----%
    % zero order phase correction
    % calculate zero order phase correction coefficient
    pc0_idx = fitobj.sub_mask.pc0.full(:, zz)';
    zero_order_phase_coeff = 1i*pc0(pc0_idx)*pi/180;
    
    % metabolite specific exp filter
    % calculate the metabolite specific exponential filter coefficient
    em_idx = fitobj.sub_mask.em.full(:, zz)';
    metabolite_filter_coeff = -em(em_idx)*pi;

    % frequency shift in the F2
    % calculate the frequency shift in the F2 coefficient
    df2_idx = fitobj.sub_mask.df2.full(:, zz)';
    frequency_shift_F2_coeff = 1i*df2(df2_idx)*2*pi;
        
    % gauss filter
    % calculate the gauss filter coefficients
    gm_idx = fitobj.sub_mask.gm.full(:, zz)';
    gauss_filter_coeff1 = gm(gm_idx)*pi;
    gauss_filter_coeff2 = -(4*log(2));
    gauss_filter_coeff = gauss_filter_coeff1 ^2 /gauss_filter_coeff2;
    %--------------- end of coefficient precalculations ------------------%
    
    
    %------- apply the corrections and filters on the basis_fid ----------%
    % calculate the exponential cofficient matrix to apply on basis_fid
    % and apply the corrections and filters on the basis_fid
    basis_fid = basis_fid .* exp(...
        ... zero order phase correction ...
        zero_order_phase_coeff + ...
        ... metabolite specific exp filter ...
        metabolite_filter_coeff * (data.te + data.timeVect) + ...
        ... frequency shift in the F2 ...
        frequency_shift_F2_coeff * data.timeVect + ...
        ... gauss filter ...
        gauss_filter_coeff * (data.timeVect.^2));
    %---------- end of corrections and filters on basis_fid --------------%

    % Filter the basis sets and truncate all the noise from where the signal has decayed
    % Use same settings as the one which was applied on the acquired fid
    basis_fid = basis_fid .* data.fid_weights;
    % save tilted version of basis spectrum     
	basis_spec = fft(basis_fid);
    
	% first order phase correction applied in the spectral domain
    pc12_idx = fitobj.sub_mask.pc12.full(:, zz)';
    % basis_fid = basis_fid .* exp(1i*pc1(pc1_idx)*pi/180*data.phase_1_mx);
    
    % Define the region of interest (ROI) of the spectrum and perfoming the
    % computations in the following only on this region.
    spectrum_roi = [data.fitroi.bounds(1,1) data.fitroi.bounds(1,2); data.fitroi.bounds(2,1) data.fitroi.bounds(2,2)];
	
    % cut it to the size of the quantification window
    basis_spec = basis_spec(spectrum_roi(1,1):spectrum_roi(2,1), ...
                            spectrum_roi(1,2):spectrum_roi(2,2));

    % precompute coefficient needed to define the parameter unit as degree /ppm
    coeff_F2_degree_ppm = 1i*pc12(pc12_idx)*pi/180;
    
    % try to define the paramter unit as degree / ppm
    basis_spec = basis_spec .* exp( ... 
        ... applying the F2 corrections ...
        coeff_F2_degree_ppm * ...
        data.phase_1_f2_mx(spectrum_roi(1,1):spectrum_roi(2,1), spectrum_roi(1,2):spectrum_roi(2,2)));

            
    % fixed for concentrations preliminary handled
    conc_idx = fitobj.sub_mask.conc.free(:, zz)';
    if (~any(conc_idx) && any(fitobj.sub_mask.conc.fixed(:,zz)))
        tmp_idx = any(fitobj.sub_mask.conc.fixed, 2)';
        fixed_value_idx = fitobj.sub_mask.conc.fixed(tmp_idx,:);
        processed_spec = processed_spec - basis_spec * fitobj.fixed_values.conc(fixed_value_idx);
        fixed_basis = fixed_basis + basis_spec(:) * fitobj.fixed_values.conc(fixed_value_idx);
    else
        basis_matrix(:, conc_idx) = basis_matrix(:, conc_idx) + basis_spec(:);        
    end
end

if (fitobj.use_sp_baseline)
    options = optimset('Display', 'off', 'useParallel', 'always', 'LargeScale', 'on', 'MaxIter', 1000);
    
    fit_lambda = fitobj.spbase_lambda(2);
    if ~isempty(spbase.A)
        error('Sorry spbase.A is not empty');
    end
    [conc,resnorm,residual,exitflag,output,lambda] = ...
        ... actualy returns: [conc,resnorm,residual,exitflag,output,lambda] ...
        lsqlin([real(basis_matrix) spbase.BB zeros(size(spbase.BB)); imag(basis_matrix) zeros(size(spbase.BB)) spbase.BB; ... 
        zeros(size(spbase.P,1), n_met) sqrt(fit_lambda)*spbase.P zeros(size(spbase.P)); ...
        zeros(size(spbase.P,1), n_met) zeros(size(spbase.P)) sqrt(fit_lambda)*spbase.P ], ...
        [real(processed_spec(:)); imag(processed_spec(:)); zeros(size(spbase.P,1),1);zeros(size(spbase.P,1),1)], ...
        spbase.A, zeros(size(spbase.A,1),1), [], [], spbase.lb, spbase.ub, spbase.x0, options);
    
    % eliminate fitted concentrations below the noise level. Those are nonsense.
    if lastIter
        for indexMet = 1 : n_sig
            realMetSpec = real(basis_matrix(:,indexMet)).*conc(indexMet);
            maxMetSpec = max(realMetSpec);
            if maxMetSpec < data.noise_std/5
                conc(indexMet) = 0;
            end
        end
    end
    
    tmp = [real(basis_matrix) spbase.BB zeros(size(spbase.BB)); imag(basis_matrix) zeros(size(spbase.BB)) spbase.BB] * conc;
    tmp = reshape(tmp, [length(tmp)/2, 2]);
    fitted_spec = tmp(:,1) + 1i*tmp(:,2);
    spBB = spbase.BB;
else
    
    options = optimset('Display', 'off', 'useParallel', 'always', 'LargeScale', 'on', 'MaxIter', 250); 

    conc_lb = fitobj.lower_bounds.conc;
    conc_ub = fitobj.upper_bounds.conc;
    conc_A  = fitobj.lin_priors.conc;
    
    % handle start values
    conc_x0  = fitobj.start_values.conc;
    if ~isempty(conc_A)
        error('Sorry conc_A is not empty');
    end
    
    [conc,resnorm,residual,exitflag,output,lambda] = ...
        ... actualy returns: [conc,resnorm,residual,exitflag,output,lambda] ...
        lsqlin([real(basis_matrix); imag(basis_matrix)], [real(processed_spec(:)); imag(processed_spec(:))], ...
        conc_A, zeros(size(conc_A,1),1), [], [], conc_lb, conc_ub, conc_x0, options);

   % eliminate fitted concentrations below the noise level. Those are nonsense. 
   if lastIter
       for indexMet = 1 : n_sig
           realMetSpec = real(basis_matrix(:,indexMet)).*conc(indexMet);
           maxMetSpec = max(realMetSpec);
           if maxMetSpec < data.noise_std/5
               conc(indexMet) = 0;
           end
       end
   end
    
    fitted_spec = basis_matrix * conc;
end

% conc = pinv([real(basis_matrix); imag(basis_matrix)])*[real(spec(:)); imag(spec(:))];

fitted_spec = reshape(fitted_spec, size(processed_spec));
fixed_basis = reshape(fixed_basis, size(processed_spec));

if(data.createVideo == 1) %could also use data.debug The output is also useful for debugging 
    createVideoFrames(data, processed_spec, fitted_spec, spbase, conc, basis_matrix);
else
    %nothing to do
end

fullResidual = processed_spec - fitted_spec;
residual = [real(fullResidual); imag(fullResidual)];
residual2 = [real(fullResidual.*data.summed_mask_fit_roi'); imag(fullResidual.*data.summed_mask_fit_roi')];
truncPoint = data.truncPointMasked;
residualFid = [real(ifft(fullResidual)); imag(ifft(fullResidual))];
residualFid = residualFid(:,1:truncPoint);

% use the chosen cost function for the lsqnonlin optimization
switch data.costFunction
    case 'R1'
        F = [residual(:);] ; %R1
    case 'R2'
        F = [residual(:); residualFid(:);] ; %R2
    case 'R3'
        F = [residual(:); 3*residual2(:); residualFid(:);] ;% R3
    case 'R3_weighted_only'
        F = [residual2(:);] ;% R3_weighted_only
    case 'R3_weighted+FID'
        F = [3*residual2(:); residualFid(:);] ;% R3_weighted+FID
    otherwise
        error('Cost function type was not defined accordingly')
end
end
