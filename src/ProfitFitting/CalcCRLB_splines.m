function crlb = CalcCRLB_splines(B, spbase, lambda, noise_var)
    
    % based on Sima diss p.126
    BB = [real(B); imag(B)];
    m  = size(BB,1);
    A  = [spbase.BB zeros(size(spbase.BB));zeros(size(spbase.BB)) spbase.BB];
    P  = [spbase.P zeros(size(spbase.P)); zeros(size(spbase.P)) spbase.P ];
    A_lambda = A / (A'*A + lambda*P) * A';
    
    CC = (eye(m) - A_lambda) * BB / (BB' * (eye(m) - A_lambda) * BB);

    invF = (CC'*CC) * noise_var;
    crlb = sqrt(diag(invF))';
end