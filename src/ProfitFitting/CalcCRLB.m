function crlb = CalcCRLB(B, noise_var)
%Fischer information matrix
    F = real(B'*B) / noise_var;
    
    crlb = sqrt(diag(inv(F)))';
end