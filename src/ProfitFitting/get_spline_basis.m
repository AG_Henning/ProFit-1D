function [BSpline, RegularizationMatrix] = get_spline_basis(fitobj, data)

knot2knot = fitobj.spbase_knot_spacing;    % knot spacing in ppm
maxResolution = 1./data.pts_ppm;
if any(knot2knot < maxResolution)
    knot2knot(knot2knot < maxResolution) = maxResolution(knot2knot < maxResolution);
    fprintf('Due to resolution limits the knot to knot spacing had to be changed to %4.2f / %4.2f ppm\n', knot2knot(1), knot2knot(2));
end

cutRange = [min(data.fitroi.ppm2) max(data.fitroi.ppm2)]; 
numberOfKnots = round(diff(cutRange,1,2)'./knot2knot);
% numberOfKnots = [0,2]; %TODO URGENT this is it
gridDimension = diff(data.fitroi.bounds, [], 1) + 1;

%TODO: check if correct for 1D

% create a cubic spline order
splineOrder = 4;
%create B_spline in y direction only
[BSpline, ky, knots{1}] = create_B_spline(gridDimension(2), numberOfKnots(2), splineOrder);

%get regularization matrix for the y direction
RegularizationMatrix = set_up_spline_regularization_matrix(ky, knots{1});


% BSplineZ = conj(hilbert(BSpline));   % hilbert transform along the knot dimension to create imaginary versions
end