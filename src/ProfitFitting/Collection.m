classdef Collection < handle
% this class is used to separate the involved parameters into free and fixed 
% variants. Furthermore combined information is held in the full property

    properties
        
        full;
        free;
        fixed;
    end
    
end