function spbase = setup_spbase(fititer, data, conc_x0, conc_ub, conc_lb, conc_A)

        bl_comps_pppm = 15;
        splineBasis = createSplineBasis(data.ppm2, min(data.fitroi.ppm2), max(data.fitroi.ppm2), bl_comps_pppm);

        spbase.BB = splineBasis.bl_bas;
        spbase.P = splineBasis.deriv_mat;
%         [spbase.BB, spbase.P] = get_spline_basis(fititer, data);
        spbase.x0 = horzcat(conc_x0, 0*ones(1, 2*size(spbase.BB,2)));
        spbase.ub = horzcat(conc_ub, Inf*ones(1, 2*size(spbase.BB,2)));
        spbase.lb = horzcat(conc_lb, -Inf*ones(1, 2*size(spbase.BB,2)));
        spbase.A  = [conc_A, zeros(size(conc_A,1), 2*size(spbase.BB, 2))];
end