
function res = saveResultsProFit(fitSettings, cdres, exitflag, output, lambda, xSolver, fitted_spec, fixed_basis, processed_spec, ...
    basis_matrix, spBB, spbase, conc_lb, conc_ub, conc, g_damping, data)

    res = Results(fitSettings, cdres);
    
    res.optim_exit = exitflag;
    res.optim_output = output;
    res.optim_lambda = lambda;
    res.optim_x = xSolver;
    
    res.fitted_spec = fitted_spec + fixed_basis;
    res.basis_matrix = basis_matrix;
    res.fixed_basis = fixed_basis;
    res.processed_spec = processed_spec;
    
    res.splines_baseline.spbase_lambda = fitSettings.spbase_lambda;
    if (fitSettings.use_sp_baseline)
        res.splines_baseline.basis_matrix = spBB;
        res.splines_baseline.reg_matrix   = spbase.P;
        res.splines_baseline.lb           = spbase.lb(length(conc_lb)+1:end);
        res.splines_baseline.ub           = spbase.ub(length(conc_ub)+1:end);
        res.splines_baseline.coeff        = conc(fitSettings.nr.conc.free+1:end);
    end
        
    res.fitted_values.conc = conc(1:fitSettings.nr.conc.free)';
    offset = 1;
    for current_par={'em', 'gm', 'pc0', 'pc12', 'df2'}
        
        current_par_string = current_par{:};
        
        full_nr     = fitSettings.nr.(current_par_string).full;
        free_nr     = fitSettings.nr.(current_par_string).free;
        free_idx    = any(fitSettings.sub_mask.(current_par_string).free, 2)';
        fixed_idx   = any(fitSettings.sub_mask.(current_par_string).fixed, 2)';
        fixed_value = zeros(1,full_nr-free_nr);
        tmp = fitSettings.sub_mask.(current_par_string).fixed(fixed_idx,:);
        for tt = 1:full_nr-free_nr
            idx = find(tmp(tt,:)==true, 1, 'first');
            fixed_value(tt) = fitSettings.fixed_values.(current_par_string)(idx);
        end
        
        fitted_values = nan(1, full_nr);
        % beware of end of array xSolver
        if any(free_idx)
            fitted_values(free_idx)  = xSolver(offset:offset+free_nr-1);
        end
        fitted_values(fixed_idx) = fixed_value;
        
        offset = offset + free_nr;
        res.fitted_values.(current_par_string) = fitted_values;
    end
     
    % calculate CRLBs for the specific iteration
    if (fitSettings.use_sp_baseline)
       res.crlb = zeros(size(basis_matrix));%TODO THIS IS THE PROBLEM CalcCRLB_splines(basis_matrix, spbase, fitSettings.spbase_lambda(2), data.noise_var) ./ res.fitted_values.conc * 100;
    else
        res.crlb = CalcCRLB(basis_matrix, data.noise_var) ./ res.fitted_values.conc * 100;
    end
    res.noise_var = data.noise_var;
end