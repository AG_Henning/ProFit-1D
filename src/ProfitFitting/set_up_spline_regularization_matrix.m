function [ SecondOrderDifferenceOperator ] = set_up_spline_regularization_matrix( splineOrder, knotsSequence )
%SET_UP_SPLINE_REGULARIZATION sets up the second order difference operator
%for the spline regularization

% set up regularization
sparseMatrixSize = length(knotsSequence)-splineOrder;
% second order difference Operator: a sparse matrix with the 
% [1 -2 1] difference operator kernel
D = sparse(1:sparseMatrixSize,1:sparseMatrixSize,-2*ones(1,sparseMatrixSize),sparseMatrixSize,sparseMatrixSize);
E = sparse(2:sparseMatrixSize,1:sparseMatrixSize-1,ones(1,sparseMatrixSize-1),sparseMatrixSize,sparseMatrixSize);
SecondOrderDifferenceOperator = E+D+E';
end

