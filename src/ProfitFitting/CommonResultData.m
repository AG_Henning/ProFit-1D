classdef CommonResultData < handle
   
    % this class just exists to implement something like static properties
    properties
        
        spec;
        data;
    end
    
    properties (Dependent);
       
        fid;
    end
    
    methods
        
        function value = get.fid(this)
            
            value = ifft(ifft(this.spec,[],1), [], 2);
        end
    end
end