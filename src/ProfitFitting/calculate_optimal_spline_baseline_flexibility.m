function [optimal_lambda, ed_vec, optim_model_vec, optim_idx] =  calculate_optimal_spline_baseline_flexibility(fititer, data, spbase, x0Solver, spec, basis)
        data.debug = 0;

        % Configuration options for the baseline
        bl_flex_n = 30; % array of bl flex values to try
        max_bl_ed_pppm = 10;%7 in M. Wilson AB-Fit paper
        min_bl_ed_pppm = [];
        aic_smoothing_factor = 17;%5 in M. Wilson AB-Fit paper; 17 works the best for our data
        %I am not sure anymore (because of in vivo data fits), whether the m value for mAIC (aic_smoothing_factor) of 15 is correct.
        %Nevertheless, the parameter seems to need a bit stronger than linear scaling with the zerofilling.
        aic_smoothing_factor = aic_smoothing_factor * (data.zero_fill_factor^1.1);
                
        ppm_range = max(data.fitroi.ppm2) - min(data.fitroi.ppm2);
        
        % calculate the right lambda
        lambda_start = calc_lambda_from_ed(spbase.BB, spbase.P, max_bl_ed_pppm * ppm_range);
        
        if (isempty(min_bl_ed_pppm))
            lambda_end   = calc_lambda_from_ed(spbase.BB, spbase.P, 2.01);
        else
            lambda_end   = calc_lambda_from_ed(spbase.BB, spbase.P, min_bl_ed_pppm * ppm_range);
        end
        
        lambda_vec   = 10 .^ (linspace(log10(lambda_start), log10(lambda_end), bl_flex_n));
        
        optim_model_vec = zeros(1, bl_flex_n);
        ed_vec          = zeros(1, bl_flex_n);
        
        % loop over candidate baselines
        for n = 1:bl_flex_n
            ed_vec(n) = calc_ed_from_lambda(spbase.BB, spbase.P, lambda_vec(n));
            fititer.spbase_lambda(2) = lambda_vec(n);
            [residual, conc, basis_matrix, spBB, fitted_spec, fixed_basis, processed_spec] = ...
                CalcCostFun(x0Solver, spec, fititer, basis, data, spbase);
            
            % using mAIC (Akaike's information criterion)
            optim_model_vec(n) = log(sum(residual .^ 2)) + aic_smoothing_factor * 2 * ed_vec(n) / size(spbase.BB,1);
            if(data.debug == 1)
                title(['ED: ', num2str(ed_vec(n),2), ' -> mAIC: ', num2str(optim_model_vec(n),3)]);
                figure;
            end
        end
        
        % find the optimal flexability
        [~, optim_idx] = min(optim_model_vec);
        
        if (optim_idx == 1)
            warning('The most flexible baseline available was used - could indicate a problem');
        end
        
        if data.debug
            figure;
            plot(ed_vec, optim_model_vec)
            set(gca,'Xscale','log')
            title(['ED: ', num2str(ed_vec(optim_idx),2), ' -> mAIC: ', num2str(optim_model_vec(optim_idx),3)]);
        end
        optimal_lambda = lambda_vec(optim_idx);
        
        data.debug = 0;
end