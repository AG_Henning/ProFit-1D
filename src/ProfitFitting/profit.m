function res = profit(fid, fs, data)
%% Messages to future users
warning('Dear user,');
warning(['The ProFit-1D software might be a perfect tool to develop new', ...
   'fitting capabilities, beyond what LCModel can achieve.']);
warning(['ProFit-1D may achieve a higher accuracy, for example, if the ', ...
    'macromolecular spectrum is measured and should not include phase, line broadening, etc.']);
warning(['However, if you look for means to get reproducible results for clinical studies, ' ...
    'where differences between for example patient populations are more important, ' ...
    'I recommend using LCModel instead.']);
warning(['If you work on more complicated spectral features, did you check in LCModel: ', ...
    'dkntmn - baseline stiffness, nratio - soft constraints, chsimu - simulated Voigt lines', ...
    'nsdsh - individual chemical shifts, nsdt2 - individual t2 relaxations; or just manipulating the basis sets afore-hand?']);
warning('Best wishes, Tamas Borbath');
warning('P.S. The MR_SpectroS recon code is though really useful. All those dimensions are superflous, but the algorithms are good.');
%% FUTURE DEVELOPER
% If you are a future developer, PhD student: fitting is a very tedious process, it is better
% to work on projects actively being maintained by other scientist. Check
% what the MRS community actively works on: is it LCModel (open source
% now), Osprey (Georg Oeltzschner), AB-Fit (Martin Wilson). If the
% developers are active it is always better to collaborate.
% If you choose ProFit-1D development, you are alone; I was alone,
% Alexander Fuchs (ProFit - revisited) was alone. I will help you, as much
% as all my other obligations permit, but that won't be enough compared to
% your own efforts needed.
% All the best, 
%  Tamas Borbath
% P.S. I can't predict where I will be in future, but you may stalk me online
% and always find a means to contact me: MPI webpage email, a company email
% I will work at, LinkedIn, Facebook, Xing, etc.
% P.P.S. The MR_SpectroS recon code is though really useful. All those
% dimensions are superflous, but the algorithms are good.
%%

g_damping = [];

% setup the actual fitting process
nr_iter = fs.nr_iter;

start_time = tic;
iter_time = zeros(1, nr_iter);

% initializing result object stuff
res   = cell(1,nr_iter);
cdres = CommonResultData;
cdres.spec = fft(fft(fid,[],2),[],1);
cdres.data = data;

for iter=1:nr_iter

    if (iter>1)
        % Update starting values and bound constraints with information
        % from previous iterations
        fs.seq{iter}.UpdateLowerBounds(fs.seq{iter}, res{iter-1});
        fs.seq{iter}.UpdateUpperBounds(fs.seq{iter}, res{iter-1});
        fs.seq{iter}.UpdateStartingValues(fs.seq{iter}, res{iter-1});
    end
    
    % collect all neccessary data for current fitting iteration
    conc_lb = fs.seq{iter}.lower_bounds.conc;
    em_lb   = fs.seq{iter}.lower_bounds.em;
    gm_lb   = fs.seq{iter}.lower_bounds.gm;
    pc0_lb  = fs.seq{iter}.lower_bounds.pc0;
    pc12_lb = fs.seq{iter}.lower_bounds.pc12;
    df2_lb  = fs.seq{iter}.lower_bounds.df2;
    
    conc_ub = fs.seq{iter}.upper_bounds.conc;
    em_ub   = fs.seq{iter}.upper_bounds.em;
    gm_ub   = fs.seq{iter}.upper_bounds.gm;
    pc0_ub  = fs.seq{iter}.upper_bounds.pc0;
    pc12_ub = fs.seq{iter}.upper_bounds.pc12;
    df2_ub  = fs.seq{iter}.upper_bounds.df2;
    
    conc_A  = fs.seq{iter}.lin_priors.conc;
    em_A    = fs.seq{iter}.lin_priors.em;
    gm_A    = fs.seq{iter}.lin_priors.gm;
    pc0_A   = fs.seq{iter}.lin_priors.pc0;
    pc12_A  = fs.seq{iter}.lin_priors.pc12;
    df2_A   = fs.seq{iter}.lin_priors.df2;
    
    % handle start values 
    conc_x0  = fs.seq{iter}.start_values.conc;
    em_x0    = fs.seq{iter}.start_values.em;
    gm_x0    = fs.seq{iter}.start_values.gm;
    pc0_x0   = fs.seq{iter}.start_values.pc0;
    pc12_x0  = fs.seq{iter}.start_values.pc12;
    df2_x0   = fs.seq{iter}.start_values.df2;
           
    lb = [em_lb, gm_lb, pc0_lb, pc12_lb, df2_lb];
    ub = [em_ub, gm_ub, pc0_ub, pc12_ub, df2_ub];
    x0Solver = [em_x0, gm_x0, pc0_x0, pc12_x0, df2_x0];
    
    % put basis set together for the specific iteration
    map_idx = 1:length(fs.basis.met);
    map_idx = map_idx(fs.seq{iter}.active_mets);
    basis = cell(1, length(map_idx));
    for kk = 1:length(map_idx)
        basis{kk} = fs.basis.fid{map_idx(kk)}; 
    end
    
    data = createSummedMaskFitRoi(cdres.spec,fs.seq{iter},basis,data,em_x0, gm_x0, pc0_x0,pc12_x0,df2_x0);
    
    if (fs.seq{iter}.use_sp_baseline)
        spbase = setup_spbase(fs.seq{iter}, data, conc_x0, conc_ub, conc_lb, conc_A);
        if fs.seq{iter}.estimate_sp_baseline_stiffness == true
            [fs.seq{iter}.spbase_lambda(2), fs.seq{iter}.spbase_ed_vec, fs.seq{iter}.spbase_optim_model_vec, fs.seq{iter}.spbase_optim_idx] = ...
                calculate_optimal_spline_baseline_flexibility(fs.seq{iter}, data, spbase, x0Solver, cdres.spec, basis);
        else
            fs.seq{iter}.spbase_lambda =  res{iter-1}.splines_lambda;
            fs.seq{iter}.spbase_ed_vec =  res{iter-1}.splines_ed_vec;
            fs.seq{iter}.spbase_optim_model_vec =  res{iter-1}.splines_optim_model_vec;
            fs.seq{iter}.spbase_optim_idx =  res{iter-1}.splines_optim_idx;
        end
%         [fid, fitsettings, pc0, df2, pc12] = preprocessWithBaseline(fid, fs.seq{iter}, data);
    else
        spbase = [];
    end

    % specifiy algorithm details used for the optimization process
    options = optimset('Display', 'iter', ...
                       'Algorithm', 'trust-region-reflective', ...
                       'useParallel', 'always', ...
                       'FinDiffType', 'forward', ...
                       'Jacobian', 'off', ... 
                       'DerivativeCheck', 'off', ...
                       'Diagnostics', 'on', ...
                       'TolFun', 1e-5, ... % 1e-5 1e-12
                       'MaxIter', 1500, ... % 1500 %70 for movie
                       'MaxFunEvals', 5000); % 5000 % 10000
        
    [xSolver, ~, ~, exitflag, output, lambda, ~] = ...
        ... The standard return is: [xSolver, resnorm, residual, exitflag, output, lambda, jacobian] ...
        lsqnonlin(@(xSolver) CalcCostFun(xSolver, cdres.spec, fs.seq{iter}, basis, data, spbase), x0Solver, lb, ub, options);
    
    lastIter = true;
    fid_weights = data.fid_weights;
    data.fid_weights = ones(1,data.pts(2));
   [~, conc, basis_matrix, spBB, fitted_spec, fixed_basis, processed_spec] = CalcCostFun(xSolver, cdres.spec, fs.seq{iter}, basis, data, spbase, lastIter);
   data.fid_weights = fid_weights;
     
      % eliminate fitted concentrations below the noise level. Those are nonsense. 
%    for indexMet = 1 : n_sig
%        realMetSpec = real(basis_matrix(:,indexMet)).*conc(indexMet);
%        maxMetSpec = max(realMetSpec);
%        if maxMetSpec < data.noise_std
%            conc(indexMet) = NaN;
%        end
%    end
   
   res{iter} = saveResultsProFit(fs.seq{iter}, cdres, exitflag, output, lambda, xSolver, fitted_spec, fixed_basis, processed_spec, ...
       basis_matrix, spBB, spbase, conc_lb, conc_ub, conc, g_damping, data);
   
   FQN = calculate_fit_quality_number(res{iter}.residual_spec, res{iter}.common_data.data.fitroi.ppm2, ...
       cdres.spec, data.ppm2);
   res{iter}.fit_quality_number = FQN;
   FQN2 = calculate_fit_quality_number(res{iter}.residual_spec, res{iter}.common_data.data.fitroi.ppm2, ...
       cdres.spec, data.ppm2, [1.95 4.1]);
   res{iter}.fit_quality_number2 = FQN2;

   iter_time(iter) = toc(start_time);
    
end

fprintf('\nElapsed time statistics **************************************\n');
for zz=1:nr_iter
    if zz==1
        duration = iter_time(zz);
    else
        duration = iter_time(zz) - iter_time(zz-1);
    end
    fprintf('Elapsed time %d. iteration: %02d:%02d:%02d:%02d hh:mm:ss:hs\n', zz, floor(duration/3600), mod(floor(duration/60),60), floor(mod(duration,60)), round(100*mod(duration,1)));
end
total_dur = iter_time(nr_iter);
fprintf('**************************************************************\n');
fprintf('Total elapsed time:        %02d:%02d:%02d:%02d hh:mm:ss:hs\n\n', floor(total_dur/3600), mod(floor(total_dur/60),60), floor(mod(total_dur,60)), round(100*mod(total_dur,1)));

    
end