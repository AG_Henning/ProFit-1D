function [BSpline, RegularizationMatrix, knots] = get_spline_basis_ls(fitobj, data)

%calculate the data characterizing the splines: numberOfKnots and
%gridDimension
switch data.spectral_dimension
    case 1
        gridDimension = round(data.fitroi.pts(2)./data.zero_fill_factor(2)); % measured fid size prior to zerofilling
        
        knot2knot = 5e-3; %  10e-3; % knot spacing in ms -> this should be configurable in iteration obj
        maxResolution = data.fitroi.dt(2);
        if any(knot2knot < maxResolution)
            knot2knot(knot2knot < maxResolution) = maxResolution(knot2knot < maxResolution);
            fprintf('Due to resolution limits the knot to knot spacing had to be changed to %6.2f ms\n', knot2knot(1)*1e3);
        end
        
        numberOfKnots = round(data.fitroi.t2(gridDimension)/knot2knot) + 1;
    case 2
        gridDimension = round(data.fitroi.pts./data.zero_fill_factor); % measured fid size prior to zerofilling
        
        knot2knot = [0.5e-3 10e-3];    % knot spacing in ms -> this should be configurable in iteration obj
        maxResolution = data.fitroi.dt;
        if any(knot2knot < maxResolution)
            knot2knot(knot2knot < maxResolution) = maxResolution(knot2knot < maxResolution);
            fprintf('Due to resolution limits the knot to knot spacing had to be changed to %6.2f / %6.2f ms\n', knot2knot(1)*1e3, knot2knot(2)*1e3);
        end
        
        numberOfKnots = round([data.fitroi.t1(gridDimension(1)) data.fitroi.t2(gridDimension(2))]./knot2knot) + 1;
end


%Calculate B-Spline and Regularization term
switch data.spectral_dimension
    case 1
        %TODO: check if correct for 1D
        splineOrder = 4;
        %create B_spline in y direction only
        [BSpline, ky, knots{1}] = create_B_spline(gridDimension, numberOfKnots, splineOrder ); %TODO URGENT
%         [BSpline, ky, knots{1}] = create_B_spline(482, numberOfKnots, splineOrder );
        
        %get regularization matrix for the y direction
        RegularizationMatrix = set_up_spline_regularization_matrix(ky, knots{1});
        %add zero boundary conditions, since the lineshape has to be "free" on the
        %boundary
        RegularizationMatrix (1,1:2) = [0 0];
        RegularizationMatrix (end,end-1:end) = [0 0];
    case 2
        splineOrder = 4;
        %create B_spline in x direction
        [Bx, kx, knots{1}] = create_B_spline(gridDimension(1), numberOfKnots(1), splineOrder );
        %create B_spline in y direction
        [By, ky, knots{2}] = create_B_spline(gridDimension(2), numberOfKnots(2), splineOrder );
        %create 2D Bspline
        BSpline = sparse(kron(By,Bx));
       
        %get regularization matrix for the x direction
        RegularizationX = set_up_spline_regularization_matrix(kx, knots{1});
        %get regularization matrix for the y direction
        RegularizationY = set_up_spline_regularization_matrix(ky, knots{2});
        %add zero boundary conditions, since the lineshape has to be "free" on the
        %boundary
        RegularizationX(1,1:2) = [0 0];
        RegularizationX(end,end-1:end) = [0 0];
        RegularizationY(1,1:2) = [0 0];
        RegularizationY(end,end-1:end) = [0 0];
        %create regularization matrix P for the entire domain using stencil
        %[0  1  0]
        %[1 -4  1]
        %[0  1  0]
        RegularizationTermX = kron(eye(length(knots{2})-kx), RegularizationX);
        RegularizationTermY = kron(RegularizationY, eye(length(knots{1})-ky));
        RegularizationMatrix = (RegularizationTermX + RegularizationTermY);
end % other dimensionalities not handled. Should have been taken care of before
end