classdef Parameters < handle
% defines the class of model parameters that are used during fitting

    properties
        
        conc;
        em;
        gm;
        pc0;
        pc12;
        df2;
    end
    
    events
        
        needUpdate;
    end
    
    methods
        
        function this = Fill(this, type, content)
                        
            if  ~iscellstr(type)
                error('Parameters:Fill:WrongInput', 'The input of the Fill type needs to be a cell string');
            end
            
            if ((length(type)==1) && strcmpi(type, 'all'));
                
                mobj = ?Parameters;
                type = cellfun(@(prop) prop.Name, mobj.Properties, 'UniformOutput', false);
            end
            
            for type_cnt=1:length(type)
                
                try
                    this.(type{type_cnt}) = content;
                catch exception
                   error('Parameters:Fill:NonExistingField', 'Trying to access non a existing field'); 
                end
            end
            
            notify(this, 'needUpdate');
        end
    end
end
