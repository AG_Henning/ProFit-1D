function illustrateFilters(filenameData, dataPath, filenameWater, waterPath, TE, TR, outputPath)

dataPath = 'D:\Software\Spectro Data\TestDataProFit\1 Normal recon\Subj_3\\\\TE24\\';
filenameData = 'Subj_3_TE24';
TE = 24;
TR = 6000;
%INIT_PROFIT
usePpmGap = false;
nuclues = '1H';
%read in the *.RAW LCModel file
[fid, bandwidth, scanFrequency, ~, ~] = ...
    ImportLCModelBasis(dataPath, filenameData, false, nuclues);

%% load fit specific settings
pathName = 'DF data path';
pathBasisSets = pathToDataFolder(pathName, {'Basis_sets'});

fitSettingsFileName = 'Basis_sets\final_T2_met_MM_paper/FitSettings_sLASER_3Iter_T2_no_emg_sLASER_7ppm_moiety_UF_basis_TE24';
load([pathBasisSets, fitSettingsFileName], 'data', 'fitsettings');

% 1D spectral data set
data.spectral_dimension = 1;

% add additional data set specific information and settings
data.dt         = [0, 1/bandwidth];
data.bw         = [0 bandwidth];

data.f0         = scanFrequency;      % Hz/ppm
data.te         = TE * 1e-3;
data.pts        = size(fid);
data.total_ppm  = data.bw ./ data.f0;
data.pts_ppm    = data.pts ./ data.total_ppm;
data.tr         = TR;
data.center_ppm = 4.7;
refFreqPpm = -2.3; % the reference Frequency for the excitation pulse, given in ppm compared to the water (minus is upfield, + downfield)
data.phasePivot_ppm = data.center_ppm + refFreqPpm; % in ppm

% set initial vector of time and frequency domain axis
data.t2 = (0:data.pts(2) - 1) * data.dt(2);
data.w1 = linspace(-data.bw(1)/2, data.bw(1)/2 - data.bw(1)/data.pts(1), data.pts(1));
data.w2 = linspace(-data.bw(2)/2, data.bw(2)/2 - data.bw(2)/data.pts(2), data.pts(2));
data.ppm2 = linspace(-data.total_ppm(2)/2, data.total_ppm(2)/2-data.total_ppm(2)/data.pts(2), data.pts(2)) + data.center_ppm;

% setup shift and tilting matrices
data.shift_t2_mx   = ones(data.pts(1),1)*data.t2;
data.lb_t1_mx      = data.te * ones(1, data.pts(2));

data.phase_1_f2_mx = data.ppm2 - data.phasePivot_ppm;

% automatic noise determination
[data.noise_var, data.noise_std] = calcNoiseLevel1D(fid, data);

% define mask and bounding box for fit region
data = DefineFitROI(data);

% do the same for the basis set metabolites (differentiate on
% 1D/2D)
%create a tilt matrix for the metabolite spectra, such that we can
%select the correct one and cut out the spectra of interest from it
for zz=1:length(fitsettings.basis.fid)
    %apply the shifting
    spec = fftshift(fft(fitsettings.basis.fid{zz}));
    if usePpmGap
        indexPpmGap = (data.ppm2 < 5.5) & (data.ppm2 > 4.1);
        spec(indexPpmGap)=0;
    end
    fitsettings.basis.fid{zz} = ifft(spec);
end

zeroFillFactor = 1; indices = [2.008; 3.028; 3.210; 3.921]; searchArea =0.1; 
useReal = false; doSplineSmoothing = false; splineSmoothingCoeff = 0.01;
% do Frequency Alignment
[frequencyShiftFactorPpm] = alignSpectraFrequencyDomainFactorPpm(fid, scanFrequency*1e6, bandwidth, ...
    zeroFillFactor, indices, searchArea, useReal, doSplineSmoothing, splineSmoothingCoeff);
df2_global = frequencyShiftFactorPpm *scanFrequency;
% frequency shift in the F2
fid = fid .* exp(1i*-df2_global*2*pi*data.shift_t2_mx);

% prepare spectrum by shifting and tilting
spec = fftshift(fft(fid));
fid  = ifft(spec);    

%determine truncation point based on data
[data.truncPoint, data.decayPoint] = getTruncationPoint(fid);
data.truncPointMasked = getTruncationPoint(ifft(spec(data.fitroi.mask)));

figure;
% set(gcf,'Position', [0.235416666666667,0.515740740740741,0.409895833333333,0.388888888888889])
subplot(2,2,1)
plot(data.t2*1e3, real(fid))
xlabel('time (ms)')
ylabel('signal (arb.u.)')
xl1 = xline(data.t2(data.decayPoint)*1e3,'-.',{'noiseBaseline'},'Color','r');
xl2 = xline(data.t2(data.truncPoint)*1e3,'-.','truncPoint','Color','k');
xl1.LabelVerticalAlignment = 'middle';
xl1.LabelHorizontalAlignment = 'right';
xl1.LabelOrientation = 'horizontal';
xl1.FontSize = 10;
xl2.LabelVerticalAlignment = 'bottom';
xl2.LabelHorizontalAlignment = 'center';
xl2.FontSize = 10;
xlim([0,max(data.t2*1e3)])

set(gcf,'Units','normalized');
xa = [230 200];
ya = [max(real(fid))*0.3 0];
[xaf,yaf] = axescoord2figurecoord(xa,ya);
ha = annotation('textarrow',xaf, yaf,'String',{'Truncation in','postprocessing:','200 ms'});
ha.HorizontalAlignment = 'center';

%% maybe add formula for noise baseline and trunc point
% xa = [ppmEnd_lcm 0]+0.1;
% ya = [spectrumInputOffset+offsetPlotsScaled 0];
% [xaf,yaf] = axescoord2figurecoord(xa,ya);
% str = {'Plot','offsets'};
% annotation('textbox',[xaf(1) yaf(1) 0.1 0.1],'String',str,'FitBoxToText','On', ...
%     'EdgeColor','none', 'FontWeight','bold','Color',arrowsColor);

data = getFidWeighting(data, 'trunc');
truncWeights = data.fid_weights;
data = getFidWeighting(data, 'sine-bell');
sineBellWeights = data.fid_weights;
data = getFidWeighting(data, 'matched_filter');
matchedFilterWeights = data.fid_weights;

subplot(2,2,3)
hold on
plot(data.t2*1e3, truncWeights, 'Color',[0 114/255 104/255])
plot(data.t2*1e3, sineBellWeights, 'Color', 'r')
plot(data.t2*1e3, matchedFilterWeights, 'Color', 'k')
legend('F_{trunc}', 'F_{sine-bell}', 'F_{matched}')
xlim([0,max(data.t2*1e3)])
xlabel('time(ms)')
ylabel('Fid weights')

subplot(2,2,[2,4])
offsetPlots = 0.1 * max(real(fft(fid)));
hold on
plot(data.ppm2, real(fft(fid)))
plot(data.ppm2, real(fft(fid .* truncWeights))-offsetPlots, 'Color',[0 114/255 104/255])
plot(data.ppm2, real(fft(fid .* sineBellWeights))-offsetPlots*2, 'Color','r')
plot(data.ppm2, real(fft(fid .* matchedFilterWeights))-offsetPlots*3, 'Color', 'k')

xlim([0.5 5.0])
ylim([min(real(fft(fid)))-offsetPlots*4 1.05 * max(real(fft(fid)))])
set(gca,'xdir','reverse')
set(gca,'ytick',[])
legend('spec.', sprintf('spec. *\nF_{trunc}'), sprintf('spec. *\nF_{sine-bell}'),...
    sprintf('spec. *\nF_{matched}'), 'Location','NorthEast')
xlabel('\delta (ppm)')
hl = ylabel('Signal (arb.u.)');
hl.HorizontalAlignment = 'right';

axes('Position',[.51 .56 .2 .37])
box on;
offsetPlots = offsetPlots /3;
hold on
plot(data.ppm2, real(fft(fid)))
plot(data.ppm2, real(fft(fid .* truncWeights))-offsetPlots, 'Color',[0 114/255 104/255])
plot(data.ppm2, real(fft(fid .* sineBellWeights))-offsetPlots*2, 'Color','r')
plot(data.ppm2, real(fft(fid .* matchedFilterWeights))-offsetPlots*3, 'Color', 'k')

xlim([3.95 4.5])
set(gca,'xdir','reverse')
set(gca,'ytick',[])

% Some arrangments are still needed manually. Way easier than coding it.
end
