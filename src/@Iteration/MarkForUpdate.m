function this = MarkForUpdate(this, ~, ~)
    % this function should be called as a response to notifications or
    % other listener events that should mark the object as outdated

    this.needUpdate = true;
end
