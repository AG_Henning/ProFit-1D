function this = ConnectMets(this, mets, type)
    % this function sets up internal data structures that will be parsed by
    % UpdateIteration and the neccessary information about which parameters
    % of different metabolite signals are linked
    
    % rudimentary check input data
    if isempty(mets)
        error('Iteration:ConnectMets:NotEnoughInput', 'Not enough metabolite input given');
    end
    switch class(mets)
        case 'cell'
            if (any(ismember(mets, {'all', 'global', 'individual', 'name'})) && (length(mets)~=1))
                error('Iteration:ConnectMets:TooManyArguments', 'Too many input arguments in metabolite list');
            end
        case 'double'
            if (min(mets)<1 || max(mets)>this.nr_mets)
                error('Iteration:ConnectMets:IndexExceeds', 'Supplied metabolite signal indices exceed array dimensions');
            end
            mets = this.basis.met(mets);
        case 'logical'
            if ~any(mets)
                error('Iteration:ConnectMets:NoMetsSelect', 'No metabolite signal is selected');
            end
            if (length(mets)~=this.nr_mets)
                error('Iteration:ConnectMets:IndexExceeds', 'Supplied metabolite signal indices exceed array dimensions');
            end
            mets = this.basis.met(mets);
        otherwise
            error('Iteration:ConnectMets:WrongType', 'Input arguments need to be cell strings, double array or logical array');
    end
    
    if isempty(type)
        error('Iteration:ConnectMets:NotEnoughInput', 'Not enough type input given');
    end
    if ~iscellstr(type)
        error('Iteration:ConnectMets:WrongType', 'Input arguments to type need to be cell strings');
    end
    
    if (strcmpi(type, 'all'));
        mobj = ?Parameters;
        type = cellfun(@(prop) prop.Name, mobj.Properties, 'UniformOutput', false);
    end
    
    % handle string list of metabolite signal names
    switch mets{1}
        case 'global'
            for type_cnt = 1:length(type)
                current_type = type{type_cnt};
                try
                    this.met_connections.(current_type) = true(this.nr_mets);
                catch exception
                    error('Iteration:ConnectMets:WrongArgument', 'The specified parameter type doesn''t exist');
                end
            end
        case 'individual'
            for type_cnt = 1:length(type)
                current_type = type{type_cnt};
                try
                    this.met_connections.(current_type) = logical(eye(this.nr_mets));
                catch exception
                    error('Iteration:ConnectMets:WrongArgument', 'The specified parameter type doesn''t exist');
                end
            end
        case 'name'
            mets = this.basis.met;
            for type_cnt = 1:length(type)
                current_type = type{type_cnt};
                for met_cnt = 1:length(mets)
                    current_met = mets{met_cnt};
                    try
                        idx = this.met_idx(current_met);
                    catch exception
                        error('Iteration:ConnectMets:NoKey', 'The specified metabolite signals do not exist');
                    end
                    for idx_cnt = 1:length(idx)
                        try
                            this.met_connections.(current_type)(idx(idx_cnt), :) = false(1,this.nr_mets);
                            this.met_connections.(current_type)(idx(idx_cnt), idx) = true;
                        catch exception
                            error('Iteration:ConnectMets:WrongArgument', 'The specified parameter type doesn''t exist');
                        end
                    end
                end
            end
        otherwise
            if (strcmpi(mets, 'all'));
                mets = this.basis.met;
            end
            
            for type_cnt = 1:length(type)
                current_type = type{type_cnt};
                idx =[];
                for met_cnt = 1:length(mets)
                    current_met = mets{met_cnt};
                    try
                        idx = horzcat(idx, this.met_idx(current_met));
                    catch exception
                        error('Iteration:ConnectMets:NoKey', 'The specified metabolite signals do not exist');
                    end
                end
                for idx_cnt = 1:length(idx)
                    try
                        this.met_connections.(current_type)(idx(idx_cnt), :) = false(1,this.nr_mets);
                        this.met_connections.(current_type)(idx(idx_cnt), idx) = true;
                        this.met_connections.(current_type)(:, idx(idx_cnt)) = false(this.nr_mets, 1);
                        this.met_connections.(current_type)(idx, idx(idx_cnt)) = true;
                    catch exception
                        error('Iteration:ConnectMets:WrongArgument', 'The specified parameter type doesn''t exist');
                    end
                end
            end
    end
    % mark for needed update
    this.needUpdate = true;
end
