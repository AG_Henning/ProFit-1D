function ValueInputValidator(values)

    if isempty(values)
        error('Iteration:ValuesValidator:NotEnoughInput', 'Not enough values input given');
    end
    if ~(ismember(class(values), {'double', 'logical'}))
        error('Iteration:ValuesValidator:WrongType', 'Wrong input types given');
    end
    if (length(values)>1)
        error('Iteration:ValuesValidator:TooMuchInput', 'Too much value input given');
    end
end