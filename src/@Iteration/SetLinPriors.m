function this = SetLinPriors(this, mets, type, values)
    
    mets = this.MetInputValidator(this, mets);
    type = this.TypeInputValidator(type);
    
    if ~isempty(values)
        prior_mx = this.CreateLinPriorMatrix(this, mets, values);
    else
        prior_mx = values;
    end
    
%     this.full_lin_priors.Fill(type, prior_mx);
    this.user_full_lin_priors.Fill(type, prior_mx);

    % mark for needed update
    this.needUpdate = true;
end