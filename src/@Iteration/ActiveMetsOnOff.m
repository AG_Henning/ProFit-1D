function this =  ActiveMetsOnOff(this, mets, onoff)
    % handle the actual activation or deactivation of specific metabolite
    % signals
    % control the logical index vector that contains the currently active metabolites
    % the input can either be cellstring array containing the signal names, a
    % double vector containing the indices or a logical vector

    switch class(mets)

        case 'cell'

            if ((length(mets)==1) && strcmpi(mets, 'all'))
                mets = this.met_idx.keys;
            end
            
            active_mets_backup = this.active_mets;     % in case of an error restore state before call
            for met_cnt = 1:length(mets)
                current_met = mets{met_cnt};

                try
                    this.active_mets(this.met_idx(current_met)) = onoff;
                catch exeption
                    this.active_mets = active_mets_backup;
                    error('Iteration:MetsOnOff:WrongMet', 'The specified metabolite signal "%s" does not exist.', current_met);
                end
            end

        case 'double'

            if any(mets<1 | mets>this.nr_mets)
                error('Iteration:MetsOnOff:IndexExceeds', 'Some indices exceed the number of available metabolte signals');
            end
            this.active_mets(mets) = onoff;

        case 'logical'

            if (length(mets)~=this.nr_mets)
                error('Iteration:MetsOnOff:IndexExceeds', 'Some indices exceed the number of available metabolte signals');
            end
            this.active_mets(mets) = onoff;

        otherwise
            error('Iteration:MetsOnOff:WrongInputFormat', 'The specified input arguments are not recognized');
    end
 
end