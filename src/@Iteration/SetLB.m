function this = SetLB(this, mets, type, values)

    this.SetInternalParameters('this.full_lower_bounds', mets, type, values);
    
    % mark for needed update
    this.needUpdate = true;
end