function this = SetInternalParameters(this, par, mets, type, values)

    mets = this.MetInputValidator(this, mets);
    type = this.TypeInputValidator(type);
    this.ValueInputValidator(values);
    
    for type_cnt = 1:length(type)
        current_type = type{type_cnt};
        idx =[];
        for met_cnt = 1:length(mets)
            current_met = mets{met_cnt};
            idx = horzcat(idx, this.met_idx(current_met));
        end
        % set the specified parameter field
        eval([par '.(current_type)(idx) = ' num2str(values) ';']);
    end
end