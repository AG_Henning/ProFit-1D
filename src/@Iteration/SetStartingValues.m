function this = SetStartingValues(this, mets, type, values)

    this.SetInternalParameters('this.full_start_values', mets, type, values);
    
    % mark for needed update
    this.needUpdate = true;
end