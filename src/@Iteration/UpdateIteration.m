function this = UpdateIteration(this)
    % This function is reponsible to aggregate all the currently configured
    % parameters and update the internal and external properties
    % accordingly. Every time some change of a parameter in the iteration
    % object is done, this function has to be called to make these
    % configurations applicable
    
    % calculate index properties
    mobj = ?Parameters;
    for par_cnt=1:length(mobj.Properties)
        current_par = mobj.Properties{par_cnt}.Name;
        
        % calculating index parameters properties
        tmp_idx = this.met_connections.(current_par);
        tmp_idx = triu(tmp_idx,1);
        tmp_idx_lookback = tril(this.met_connections.(current_par),-1);
        running_idx = 1;
        for met_cnt=1:this.nr_mets
            pos = false(1,this.nr_mets);
            if ~any(tmp_idx_lookback(met_cnt,:))
                pos = tmp_idx(met_cnt,:);
                pos(met_cnt) = true;
            end
            this.idx.(current_par)(pos) = running_idx;
            if any(pos)
                running_idx = running_idx + 1;
            end
        end
        
        % calculate full mask properties
        row_idx = this.idx.(current_par);
        col_idx = 1:this.nr_mets;
        act_idx = sub2ind(size(this.full_mask.(current_par).full), row_idx(this.active_mets), col_idx(this.active_mets));
        
        % setup full masking matrix
        this.full_mask.(current_par).full = false(this.nr_mets);
        this.full_mask.(current_par).full(act_idx) = true;
        % set up full matrix of fixed parameters
        this.full_mask.(current_par).fixed = this.full_mask.(current_par).full & repmat(this.fixed_mets.(current_par), [this.nr_mets 1]);
        % set up full matrix of free parameters
        this.full_mask.(current_par).free = this.full_mask.(current_par).full & repmat(~this.fixed_mets.(current_par), [this.nr_mets 1]);
        
        % create submatrices according to the metabolite activation pattern
        row_map = any(this.full_mask.(current_par).full, 2);
        col_map = any(this.full_mask.(current_par).full, 1);
        
        % create mapping matrix use this to translate indices
        % from sub matrices to full matrices
        tmp_mapping_mx = reshape(1:this.nr_mets*this.nr_mets, this.nr_mets*ones(1,2));
        this.mapping_mx.(current_par) = tmp_mapping_mx(row_map, col_map);
        
        % create reduced masking specific for that iteration and activated
        % metabolites
        this.sub_mask.(current_par).full  = this.full_mask.(current_par).full(row_map, col_map);
        this.sub_mask.(current_par).fixed = this.full_mask.(current_par).fixed(row_map, col_map);
        this.sub_mask.(current_par).free  = this.full_mask.(current_par).free(row_map, col_map);
        
        this.nr.(current_par).fixed = sum(any(this.sub_mask.(current_par).fixed, 2));
        this.nr.(current_par).free  = sum(any(this.sub_mask.(current_par).free, 2));
        this.nr.(current_par).full  = this.nr.(current_par).fixed + this.nr.(current_par).free;
        
        % calculate index maps for free parameters. this is needed to
        % identify in which line of the sub_masks the actual free
        % metabolite indices are located. the result is logical array with
        % set true at the correct line positions.
        % get rid of columns containing no free parameters
        free_cols = this.sub_mask.(current_par).free(:,any(this.sub_mask.(current_par).free,1));  
        % get rid of duplicates columns
        [ ~, uidx] = unique(free_cols', 'rows', 'first');
        free_cols = free_cols(:, sort(uidx));

        % update starting values
        start_val = nan(1, this.nr.(current_par).free);
        for free_cnt=1:this.nr.(current_par).free
            mapping_idx = this.mapping_mx.(current_par)(free_cols(:,free_cnt), this.sub_mask.(current_par).free(free_cols(:,free_cnt),:));
            [~, col_idx] = ind2sub(this.nr_mets*ones(1,2), mapping_idx);
            tmp = this.full_start_values.(current_par)(col_idx);
            if length(tmp)>1
                if ~all(tmp==tmp(1))
                    warning('conflicting starting values. Taking mean value');
                end
                tmp = mean(tmp);
            end
            start_val(free_cnt) = tmp;
        end
        this.start_values.(current_par) = start_val;
        
        % update lb/ub for active metabolites
        lb = nan(1, this.nr.(current_par).free);
        ub = nan(1, this.nr.(current_par).free);
        for free_cnt=1:this.nr.(current_par).free
            % go through every free parameter and find the
            % corresponding positions in the sub mask in the full
            % matrix and translate that to colum positions of the
            % full bound vectors. read all values of connected
            % parameters and through one away after issuing a
            % warning
            mapping_idx = this.mapping_mx.(current_par)(free_cols(:,free_cnt), this.sub_mask.(current_par).free(free_cols(:,free_cnt),:));
            [~, col_idx] = ind2sub(this.nr_mets*ones(1,2), mapping_idx);
            tmp = this.full_lower_bounds.(current_par)(col_idx);
            tmp(isnan(tmp)) = -Inf;
            if length(tmp)>1
                if ~all(tmp==min(tmp))
                    warning('conflicting lower bounds. Taking lower one');
                end
                tmp = min(tmp);
            end
            lb(free_cnt) = tmp;
            
            tmp = this.full_upper_bounds.(current_par)(col_idx);
            tmp(isnan(tmp)) = Inf;
            if length(tmp)>1
                if ~all(tmp==max(tmp))
                    warning('conflicting uppper bounds. Taking highest one');
                end
                tmp = max(tmp);
            end
            ub(free_cnt) = tmp;
        end
        this.lower_bounds.(current_par) = lb;
        this.upper_bounds.(current_par) = ub;
        
        % update matrix of linear inequality constraints for active
        % free metabolites
        this.ResolveConncetionsLinPriorMatrix(current_par);
        if ~isempty(this.full_lin_priors.(current_par))
            active_full = any(this.full_mask.(current_par).full, 1);
            % cut out active columns
            tmp_priors = this.full_lin_priors.(current_par)(:, active_full);
            % clean empty lines
            tmp_priors = tmp_priors(any(tmp_priors, 2), :);
            % pre-allocate memory
            reduced_priors = zeros(size(tmp_priors,1), this.nr.(current_par).free);
            
            % collapse columns that are linked together by idx
            for free_cnt=1:this.nr.(current_par).free
                current_line = this.sub_mask.(current_par).free(free_cols(:,free_cnt), :);
                if sum(current_line)>1
                    conflict_pos = all(tmp_priors(:, current_line), 2);
                    if any(conflict_pos)
                        error('Iteration:UpdateIteration:LinPriorConflict', 'Conflicting Linear Constraints in priors lines %d\n', conflict_pos);
                    end
                end
                
                % map all values from the connected cols to
                % one
                connected_cols = tmp_priors(:, current_line);
                value_idx = connected_cols~=0;
                
                % if only a few row elements are set just copy the first
                % column of the connected parameters for non
                % set column entries
                if any(~any(value_idx, 2))
                    value_idx(~any(value_idx, 2),1) = true;
                end
                
                % totake the values out of the correct columns
                % some resorting needs to be done because the
                % assignments need to be made line wise and not
                % column wise which is the usual matlab way
                [row, col] = find(value_idx==true);
                [sorted_row, sort_idx] = sort(row);
                sorted_col = col(sort_idx);
                value_pos = sub2ind(size(value_idx), sorted_row, sorted_col);
                
                new_col = connected_cols(value_pos);
                reduced_priors(:, free_cnt) = new_col;
            end
            
            % check and remove duplicated lines
            [ ~, uidx] = unique(reduced_priors, 'rows', 'first');
            reduced_priors = reduced_priors(sort(uidx), :);
            
            % check if only one element is left in a line ->
            % this should ne stated as lower/upper bound but
            % this is not done automatically
            if any(sum(reduced_priors~=0, 2)==1)
                warning('One line contains only one factor. This should be stated as lower/bounds. This line will be deleted!');
            end
            
            % further conflict will not be investigated at this
            % point
        else
            reduced_priors = [];
        end
        this.lin_priors.(current_par) = reduced_priors;

        % update fixed values for active metabolites
        this.fixed_values.(current_par) = this.full_fixed_values.(current_par)(this.active_mets);
    end
       
    this.apply_sp_lineshape = this.full_apply_sp_lineshape(this.active_mets);
    this.needUpdate = false;
end