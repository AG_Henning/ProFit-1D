function type = TypeInputValidator(type)
    % check the consistency of the supplied parameter array. Often used
    % code therefore put into a separate function
    
    if isempty(type)
        error('Iteration:TypeValidator:NotEnoughInput', 'Not enough metabolite input given');
    end
    if ~iscellstr(type)
        error('Iteration:TypeValidator:WrongType', 'Input arguments to type need to be cell strings');
    end
    if (any(strcmpi(type, 'all')) && (length(type)~=1))
        error('Iteration:TypeValidator:TooManyArguments', 'Too many input arguments in metabolite list');
    end
    
    mobj = ?Parameters;
    available_types   = cellfun(@(x) x.Name, mobj.Properties, 'UniformOutput', false);
    
    if ~all(ismember(type, [available_types; {'all'}]))
        error('Iteration:TypeValidator:NoKey', 'The specified metabolite signals do not exist');
    end
    
    if strcmpi(type, 'all');
        type = available_types;
    end
end