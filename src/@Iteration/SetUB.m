function this = SetUB(this, mets, type, values)

    this.SetInternalParameters('this.full_upper_bounds', mets, type, values);
        
    % mark for needed update
    this.needUpdate = true;
end