function this = ResolveConncetionsLinPriorMatrix(this, type)

    tmp_prior_mx = [];
    for lin_cnt=1:size(this.user_full_lin_priors.(type),1)
        mets_idx    = find(this.user_full_lin_priors.(type)(lin_cnt,:)~=0);

        for mm=1:length(mets_idx)
            idx = this.met_connections.(type)(mets_idx(mm),:);
            idx_mx = diag(idx);
            idx_mx = idx_mx(any(idx_mx,2)~=0,:);    % remove all zeros lines
            if ((mm>1))% && size(idx_mx,1)>1)
                n = size(idx_mx, 1);
                prior_mx = repmat(prior_mx, [n 1]);
                idx_mx = repmat(idx_mx, [size(prior_mx,1)/n 1]);
                cc = [(1:n); n*ones(1,n)];
                eval(['idx_mx = [' sprintf('idx_mx(%d:%d:end,:); ', cc) '];']);
            else
                prior_mx = zeros(size(idx_mx));
            end
            prior_mx(idx_mx(:)) = this.user_full_lin_priors.(type)(lin_cnt, mets_idx(mm));
        end
        tmp_prior_mx = vertcat(tmp_prior_mx, prior_mx);
    end
    
    this.full_lin_priors.(type) = tmp_prior_mx;
end