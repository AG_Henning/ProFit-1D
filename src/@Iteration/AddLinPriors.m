function this = AddLinPriors(this, mets, type, values)

    mets = this.MetInputValidator(this, mets);
    type = this.TypeInputValidator(type);

    new_priors = this.CreateLinPriorMatrix(this, mets, values);
    
    for par_cnt = type
        current_par = char(par_cnt);
%         this.full_lin_priors.(current_par) = vertcat(this.full_lin_priors.(current_par), new_priors);
        this.user_full_lin_priors.(current_par) = vertcat(this.user_full_lin_priors.(current_par), new_priors);
    end
    
    % mark for needed update
    this.needUpdate = true;
end