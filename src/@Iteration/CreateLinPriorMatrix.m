function prior_mx = CreateLinPriorMatrix(obj, mets, values)

    % check consistent input values
    if ~isnumeric(values)
        error('Iteration:CreatePriorsMx:WrongType', 'The specified values are of wrong type');
    end
    if length(values)~=length(mets)
        error('Iteration:CreatePriorsMx:InconsArgs', 'Inconsistent arguments');
    end
    
    for mm=1:length(mets)
        idx = obj.met_boolidx(mets{mm});
        idx_mx = diag(idx);
        idx_mx = idx_mx(any(idx_mx,2)~=0,:);
        if ((mm>1))% && size(idx_mx,1)>1)
            n = size(idx_mx, 1);
            prior_mx = repmat(prior_mx, [n 1]);
            idx_mx = repmat(idx_mx, [size(prior_mx,1)/n 1]);
            cc = [(1:n); n*ones(1,n)];
            eval(['idx_mx = [' sprintf('idx_mx(%d:%d:end,:); ', cc) '];']);
        else
            prior_mx = zeros(size(idx_mx));
        end
        prior_mx(idx_mx(:)) = values(mm);
    end
end