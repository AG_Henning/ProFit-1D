function mets = MetInputValidator(obj, mets)
    % check the consistency of the supplied metabolite array. Often used
    % code therefore put into a separate function
    
    if isempty(mets)
        error('Iteration:MetValidator:NotEnoughInput', 'Not enough metabolite input given');
    end   
    
    switch class(mets)
        case 'cell'
            if ~iscellstr(mets)
                error('Iteration:MetValidator:WrongType', 'Input arguments need to be cell strings, double array or logical array');
            end
            if (any(strcmpi(mets, 'all')) && (length(mets)~=1))
                error('Iteration:MetValidator:TooManyArguments', 'Too many input arguments in metabolite list');
            end
            if (strcmpi(mets, 'all'));
                mets = obj.basis.met;
            end
        case 'double'
            if (min(mets)<1 || max(mets)>obj.nr_mets)
                error('Iteration:MetValidator:IndexExceeds', 'Supplied metabolite signal indices exceed array dimensions');
            end
            mets = obj.basis.met(mets);
        case 'logical'
            if ~any(mets)
                error('Iteration:MetValidator:NoMetsSelect', 'No metabolite signal is selected');
            end
            if (length(mets)~=obj.nr_mets)
                error('Iteration:MetValidator:IndexExceeds', 'Supplied metabolite signal indices exceed array dimensions');
            end
            mets = obj.basis.met(mets);
        otherwise
            error('Iteration:MetValidator:WrongType', 'Input arguments need to be cell strings, double array or logical array');
    end
    
    if ~all(ismember(mets, [obj.met_idx.keys() {'all'}]))
       error('Iteration:MetValidator:NoKey', 'The specified metabolite signals do not exist'); 
    end
end