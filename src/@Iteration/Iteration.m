classdef Iteration < handle
    % This class contains all the configurable information that represents a
    % full iteration within the profit main loop. All kinds of aspects from
    % starting values, upper/lower bounds, free/fixed parameter conntected
    % parameters are saved and processed within this object.
    
    properties (SetObservable)
        
        active_mets;
    end
    
    properties (Access=public)
                
        % this properties should be used to hold function handles to the
        % functions that can change starting values and constraints from
        % previous iterations. These actual function are implemented externally and
        % shoul expect the current iteration object and the result object
        % of the previous iteration as input arguments. A updated iteration
        % object needs to be returned.
        UpdateStartingValues;
        UpdateLowerBounds;
        UpdateUpperBounds;
        
        % booleans to include or exclude spline baseline or spline line
        % shape model the default value is false
        estimate_sp_baseline_stiffness;
        use_sp_baseline;
        use_sp_lineshape;
        % be careful setting these arrays. These should probably be
        % accessed more securly by stil to implement set functions
        full_apply_sp_lineshape; % boolean array to set specifically to which basis signals global lineshape model will be applied
        apply_sp_lineshape; % boolean array to set specifically to which basis signals global lineshape model will be applied
 
        % spline baseline relevant properties
        spbase_knot_spacing;
        spbase_lambda;
        spbase_ed_vec;
        spbase_optim_model_vec;
        spbase_optim_idx;
    end
    
    properties (Dependent)

        nr_act_mets;     % number of active metabolites for this iteration
    end
    
    properties (SetAccess=protected)
        
        basis;          % link to the basis set that is associated with this fit iteration
        met_idx;        % containing a hash table with basis signal names as keys and double indices as values
        met_boolidx;    % containing a hash table with basis signal names as keys and logical indices as values
        nr_mets;        % number of metabolites included in the basis set that is associated with this object instance
        
        met_connections; % a Parameter object holding mapping matrices for containing information about connected signal parameters
        idx;            % containing the relationship between certain metabolites for each fittable parameter
        fixed_mets;     % class of logical vectors specifiying which parameters are fixed to certain values for which metabolites in the basis set

        full_mask;      % contains a full matrix of fittable parameters vs. metabolites and their relationship with respect to the full available basis
        sub_mask;       % contains a reduced matrix of fittable parameters vs. metabolites and their relationship with respect to only the activated metabolites
        nr;             % information of number of parameters, how many are free and how many are fixed
        mapping_mx;     % needed to calculate signal relations
        
        full_start_values;  % contains all starting values that will be used for the fit
        full_lower_bounds;  % contains all lower bounds on the parameters that will be used for the fit
        full_upper_bounds;  % contains all upper bounds on the parameters that will be used for the fit
        full_fixed_values;  % the actual fixed value that will be used for a specific parameter of a specific metabolite
        full_lin_priors;    % contains all linear contraint prior knowledge on the parameters that will be used for the fit
        user_full_lin_priors; % temporary storage so full_lin_priors won't explode
        
        % the following properties hold the corresponding values only of
        % the currently activated metabolites
        fixed_values;   
        start_values;
        upper_bounds;
        lower_bounds;
        lin_priors;
                
        hUpdates;       % structure to store listener events so that an needed update can be shown
        needUpdate;     % boolean to check if masks and output matrices should be updated
    end
    
    methods
        
        function this = Iteration(BasisSet)
            % The constructor of this object takes an instance of a basis
            % set object and two strings that specify the class
            % implementations of Parameters and ParameterCollections. To
            % keep it as configurable and testable as possible the class
            % name is assigned as a string and evaluated to create an
            % instance.
            
            this.basis = BasisSet;
            this.nr_mets = length(this.basis.met);
            this.active_mets = false(1, this.nr_mets);
            
            this.met_connections = Parameters;
            this.idx             = Parameters;
            this.fixed_mets      = Parameters;
            
            this.full_mask   = ParameterCollection;
            this.sub_mask    = ParameterCollection;
            this.nr          = ParameterCollection;
            this.mapping_mx  = Parameters;
            
            this.full_start_values = Parameters;
            this.full_lower_bounds = Parameters;
            this.full_upper_bounds = Parameters;
            this.full_fixed_values = Parameters;
            this.full_lin_priors   = Parameters;
            this.user_full_lin_priors = Parameters;
            
            this.start_values = Parameters;
            this.upper_bounds = Parameters;
            this.lower_bounds = Parameters;
            this.lin_priors   = Parameters;
            this.fixed_values = Parameters;
            
            % by default just return the same object for updating
            % function handles
            this.UpdateStartingValues = @(obj,res) this;
            this.UpdateLowerBounds    = @(obj,res) this;
            this.UpdateUpperBounds    = @(obj,res) this;
            
            this.InitValues();
            this.CreateListeners();
            this.SetupHashTables();
        end
        
        function value = get.nr_act_mets(this)
            
            value = sum(this.active_mets);
        end
        
        this = ActivateMets(this, mets);
        this = DeactivateMets(this, mets);
        this = UpdateIteration(this);
        this = ConnectMets(this, mets, type);
        this = FixMets(this, mets, type);
        this = FreeMets(this, mets, type);
        this = SetStartingValues(this, mets, type, values);
        this = SetLB(this, mets, type, values);
        this = SetUB(this, mets, type, values);
        this = SetFixedValues(this, mets, type, values);
        this = SetLinPriors(this, mets, type, values);
        this = AddLinPriors(this, mets, type, values);
    end
    
    methods (Access = private)
        
        this = SetupHashTables(this);
        this = CreateListeners(this);
        this = ResolveConncetionsLinPriorMatrix(this, type);
        this = MarkForUpdate(this, ~, ~);
        this = ActiveMetsOnOff(this, mets, onoff);
        this = InitValues(this);
        this = SetInternalParameters(this, par, mets, type, values);
    end
    
    methods (Static = true)
       mets = MetInputValidator(obj, mets);
       type = TypeInputValidator(type);
       ValueInputValidator(values);
       prior_mx = CreateLinPriorMatrix(obj, mets, values);
    end
    
end
