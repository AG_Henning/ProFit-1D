function this = CreateListeners(this)
    % this function had to be created so it can also be used by the
    % loadobj function to correctly initialize the object

    this.hUpdates{1} = addlistener(this, 'active_mets', 'PostSet', @this.MarkForUpdate);
    this.hUpdates{2} = addlistener(this.met_connections, 'needUpdate', @this.MarkForUpdate);
end