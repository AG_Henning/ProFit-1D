function this = FixMets(this, mets, type)
    % this function sets up internal data structures that will be parsed by
    % UpdateIteration and the neccessary information about which parameters
    % of different metabolite signals should be handled as fixed parameters
    % during the fitting procedure
    
    this.SetInternalParameters('this.fixed_mets', mets, type, true);

    % mark for needed update
    this.needUpdate = true;
end