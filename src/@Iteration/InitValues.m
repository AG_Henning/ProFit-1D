function this = InitValues(this)
    % initialize some properties to certain values
    
    this.active_mets = false(1, this.nr_mets);
    this.full_mask.Fill({'all'}, {'all'}, false(this.nr_mets));
    this.met_connections.Fill({'all'}, logical(eye(this.nr_mets)));
    this.idx.Fill({'all'}, 1:this.nr_mets);
    this.fixed_mets.Fill({'all'}, false(1,this.nr_mets));
    this.full_start_values.Fill({'conc'}, ones(1, this.nr_mets));
    this.full_start_values.Fill({'em', 'gm', 'pc0', 'pc12', 'df2'}, zeros(1, this.nr_mets));
    this.full_lower_bounds.Fill({'all'}, nan(1, this.nr_mets));
    this.full_upper_bounds.Fill({'all'}, nan(1, this.nr_mets));
    this.full_fixed_values.Fill({'all'}, nan(1, this.nr_mets));
    this.estimate_sp_baseline_stiffness = false;
    this.use_sp_baseline = false;
    this.use_sp_lineshape = false;
    this.full_apply_sp_lineshape = true(1, this.nr_mets);
    this.needUpdate = false;
end
