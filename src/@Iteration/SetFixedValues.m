function this = SetFixedValues(this, mets, type, values)

    this.SetInternalParameters('this.full_fixed_values', mets, type, values);
    
    % mark for needed update
    this.needUpdate = true;
end