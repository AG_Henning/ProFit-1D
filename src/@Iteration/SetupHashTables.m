function this = SetupHashTables(this)
    % create hash tables using the signal names contained in the basis set
    % associated with this object as key.
    % create one table containing the double indices for each signal name
    % and on table containing the logical indices for each signal name

    % Matlab doesn't seem to recognize an array of doubles or logicals to
    % be proper Valuetypes for map containers. So the ValueType has to be
    % set to any for the moment
    this.met_idx = containers.Map('KeyType', 'char', 'ValueType', 'any');
    this.met_boolidx = containers.Map('KeyType', 'char', 'ValueType', 'any');

    sig_names = unique(this.basis.met);
    for name_cnt = 1:length(sig_names)
        current_name = sig_names{name_cnt};
        this.met_idx(current_name) = find(strcmp(this.basis.met(:), current_name)');
        this.met_boolidx(current_name) = logical(strcmp(this.basis.met(:), current_name)');
    end
end