classdef basis_set < handle
    
    properties
        f0;     % resonance frequency
        dt;     % sampling frequency in both dimensions
        bw;     % bandwidth in both dimensions
        pts;    % number of points in both dimensions
        met;    % names of the included metabolites
        
        fid;    % actual time domain data of the metabolites
        
    end
    
   
    methods
        
        function this = basis_set(param1, param2)
            
            if (nargin>0)
                if (nargin == 1)
                    sim_infos = param1;
                    this.f0  = sim_infos.f0;
                    this.dt  = sim_infos.dt;
                    this.bw  = sim_infos.bw;
                    this.pts = sim_infos.pts;
                    
                    cmd = sim_infos.cmd;
                    for zz=1:length(sim_infos.met)
                        % parse command string
                        tmp = regexp( sim_infos.cmd, '\$met\$$');
                        cmd{logical(~cellfun(@isempty, tmp))} =  sim_infos.met{zz}.filename ;
                        tmp = regexp( sim_infos.cmd, '\$met_scale\$$');
                        cmd{logical(~cellfun(@isempty, tmp))} =  num2str(sim_infos.met{zz}.scaling) ;
                        cmd_str = sprintf('%s ', cmd{:})
                        system(cmd_str);
                        load(sim_infos.met{zz}.filename, 'fid');
                        
                        this.fid{1, zz}  = fid;
                        this.met{1, zz} = sim_infos.met{zz}.name;
                    end
                else
                    basisStruct = param1;
                    this.f0  = basisStruct.f0;
                    this.dt  = basisStruct.dt;
                    this.bw  = basisStruct.bw;
                    this.pts = basisStruct.pts;
                    
                    for zz=1:length(basisStruct.met)
                        this.fid{1, zz}  = basisStruct.fid{1,zz};
                        this.met{1, zz} = basisStruct.met{1,zz};
                    end
                end
            end
            
        end
        
        function this = SetBaseline(this, mmfid)
           
            if ~all(size(mmfid)==this.pts)
                error('wrong size of input baseline FID');
            end
            
            this.met = horzcat(this.met, 'baseline');
            this.fid = horzcat(this.fid, mmfid);
        end
        
    end    
end