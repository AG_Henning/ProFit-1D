classdef ProfitSplineBaseLine < ProfitSplineBase
    %PROFITSPLINEBASELINE calculates the base line and the regularization
    %terms for the spline base line
    
    properties
        %uses the ones from the base class
    end
    
    methods (Access = public)
        %constructor of the base class
        function this = ProfitSplineBaseLine(fitobj, data)
            %call parent constructor
            this@ProfitSplineBase(data);
            
            knot2knot = fitobj.spbase_knot_spacing;    % knot spacing in ppm
            maxResolution = 1./data.pts_ppm;
            if any(knot2knot < maxResolution)
                knot2knot(knot2knot < maxResolution) = maxResolution(knot2knot < maxResolution);
                fprintf('Due to resolution limits the knot to knot spacing had to be changed to %4.2f / %4.2f ppm\n', knot2knot(1), knot2knot(2));
            end
            
            cutRange = [min(data.fitroi.ppm1) max(data.fitroi.ppm1); ...
                min(data.fitroi.ppm2) max(data.fitroi.ppm2)];
            numberOfKnots = round(diff(cutRange,1,2)'./knot2knot);
            gridDimension = diff(data.fitroi.bounds, [], 1) + 1;
            
            %iterate over the dimensions and create the B-splines for each
            %dimension
            for dimension = ndims(numberOfKnots) : -1 : ndims(numberOfKnots) - this.splineDimensions_ + 1
                this.createBSpline( ndims(numberOfKnots) - dimension + 1, gridDimension(dimension), numberOfKnots(dimension))
            end
            
            switch this.splineDimensions_
                %TODO ? does an iteration over the dimensions make more
                %sense?
                case 1
                    %TODO: check if correct for 1D
                    %use B_spline in y direction only. i.e. the first dim
                    this.bSpline_ = this.bSplines_{1};
                case 2
                    %create 2D Bspline
                    this.bSpline = sparse(kron(this.bSplines_{1}, this.bSplines_{2}));
            end% other dimensionalities not handled. Should have been taken care of before
        end
    end
    
    methods (Access = protected)
        %create a B_spline based on the grid dimension and the number of knots
        function this = createBSpline( this, dimension, gridDimension, numberOfKnots )
            createBSpline@ProfitSplineBase(this, dimension, gridDimension, numberOfKnots)
        end
    end
    
    methods (Access = public)
        %calculate and return the regularization matrix corresponding to
        %the b-spline created in the class
        function [RegularizationMatrix] = calculateRegularization(this)
%             %Calculate B-Spline and Regularization term
%             switch this.splineDimensions_
%                 case 1
%                     %get regularization matrix for the y direction
%                     RegularizationMatrix = set_up_spline_regularization_matrix(ky, knotsy);
%                 case 2
%                     %get regularization matrix for the x direction
%                     RegularizationX = set_up_spline_regularization_matrix(kx, knotsx);
%                     %get regularization matrix for the y direction
%                     RegularizationY = set_up_spline_regularization_matrix(ky, knotsy);
%                     %create regularization matrix P for the entire domain using stencil
%                     %[0  1  0]
%                     %[1 -4  1]
%                     %[0  1  0]
%                     RegularizationTermX = kron(eye(length(knotsy)-kx), RegularizationX);
%                     RegularizationTermY = kron(RegularizationY, eye(length(knotsx)-ky));
%                     RegularizationMatrix = (RegularizationTermX + RegularizationTermY);
        end
    end
    
end

