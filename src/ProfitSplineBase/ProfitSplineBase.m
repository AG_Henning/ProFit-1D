classdef (Abstract) ProfitSplineBase
    %PROFITSPLINEBASE abstract class defining basic functionality for
    %BSplines
    
    properties (Access = protected)
        %number of dimensions used for the splines
        splineDimensions_
        %vector containing the spline order for each dimension
        splineOrders_
        %matrix containing the knot sequences for each dimension
        knotSequences_
        %cell array containing the B-splines for each dimension
        bSplines_
        %matrix containing the B-spline for the data
        bSpline_
    end
    
    methods (Access = protected)
        %constructor of the base class
        function this = ProfitSplineBase(data)
            this.splineDimensions_ = data.spectral_dimension;
        end
        
        %create a B_spline based on the grid dimension and the number of knots
        function this = createBSpline( this, dimension, gridDimension, numberOfKnots )
            %CREATE_B_SPLINE create a B_spline given the grid dimension and the number
            %of knots
            
            % create a cubic spline order
            this.splineOrders_(dimension) = 4;
            % create knot sequence
            this.knotSequences_{dimension} = augknt(linspace(1, gridDimension, numberOfKnots), this.splineOrders_(dimension));
            %generate spline
            this.bSplines_{dimension} = sparse(spcol(this.knotSequences_{dimension}, this.splineOrders_(dimension), linspace(1,gridDimension, gridDimension)));
        end
    end
    methods (Access = public , Abstract)
        %calculate and return the regularization matrix corresponding to
        %the b-spline created in the class
        [RegularizationMatrix] = calculateRegularization(this)
    end
    
    methods (Access = public)
        %getter for the knotSequences variable. Returns the knot
        %sequences constructed at the class declaration
        function [knotSequences] = getKnotSequences(this)
            knotSequences = this.knotSequences_;
        end
        
        %getter for the Bspline variable. Returns the BSpline
        %constructed at the class declaration
        function BSpline = getBSpline(this)
            BSpline = this.bSpline_;
        end
    end
    
end

