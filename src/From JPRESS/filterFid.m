function [filteredFid]=filterFid(fid, te, bandWidth, dwellTime1, gauss1, gauss2, exp1)
%
%         gauss1 = 2;
%         gauss2 = 2;
%         exp1 = 0;
%         dwellTime1 = 2;
%         te = TE1+TE2;

bw1 = bandWidth(1);
bw2 = bandWidth(2);
si = size(fid);
zf2 = si(2);
rows = si(1);
ts = [te:dwellTime1:(te+dwellTime1*(rows-1))]*1d-3;
ts = ts(:);

% calculate sequence timings
tshift = -0.5*(ts-ts(1));

t_mat = ones(si(1),1)*[0:si(2)-1]/bw2 + ...
    tshift*ones(1,si(2));
filtm = exp(-(pi*gauss2*t_mat).^2/(2*log(2)));
fid = fid.*filtm;

w = [-zf2/2:(zf2/2-1)]*bw2/zf2;
fidSpec = fftshift(fft(fid,zf2,2),2);	% fid in t1/spec in t2
fidSpec = fidSpec.*exp(-2*pi*1i*tshift*w);	% tl: max echo
fidSpec = fidSpec.*(lb_fun(si(1),bw1,[exp1 gauss1]).'*ones(1,si(2)));
fidSpecReverse = fidSpec.*exp(+2*pi*1i*tshift*w);	% tl: max echo
filteredFid = ifft(ifftshift(fidSpecReverse,2),[],2);	% fid in t1/spec in t2


figure; imagesc(1:si(2), 1:si(1), real(fftshift(fft(fidSpec,[],1),1)));
figure; plot(1:si(2), mean(real(fftshift(fft(fidSpec,[],1),1))));

end