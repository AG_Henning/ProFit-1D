function [zps,detect] = jzps(spec,bw,zp2,singlets)
% JZPS Detect zero points
%  [zps,detect] = jzps(spec,bw,zp2,singlets)
%
%      zps: detected zero points in 2D  [ppm]
%   detect: 1=phantom
%           2=in vivo
%     spec: jpress spectrum
%       bw: bandwidth                   [ppm]
%      zp2: initial zero point in f2    [ppm]
% singlets: positions of singlets       [ppm]
%
% See also JPRESS, JPCS.

if nargin<1, help(mfilename); return; end
narginchk(4,4);

% misc parameters
wi_roi  = [0.5 0.3 0.25];	% width (along f2 in ppm) of rois

si = size(spec);

% axes
ax1_dt = [-si(1)/2:(si(1)-1)/2]/si(1);
ax2_dt = [-si(2)/2:(si(2)-1)/2]/si(2);
ax1 = ax1_dt*bw(1);
ax2 = ax2_dt*bw(2);

detect = 2;

sing = singlets;

% *****************************
% determine zp through maximum of magnitude spectrum
ll = length(sing);
zps = [];
for l=1:ll
  ind = find((ax2+zp2)>(sing(l)-wi_roi(l)/2) & (ax2+zp2)<sing(l)+wi_roi(l)/2);
  spec_roi = abs(spec(:,ind));
  [yy1,ii1] = max(spec_roi,[],1);
  [yy2,ii2] = max(yy1);
  zp1 = ax1(ii1(ii2));
  zp2 = -ax2_dt(ii2+ind(1))*bw(2)+sing(l);
  zps = [zps ; zp1 zp2];
end
szps = std(zps);
zps = mean(zps);


fprintf('zp = [%g, %g]\n',zps(1),zps(2));
fprintf('std(zp) = [%g, %g]\n',szps(1),szps(2));
if any(szps>0.2),
  warning('big standard deviation');
end
