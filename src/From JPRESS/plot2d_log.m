function smax = plot2d_log(varargin)
% PLOT2D_LOG  Scaled coutour plot.
%    plot2d_log(xaxis,yaxis,data,[opt])
%    plot2d_log(data[,opt])
%
%    xaxis:
%    yaxis:
%     data:
%      opt: if 'cp' specified -> contourplot
%
%  Logarithmic imagesc or contourplot
% 
% See also CONTOUR, PLOTTING, CSSF.
if nargin<1, help(mfilename); return; end
narginchk(1,5);

ct = false;
nfac = 7;
% nfac = 150;
% nfac = 100;
% nfac = 50;
maxfac = 1;
k = 2;				% exponential spacing constant
                                % -> logarithm factor

% parse input
args = nargin;
while isstr(varargin{args}),
  str = lower(varargin{args});
  switch str(1:2)
   case 'ct', ct = true;
   case 'mf', 
    val = str2double(str(4:end));
    if ~isnan(val),
      maxfac = val;
    else
      fprintf('Warning: invalid input string: %s\n',str);
    end
   otherwise, warning('option not found');
  end
  args = args-1;
end

data = varargin{args};
args = args-1;
if args>2, error('input'); end
if args>0,
  xaxis = varargin{1};
end
if args>1,
  yaxis = varargin{2};
end
si = size(data);
if exist('xaxis')~=1, xaxis = [1:si(2)]; end
if exist('yaxis')~=1, yaxis = [1:si(1)]; end

if si(2)~=length(xaxis) | si(1)~=length(yaxis),
  si
  length(xaxis)
  length(yaxis)
  error('axis');
end

if ~isreal(data),
  warning('data input complex: taking magnitude');
  data = abs(data);
end

% maximum excluding 20% stripe in the middle (no water)
smax = maxfac*max(max(abs(data(:,[1:round(0.4*si(2)),round(0.6*si(2)):end]))));

if ct
  noise = nfac*mean(data(:,end))+6*std(data(:,end));
  % background niose level  x  factor
  
  vv = noise*k.^[0:20];			% level of contour lines
  % cut contour levels to below the maximum
  [aa,bb,vv] = find((vv<smax).*vv);
  if min(data(:))<0,			% add negative levels
    vvn = -noise*k.^[0:20];			% level of contour lines
    [aa,bb,vvn] = find((vvn>-smax).*vvn);
    vv = [vvn vv];
  end
  contour(xaxis,yaxis,data,vv);
else
  if min(data(:))<0,
    data = (log(data.*(data>0)+1)-log(-(data.*(data<0)-1)))/log(k);
  else
    data = log(data+1)/log(k);
  end
  smax = log(smax+1)/log(k);
  imagesc(xaxis,yaxis,data);
end
