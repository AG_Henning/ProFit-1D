function J = black_jet
% black_jet  Colormap with black in the middle, optimized for best
% visualization
%
%   See also COLORMAP.

load('BlackJet','mycmap');
J = mycmap;
