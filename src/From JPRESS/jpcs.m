function [pc0,pc1] = jpcs(spec,bw,zp2,singlets,detect)
% JPCS Automatic detection of phase for JPRESS (area+maximum)
%   [pc0,pc1] = jpcs(spec,bw,zp2,singlets,detect)
%
%      pc0: zeroth order phase
%      pc1: linear phase (only in phantom)
%     spec: 2D JPRESS
%       bw: bandwidths           [ppm]
%      zp2: zero point in f2     [ppm]
% singlets: position of singlets [ppm]
%   detect: 1=phantom
%           2=in vivo
%
% See also JPRESS, JZPS.

if nargin<1, help(mfilename); return; end
narginchk(5,5);

switch detect
    case 1,
        wi_roi = 0.4;			% width [ppm] around peak
    case 2,
        wi_roi = 0.2;			% width [ppm] around peak
end

sing = singlets;
si = size(spec);

ax1 = [-si(1)/2:(si(1)-1)/2]/si(1);
ax2 = [-si(2)/2:(si(2)-1)/2]/si(2);
phis = [-1:0.005:0.995]*pi;
l_phis = length(phis);		% length of index (phis) list
ls = length(sing);
plt = true;

fprintf('Automatic phase correction\n');
fprintf('ppm\tmax\tint\tint neg\tmin neg\tmean\n');

for l=1:ls %run for all singlets
    %select centrum of spectral indices in dim 1
    ind1 = find(((ax1*bw(1))>-wi_roi/2) & ((ax1*bw(1))<wi_roi/2));
    i1h = (ind1(end)-ind1(1))/2; %middle index in dim1
    
    ind2 = find(((ax2*bw(2)+zp2)>sing(l)-wi_roi/2) & ...
        ((ax2*bw(2)+zp2)<sing(l)+wi_roi/2));
    ma = [];
    in = [];
    ineg = [];
    mineg = [];
    for phi=phis
        % iterating is unelegant methods for some:
        %  could be calculated through combintation of real and
        %  imaginary part
        spec_roi = real(spec(ind1,ind2)*exp(-i*phi));
        spec_up  = spec_roi([1:i1h],:);
        spec_down = spec_roi([i1h+1:end],:);
        % 1: determine maximum
        ma = [ma max(spec_roi(:))];
        % 2: determine integral of whole peak
        in = [in sum(spec_roi(:))];
        % 3: equal integrals of negative parts
        tmp = sum((spec_up(:)<0).*spec_up(:)) - ...
            sum((spec_down(:)<0).*spec_down(:));
        ineg = [ineg tmp];
        % 4: equal minima of negative parts
        tmp = min(spec_up(:))-min(spec_down(:));
        mineg = [mineg tmp];
        
        % 5: integral of whole peak = 0 => phase+-pi/2
        % 6: minimum=maximum => phase+-pi/2
        
    end
    
    % [yy_max,ii_max] = max(ma);
    
    % centre of gravity of maxima (much more reliable)
    [yy_mami,ii_mami] = min(ma);
    ma = ma-yy_mami;
    if ii_mami<l_phis/2
        cg_phis = phis + ([1:l_phis]<ii_mami)*2*pi;
    else
        cg_phis = phis - ([1:l_phis]>=ii_mami)*2*pi;
    end
    
    pc_max = sum(ma.*cg_phis)/sum(ma);
    
    [yy,ii_int] = max(in);
    
    % determine the closer aread to search
    % ia: index list around
    
    iawi = l_phis/10;		% widths of indices around
    if ii_int<=iawi,
        ia = [1:(ii_int+iawi) (l_phis-iawi+ii_int):l_phis];
    else
        if ii_int>l_phis-iawi
            ia = [1:(ii_int-l_phis+iawi) (ii_int-iawi:l_phis)];
        else
            ia = [ii_int-iawi:ii_int+iawi];
        end
    end
    [yy,ii_ineg] = min(abs(ineg(ia)));
    [yy,ii_mineg] = min(abs(mineg(ia)));
    
    % different phases in [rad]
    pc_all = [pc_max phis(ii_int) ...
        phis(ia(ii_ineg)) phis(ia(ii_mineg))];
    pcs(l) = mean(unwrap(sort(pc_all)));
    fprintf('%g\t%0.3g\t%0.3g\t%0.3g\t%0.3g\t%0.3g\n',...
        sing(l),pc_all(1)*180/pi,pc_all(2)*180/pi,...
        pc_all(3)*180/pi,pc_all(4)*180/pi,pcs(l)*180/pi);
    
    
    % plotting
    if plt
        figure(30);
        cmax = log(max(ma)+1)/log(2);
        spec_roi = spec(ind1,ind2);
        siroi = size(spec_roi);
        
        subplot(length(sing)+1,2,2*l-1);
        plot2d_log(real(spec_roi*exp(-i*pcs(l))));
        hold on;
        plot([1 siroi(2)],[siroi(1)/2 siroi(1)/2],'g:',...
            [siroi(2)/2 siroi(2)/2],[1 siroi(1)],'g:');
        hold off;
        caxis([-cmax cmax]); colorbar; colormap(cmap_dark);
        title([num2str(sing(l)) ' ppm']);
        
        subplot(length(sing)+1,2,2*l);
        plot(phis*180/pi,ma/max(ma),'b',...
            phis*180/pi,in/max(in),'r',...
            phis*180/pi,abs(ineg)/max(ineg),'g',...
            phis*180/pi,abs(mineg)/max(mineg),'m');
        xlabel('b: max; r: int; g: int neg; m: min neg');
        axis tight;
    end
end
pcs_tmp = pcs;
pcs = unwrap(pcs)*180/pi;
if any(pcs_tmp~=unwrap(pcs_tmp)),
    fprintf('Unwrapping\n');
    pcs
end


% switch detect
switch 2
    case 1,		% phantom
        p = polyfit((zp2-sing)/bw(2),pcs,1);
        pc0 = p(2);
        pc1 = -p(1);
    case 2,		% in vivo
        pc0 = mean(pcs);
        pc1 = 0;
    otherwise, error('nothing detected');
end
if pc0>180,
    pc0 = pc0-360;
end
if pc0<-180,
    pc0 = pc0+360;
end

if plt,
    figure(30);
    mask = jtrunc_mask(si,0,wi_roi/bw(1),1);
    spec_tru = mean(mask.*spec,1);
    subplot(length(sing)+1,1,length(sing)+1);
    plot(real(spec_tru.*exp(-i*pi/180*(pc0+pc1*ax2))));
    axis tight
end
