function fil = lb_fun(n,bw,lbs,t0)
% LB_FUN  Line broadening function
%   fil = lb_fun(n,bw,lbs,t0)
%
%        n: length
%       bw: bandwidth/spectral width        [Hz]
%      lbs: filter widths                   [Hz]
%           [exp gauss]
%       t0: shift filter by this time        [s]
%           'h' = filter reference in middle

% author: rolf.schulte@ethz.ch

if nargin<1, help(mfilename); return; end
narginchk(3,4);

% parse input
if exist('t0')~=1, t0=0; end;

% time
t = [0:n-1]/bw;

if ischar(t0),
  % filter reference on half
  t0 = n/2/bw;
end

fil = ones(1,n);
% generate filter functions
for l=1:length(lbs),
  if lbs(l)~=0,
    switch l
     case 1,			% exponential filter
      fil = fil.*exp(-pi*lbs(l)*abs(t-t0));
      % Ref: NMR data processing/ Hoch+Stern
     case 2,			% Gaussian filter
      fil = fil.*exp(-(pi*lbs(l)*(t-t0)).^2/(2*log(2)));
      % Ref: Pearson JMR 74, 1987; 541.
      % the factor 2 comes from considering only one half in the time domain
     otherwise, error('unknown filter function: lbs too long');
    end
  end
end
