 function [jp_fidspec,jp_fid] = jupdate_fid(fid,ts,bw2,exp2,gauss2,zf2,trunc,f2shift)
% JUPDATE_FID  Calculate FID with top in middle from max echo series.
%    data = jupdate_fid(fid,ts,bw2,exp2,gauss2,zf2,trunc,f2shift)
%
%        fid: original FID (max echo data)
%         ts: echo times of 2D experiment             [s]
%        bw2: bandwidth (sampling frequency) in t2/f2 [Hz]
%       exp2: amount of exponential filtering in t2   [Hz]
%     gauss2: amount of Gaussian filtering in t2      [Hz]
%        zf2: zero filling in t2 (if empty/zero, no zf)
%      trunc: truncate left half
%    f2shift: shift spectrum along f2 (optional)      [Hz]
%
% jp_fidspec: FFT along t2
%     jp_fid: 2d-FID
%
% See also JPRESS.



%   The first experiment is exactly half echo.
%   The acquisition window travels with the same speed forward
%   as the second refocusing pulse. Therefore, the acqusition begin
%   can be determined as follows:
%   t_acqbegin = te1 + te2_n/2 + te2_1/2
%   where te1 is the (constant) first echo period, te2_n is the
%   second echo period in the nth experiment, and te2_1 is the
%   second echo period of the first experiment.

% input
if nargin<1, help(mfilename); return; end
narginchk(7,8);
si = size(fid);
if mean(diff(ts))>0.1,       error('ts too big: ms?'); end
if max(diff(ts))-min(diff(ts))>1d-14, error('ts not equidistant'); end
if any(size(bw2)~=[1 1]),    error('size bw2'); end
if any(size(exp2)~=[1 1]),   error('size exp2'); end
if any(size(gauss2)~=[1 1]), error('size gauss2'); end
if isempty(zf2) | zf2==0, zf2 = si(2); end
if any(size(zf2)~=[1 1]),    error('size zf2'); end
if zf2<1, error('zf2<1'); end
if any(size(trunc)~=[1 1]),  error('size trunc'); end

ts = ts(:);

if si(1) == 1
    %dummy shift to echo
    shiftToEcho = 0;
    %times to the echo top, can be positive or negative (mind full echo sampling)
    timeToEcho = ones(si(1),1)*[0:si(2)-1]/bw2;
else
    %cross check first, if data is correct
    if length(ts)~=si(1),        error('length(ts)~=si(1)'); end
    % calculate sequence timings
    shiftToEcho = -0.5*(ts-ts(1));
    %times to the echo top, can be positive or negative (mind full echo sampling)
    timeToEcho = ones(si(1),1)*[0:si(2)-1]/bw2 + shiftToEcho*ones(1,si(2));
end

% apply filter in t2
if gauss2~=0 | exp2~=0,
  % only approximately correct, as the Gaussian filtering should be done
  % after shifting. Error small for large zf2
  filtm = exp(-(pi*gauss2*timeToEcho).^2/(2*log(2))) .* exp(-pi*exp2*abs(timeToEcho));
  fid = fid.*filtm;
  % figure(6); imagesc(filtm)
end

% shift frequency around (linear phase in t-domain)
if exist('f2shift')==1,
  % FID is still max echo here, but the shifting should be ok
  phafu = exp(i*2*pi*timeToEcho*f2shift);
  fid = fid.*phafu;
end

w = [-zf2/2:(zf2/2-1)]*bw2/zf2;
jp_fidspec = fftshift(fft(fid,zf2,2),2);	% fid in t1/spec in t2
jp_fidspec = jp_fidspec.*exp(-2*pi*i*shiftToEcho*w);	% tl: max echo
if (nargout>1 | trunc),
  jp_fid = ifftshift(ifft(ifftshift(jp_fidspec,2),[],2),2);
end

% truncate
if trunc
  zf2hp = floor(zf2/2+0.5);
  zf2hm = floor(zf2/2);
  zf1 = size(jp_fid,1);
  jp_fid = [zeros(zf1,zf2hm) jp_fid(:,[zf2hp+1:zf2])];
  jp_fidspec = fftshift(fft(ifftshift(jp_fid,2),zf2,2),2);
end
