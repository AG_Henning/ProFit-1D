function spec = jrecon_t1dim(fid,bw1,exp1,gauss1,zf1,zp1)
% JRECON_T1DIM  Reconstruct t1 domain of 2d experiments 
%  (JPRESS)
%    spec = jrecon_t1dim(fid,bw1,exp1,gauss1,zf1,zp1,f0)
%
%        spec: reconstructed spectrum
%         fid: fid in t1, spec or fid in t2
%         bw1: bandwidth in indirect dimension  [Hz]
%        exp1: exponential filter in t1         [Hz]
%      gauss1: gaussian filter in t1            [Hz]
%         zf1: zero fill in t1 (if omitted, no zero fill)
%         zp1: shift zero point in f1 dimension [Hz]
%

narginchk(4,6);

si = size(fid);

if ~exist('zp1'), zp1 = 0; end
if ~exist('zf1'), 
  zf1 = si(1);
else
  if isempty(zf1) | zf1<size(fid,1),
    zf1 = si(1);
  end
end

% shift spectrum in f1 dimension (move water down)
if zp1~=0,
  t1 = [0:si(1)-1].'/bw1;

  % reference frequency on water
  phafu = exp(-i*2*pi*t1*zp1)*ones(1,size(fid,2));
  fid = fid.*phafu;
end
if si(1) ~= 1 %no correction to do for 1D spectra
    fid = fid.*(lb_fun(si(1),bw1,[exp1 gauss1]).'*ones(1,si(2)));
end
spec = fftshift(fft(fid,zf1,1),1);
