function J = cmap_dark(m)
% CMAP_DARK  Colormap with black in the middle
%
%   See also COLORMAP.

if nargin < 1
   m = size(get(gcf,'colormap'),1);
end

r = [1 0 0.00 0 0.75 1 1];
g = [1 0 0.75 0 0.75 0 1];
b = r(end:-1:1);

  
  
x = linspace(1,m,7);
xi = [1:m];
J = interp1(x,[r;g;b].',xi,'linear');
