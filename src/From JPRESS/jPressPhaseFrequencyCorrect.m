function [correctedFid, pc0, df1, df2] = jPressPhaseFrequencyCorrect(fid, te, dwellTime1, bandWidth, f0, data)
% Does a phase and frequency correction based on the algorithms used in the
% JPRESS software.
% Parameters:
%    fid = nonzerofilled, FID maximum echo sampled
%    te = echo time given in ms
%    dwellTime1 = the echo time step between the different acquisitions
%                 (J-resolved rows) given in ms
%    bandWidth = bandWidth of the fid in both dimensions
% Return:
%    correctedFid = fid corrected for zero phase and frequency shifts in
%                   both dimensions df1 & df2
%    pc0 = zeroPhase in degrees
%    df1 = frequency Shift in dimension 1 given in Hz
%    df2 = frequency Shift in dimension 2 given in Hz
% Author: Tamas Borbath tamas.borbath@tuebingen.mpg.de

%default values:
exp2 = 0;
gauss2 = 2;
trunc = false;
zp2 = 4.72;
singlets = [2.008 3.027 3.21];% in vivo: NAA/Cre/Cho

rows = size(fid,1);
zf2 = size(fid,2) * 2;
echoTimes = [te:dwellTime1:(te+dwellTime1*(rows-1))]*1d-3;
[jp_fidspec,~] = jupdate_fid(fid,echoTimes,bandWidth(2), exp2,gauss2,zf2,trunc);
if rows > 1
    spec = jrecon_t1dim(jp_fidspec,bandWidth(1),0,2,1024,0);
else
    spec = jrecon_t1dim(jp_fidspec,bandWidth(1),0,2,[],0);
end

% determine zero points through finding maximum
[zeroPointShift, detect] = jzps(spec,bandWidth/f0,zp2,singlets);

% determine phases through maximising maximum and integral
if rows > 1
    spec = jrecon_t1dim(jp_fidspec,bandWidth(1),0,2,1024,zeroPointShift(1)*f0);
%     pc0 = jpcs2D(spec,bandWidth/f0,zeroPointShift(2),singlets);
    [pc0, pc1] = jpcs(spec,bandWidth/f0,zeroPointShift(2),singlets, detect);
else
    spec = jrecon_t1dim(jp_fidspec,bandWidth(1),0,2,[],zeroPointShift(1)*f0);
    pc0 = 0;%TODO remove this
% TODO solve     pc0 = pcs1D(spec,bandWidth/f0,zeroPointShift(2),singlets);
end

df1 = zeroPointShift(1) * f0; %transform to Hz ? TODO
df2 = (zp2 - zeroPointShift(2)) * f0; %transform to Hz
% zero order phase correction
correctedFid = fid .* exp(1i*-pc0*pi/180*data.phase_0_mx);

% frequency shift in the F1
correctedFid  = correctedFid .* exp(1i*-df1*2*pi*data.shift_t1_mx);

% frequency shift in the F2
si = size(fid);
f2shift = (zp2-zeroPointShift(2))*f0;
ts = echoTimes(:);

if rows > 1
    % calculate sequence timings
    shiftToEcho = -0.5*(ts-ts(1));
else
    shiftToEcho = 0;
end

%times to the echo top, can be positive or negative (mind full echo sampling)
timeToEcho = ones(si(1),1)*[0:si(2)-1]/bandWidth(2) + shiftToEcho*ones(1,si(2));
phafu = exp(-1i*2*pi*timeToEcho*f2shift);
correctedFid = correctedFid.*phafu;

end