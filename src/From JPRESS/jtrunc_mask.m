function mask = jtrunc_mask(si,off,win,dim)
% JTRUNC_MASK  Generate a mask with Gaussian filtered rect
%   mask = jtrunc_mask(si,off,win,dim)
%
%   mask: mask for filtering 2d jpress
%
%     si: size of mask to generate
%    off: offset (relative to centre and si)
%    win: width of window to filter (relative to si)
%    dim: filtering dimension
%

if nargin<1, help(mfilename); return; end
narginchk(4,4);

plt = false;

if length(si)~=2, error('size not length 2'); end
if abs(off)>0.5, error('offset exceeds +-0.5'); end
if win<=0, error('negative window'); end
if win>1, error('window exceeds 1'); end

% axes
ax1 = [-si(1)/2:(si(1)-1)/2].';
ax2 = [-si(2)/2:(si(2)-1)/2];

% convert normalised units into point units
off = off*si(dim);
win = win*si(dim)/2;

% generate mask
switch dim
 case 1,
  a1 = abs(ax1-off)<win;
  a1 = conv(double(a1),exp(-10*(ax1/win).^2));
  a1 = a1([si(1)/2:end-si(1)/2])/max(a1);
  a2 = ones(1,si(2));
  if plt, figure(13); plot(ax1/si(1),a1); end
 case 2,
  a1 = ones(si(1),1);
  a2 = abs(ax2-off)<win;
  a2 = conv(a2,exp(-10*(ax2/win).^2));
  a2 = a2([si(2)/2:end-si(2)/2])/max(a2);
  if plt, figure(13); plot(ax2/si(2),a2); end
 otherwise, error('dim not 1 or 2');
end

mask = a1*a2;


if any(size(mask)~=si),
  si
  size(mask)
  error('size of mask incorrect');
end
if plt,
  figure(14);
  imagesc(ax2/si(2),ax1/si(1),mask);
  colorbar
end
