function lambda = calc_lambda_from_ed(spline_basis, deriv_mat, target_ed, upper_lim, lower_lim, start_val)
   if ~exist('start_val', 'var')
		start_val = 1.0;
   end
   if ~exist('lower_lim', 'var')
		lower_lim = 1e-6;
   end
   if ~exist('upper_lim', 'var')
		upper_lim = 1e10;
   end
%   res <- stats::optim(start_val, ed_obj_fn, method = "Brent", lower = lower_lim,
%                upper = upper_lim, spline_basis = spline_basis,
%                deriv_mat = deriv_mat, target_ed = target_ed)
    % specifiy algorithm details used for the optimization process
    options = optimset('Algorithm', 'trust-region-reflective', ...
                       'FinDiffType', 'central', ...
                       'Jacobian', 'off', ... 
                       'DerivativeCheck', 'off', ...
                       'TolFun', 1e-40, ...
                       'TolX', 1e-10, ... 
                       'MaxFunEvals', 20000);
        
     [xLambda,~,functionValue,~,output,~,~] = ...
        lsqnonlin(@(xSolver) ed_obj_fn(xSolver, spline_basis, deriv_mat, target_ed),...
		start_val, lower_lim, upper_lim, options);
    
	if (functionValue > 1e-6) 
		warning('correct lambda not found')
	end
   lambda = xLambda;
end


function residual_ED = ed_obj_fn(lambda, spline_basis, deriv_mat, target_ed) 
  ed = calc_ed_from_lambda(spline_basis, deriv_mat, lambda);
  residual_ED = (ed - target_ed) .^ 2;
end