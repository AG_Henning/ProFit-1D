function [fid, fs] = preprocessMMB(fid, fs, data)

% 	fiter = Iteration(fs.basis);
% 	fiter.ActivateMets({'NAA', 'Cr(CH2)'});
% 	fiter.ConnectMets({'name'}, {'conc', 'df2', 'em'});
% 	fiter.ConnectMets({'global'}, {'em_g', 'df1', 'gm', 'pc0', 'pc11', 'pc12', 't1'});
% 	fiter.FixMets({'all'}, {'t1', 'pc11', 'pc12'});
% 	fiter.SetFixedValues({'all'}, {'t1'}, Inf);
% 	fiter.SetFixedValues({'all'}, {'df1'}, 0);
% 	fiter.SetFixedValues({'all'}, {'pc11', 'pc12'}, 0);
%     fiter.SetStartingValues({'all'}, {'em'}, 6);
%     fiter.SetStartingValues({'all'}, {'em_g'}, 6);
%     fiter.SetStartingValues({'all'}, {'gm'}, 40);
%     fiter.SetLB({'all'}, {'em', 'em_g', 'gm'}, 0);
% 
% 	fiter.UpdateIteration;
%     
    fiter = fs.seq{1};

	% TODO copied from profit.m -> somehow this code should not be present 2 times
	spec = fft(fft(fid,[],2).*data.tilt_mx,[],1);
	% put basis set together for the specific iteration
    map_idx = 1:length(fiter.basis.met);
    map_idx = map_idx(fiter.active_mets);
    basis = cell(1, length(map_idx));
    for kk = 1:length(map_idx)
        basis{kk} = fs.basis.fid{map_idx(kk)}; 
    end

	% handle start values 
    em_x0    = fiter.start_values.em;
    em_g_x0  = fiter.start_values.em_g;
    gm_x0    = fiter.start_values.gm;
    pc0_x0   = fiter.start_values.pc0;
    pc11_x0  = fiter.start_values.pc11;
    pc12_x0  = fiter.start_values.pc12;
    df1_x0   = fiter.start_values.df1;
    df2_x0   = fiter.start_values.df2;
    t1_x0    = fiter.start_values.t1;
    
    x0 = [em_x0, em_g_x0, gm_x0, pc0_x0, pc11_x0, pc12_x0, df1_x0, df2_x0, t1_x0];

	fprintf('\nPre-aligning and pre-phasing spectrum...\n');
    % specifiy algorithm details used for the optimization process
    options = optimset('Display', 'off', ...
                       'Algorithm', 'trust-region-reflective', ...
                       'useParallel', 'always', ...
                       'FinDiffType', 'forward', ...
                       'Jacobian', 'off', ...
                       'DerivativeCheck', 'off', ...
                       'Diagnostics', 'off', ...
                       'MaxIter', 150, ...
					   'TolFun', 1e-12, ...
                       'MaxFunEvals', 3000);
        
    [x, resnorm, residual, exitflag, output, lambda, jacobian] = lsqnonlin(@(x) CalcCostFun(x, spec, fiter, basis, data, [], []), x0, [], [], options);
 CalcCostFun(x, spec, fiter, basis, data, [], [])
	% correct coares shifts and zero order phase of measured fid
	
	df2 = x(11);
% df2 = 30;

	% output results
% 	fprintf('estimated pc0:%6.2f deg\n', pc0);
	fprintf('estimated df2:%6.2f Hz\n\n', df2);

	% process fid
% 	% zero order phase correction
%     fid = fid .* exp(1i*-pc0*pi/180*data.phase_0_mx);
    
    % frequency shift in the F2
    fid = fid .* exp(1i*-df2*2*pi*data.shift_t2_mx);

end
