% Calculate the effective dimensions of a spline smoother from lambda.
% @param spline_basis spline basis.
% @param deriv_mat derivative matrix.
% @param lambda smoothing parameter.
% @return the effective dimension value.
function ED = calc_ed_from_lambda(spline_basis, deriv_mat, lambda)
   inv_mat = inv(spline_basis' * spline_basis + lambda * (deriv_mat' * deriv_mat));%TODO check what it calculates
   H       = inv_mat * (spline_basis' * spline_basis);
   ED = trace(H);
end 


function ED = calc_ed_from_lambda_stable(spline_basis, deriv_mat, lambda)
   inv_mat  = inv([spline_basis; (lambda ^ 0.5) .* deriv_mat]);
   zero_mat = zeros(size(deriv_mat));
   G        = inv_mat * [spline_basis; zero_mat];
   ED = trace(G);
end
