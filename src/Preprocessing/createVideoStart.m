function [data, fid] = createVideoStart(data, fid)
global frames;
global iframes;
data.debug = 1;
frames = cell(1,50);
iframes = 1;

%% add some crazy phase and frequency shift to make video more interesting
	pc0 = 15;
	pc12 = 3;
	df2 = 20;

	% output results
	fprintf('estimated pc0:%6.2f deg\n', pc0);
	fprintf('estimated pc12:%6.2f deg\n', pc12);
	fprintf('estimated df2:%6.2f Hz\n\n', df2);
    
	% process fid
	% zero order phase correction
    fid = fid .* exp(1i*-pc0*pi/180*data.phase_0_mx);
    
    % frequency shift in the F2
    fid = fid .* exp(1i*-df2*2*pi*data.timeVect);
    
    spec = fft(fid); %fftshift is not needed and is counter productive due to the data.phase_1_f2_mx
    % precompute coefficient needed to define the parameter unit as degree /ppm
    coeff_F2_degree_ppm = 1i*-pc12*pi/180;
    spec = spec .* exp(coeff_F2_degree_ppm * data.phase_1_f2_mx);

    fid = ifft(spec);
%%
end