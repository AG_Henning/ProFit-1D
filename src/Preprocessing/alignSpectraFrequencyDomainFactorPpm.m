function [frequencyShiftFactorPpm] = alignSpectraFrequencyDomainFactorPpm(fid, scanFrequency, bandwidth_Hz, ...
    zeroFillFactor, indices, searchArea, useReal, doSplineSmoothing, splineSmoothingCoeff)

%set zeroFillingParameter to get smooth approximations
if ~exist('zeroFillFactor', 'var')
    zeroFillFactor = 50; 
end
%indices of Interest: given in ppm
if ~exist('indices','var')
    %NAA, tCr-CH3 (average of Cr [3.027], PCr [3.029])
    %tCho [3.210] ignoring Cho, since Cho = 3.185, GPC = 3.212, PCho = 3.208
    %tCr-CH2 [3.921] (average of Cr [3.913], PCr [3.93])
    indices  = [2.008; 3.028; 3.210; 3.921];
end
%search area (+-) in ppm
if ~exist('searchArea','var')
    searchArea = [0.1];
end
%set use Real. If False, the absolute spectrum will be used
if ~exist('useReal', 'var')
    useReal = true;
end
%flag to do spline filtering
if ~exist('doSplineSmoothing', 'var')
    doSplineSmoothing = false;
end
%spline filtering coefficient
if ~exist('splineSmoothingCoeff', 'var')
    splineSmoothingCoeff = 0.01;
end

samples     = length(fid);
numberOfIndices = length(indices);
%group of searchAreas
indicesSearchAreas = [indices-searchArea indices+searchArea];

%[ppm] vector 
bw     = linspace(-bandwidth_Hz/2,bandwidth_Hz/2,samples * zeroFillFactor);
ppm    = bw/(scanFrequency*1e-6) + 4.7;

%ppm groups of indices to search for
metaboliteIndexes= zeros(numberOfIndices, samples * zeroFillFactor);
for index = 1: numberOfIndices
   index1 = ppm >= indicesSearchAreas(index,1);
   index2 = ppm <= indicesSearchAreas(index,2);
   metaboliteIndexes(index,:) = index1 & index2; 
end

%do spline smoothing if flag is set
if doSplineSmoothing
    fid = MR_spectroS.smoothArray(fid, splineSmoothingCoeff, false);
end
%frequency Domain signal (zerofilled to get better resolution)
spectrum      = fftshift(fft(fid, samples*zeroFillFactor));
if useReal
    spectrumToAlign = real(spectrum);
else
    spectrumToAlign = abs(spectrum);
end

%threshold to check if peak is well defined: bigger than 70% of left and
%right signal
threshold = 0.7;
%define the ppmDifferences
ppmMetaboliteDifference = zeros(1,numberOfIndices);
% calculate index of peak with the max amplitude using the ppm masks
for index = 1:numberOfIndices
    %define peak search area
    metaboliteIndexesToSearch = logical(metaboliteIndexes(index,:));
    peakMetabolite          = spectrumToAlign(metaboliteIndexesToSearch);
    %get the position of the maximal amplitude
    [maxValue,  maxPosMetabolite] = max(abs(peakMetabolite));
    % check if peak well defined
    minLeft = min(abs(peakMetabolite(1:maxPosMetabolite)));
    minRight = min(abs(peakMetabolite(maxPosMetabolite:end)));
    if (minLeft < threshold * maxValue) & (minRight < threshold * maxValue)
        %retrieve the ppmValue of the peak
        ppmMetaboliteIndexes = ppm(metaboliteIndexesToSearch);
        ppmMetaboliteDifference(index) = ppmMetaboliteIndexes(maxPosMetabolite) - indices(index);
    else
       ppmMetaboliteDifference(index) = NaN;
       warning(['The %.3f peak is not well defined in the spectrum. \n', ...
           'The peak is %.2f, whereas the minimums on both sides are [%.2f, %.2f].'], ...
           indices(index), maxValue, minLeft, minRight);
    end
end
%get mean value of the ppm differences
frequencyShiftFactorPpm = mean(ppmMetaboliteDifference, 'omitnan');

end
