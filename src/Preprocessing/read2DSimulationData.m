function [fid2D] = read2DSimulationData(fileNamePath, loopIndex, indirectDimensionSize)


    [pathName,fileName,extension] = fileparts(fileNamePath);
    
    fileNameCellArray = textscan(fileName,'%s %s','Delimiter','-');
    fileNameBase = fileNameCellArray{1};
    loopExtension = fileNameCellArray{2};
    simulationLoopIndexes = textscan(loopExtension{1},'%d %d %d', 'Delimiter', '_');
    simulationLoopIndex1 = num2str(simulationLoopIndexes{1});
    simulationLoopIndex2 = num2str(simulationLoopIndexes{2});
    simulationLoopIndex3 = num2str(simulationLoopIndexes{3});
    
    fid = readJmruiSimulationData(fileNamePath);
    %create array
    fid2D = complex(zeros(indirectDimensionSize, size(fid,2)));
    for fileIndex = 0:indirectDimensionSize-1
        %calculate the ending of the file
        switch loopIndex
            case 1
                loopIndexString = ['-' num2str(fileIndex) '_' simulationLoopIndex2 '_' simulationLoopIndex3];
            case 2
                loopIndexString = ['-' simulationLoopIndex1 '_' num2str(fileIndex) '_' simulationLoopIndex3];
            case 3
                loopIndexString = ['-' simulationLoopIndex1 '_' simulationLoopIndex2 '_' num2str(fileIndex)];
            otherwise
                error('Loop Index for read2DSimulationData is wrong. Should be 1,2 or 3.');
        end;
        %concatenate the file which we want to open next
        SimulationFileName = [pathName '\' fileNameBase{1} loopIndexString extension];
        if (exist(SimulationFileName,'file')==0)
            error('Following file does not exist: %s'); 
        end
        fid = readJmruiSimulationData(SimulationFileName);
        fid2D(fileIndex+1,:) = fid(:);
    end
end