function createVideoFrames(data, processed_spec, fitted_spec, spbase, conc, basis_matrix)
global frames;
global iframes;
figure(2016)
LineWidth = 2;
offsetResidual = 0.1 * max(real(processed_spec(data.fitroi.mask)));
plotImag = false;
plot(data.fitroi.ppm2, real(processed_spec),'LineWidth',LineWidth);
hold on
plot(data.fitroi.ppm2, real(fitted_spec),'LineWidth',LineWidth);
%residual
plot(data.fitroi.ppm2, real(processed_spec(data.fitroi.mask)) - real(fitted_spec(data.fitroi.mask))-offsetResidual*2,'LineWidth',LineWidth,'Color',[0.49 0.18 0.56]);

%%%
if ~isempty(spbase)
    concSplineReal = conc(size(basis_matrix,2)+1:size(basis_matrix,2)+size(spbase.BB,2));
    concSplineImag = conc(size(basis_matrix,2)+size(spbase.BB,2)+1:end);
    baselineReal = (spbase.BB * concSplineReal(1:size(spbase.BB,2)))';
    baselineImag = (spbase.BB * concSplineImag(1:size(spbase.BB,2)))';
    baseline = complex(baselineReal, baselineImag);
    plot(data.fitroi.ppm2, baselineReal-offsetResidual,'LineWidth',LineWidth,'Color',[0.5 0.5 0.5])
end
if plotImag ==  true
    hold on;
    %%%
    plot_type = ':';
    plot(data.fitroi.ppm2, imag(processed_spec),plot_type,'LineWidth',LineWidth);
    plot(data.fitroi.ppm2, imag(fitted_spec),plot_type,'LineWidth',LineWidth);
    %residual
    plot(data.fitroi.ppm2, imag(processed_spec(data.fitroi.mask)) - imag(fitted_spec(data.fitroi.mask))-offsetResidual,plot_type,'LineWidth',LineWidth);
    
    if ~isempty(spbase)
        plot(data.fitroi.ppm2, baselineImag-offsetResidual, plot_type,'LineWidth',LineWidth)
    end
    legend('spectrum Re', 'fitted Re', 'residual Re', 'baseline Re', 'spectrum Im', 'fitted Im', 'residual Im', 'baseline Im')
else
    legend('Data', 'Fit', 'Residual', 'Baseline')
end
hold off
FontSize = 12;
xlim([min(data.fitroi.ppm2), max(data.fitroi.ppm2)])
ylim([-3000, 7400])
xlabel('\delta (ppm)')
ylabel('Signal (arb. u.)')
set(gca,'xDir','reverse')
set(gca,'ytick',[]);
set(gca,'fontsize',FontSize);
set(gca,'FontWeight','bold');
if ~isempty(iframes)
    frames{iframes} =  getframe(gcf) ;
    iframes = iframes + 1;
end
drawnow
end