function noise_var = calculateNoiseVariation2D(spec, noise_window_ppm, pts_ppm, total_ppm, ppm1, ppm2, pts, plotNoiseWindow)

% shift the F2 dimension by the negativ part of the linear ppm function
noise_window_pts = noise_window_ppm .* repmat(pts_ppm, [size(noise_window_ppm,1), 1]) + ...
    repmat([total_ppm(1)/2 abs(ppm2(1))] .* pts_ppm, [size(noise_window_ppm,1) 1]);

%create a mask to extract the noise variance
mask = poly2mask(noise_window_pts(:,2), noise_window_pts(:,1), pts(1), pts(2));

%calculate noise variance of the spectrum in the selected mask area
noise_var = var(spec(mask));

if plotNoiseWindow
    %plot full spectra highlingting noise area
    figure;
    imagesc(ppm2, ppm1, real(spec));
    line([noise_window_ppm(:,2); noise_window_ppm(1,2)], [noise_window_ppm(:,1); noise_window_ppm(1,1)], 'LineWidth', 1, 'Color', 'g');
    set(gca, 'XDir', 'reverse');
    xlabel('F2 [ppm]');
    ylabel('F1 [ppm]');
    %plot the area cropped out as the noise window
    figure;
    imagesc(ppm2, ppm1, real(spec.*mask));
    line([noise_window_pts(:,2); noise_window_pts(1,2)], [noise_window_pts(:,1); noise_window_pts(1,1)], 'LineWidth', 1, 'Color', 'g');
    set(gca, 'XDir', 'reverse');
    xlabel('F2 [ppm]');
    ylabel('F1 [ppm]');
end