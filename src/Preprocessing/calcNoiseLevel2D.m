function noise_var = calcNoiseLevel2D(fid, data)
% cut out a window according to the noise window in the data structure and
% calculate the variance of the noise to be later used for the fitting
% process
debug = data.debug;
plotNoiseWindow = data.debug;
plotNoiseWindow = 1;
noise_window_ppm = data.noise_window_ppm;
pts_ppm = data.pts_ppm;
total_ppm = data.total_ppm;
ppm1 = data.ppm1;
ppm2 = data.ppm2;
pts = data.pts;
tilt_mx = data.tilt_mx;

spec = fftshift(fft(fft(fid, [], 2) .* tilt_mx, [], 1));

%overwriting the noise window with the acceptable values for all bandwidth etc.
noise_window_F2 = [noise_window_ppm(1,2) noise_window_ppm(2,2)];
noise_window_F1 = [total_ppm(1)/4 total_ppm(1)/2]; %take the lower end of the spectrum
noise_window_ppm = [...
    noise_window_F1(2) noise_window_F2(1);...
    noise_window_F1(2) noise_window_F2(2);...
    noise_window_F1(1) noise_window_F2(2);...
    noise_window_F1(1) noise_window_F2(1)];

noise_var = calculateNoiseVariation2D(spec, noise_window_ppm, pts_ppm, total_ppm, ppm1, ppm2, pts, plotNoiseWindow);

data.debug = debug;
end