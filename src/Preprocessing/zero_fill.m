function [fid, fitsettings, data] = zero_fill(fid, fitsettings, data, zf_factor)

if (zf_factor==1)
    zq3_mx = [];
else
    zq3_mx = zeros(data.pts(1), (zf_factor-1)*data.pts(2));
end

fid = [fid zq3_mx];

for zz=1:length(fitsettings.basis.fid)
    fitsettings.basis.fid{zz} = [fitsettings.basis.fid{zz} zq3_mx;];
end

data.pts       = size(fid);
data.pts_ppm   = data.pts ./ data.total_ppm;
                
% set initial vector of time and frequency domain axis
data.timeVect = (0:data.pts(2) - 1) * data.dt(2);
data.w1 = linspace(-data.bw(1)/2, data.bw(1)/2 - data.bw(1)/data.pts(1), data.pts(1));
data.w2 = linspace(-data.bw(2)/2, data.bw(2)/2 - data.bw(2)/data.pts(2), data.pts(2));
data.ppm2 = linspace(-data.total_ppm(2)/2, data.total_ppm(2)/2-data.total_ppm(2)/data.pts(2), data.pts(2)) + data.center_ppm;

% setup shift and tilting matrices
data.phase_0_mx    = ones(data.pts);
data.phase_1_f2_mx = data.ppm2 - data.phasePivot_ppm;

% dimensions of basis fids have also to be updated
fitsettings.basis.pts       = size(fid);

end
