function SNR_NAA = getNaaSnr(spec, bandWidth, f0, noiseVariance)

zp2 = 4.72;
NaaSingletPpm = 2.008;
wi_roi = 0.2;			% width [ppm] around peak
si = size(spec);

bandWidth = bandWidth / f0;

ax1 = [-si(1)/2:(si(1)-1)/2]/si(1);
ax2 = [-si(2)/2:(si(2)-1)/2]/si(2);

%find indices around NAA peak
ind2 = find(((ax2*bandWidth(2)+zp2) > NaaSingletPpm - wi_roi / 2) & ...
    ((ax2*bandWidth(2)+zp2) < NaaSingletPpm + wi_roi / 2));
ind1 = find(((ax1*bandWidth(1))>-wi_roi/2) & ((ax1*bandWidth(1))<wi_roi/2));

spec_roi = real(spec(ind1,ind2));

NaaPeakValue = max(spec_roi(:))
noiseVariance
SNR_NAA = NaaPeakValue / sqrt(noiseVariance)
end