function [fid] = readJmruiSimulationData(SimulationFileName)
fileID = fopen(SimulationFileName);
%fill in the values based on the MRScanFile txt file
headerInfo = textscan(fileID, '%s', 21, 'Delimiter','\n');
valuesCellArray = textscan(fileID,'%f %f %f %f','Delimiter',' ','MultipleDelimsAsOne', 1);

fid = complex(valuesCellArray{1}, valuesCellArray{2});
% spec = complex(valuesCellArray{3}, valuesCellArray{4});
fid = fid';
end