function createVideoEnd()
global frames;
global iframes;
% create the video writer with 1 fps
writerObj = VideoWriter('fitting_iterations.avi');
writerObj.FrameRate = 5;
% set the seconds per image
% open the video writer
open(writerObj);
% write the frames to the video
for i=1:iframes-1
    % convert the image to a frame
    frame = frames{i} ;    
    writeVideo(writerObj, frame);
end
% close the writer object
close(writerObj);
end