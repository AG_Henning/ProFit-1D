function splineBasis = createSplineBasis(ppmVector, ppm_right, ppm_left, bl_comps_pppm)
   indeces 	= 1:length(ppmVector); 
   inds 	= indeces((ppmVector <= ppm_left) & (ppmVector >= ppm_right));
   x_scale 	= ppmVector((ppmVector <= ppm_left) & (ppmVector >= ppm_right));
   ppm_range = ppm_left - ppm_right;
   comps     = round(bl_comps_pppm * ppm_range);
   bl_bas    = bbase(length(inds), comps);
   bl_comps  = comps + 3;
   deriv_mat = getDifferenceMatrix(bl_comps, 2);
   splineBasis.inds		 = inds;
   splineBasis.x_scale	 = x_scale;
   splineBasis.bl_bas	 = bl_bas;
   splineBasis.bl_comps	 = bl_comps;
   splineBasis.deriv_mat = deriv_mat;
   % Note:
   % The baselines are only having a real part (no imaginary). 
   % I tried creating them as appropriate complex vectors, but it's not
   % possible, the fid is symmetric and if one truncates it to the half,
   % phasing seems to work, but actually the imaginary part becomes
   % Lorentzian, whereas the lineshape is actually Gaussian.
   % What could be interesting is to change the CalcCostFun to actually
   % include the concentration in the optimization and eliminate the lsqlin
   % optimization and make the cost function handle also lambda etc...
end


% Directly from "Splines, knots, and penalties", Eilers 2010
function res = tpower(x, t, p)
  % Truncated p-th power function
  res = (x - t) .^ p * (x > t);
end

function D = getDifferenceMatrix(n, degree)
  D = zeros(n-degree,n);
  binomialMat = zeros(1,degree+1);
  for i = 1:degree+1
	binomialMat(i) = nchoosek(degree,i-1) * (-1)^(i+degree+1);
  end
  for i = 1:n-degree
	D(i,i:i+degree) = binomialMat;
  end
end

% Generate a spline basis, slightly adapted from : "Splines, knots, and penalties", Eilers 2010.
% @param N number of data points.
% @param number number of spline functions.
% @param deg spline degree : deg = 1 linear, deg = 2 quadratic, deg = 3 cubic.
% @return spline basis as a matrix.
function B = bbase(N, number, deg)
  % Construct a B-spline basis of degree
  if ~exist('deg','var')
	deg = 3;
  end
  x = 1:N;
  xl = min(x);
  xr = max(x);
  dx = (xr - xl) / number;
  knots = xl - deg * dx:dx:xr + deg * dx;
  M = length(knots);
  P = zeros(N, M);
  for index1 = 1 : N
      for index2 = 1 : M
          P(index1,index2) = tpower(x(index1), knots(index2), deg);
      end
  end
  D = getDifferenceMatrix(M, deg + 1) ./ (gamma(deg + 1) * dx ^ deg);
  
  B = (-1) ^ (deg + 1) * P * D';
end