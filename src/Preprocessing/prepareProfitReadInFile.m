% Prepare Batch File to start Profit for a certain data set
function [nameOfOutputFile, status]=prepareProfitReadInFile(pathstrPF, namePF)
status=0;
filenameHandle=0;

%initialize variable empty
nameOfOutputFile ='';
% Switch if filename is already be given
switch nargin
    
    case 2
        fileName=namePF;
        pathName=[pathstrPF '/']; 
        filenameHandle=1;
       
    %case 1
    %    display('Filename given but path is not fully described.']);
    %    return
    otherwise
        [fileName,pathName]=uigetfile('*.sdat', ...
        'Please select .sdat files to be fitted with ProFit.',...
        pwd,'MultiSelect','on');
        if isequal(fileName,0)
            status=-1;
            nameOfOutputFile='';
            display('Selection aborted');
            return
        end
        display('Filename selected')
 end
single_file= ischar(fileName);
if single_file
    numberOfAllFiles=1;
else
    numberOfAllFiles=size(fileName,2);
end;

% Define Data Struct with Name, Location and Basisset Information for all
% files to be fitted
data=struct;
data.name={};
data.profitLoc={};
data.basisset={};

for counter=1:numberOfAllFiles
    if filenameHandle
            curr_name=fileName;
    else
            if single_file
                [~,curr_name,~] = fileparts(fileName);
            else    
                [~,curr_name,~] = fileparts(fileName{counter});
            end
    end
    data.name{counter}=[pathName curr_name];
    data.profitLoc{counter}=[pathName 'profit/' curr_name '_profit.mat'];
    par=read_spar([pathName curr_name '.SPAR']);
    currentFolder=pwd;
    switch round(par.rows)
        case 100
            switch round(par.echo_time)
                case 24
%                     data.basisset{counter}=[currentFolder '/fitSettings/final_settings_004_JPress_basis_te24_3t'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress3T8TE'];
                    data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress3T12TE'];
                    display('Basisset for TE=24 chosen');
                case 26
                    data.basisset{counter}=[currentFolder '/fitSettings/final_settings_004_JPress_basis_te26_3t'];
                    display('Basisset for TE=26 chosen');
                case 28
                    data.basisset{counter}=[currentFolder '/fitSettings/final_settings_004_JPress_basis_te28_3t'];
                    display('Basisset for TE=28 chosen');
                case 30
                    data.basisset{counter}=[currentFolder '/fitSettings/FinalFitSettings_004+_jpress_2D_basis_te30_3t_aq1.00125_dt2ms'];%'/fitSettings/OldGovindaraju/final_settings_004_JPress_basis_te30_3t'];%
                    display('Basisset for TE=30 chosen');
                case 31
                    if (round(par.samples) == 1024)
                        data.basisset{counter}=[currentFolder '/fitSettings/FinalFitSettings_004+_jpress_2D_basis_te31_3t++']; %
                    else % 2048pts
                        data.basisset{counter}=[currentFolder '/fitSettings/FinalFitSettings_004+_jpress_2D_basis_te31_3t_aq1_dt2ms_2048pts'];
                    end
                    display('Basisset for TE=31 chosen');                    
                otherwise
                    data.basisset{counter}='';
                    display('ERROR: No correct basis set chosen.');
                    nameOfOutputFile='';
                    status=-1;
                    return;
            end;
        case 50
             switch round(par.echo_time)
                case 24
%%
%                     data.basisset{counter}=[currentFolder '/fitSettings/final_settings_004_JPress_basis_te24_3t'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/DUMMY_settings_004_JPress_basis_te24_3t'];
                    data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz24TE'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz20TE'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz16TE'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz14TE'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz13TE'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz12TE'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz10TE'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz8TE'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz65TE'];
%%
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_NoTotal_4Iter_JPpress400Mhz13TE_NoTotal'];
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_NoTotal_4Iter_JPpress400Mhz10TE_NoTotal'];
                    display('Basisset for TE=24 chosen');
             end;
        case 85
             switch round(par.echo_time)
                case 24
%                     data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz85steps10TE'];
                    data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsJSemiLaser_4Iter_JPpress400Mhz85steps12TE'];
                    display('Basisset for TE=24 chosen');
             end;
        case 96 %COSY
            data.basisset{counter} = [currentFolder '/fitSettings/FitSettingsCOSY'];
        case 1
            switch round(par.echo_time)
                case 30
                    data.basisset{counter}=[currentFolder '/fitSettings/final_settings_004_Press_basis_te30_3t'];%final_settings_004_Press_basis_te30_3t_gaussian_mm
                    display('Basisset for TE=30 chosen');
                case 28
                    data.basisset{counter}=[currentFolder '/fitSettings/FitSettingsPress4Iter_1D_basis_te30_2.89t_mm3'];%final_settings_004_Press_basis_te30_3t_gaussian_mm
                    display('Basisset for TE=28 chosen');
                case 24 %TODO this is different, new sLASER!!
                    if contains(fileName, 'MMB')
                        data.basisset{counter}=['D:\Software\Spectro Data\AminoAcids\AminoAcids\FitSettings_MMB_basis_set'];
                        display('Basisset for TE=24 MMB chosen');
                    else
                        data.basisset{counter}=['D:\Software\Spectro Data\DATA_df\Basis_sets\final_T2_met_MM_paper/FitSettings_sLASER_3Iter_sLASER_7ppm_moiety_UF_basis_TE24'];
                        display('Basisset for TE=24 chosen');
                    end
                otherwise
                    data.basisset{counter}='';
                    display('ERROR: No correct basis set chosen.');
                    nameOfOutputFile='';
                    status=-1;
                    return;
            end;
        otherwise
            data.basisset{counter}='';
            display('ERROR: No correct basis set defined for (J)PRESS different than 1 or 100.');
            nameOfOutputFile='';
            status=-1;
            return;
    end
end

nameOfOutputFile=[pathName 'runProfit_' datestr(now,'yyyymmdd_HHMMSS') '.txt'];

% Write txt file for Profit-Start:
fid=fopen(nameOfOutputFile , 'w');
fprintf(fid, ['JPRESS batch file for intra data set fits on colombo06 \n ' ...
           '****************************************************** \n \n' ...
           'data \t spar \t output path \t water se series \t setting file ' ...
           '\t use \n']);
for counter=1:numberOfAllFiles
    fprintf(fid, '%s\t%s\t%s\t\t%s\t%s\n',data.name{counter}, data.name{counter}, data.profitLoc{counter},data.basisset{counter},'yes');
end;
fclose(fid);

end
