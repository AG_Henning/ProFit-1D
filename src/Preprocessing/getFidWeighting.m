function [data] = getFidWeighting(data, weighting)

t_acq = data.decayPoint;
t = 1:data.pts(2);

switch weighting
    case 'sine-bell'
        phi = pi/4;
        w = sin((pi-phi)*t/t_acq + phi);
        w(t_acq:end) = 0;
    case 'sine-bell-squared'
        phi = pi/4;
        w = sin((pi-phi)*t/t_acq + phi).^2; 
        w(t_acq:end) = 0;
    case 'matched_filter'
        R_LB = 1/(pi*80*1e-3); % Hz linewidth - assumes a Lorentzian of the spectra corresponding to 80 ms T2 decay
        w = exp(-R_LB*data.dt(2)*t);
    case 'trunc'
        w = ones(1,data.pts(2)); % no weighting
        w(t_acq:end) = 0;
end

% figure; plot(w)
data.fid_weights = w;
end