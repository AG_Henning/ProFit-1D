function fittingProFit(filenameData, dataPath, filenameWater, waterPath, TE, TR, outputPath, refFreqPpm, fitSettingsFileName, waterBasisFileName)

if ~exist('refFreqPpm','var')
    refFreqPpm = -2.3; % the reference Frequency for the excitation pulse, given in ppm compared to the water (minus is upfield, + downfield)
end
if ~exist('fitSettingsFileName','var')
    % default fit settings files
    pathName = 'DF data path';
    pathBasisSets = pathToDataFolder(pathName, {'Basis_sets'});
    fitSettingsFileName = [pathBasisSets, 'Basis_sets\final_T2_met_MM_paper/FitSettings_sLASER_3Iter_T2_no_emg_sLASER_7ppm_moiety_UF_basis_TE24'];
    if ~exist('waterBasisFileName','var')
        waterBasisFileName = [pathBasisSets, 'Basis_sets\final_T2_met_MM_paper/sLASER_7ppm_Water_TE24_basis.mat'];
    end
end

%INIT_PROFIT
usePpmGap = false;
nuclues = '1H';
%read in the *.RAW LCModel file
[fid, bandwidth, scanFrequency, ~, ~] = ...
    ImportLCModelBasis(dataPath, filenameData, false, nuclues);

%load fitsettings
load(fitSettingsFileName, 'data', 'fitsettings');

if isempty(filenameWater)
    fitWaterRef = false;
else
    %retrieve the water FID
    [waterRef, ~, ~, ~, ~] = ImportLCModelBasis(waterPath, filenameWater, false, nuclues);
    load(waterBasisFileName, 'basis');
    water_basis = basis;
    fitWaterRef = true;
end

data.createVideo = false;
data.costFunction = 'R3'; %used to set up the cost function in CalcCostFun according to the different options described in the ProFit-1D paper
% 1D spectral data set
data.spectral_dimension = 1;

% add additional data set specific information and settings
data.dt         = [0, 1/bandwidth];
data.bw         = [0 bandwidth];

data.f0         = scanFrequency;      % Hz/ppm
data.te         = TE * 1e-3;
data.pts        = size(fid);
data.total_ppm  = data.bw ./ data.f0;
data.pts_ppm    = data.pts ./ data.total_ppm;
data.tr         = TR;
data.center_ppm = 4.7;
data.phasePivot_ppm = data.center_ppm + refFreqPpm; % in ppm

% set initial vector of time and frequency domain axis
data.timeVect = (0:data.pts(2) - 1) * data.dt(2);
data.w1 = linspace(-data.bw(1)/2, data.bw(1)/2 - data.bw(1)/data.pts(1), data.pts(1));
data.w2 = linspace(-data.bw(2)/2, data.bw(2)/2 - data.bw(2)/data.pts(2), data.pts(2));
data.ppm2 = linspace(-data.total_ppm(2)/2, data.total_ppm(2)/2-data.total_ppm(2)/data.pts(2), data.pts(2)) + data.center_ppm;

data.phase_1_f2_mx = data.ppm2 - data.phasePivot_ppm;

% automatic noise determination
[data.noise_var, data.noise_std] = calcNoiseLevel1D(fid, data);
% zero filling
[fid, fitsettings, data] = zero_fill(fid, fitsettings, data, data.zero_fill_factor);

% define mask and bounding box for fit region
data = DefineFitROI(data);

for zz=1:length(fitsettings.basis.fid)
    %apply the shifting
    spec = fftshift(fft(fitsettings.basis.fid{zz}));
    if usePpmGap
        indexPpmGap = (data.ppm2 < 5.5) & (data.ppm2 > 4.1);
        spec(indexPpmGap)=0;
    end
    fitsettings.basis.fid{zz} = ifft(spec);
end

if data.createVideo
    [data, fid] = createVideoStart(data, fid);
end

%% do Frequency Alignment
zeroFillFactor = 1; indices = [2.008; 3.028; 3.210; 3.921]; searchArea =0.1; 
useReal = false; doSplineSmoothing = false; splineSmoothingCoeff = 0.01;

[frequencyShiftFactorPpm] = alignSpectraFrequencyDomainFactorPpm(fid, data.f0*1e6, data.bw(2), ...
    zeroFillFactor, indices, searchArea, useReal, doSplineSmoothing, splineSmoothingCoeff);
df2_global = frequencyShiftFactorPpm *data.f0;
% frequency shift in the F2
fid = fid .* exp(1i*-df2_global*2*pi*data.timeVect);

% prepare spectrum
spec = fftshift(fft(fid)); 
fid = ifft(spec);

%% filtering truncation
%determine truncation point based on data
[data.truncPoint, data.decayPoint] = getTruncationPoint(fid);
data.truncPointMasked = getTruncationPoint(ifft(spec(data.fitroi.mask)));
data = getFidWeighting(data, 'trunc');

%% iter 0
% pre-fitting determination of global df1 df2 & pc0 to correct measured spectrum
[fid, fitsettings, pc0, df2, pc12] = preprocess(fid, fitsettings, data);

%determine truncation point based on data
[data.truncPoint, data.decayPoint] = getTruncationPoint(fid);
data.truncPointMasked = getTruncationPoint(ifft(spec(data.fitroi.mask)));
data = getFidWeighting(data, 'trunc');

%% iterations 1 to 4 (paper settings)
%% start fitting process
fitresult = profit(fid, fitsettings, data);

if fitWaterRef == true
    %% fit the water reference
    [waterRef, concH2O] = fitWater(waterRef, water_basis, data);
    save([outputPath, filenameData, '_profit_H2O.mat'], 'concH2O');
end

%% corrections from preprocessing
for index = 1 : length(fitresult)
    fitresult{index}.preprocessedValues.pc0 = pc0;
    fitresult{index}.preprocessedValues.pc12 = pc12;
    fitresult{index}.preprocessedValues.df2 = df2 + df2_global;
    %
    pc0_result = fitresult{index}.fitted_values.pc0 + pc0;
    pc12_result = fitresult{index}.fitted_values.pc12 + pc12;
    df2_result = fitresult{index}.fitted_values.df2 + df2 + df2_global;
    df2_result_global = mean(fitresult{index}.fitted_values.df2) + df2 + df2_global;
    %
    fitresult{index}.fitted_values.pc0 = pc0_result;
    fitresult{index}.fitted_values.pc12 = pc12_result;
    fitresult{index}.fitted_values.df2 = df2_result;
    fitresult{index}.preprocessedValues.df2_global = df2_result_global;
end

if data.createVideo
    createVideoEnd();
end

%save the fitresults matrix to a file (final results of the
%fitting, which eventually can be visualized using
%PLOTMETABOLITEFITTING
save([outputPath, filenameData, '_profit.mat'], 'fitresult');
end
