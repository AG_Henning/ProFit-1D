function [fid, fs, pc0, df2, pc12] = preprocess(fid, fs, data)

	fiter = Iteration(fs.basis);
	fiter.ActivateMets({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P', 'Leu'});
    
	fiter.ConnectMets({'name'}, {'conc', 'df2', 'em'});
	fiter.ConnectMets({'global'}, {'pc0', 'pc12'});
	fiter.ConnectMets({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P'}, {'gm'});
	fiter.ConnectMets({'global'}, {'df2'});
	fiter.FixMets({'Leu'}, {'em', 'gm'});
    fiter.SetFixedValues({'Leu'}, {'em'}, 0);
    fiter.SetFixedValues({'Leu'}, {'gm'}, 0);
    fiter.SetStartingValues({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P'}, {'em'}, 6);
    fiter.SetStartingValues({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P'}, {'gm'}, 30);
    fiter.SetLB({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P'}, {'em', 'gm'}, 0);
    fiter.SetLB({'all'}, {'df2'}, -5);
    fiter.SetUB({'all'}, {'df2'}, 5);

	fiter.UpdateIteration;

	% TODO copied from profit.m -> somehow this code should not be present 2 times
	spec = fft(fid);
	% put basis set together for the specific iteration
    map_idx = 1:length(fiter.basis.met);
    map_idx = map_idx(fiter.active_mets);
    basis = cell(1, length(map_idx));
    for kk = 1:length(map_idx)
        basis{kk} = fs.basis.fid{map_idx(kk)}; 
    end

	% handle start values 
    em_x0    = fiter.start_values.em;
    gm_x0    = fiter.start_values.gm;
    pc0_x0   = fiter.start_values.pc0;
    pc12_x0  = fiter.start_values.pc12;
    df2_x0   = fiter.start_values.df2;
    
    x0 = [em_x0, gm_x0, pc0_x0, pc12_x0, df2_x0];
	% handle lower bounds
    em_lb    = fiter.lower_bounds.em;
    gm_lb    = fiter.lower_bounds.gm;
    pc0_lb   = fiter.lower_bounds.pc0;
    pc12_lb  = fiter.lower_bounds.pc12;
    df2_lb   = fiter.lower_bounds.df2;
    
    lb = [em_lb, gm_lb, pc0_lb, pc12_lb, df2_lb];
	% handle upper values 
    em_ub    = fiter.upper_bounds.em;
    gm_ub    = fiter.upper_bounds.gm;
    pc0_ub   = fiter.upper_bounds.pc0;
    pc12_ub  = fiter.upper_bounds.pc12;
    df2_ub   = fiter.upper_bounds.df2;
    
    ub = [em_ub, gm_ub, pc0_ub, pc12_ub, df2_ub];

    data = createSummedMaskFitRoi(spec,fiter,basis,data,em_x0, gm_x0, pc0_x0,pc12_x0,df2_x0);
	fprintf('\nPre-aligning and pre-phasing spectrum...\n');
    % specifiy algorithm details used for the optimization process
    options = optimset('Display', 'off', ...
                       'Algorithm', 'trust-region-reflective', ...
                       'useParallel', 'always', ...
                       'FinDiffType', 'forward', ...
                       'Jacobian', 'off', ...
                       'DerivativeCheck', 'on', ...
                       'Diagnostics', 'off', ...
                       'MaxIter', 15000, ...
					   'TolFun', 1e-7, ...
                       'MaxFunEvals', 150000);
        
    [x, resnorm, residual, exitflag, output, lambda, jacobian] = lsqnonlin(@(x) CalcCostFun(x, spec, fiter, basis, data, []), x0, lb, ub, options);
    lastIter = true;
    [F, conc, basis_matrix, spBB, fitted_spec, fixed_basis, processed_spec]=CalcCostFun(x, spec, fiter, basis, data, [], lastIter);
	% correct coares shifts and zero order phase of measured fid
	pc0 = x(6);
	pc12 = x(7);
	df2 = x(8);

	em   = mean(x(1:4));
	gm   = x(5);

	% output results
	fprintf('estimated pc0:%6.2f deg\n', pc0);
	fprintf('estimated pc12:%6.2f deg\n', pc12);
	fprintf('estimated df2:%6.2f Hz\n\n', df2);
    
	% process fid
	% zero order phase correction
    fid = fid .* exp(1i*-pc0*pi/180*data.phase_0_mx);
    
    % frequency shift in the F2
    fid = fid .* exp(1i*-df2*2*pi*data.timeVect);
    
    spec = fft(fid); %fftshift is not needed and is counter productive due to the data.phase_1_f2_mx
    % precompute coefficient needed to define the parameter unit as degree /ppm
    coeff_F2_degree_ppm = 1i*-pc12*pi/180;
    spec = spec .* exp(coeff_F2_degree_ppm * data.phase_1_f2_mx);

    fid = ifft(spec);
end
