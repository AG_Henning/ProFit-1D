function data = water_separation(water, data)
% determine water relaxation times
% load water TE series
h2o_series = water;
h2o_series_echos = [32 42 53 65 78 93 108 126 146 169 196 230 272 333 435 1000];

water_relax = abs(h2o_series(:,3));

[cfun, gof, ~] = fit(h2o_series_echos(:), water_relax(:), 'exp2');
conf_inter     = predint(cfun, h2o_series_echos(:), 0.95);

if data.debug
    disp(cfun);
    disp(gof);
end

coeffs = coeffvalues(cfun);

t2 = -1./coeffs([2 4]);
content = coeffs([1 3])./sum(coeffs([1 3]));

data.csf_t2 = max(t2);
data.tissue_t2 = min(t2);

data.tissue_content = max(content);
data.csf_content    = min(content);

fprintf('\nRelaxation rates of CSF and tissue water ####################\n');
fprintf('Relaxationtime tissue water:  %8.2fms\n', data.tissue_t2);
fprintf('Relaxationtime CSF water:     %8.2fms\n\n', data.csf_t2);

fprintf('Tissue water signal content:  %8.2f%%\n', data.tissue_content*100);
fprintf('CSF water signal content:     %8.2f%%\n\n', data.csf_content*100);

if data.debug
    figure;
    plot(h2o_series_echos, water_relax, '.');
    hold on;
    plot(cfun);
    plot(h2o_series_echos, conf_inter, 'm--');
    hold off;
end

end