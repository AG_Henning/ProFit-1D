function [truncPoint, decayPoint] = getTruncationPoint(fid)
dataPts = length(fid);
% noiseWindowSize eqaul a 1/50 fraction of the fid, but not smaller than 30 points
if dataPts/50 > 30
    noiseWindowSize = floor(dataPts/50);
else
    noiseWindowSize = 30;
end

noiseWindows = 1:noiseWindowSize:dataPts-noiseWindowSize;

noiseLevels = zeros(1,length(noiseWindows));
% The std deviation in the fid is measured in chuncks of noiseWindowSize FID points
for index = 1:length(noiseWindows)
    currentNoiseWindow = fid(noiseWindows(index):noiseWindows(index)+noiseWindowSize);
    
%     fitobject = fit((0:length(currentNoiseWindow)-1)',currentNoiseWindow','poly1'); %fit a linear curve to noise
%     y         = feval(fitobject,(0:length(currentNoiseWindow)-1)');   % substract poly1 from noise
%     currentNoiseWindow     = currentNoiseWindow - y';% now the noise does not have linear component
    noiseLevels(index) = std(currentNoiseWindow);
end

% Find the noise noise_baseline, where consecutive noise levels are only the
% noise_baseline_level fractions apart. Using the first case where the noise
% doesn't change we try to avoid to get into the zerofilled region. The
% assumption is that we have a decayed signal at least of length 4*noiseWindowSize.
noise_baseline_level = 1.15;
decayPoint = dataPts;  %define worst case scenario
for index = 1:length(noiseWindows)-4
    noise_baseline = noiseLevels(index+4);
    if ((noiseLevels(index)/noise_baseline < noise_baseline_level) && ...
            (noiseLevels(index+1)/noise_baseline < noise_baseline_level) && ...
            (noiseLevels(index+2)/noise_baseline < noise_baseline_level) && ...
            (noiseLevels(index+3)/noise_baseline < noise_baseline_level))
        decayPoint = noiseWindows(index);
        break
    else
        %not needed since in the worst case the noise_baseline will be the last noiseWindow
    end
end

%the truncation point (data.truncPoint) will be set to the first point where the standard
%deviation of the fid is smaller than the noise_std_factor*noise_baseline
noise_std_factor = 1.3;
truncPoint = dataPts; %define worst case scenario
for index = 1:length(noiseWindows)
    if noiseLevels(index)/noise_baseline < noise_std_factor
        truncPoint = noiseWindows(index);
        break;
    end
end
if truncPoint < 600
    truncPoint = 600;
end
end