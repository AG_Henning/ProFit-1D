function [fid, ref] = readMRScan(MRScanFile, numberOfPoints)
fileID = fopen(MRScanFile);
%fill in the values based on the MRScanFile txt file
valuesCellArray = textscan(fileID,'%f %f %f %f','Delimiter',' ','MultipleDelimsAsOne', 1);
if (numberOfPoints ~= size(valuesCellArray{1}))
    ME = MException('readMRScan:WrongFileFormat', ...
        '%s file contains an array that does not match the given size: %d.', ...
        MRScanFile, numberOfPoints);
    throw(ME)
else
    fid = complex(valuesCellArray{1}, valuesCellArray{2});
    ref = complex(valuesCellArray{3}, valuesCellArray{4});
    fid = fid';
    ref = ref';  
end
end