function [ref, concH2O] = fitWater(ref, water_basis, data)

dataWater = data;
%% setup fitting
fiter = Iteration(water_basis);
fiter.ActivateMets({'H2O'});

% set up typical bounds for the first iteration
fiter.SetLB({'all'}, {'conc'}, 0);
fiter.SetLB({'all'}, {'pc0'}, -180);
% fiter.SetLB({'all'}, {'pc12'}, -5);
%     fiter.SetLB({'all'}, {'df2'}, -100);
fiter.SetLB({'all'}, {'em'}, 1); %1
fiter.SetLB({'all'}, {'gm'}, 0);

fiter.SetUB({'all'}, {'pc0'}, 180);
% fiter.SetUB({'all'}, {'pc12'}, 5);
%     fiter.SetUB({'all'}, {'df2'}, 100);
fiter.SetUB({'all'}, {'em'}, 100);%4
fiter.SetUB({'all'}, {'gm'}, 50);


% set starting values (prior knowledge)
fiter.SetStartingValues({'all'}, {'conc'}, 10);
fiter.SetStartingValues({'all'}, {'pc0'}, 0);
fiter.SetStartingValues({'all'}, {'pc12'}, 0);
fiter.SetStartingValues({'all'}, {'df2'}, 0);
fiter.SetStartingValues({'all'}, {'em'}, 2);
fiter.SetStartingValues({'all'}, {'gm'}, 10);

fiter.UpdateIteration;

% zero filling
if (dataWater.zero_fill_factor==1)
    zq3_mx = [];
else
    zq3_mx = zeros(dataWater.pts(1), (dataWater.zero_fill_factor-1)*size(ref,2));
end
ref = [ref zq3_mx];

for zz=1:length(fiter.basis.fid)
    fiter.basis.fid{zz} = [fiter.basis.fid{zz} zq3_mx;];
end
fiter.basis.pts       = size(ref);

%% redefine fit ROI
dataWater.quant_window_ppm = [0 0.6; 0 8.7];
dataWater = DefineFitROI(dataWater);

%% frequency align
zeroFillFactor = 1; indices = [4.66]; searchArea =0.2;
useReal = false; doSplineSmoothing = false; splineSmoothingCoeff = 0.01;
% do Frequency Alignment
[frequencyShiftFactorPpm] = alignSpectraFrequencyDomainFactorPpm(ref, data.f0*1e6, data.bw(2), ...
    zeroFillFactor, indices, searchArea, useReal, doSplineSmoothing, splineSmoothingCoeff);
df2_global = frequencyShiftFactorPpm *data.f0;
% frequency shift in the F2
ref = ref .* exp(1i*-df2_global*2*pi*data.timeVect);

%% prepare actual fitting
spec = fftshift(fft(ref));
% put basis set together for the specific iteration
map_idx = 1:length(fiter.basis.met);
map_idx = map_idx(fiter.active_mets);
basis = cell(1, length(map_idx));
for kk = 1:length(map_idx)
    spec_ = fftshift(fft(water_basis.fid{map_idx(kk)}));
    basis{kk} = ifft(spec_);
end

% handle lower bounds
em_lb   = fiter.lower_bounds.em;
gm_lb   = fiter.lower_bounds.gm;
pc0_lb  = fiter.lower_bounds.pc0;
pc12_lb = fiter.lower_bounds.pc12;
df2_lb  = fiter.lower_bounds.df2;

lb = [em_lb, gm_lb, pc0_lb, pc12_lb, df2_lb];
% handle upper values
em_ub   = fiter.upper_bounds.em;
gm_ub   = fiter.upper_bounds.gm;
pc0_ub  = fiter.upper_bounds.pc0;
pc12_ub = fiter.upper_bounds.pc12;
df2_ub  = fiter.upper_bounds.df2;

ub = [em_ub, gm_ub, pc0_ub, pc12_ub, df2_ub];
% handle start values
em_x0    = fiter.start_values.em;
gm_x0    = fiter.start_values.gm;
pc0_x0   = fiter.start_values.pc0;
pc12_x0  = fiter.start_values.pc12;
df2_x0   = fiter.start_values.df2;

x0 = [em_x0, gm_x0, pc0_x0, pc12_x0, df2_x0];

dataWater = createSummedMaskFitRoi(spec,fiter,basis,dataWater,em_x0, gm_x0, pc0_x0,pc12_x0,df2_x0);
fprintf('\nPre-aligning and pre-phasing spectrum...\n');
% specifiy algorithm details used for the optimization process
options = optimset('Display', 'off', ...
    'Algorithm', 'trust-region-reflective', ...
    'useParallel', 'always', ...
    'FinDiffType', 'forward', ...
    'Jacobian', 'off', ...
    'DerivativeCheck', 'off', ...
    'Diagnostics', 'off', ...
    'MaxIter', 150, ...
    'TolFun', 1e-7, ...
    'MaxFunEvals', 1500);

[x, ~, ~, ~, ~, ~, ~] = ...
    ... The standard return is: [xSolver, resnorm, residual, exitflag, output, lambda, jacobian] ...
    lsqnonlin(@(x) CalcCostFun(x, spec, fiter, basis, dataWater, []), x0, lb, ub, options);
lastIter = true;
dataWater.debug=true;
[F, concH2O, ~, ~, fitted_spec, ~] = CalcCostFun(x, spec, fiter, basis, dataWater, [], lastIter);

end
