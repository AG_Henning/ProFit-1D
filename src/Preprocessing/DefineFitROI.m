function data = DefineFitROI(data)

    % shift the F2 dimension by the negativ part of the linear ppm function
    quant_window_pts = data.quant_window_ppm.*repmat(data.pts_ppm, [size(data.quant_window_ppm,1), 1]) + ...
                       repmat([data.total_ppm(1)/2 abs(data.ppm2(1))].*data.pts_ppm, [size(data.quant_window_ppm,1) 1]);

    % if quant_window_pts exceeds matrix dimensions adjust the boundaries
    quant_window_pts(quant_window_pts(:,1)<1, 1) = 1;
    quant_window_pts(quant_window_pts(:,1)>data.pts(1), 1) = data.pts(1);
    quant_window_pts(quant_window_pts(:,2)<1, 2) = 1;
    quant_window_pts(quant_window_pts(:,2)>data.pts(2), 2) = data.pts(2);

    %create a mask for the spectral quantitation (according to the spectrum type)
    switch data.spectral_dimension
        case 1
            quantitationMask = false(data.pts);
            quantitationMask(1,round(min(quant_window_pts(:,2))):round(max(quant_window_pts(:,2))))=true;
        case 2
            quantitationMask = poly2mask(quant_window_pts(:,2), quant_window_pts(:,1), data.pts(1), data.pts(2));
    end % other dimensionalities not handled. Should have been taken care of before
   
    % find bounding box of mask
    [idx_f1, idx_f2] = find(quantitationMask==1);
    bounds = [min(idx_f1), min(idx_f2); ...
              max(idx_f1), max(idx_f2)];
    quantitationMask = quantitationMask(bounds(1,1):bounds(2,1), ...
                bounds(1,2):bounds(2,2));

    % calculate correct ppm axis tilt matrices etc for the fit roi
    pts       = size(quantitationMask);
    total_ppm = pts ./ data.pts_ppm;
    bw        = total_ppm .* data.f0;
    
    %calculate the dt (according to the spectrum type)
    % based on andi_1D_zombie TODO: delete comment if correct
    dt        = 1 ./ bw;

    t2 = (0:pts(2) - 1) * dt(2);
    w2 = linspace(-bw(2)/2, bw(2)/2 - bw(2)/pts(2), pts(2));
    
    ppm2 = data.ppm2(bounds(1,2):bounds(2,2));

    data.fitroi = struct('bounds', bounds, 'mask', quantitationMask, 'ppm2', ppm2, 'pts', pts, ...
                         't2', t2, 'w2', w2, 'bw', bw, 'dt', dt);
end