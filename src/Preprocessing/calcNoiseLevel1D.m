function [noise_var, noise_std] = calcNoiseLevel1D(fid, data)
% cut out a slices according to the noise window in the data structure and
% calculate the variance of the noise to be later used for the fitting
% process

% shift the F2 dimension by the negativ part of the linear ppm function,
% i.e. calculate the points corresponding to the given ppm values
noiseSlicePoints = data.noise_window_ppm .* data.pts_ppm(2) + ...
                   abs(data.ppm2(1)) .* data.pts_ppm(2);
               
spec = fftshift(fft(fid));

%create a mask to extract the noise variance
mask = false(data.pts);
for noiseSlice = 1 : size(noiseSlicePoints, 1)
    mask(1, round(min(noiseSlicePoints(noiseSlice,:))):round(max(noiseSlicePoints(noiseSlice,:)))) = true;
end

%extract the residual ranges to compare
noiseReal        = real(spec(mask));

%subtract a linear fit from the residual ranges
fitobject = fit((0:length(noiseReal)-1)',noiseReal','poly1'); %fit a linear curve to noise
y         = feval(fitobject,(0:length(noiseReal)-1)');   % substract poly1 from noise
noiseReal     = noiseReal - y';% now the noise does not have linear component

%calculate noise variance of the spectrum in the selected mask area
noise_var = var(noiseReal);
%calculate the standard deviation of the noise in the selected mask area
noise_std = abs(std(noiseReal));

display(['Should be equal: std(noise): ', num2str(std(noiseReal)), ...
    ' vs. rms(noise): ', num2str(rms(noiseReal))]);

if data.debug
    %plot full spectra highlingting noise area
    rangePlotSpec = [min(real(spec)); max(real(spec))];
    figure;
    plot(data.ppm2, real(spec));
    for noiseSlice = 1 : size(noiseSlicePoints, 1)
        line([data.noise_window_ppm(noiseSlice,1); data.noise_window_ppm(noiseSlice,1); ... 
            data.noise_window_ppm(noiseSlice,2); data.noise_window_ppm(noiseSlice,2); ...
            data.noise_window_ppm(noiseSlice,1)], ...
            [rangePlotSpec(1:2); rangePlotSpec(2:-1:1); rangePlotSpec(1)],...
            'LineWidth', 1, 'Color', 'g');
    end
    set(gca, 'XDir', 'reverse');
    xlabel('[ppm]');
    ylabel('[a.u.]');
    %plot the area cropped out as the noise window
    rangePlotSpec = [min(real(spec(mask))); max(real(spec(mask)))];
    figure;
    plot(data.ppm2, real(spec .* mask));
    for noiseSlice = 1 : size(noiseSlicePoints, 1)
        line([data.noise_window_ppm(noiseSlice,1); data.noise_window_ppm(noiseSlice,1); ... 
            data.noise_window_ppm(noiseSlice,2); data.noise_window_ppm(noiseSlice,2); ...
            data.noise_window_ppm(noiseSlice,1)], ...
            [rangePlotSpec(1:2); rangePlotSpec(2:-1:1); rangePlotSpec(1)],...
            'LineWidth', 1, 'Color', 'g');
    end
    set(gca, 'XDir', 'reverse');
    xlabel('[ppm]');
    ylabel('[a.u.]');
    
end

end