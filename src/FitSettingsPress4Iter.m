function FitSettingsPress4Iter(basis, outputFile)

    %% initialize fit specific settings
    data.quant_window_ppm = [ 0.0 0.5; ...
                              0.0 4.2];
    
    data.noise_window_ppm = [-3,  0; ...
                              9, 12.5];

    data.zero_fill_factor    = [2 1];
    data.debug               = false;
    data.do_water_separation = false;
    data.do_water_filtering  = true;
    
    
    %% configure individual fit iterations
    fitsettings = FitQueue(basis);

	%% set up first fitting iteration ************************************************************************
    % ******************************************************************************************************** 
    
    fititer = Iteration(basis);
    
    fititer.ActivateMets({'naa', 'cre303', 'cre391', 'tcho','mi', 'gln', 'glu','baseline'});
    fititer.ConnectMets({'name'}, {'conc', 't1', 'em', 'df1', 'df2'});
    fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12'});

    % keep concetration values for Cre303 & Cr391 the same
    fititer.ConnectMets({'cre303', 'cre391'}, {'conc'});
    fititer.ConnectMets({'baseline'}, {'em_g', 'gm'});
    fititer.ConnectMets({'glu', 'gln'}, {'em'});
    
	% fix parameters to certain values
    fititer.FixMets({'all'}, {'t1'});
    fititer.SetFixedValues({'all'}, {'t1'}, Inf);
    fititer.FixMets({'baseline'}, {'em', 'gm', 'em_g'});
    fititer.SetFixedValues({'baseline'}, {'em', 'gm', 'em_g'}, 0);
    
    % set up typical bounds for the first iteration
    fititer.SetLB({'all'}, {'conc'}, 0);
    fititer.SetLB({'all'}, {'pc0'}, -20);
    fititer.SetLB({'all'}, {'pc11', 'pc12'}, -20);
    fititer.SetLB({'all'}, {'em'}, 1);
    fititer.SetLB({'all'}, {'df1', 'df2'}, -10);
    fititer.SetLB({'all'}, {'gm', 'em_g'}, 0);
    
    fititer.SetUB({'all'}, {'em_g'}, 5);
    fititer.SetUB({'all'}, {'gm'}, 10);
    fititer.SetUB({'all'}, {'em'}, 4);
    fititer.SetUB({'all'}, {'pc0'}, 20);
    fititer.SetUB({'all'}, {'pc11', 'pc12'}, 20);
    fititer.SetUB({'all'}, {'df1', 'df2'}, 10);
    
	% set starting values (prior knowledge)
	fititer.SetStartingValues({'all'}, {'em'}, 2);
	fititer.SetStartingValues({'all'}, {'em_g'}, 0);
	fititer.SetStartingValues({'all'}, {'gm'}, 4);

    fititer.UpdateIteration;
    
    % add the just configured iteration to the queue
    fitsettings.AddIteration(fititer);

	%% set up second fitting iteration ***********************************************************************
    % ******************************************************************************************************** 
    function obj = Starting2(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
        for current_met = {'naa', 'cre303', 'cre391', 'tcho', 'mi', 'gln', 'glu', 'baseline'}
            currentMetString = current_met{:};
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
				fitted_value = res.GetFittedValues({currentMetString}, {current_par});
				if length(fitted_value)>1
					if all(fitted_value == fitted_value(1))
						fitted_value = fitted_value(1);
					else
						error('Something does not seem to be right. Check what is going on');
					end
				end
                obj.SetStartingValues({currentMetString}, {current_par}, fitted_value);
            end
        end
        
        % adjust starting values of newly added metabolites
        for current_met = {'naag', 'gaba', 'glc', 'lac', 'scy', 'tau', 'gly','gsh', 'pe', 'asp', 'ace', 'asc'}
            currentMetString = current_met{:};
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                switch current_par
                    case 'conc'
                        switch currentMetString
                            case {'gaba', 'gsh', 'asp'}
                                scale_factor = 0.2;
                            case {'glc', 'gly', 'ace', 'asc'}
                                scale_factor = 0.1;
                            case {'lac', 'scy'}
                                scale_factor = 0.05;
                            case {'pe','tau'}
                                scale_factor = 0.15;
                            case 'naag'
                                scale_factor = 0.25;
                        end
						% the factor 0.9 comes from the fact that creatine is slightly overestimated in the first iteration
                        obj.SetStartingValues({currentMetString}, {current_par}, scale_factor*0.9*res.GetFittedValues({'cre303'}, {current_par}));
					case 'em'
					    fitted_cr_em = [res.GetFittedValues({'cre303'}, {'em'}), ...
						 			    res.GetFittedValues({'cre391'}, {'em'})];
						fitted_cr_mean_t2 = mean(1./(pi.*fitted_cr_em));
					    fitted_cr_mean_em = 1/(pi*fitted_cr_mean_t2);
                        obj.SetStartingValues({currentMetString}, {current_par}, fitted_cr_mean_em);
                    otherwise
						mean_par_values = mean(unique([res.GetFittedValues({'cre303'}, {current_par}), ...
													   res.GetFittedValues({'cre391'}, {current_par}), ...
													   res.GetFittedValues({'naa'}, {current_par}), ...
													   res.GetFittedValues({'tcho'}, {current_par})]));
                        obj.SetStartingValues({currentMetString}, {current_par}, mean_par_values);
                end
            end
        end
		% set linebroadening for gln similar to the other smaller mets
		obj.SetStartingValues({'gln'}, {'em'}, fitted_cr_mean_em);

        obj.UpdateIteration;
    end

    function obj = LB2(obj, res)
        
	  
	   mean_df1 = mean(unique([res.GetFittedValues({'cre303'}, {'df1'}), ...
	  						   res.GetFittedValues({'cre391'}, {'df1'}), ...
							   res.GetFittedValues({'naa'}, {'df1'}), ...
							   res.GetFittedValues({'tcho'}, {'df1'})]));
	   mean_df2 = mean(unique([res.GetFittedValues({'cre303'}, {'df2'}), ...
	  						   res.GetFittedValues({'cre391'}, {'df2'}), ...
							   res.GetFittedValues({'naa'}, {'df2'}), ...
							   res.GetFittedValues({'tcho'}, {'df2'})]));
	   pc0_lb  = res.GetFittedValues({'cre303'}, {'pc0'}) - 5;
	   df1_lb  = mean_df1 - 3; % 3 represents with this data about 0.02ppm to further converge
	   df2_lb  = mean_df2 - 3;
	   gm_lb   = res.GetFittedValues({'cre303'}, {'gm'}) - 1;
	   em_g_lb = res.GetFittedValues({'cre303'}, {'em_g'}) - 1;
       obj.SetLB({'all'}, {'pc0'}, pc0_lb);
	   obj.SetLB({'all'}, {'df1'}, df1_lb);
       obj.SetLB({'all'}, {'df2'}, df2_lb);
	   if gm_lb<0
	   	   obj.SetLB({'all'}, {'gm'}, 0);
	   else
	   	  obj.SetLB({'all'}, {'gm'}, gm_lb);
	   end
	   if em_g_lb<0
	   	   obj.SetLB({'all'}, {'em_g'}, 0);
	   else
	   	   obj.SetLB({'all'}, {'em_g'}, em_g_lb);
	   end
       %setup identical constraints as in the first iteration for all
       %metabolites, then restrict the bounds for the newly added
       %metabolites -TODO: discuss whether all metabolites should have the
       %cre bounds and not 1
       obj.SetLB({'all'}, {'em'}, 1);
	   % setup restricted em bounds in the first iteration where all mets are present
       fitted_cr_em = [res.GetFittedValues({'cre303'}, {'em'}), ...
		   			   res.GetFittedValues({'cre391'}, {'em'})];
	   cr_lb_t2 = mean(1./(pi.*fitted_cr_em)) + 30e-3;  % has to be plus because increasing t2 in ms yields lower Hz broadening
	   cr_lb_em = 1/(pi*cr_lb_t2);
	   for current_met = {'naag', 'gaba', 'glc', 'lac', 'scy', 'tau', 'gly','gsh', 'pe', 'asp', 'ace', 'asc', 'gln'}
           currentMetString = current_met{:};
		   obj.SetLB({currentMetString}, {'em'}, cr_lb_em);
	   end
       obj.UpdateIteration;
    end

    function obj = UB2(obj, res)

	   mean_df1 = mean(unique([res.GetFittedValues({'cre303'}, {'df1'}), ...
	  						   res.GetFittedValues({'cre391'}, {'df1'}), ...
							   res.GetFittedValues({'naa'}, {'df1'}), ...
							   res.GetFittedValues({'tcho'}, {'df1'})]));
	   mean_df2 = mean(unique([res.GetFittedValues({'cre303'}, {'df2'}), ...
	  						   res.GetFittedValues({'cre391'}, {'df2'}), ...
							   res.GetFittedValues({'naa'}, {'df2'}), ...
							   res.GetFittedValues({'tcho'}, {'df2'})]));
	   df1_ub  = mean_df1 + 3; % 3 represents with this data about 0.02ppm to further converge
	   df2_ub  = mean_df2 + 3;
	   pc0_ub  = res.GetFittedValues({'cre303'}, {'pc0'}) + 5;
	   gm_ub   = res.GetFittedValues({'cre303'}, {'gm'}) + 1;
	   em_g_ub = res.GetFittedValues({'cre303'}, {'em_g'}) + 1;
       obj.SetUB({'all'}, {'df2'}, df2_ub);
       obj.SetUB({'all'}, {'pc0'}, pc0_ub);
	   obj.SetUB({'all'}, {'df1'}, df1_ub);
	   obj.SetUB({'all'}, {'gm'}, gm_ub);
	   obj.SetUB({'all'}, {'em_g'}, em_g_ub);
       %setup identical constraints as in the first iteration for all
       %metabolites, then restrict the bounds for the newly added
       %metabolites -TODO: discuss whether all metabolites should have the
       %cre bounds and not 4
       obj.SetUB({'all'}, {'em'}, 4);
	   % setup restricted em bounds in the first iteration where all mets are present
       fitted_cr_em = [res.GetFittedValues({'cre303'}, {'em'}), ...
		   			   res.GetFittedValues({'cre391'}, {'em'})];
	   cr_ub_t2 = mean(1./(pi.*fitted_cr_em)) - 30e-3;  % has to be plus because decreasing t2 in ms yields higher Hz broadening
	   cr_ub_em = 1/(pi*cr_ub_t2);
	   for current_met = {'naag', 'gaba', 'glc', 'lac', 'scy', 'tau', 'gly','gsh', 'pe', 'asp', 'ace', 'asc', 'gln'}
           current_met = current_met{:};
		   obj.SetUB({current_met}, {'em'}, cr_ub_em);
	   end
	   obj.UpdateIteration;
    end

    fititer = Iteration(basis);
    
    fititer.ActivateMets({'naa', 'cre303', 'cre391', 'tcho', 'mi', 'glu', 'gln', 'naag', 'gaba', 'glc', 'lac', ...
                          'scy','tau','gly','gsh', 'pe', 'asp', 'ace', 'asc', 'baseline'});

    fititer.ConnectMets({'name'}, {'conc', 't1', 'em', 'df1', 'df2'});
    fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12'});

	% use the same em dependent on cre for all small metabolites
	fititer.ConnectMets({'gln', 'naag', 'gaba', 'glc', 'lac', 'scy','tau','gly','gsh', 'pe', 'asp', 'ace', 'asc'}, {'em'});
	fititer.ConnectMets({'gln', 'naag', 'gaba', 'glc', 'lac', 'scy','tau','gly','gsh', 'pe', 'asp', 'ace', 'asc'}, {'df1'});
	fititer.ConnectMets({'gln', 'naag', 'gaba', 'glc', 'lac', 'scy','tau','gly','gsh', 'pe', 'asp', 'ace', 'asc'}, {'df2'});
    
    % keep concetration values for Cre303 & Cr391 the same
    fititer.ConnectMets({'cre303', 'cre391'}, {'conc'});
    fititer.ConnectMets({'baseline'}, {'em', 'em_g', 'gm'});
                
    fititer.FixMets({'all'}, {'t1'});
    fititer.SetFixedValues({'all'}, {'t1'}, Inf);
    fititer.FixMets({'baseline'}, {'em', 'gm', 'em_g'});
    fititer.SetFixedValues({'baseline'}, {'em', 'gm', 'em_g'}, 0);
    
    % set up typical bounds for the first iteration
    fititer.SetLB({'all'}, {'conc'}, 0);
    fititer.SetLB({'all'}, {'pc11', 'pc12'}, -20);

    fititer.SetUB({'all'}, {'pc11', 'pc12'}, 20);

	% add prior knowledge as linear inequality constraints
	% fititer.AddLinPriors({'cre303', 'gaba'}, {'conc'}, [-0.25, 1]);	% gaba < 0.25*cre
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'gaba'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% gaba < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'glc'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% glc < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'asp'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% asp < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'scy'}, {'conc'}, [-0.06, -0.06, -3*0.06, 1]);	% scy < 0.06*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'tau'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% tau < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
	
    % define update function handles
    fititer.UpdateStartingValues = @Starting2;
    fititer.UpdateLowerBounds    = @LB2;
    fititer.UpdateUpperBounds    = @UB2;

    fititer.UpdateIteration;
    
    % add the just configured iteration to the queue
    fitsettings.AddIteration(fititer);

	%% set up third fitting iteration ************************************************************************
    % ******************************************************************************************************** 
    
    function obj = Starting3(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
		for par_cnt=1:length(mobj.Properties)
			current_par = mobj.Properties{par_cnt}.Name;
			mask = res.full_mask.(current_par).full(any(res.full_mask.(current_par).full,2), :);
			for fit_cnt=1:length(res.fitted_values.(current_par))
				fitted_value = res.fitted_values.(current_par)(fit_cnt);
				% the starting values are upated after the bounds so if some copied starting value
				% is outside the currently set bounds e.g. for df2 change that stating value to a value
				% within allowed values. This works just for df2 now. for more general case more conflicts 
				% have to be considered
				if strcmp(current_par, 'df2')
                       if ((fitted_value>obj.upper_bounds.(current_par)(fit_cnt)) || ...
                           (fitted_value<obj.lower_bounds.(current_par)(fit_cnt)))
							fitted_value = mean(unique([res.GetFittedValues({'cre303'}, {current_par}), ...
														res.GetFittedValues({'cre391'}, {current_par}), ...
														res.GetFittedValues({'naa'}, {current_par}), ...
														res.GetFittedValues({'tcho'}, {current_par})]));
                       end
				end
				obj.full_start_values.(current_par)(mask(fit_cnt,:)) = fitted_value;
			end
		end
		% overwrite df2 & df1 starting value for baseline with previously fitted
		obj.full_start_values.df1(obj.met_idx('baseline')) = res.GetFittedValues({'baseline'}, {'df1'}); 
		obj.full_start_values.df2(obj.met_idx('baseline')) = res.GetFittedValues({'baseline'}, {'df2'}); 
        obj.UpdateIteration;
    end

    function obj = LB3(obj, res)
          
 	   fitted_pc0  = res.GetFittedValues({'cre303'}, {'pc0'});
	   fitted_gm   = res.GetFittedValues({'cre303'}, {'gm'});
	   fitted_em_g = res.GetFittedValues({'cre303'}, {'em_g'});
       gm_lb   = fitted_gm - 1;
       em_g_lb = fitted_em_g - 1;
       pc0_lb  = fitted_pc0 - 2.5;
       obj.SetLB({'all'}, {'pc0'}, pc0_lb);
	   if gm_lb<0
	   	   obj.SetLB({'all'}, {'gm'}, 0);
	   else
	   	  obj.SetLB({'all'}, {'gm'}, gm_lb);
	   end
	   if em_g_lb<0
	   	   obj.SetLB({'all'}, {'em_g'}, 0);
	   else
	   	   obj.SetLB({'all'}, {'em_g'}, em_g_lb);
	   end
	   obj.SetLB({'baseline'}, {'df1'}, res.GetFittedValues({'baseline'}, {'df1'}) - 1.27);
	   obj.SetLB({'baseline'}, {'df2'}, res.GetFittedValues({'baseline'}, {'df2'}) - 1.27);
	   
	   mask = res.full_mask.df1.full(any(res.full_mask.df1.full,2), :);
	   for fit_cnt=1:length(res.fitted_values.df1)
	       lb_df1 = res.fitted_values.df1(fit_cnt) - 1.27;
	       obj.full_lower_bounds.df1(mask(fit_cnt,:)) = lb_df1;
	   end
	   
	   mask = res.full_mask.df2.full(any(res.full_mask.df2.full,2), :);
	   for fit_cnt=1:length(res.fitted_values.df2)
	       lb_df2 = res.fitted_values.df2(fit_cnt) - 1.27;
	       obj.full_lower_bounds.df2(mask(fit_cnt,:)) = lb_df2;
       end

       %setup em lower bounds, set up first all values to 1 and then adjust
       %specific values based on res
       obj.SetLB({'all'}, {'em'}, 1);
	   mask = res.full_mask.em.full(any(res.full_mask.em.full,2), :);
	   for fit_cnt=1:length(res.fitted_values.em)
	       fitted_value = res.fitted_values.em(fit_cnt);
		   lb_t2 = 1/(pi*fitted_value) + 20e-3;
		   lb_em = 1/(pi*lb_t2);
           if lb_em < 0 %avoid non physical behaviour
               lb_em = 0;
           end
	       obj.full_lower_bounds.em(mask(fit_cnt,:)) = lb_em;
	   end

       obj.UpdateIteration;
    end

    function obj = UB3(obj, res)
         
 	   fitted_pc0  = res.GetFittedValues({'cre303'}, {'pc0'});
	   fitted_gm   = res.GetFittedValues({'cre303'}, {'gm'});
	   fitted_em_g = res.GetFittedValues({'cre303'}, {'em_g'});
       gm_lb   = fitted_gm + 1;
       em_g_lb = fitted_em_g + 1;
       pc0_lb  = fitted_pc0 + 2.5;
       obj.SetUB({'all'}, {'pc0'}, pc0_lb);
	   obj.SetUB({'all'}, {'gm'}, gm_lb);
	   obj.SetUB({'all'}, {'em_g'}, em_g_lb);
	   obj.SetUB({'baseline'}, {'df1'}, res.GetFittedValues({'baseline'}, {'df1'}) + 1.27);
	   obj.SetUB({'baseline'}, {'df2'}, res.GetFittedValues({'baseline'}, {'df2'}) + 1.27);

	   mask = res.full_mask.df1.full(any(res.full_mask.df1.full,2), :);
	   for fit_cnt=1:length(res.fitted_values.df1)
	       ub_df1 = res.fitted_values.df1(fit_cnt) + 1.27;
	       obj.full_upper_bounds.df1(mask(fit_cnt,:)) = ub_df1;
	   end
	   
	   mask = res.full_mask.df2.full(any(res.full_mask.df2.full,2), :);
	   for fit_cnt=1:length(res.fitted_values.df2)
	       ub_df2 = res.fitted_values.df2(fit_cnt) + 1.27;
	       obj.full_upper_bounds.df2(mask(fit_cnt,:)) = ub_df2;
	   end

       %setup em upper bounds, set up first all values to 4 and then adjust
       %specific values based on res
       obj.SetUB({'all'}, {'em'}, 4);
	   mask = res.full_mask.em.full(any(res.full_mask.em.full,2), :);
	   for fit_cnt=1:length(res.fitted_values.em)
	       fitted_value = res.fitted_values.em(fit_cnt);
		   ub_t2 = 1/(pi*fitted_value) - 20e-3;
		   ub_em = 1/(pi*ub_t2);
	       obj.full_upper_bounds.em(mask(fit_cnt,:)) = ub_em;
	   end
       
	   obj.UpdateIteration;
    end

    fititer = Iteration(basis);
    
    fititer.ActivateMets({'naa', 'cre303', 'cre391', 'tcho', 'mi', 'glu', 'gln', 'naag', 'gaba', 'glc', 'lac', ...
                          'scy','tau','gly','gsh', 'pe', 'asp', 'ace', 'asc', 'baseline'});
    
    fititer.ConnectMets({'name'}, {'conc', 't1', 'em', 'df1', 'df2'});
    fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12'});
    
	% untangle some multiplets to allow for separate relaxation rates
	fititer.met_connections.em(3,4) = false;	% separate naa sub signals
	fititer.met_connections.em(4,3) = false;
	% separate choline multiplets from singlets so they will have different relaxations
	idx = sub2ind(size(fititer.met_connections.em), [5 6 8 5 6 8 7 7 7 9 9 9], [7 7 7 9 9 9 5 6 8 5 6 8]);
	fititer.met_connections.em(idx) = false;

    % keep concetration values for Cre303 & Cr391 the same
    fititer.ConnectMets({'cre303', 'cre391'}, {'conc'});
    fititer.ConnectMets({'baseline'}, {'em', 'em_g', 'gm'});
        
    fititer.FixMets({'all'}, {'t1'});
    fititer.SetFixedValues({'all'}, {'t1'}, Inf);
    fititer.FixMets({'baseline'}, {'em', 'gm', 'em_g'});
    fititer.SetFixedValues({'baseline'}, {'em', 'gm', 'em_g'}, 0);

    % set up typical bounds for the first iteration
    fititer.SetLB({'all'}, {'conc'}, 0);
    fititer.SetLB({'all'}, {'pc11'}, -10);
    fititer.SetLB({'all'}, {'pc12'}, -10);
    
    fititer.SetUB({'all'}, {'pc11'}, 10);
    fititer.SetUB({'all'}, {'pc12'}, 10);
    
	% add prior knowledge as linear inequality constraints
%	fititer.AddLinPriors({'cre303', 'gaba'}, {'conc'}, [-0.25, 1]);	% gaba < 0.25*cre
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'gaba'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% gaba < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'glc'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% glc < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'asp'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% asp < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'scy'}, {'conc'}, [-0.06, -0.06, -3*0.06, 1]);	% scy < 0.06*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'tau'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% tau < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'asp', 'gln'}, {'conc'}, [1, -1]);	% asp < gln
	
    % define update function handles
    fititer.UpdateStartingValues = @Starting3;
    fititer.UpdateLowerBounds    = @LB3;
    fititer.UpdateUpperBounds    = @UB3;

    fititer.UpdateIteration;
    
    % add the just configured iteration to the queue
    fitsettings.AddIteration(fititer);

    %% set up forth fitting iteration ************************************************************************
    % ******************************************************************************************************** 
    
    function obj = Starting4(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
		for par_cnt=1:length(mobj.Properties)
			current_par = mobj.Properties{par_cnt}.Name;
			mask = res.full_mask.(current_par).full(any(res.full_mask.(current_par).full,2), :);
			for fit_cnt=1:length(res.fitted_values.(current_par))
				fitted_value = res.fitted_values.(current_par)(fit_cnt);
				obj.full_start_values.(current_par)(mask(fit_cnt,:)) = fitted_value;
			end
		end
		obj.SetStartingValues({'all'}, {'em_g', 'gm'}, 0);	
        obj.UpdateIteration;
    end

    function obj = LB4(obj, res)

		% keep the lower bounds from the previous iteration
		obj.full_lower_bounds.pc0 = res.full_lower_bounds.pc0;
		obj.full_lower_bounds.df1 = res.full_lower_bounds.df1;
		obj.full_lower_bounds.df2 = res.full_lower_bounds.df2;
		obj.full_lower_bounds.em = res.full_lower_bounds.em;
	    obj.SetLB({'all'}, {'gm'}, 0);
	    obj.SetLB({'all'}, {'em_g'}, 0); 
	 	obj.UpdateIteration;
    end

    function obj = UB4(obj, res)

		% keep the upper bounds from the previous iteration
		obj.full_upper_bounds.pc0 = res.full_upper_bounds.pc0;
		obj.full_upper_bounds.df1 = res.full_upper_bounds.df1;
		obj.full_upper_bounds.df2 = res.full_upper_bounds.df2;
		obj.full_upper_bounds.em = res.full_upper_bounds.em;
		obj.SetUB({'all'}, {'gm'}, 1);
	    obj.SetUB({'all'}, {'em_g'}, 1); 
	 	obj.UpdateIteration;
    end

    fititer = Iteration(basis);
    
    fititer.ActivateMets({'naa', 'cre303', 'cre391', 'tcho', 'mi', 'glu', 'gln', 'naag', 'gaba', 'glc', 'lac', ...
                          'scy','tau','gly','gsh', 'pe', 'asp', 'ace', 'asc', 'baseline'});
    
    fititer.ConnectMets({'name'}, {'conc', 't1'});
    fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12'});
    
	% untangle some multiplets to allow for separate relaxation rates
	fititer.met_connections.em(3,4) = false;	% separate naa sub signals
	fititer.met_connections.em(4,3) = false;
	% separate choline multiplets from singlets so they will have different relaxations
	idx = sub2ind(size(fititer.met_connections.em), [5 6 8 5 6 8 7 7 7 9 9 9], [7 7 7 9 9 9 5 6 8 5 6 8]);
	fititer.met_connections.em(idx) = false;
	
    % keep concetration values for Cre303 & Cr391 the same
    fititer.ConnectMets({'cre303', 'cre391'}, {'conc'});
    fititer.ConnectMets({'baseline'}, {'em', 'em_g', 'gm'});
        
    fititer.FixMets({'all'}, {'t1'});
    fititer.SetFixedValues({'all'}, {'t1'}, Inf);
    fititer.FixMets({'baseline'}, {'em', 'gm', 'em_g'});
    fititer.SetFixedValues({'baseline'}, {'em', 'gm', 'em_g'}, 0);

    % set up typical bounds for the first iteration
    fititer.SetLB({'all'}, {'conc'}, 0);
    fititer.SetLB({'all'}, {'pc11', 'pc12'}, -10);

    fititer.SetUB({'all'}, {'pc11', 'pc12'}, 10);
    
	% add prior knowledge as linear inequality constraints
%	fititer.AddLinPriors({'cre303', 'gaba'}, {'conc'}, [-0.25, 1]);	% gaba < 0.25*cre
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'gaba'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% gaba < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'glc'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% glc < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'asp'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% asp < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'scy'}, {'conc'}, [-0.06, -0.06, -3*0.06, 1]);	% scy < 0.06*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'tau'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% tau < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'asp', 'gln'}, {'conc'}, [1, -1]);	% asp < gln
	
	% define update function handles
    fititer.UpdateStartingValues = @Starting4;
    fititer.UpdateLowerBounds    = @LB4;
    fititer.UpdateUpperBounds    = @UB4;

    % turn on nonparametric spline baseline part 
    fititer.use_sp_baseline     = true;
    fititer.spbase_knot_spacing = [0.2 0.2];
    fititer.spbase_lambda       = 0e-1*[1 1];
    
    % also turn on spline line shape model 
    fititer.use_sp_lineshape = true;
    fititer.full_apply_sp_lineshape(fititer.met_boolidx('baseline')) = false; % turn off deconv line shape for baseline
    
    fititer.UpdateIteration;
    
    % add the just configured iteration to the queue
    fitsettings.AddIteration(fititer);

    %% save protocol file
%     save('final_settings_004+.mat', 'fitsettings', 'data');
    save(outputFile, 'fitsettings', 'data');
    
    
 end
