function FitSettings_sLASER_3Iter(basis, outputFile)

%% initialize fit specific settings
data.quant_window_ppm = [ 0.0 0.6; ...
    0.0 4.1];
% data.quant_window_ppm = [ 0.0 1.95; ...
%     0.0 4.2];

data.noise_window_ppm = [-3,  0; ...
    9, 12.5];

data.zero_fill_factor    = [2 1];
data.debug               = false;
data.do_water_separation = false;
data.do_water_filtering  = true;


%% configure individual fit iterations
fitsettings = FitQueue(basis);

%% set up first fitting iteration ************************************************************************
% ********************************************************************************************************

fititer = Iteration(basis);

fititer.ActivateMets({'NAA_ac', 'NAA_as', 'Cr', 'Cr_CH2', 'tCho_P','mI', 'Gln', 'Glu','Leu'}); %Gln NAA_as raus
fititer.ConnectMets({'name'}, {'conc',  'em', 'df2'});
fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 't1'});

% keep concetration values for Cr & Cr391 the same
% fititer.ConnectMets({'Cr', 'Cr_CH2'}, {'conc'});
fititer.ConnectMets({'Leu'}, {'em_g', 'gm'});
fititer.ConnectMets({'Glu', 'Gln'}, {'em'});

% fix parameters to certain values
fititer.FixMets({'all'}, {'t1'});
fititer.SetFixedValues({'all'}, {'t1'}, Inf);
fititer.FixMets({'all'}, {'df1'});
fititer.SetFixedValues({'all'}, {'df1'}, 0);
fititer.FixMets({'all'}, {'pc11'});
fititer.SetFixedValues({'all'}, {'pc11'}, 0);
fititer.FixMets({'all'}, {'em_g'}); % ^^^^
fititer.SetFixedValues({'all'}, {'em_g'}, 0); % ^^^^
fititer.FixMets({'Leu'}, {'em', 'gm', 'em_g'});
fititer.SetFixedValues({'Leu'}, {'em', 'gm', 'em_g'}, 0);

% set up typical bounds for the first iteration
fititer.SetLB({'all'}, {'conc'}, 0);
fititer.SetLB({'all'}, {'pc0'}, -10);
fititer.SetLB({'all'}, {'pc12'}, -5);
fititer.SetLB({'all'}, {'em'}, 0.5);
fititer.SetLB({'all'}, {'df2'}, -10);
fititer.SetLB({'all'}, {'gm'}, 0);
% fititer.SetLB({'all'}, {'em_g'}, 0); % ^^^^

% fititer.SetUB({'all'}, {'em_g'}, 10); % ^^^^
fititer.SetUB({'all'}, {'gm'}, 100);
fititer.SetUB({'all'}, {'em'}, 30);
fititer.SetUB({'all'}, {'pc0'}, 10);
fititer.SetUB({'all'}, {'pc12'}, 5);
fititer.SetUB({'all'}, {'df2'}, 10);

% set starting values (prior knowledge)
fititer.SetStartingValues({'all'}, {'em'}, 6);
% fititer.SetStartingValues({'all'}, {'em_g'}, 6); % ^^^^
fititer.SetStartingValues({'all'}, {'gm'}, 10);
fititer.SetStartingValues({'all'}, {'pc0'}, 0);
fititer.SetStartingValues({'all'}, {'pc12'}, 0);

fititer.UpdateIteration;

% add the just configured iteration to the queue
fitsettings.AddIteration(fititer);

        
    function obj = sanityCheckCorrection(obj)
        mobj = ?Parameters;
        for par_cnt=1:length(mobj.Properties)
            current_par = mobj.Properties{par_cnt}.Name;
            mask_outer_of_upper_bounds = obj.start_values.(current_par) > obj.upper_bounds.(current_par);
            obj.start_values.(current_par)(mask_outer_of_upper_bounds) = obj.upper_bounds.(current_par)(mask_outer_of_upper_bounds);
            mask_outer_of_lower_bounds = obj.start_values.(current_par) > obj.lower_bounds.(current_par);
            obj.start_values.(current_par)(mask_outer_of_lower_bounds) = obj.lower_bounds.(current_par)(mask_outer_of_lower_bounds);
        end
    end

%% set up second fitting iteration ***********************************************************************
% ********************************************************************************************************
    function obj = Starting2(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
        for current_met = {'NAA_ac', 'NAA_as', 'Cr', 'Cr_CH2', 'tCho_P', 'mI', 'Gln', 'Glu', 'Leu'}
            currentMetString = current_met{:};
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                fitted_value = res.GetFittedValues({currentMetString}, {current_par});
                if length(fitted_value)>1
                    if all(fitted_value == fitted_value(1))
                        fitted_value = fitted_value(1);
                    else
                        error('Something does not seem to be right. Check what is going on');
                    end
                end
                obj.SetStartingValues({currentMetString}, {current_par}, fitted_value);
            end
        end
        
        referenceConcentration = res.GetFittedValues({'Cr'}, {'conc'});
        fitted_cr_em = [res.GetFittedValues({'Cr'}, {'em'}), ...
            res.GetFittedValues({'Cr_CH2'}, {'em'})];
        fitted_cr_mean_t2 = mean(1./(pi.*fitted_cr_em));
        fitted_cr_mean_em = 1/(pi*fitted_cr_mean_t2);
        % adjust starting values of newly added metabolites
        for current_met = {'NAAG', 'GABA', 'Lac', 'Scyllo', 'Tau', 'Glyc','GSH', 'Asp'}
            currentMetString = current_met{:};
            for par_cnt=1:length(mobj.Properties)
                current_par = mobj.Properties{par_cnt}.Name;
                switch current_par
                    case 'conc'
                        switch currentMetString
                            case {'GABA', 'GSH', 'Asp'}
                                scale_factor = 0.2;
                            case {'Glyc'}
                                scale_factor = 0.1;
                            case {'Lac', 'Scyllo'}
                                scale_factor = 0.05;
                            case {'Tau'}
                                scale_factor = 0.15;
                            case {'NAAG'}
                                scale_factor = 0.25;
                        end
                        % the factor 0.9 comes from the fact that creatine is slightly overestimated in the first iteration
                        obj.SetStartingValues({currentMetString}, {current_par}, scale_factor*0.9*referenceConcentration);
                    case 'em'
                        obj.SetStartingValues({currentMetString}, {current_par}, fitted_cr_mean_em);
                    otherwise
                        mean_par_values = mean(unique([res.GetFittedValues({'Cr'}, {current_par}), ...
                            res.GetFittedValues({'Cr_CH2'}, {current_par}), ...
                            res.GetFittedValues({'NAA_ac'}, {current_par}), ...
                            res.GetFittedValues({'tCho_P'}, {current_par})]));
                        obj.SetStartingValues({currentMetString}, {current_par}, mean_par_values);
                end
            end
        end
        % set linebroadening for Gln simIlar to the other smaller mets
        obj.SetStartingValues({'Gln'}, {'em'}, fitted_cr_mean_em);
        
        obj.UpdateIteration;
        obj = sanityCheckCorrection(obj);
    end


    function obj = LB2(obj, res)
        
        mean_df2 = mean(unique([res.GetFittedValues({'Cr'}, {'df2'}), ...
            res.GetFittedValues({'Cr_CH2'}, {'df2'}), ...
            res.GetFittedValues({'NAA_ac'}, {'df2'}), ...
            res.GetFittedValues({'tCho_P'}, {'df2'})]));
        pc0_lb  = res.GetFittedValues({'Cr'}, {'pc0'}) - 5;
        df2_lb  = mean_df2 - 3;
        gm_lb   = res.GetFittedValues({'Cr'}, {'gm'}) - 5;
%         em_g_lb = res.GetFittedValues({'Cr'}, {'em_g'}) - 5; % ^^^^
        obj.SetLB({'all'}, {'pc0'}, pc0_lb);
        obj.SetLB({'all'}, {'df2'}, df2_lb);
        if gm_lb<0
            obj.SetLB({'all'}, {'gm'}, 0);
        else
            obj.SetLB({'all'}, {'gm'}, gm_lb);
        end
%         if em_g_lb<0  % ^^^^
%             obj.SetLB({'all'}, {'em_g'}, 0);
%         else % ^^^^
%             obj.SetLB({'all'}, {'em_g'}, em_g_lb);
%         end
        %setup identical constraints as in the first iteration for all
        %metabolites, then restrict the bounds for the newly added
        %metabolites -TODO: discuss whether all metabolites should have the
        %cre bounds and not 1
        obj.SetLB({'all'}, {'em'}, 1);
        % setup restricted em bounds in the first iteration where all mets are present
%         fitted_cr_em = [res.GetFittedValues({'Cr'}, {'em'}), ...
%             res.GetFittedValues({'Cr_CH2'}, {'em'})];
%         cr_lb_t2 = mean(1./(pi.*fitted_cr_em)) + 50e-3;  % has to be plus because increasing t2 in ms yields lower Hz broadening
%         cr_lb_em = 1/(pi*cr_lb_t2);
%         if cr_lb_em < 0 %avoid non physical behaviour
%             cr_lb_em = 0;
%         end
%         for current_met = {'NAAG', 'GABA', 'Lac', 'Scyllo', 'Tau', 'Glyc','GSH', 'Asp', 'Gln'}
%             currentMetString = current_met{:};
%             obj.SetLB({currentMetString}, {'em'}, cr_lb_em);
%         end
        obj.UpdateIteration;
    end

    function obj = UB2(obj, res)
        
        mean_df2 = mean(unique([res.GetFittedValues({'Cr'}, {'df2'}), ...
            res.GetFittedValues({'Cr_CH2'}, {'df2'}), ...
            res.GetFittedValues({'NAA_ac'}, {'df2'}), ...
            res.GetFittedValues({'tCho_P'}, {'df2'})]));
        df2_ub  = mean_df2 + 3;
        pc0_ub  = res.GetFittedValues({'Cr'}, {'pc0'}) + 5;
        gm_ub   = res.GetFittedValues({'Cr'}, {'gm'}) + 5;
%         em_g_ub = res.GetFittedValues({'Cr'}, {'em_g'}) + 5; % ^^^^
        obj.SetUB({'all'}, {'df2'}, df2_ub);
        obj.SetUB({'all'}, {'pc0'}, pc0_ub);
        obj.SetUB({'all'}, {'gm'}, gm_ub);
%         obj.SetUB({'all'}, {'em_g'}, em_g_ub); % ^^^^
        %setup identical constraints as in the first iteration for all
        %metabolites, then restrict the bounds for the newly added
        %metabolites -TODO: discuss whether all metabolites should have the
        %cre bounds and not 4
        obj.SetUB({'all'}, {'em'}, 30);
%         % setup restricted em bounds in the first iteration where all mets are present
%         fitted_cr_em = [res.GetFittedValues({'Cr'}, {'em'}), ...
%             res.GetFittedValues({'Cr_CH2'}, {'em'})];
%         cr_ub_t2 = mean(1./(pi.*fitted_cr_em)) - 50e-3;  % has to be plus because decreasing t2 in ms yields higher Hz broadening
%         cr_ub_em = 1/(pi*cr_ub_t2);
%         for current_met = {'NAAG', 'GABA', 'Lac', 'Scyllo', 'Tau', 'Glyc','GSH', 'Asp', 'Gln'}
%             currentMetString = current_met{:};
%             obj.SetUB({currentMetString}, {'em'}, cr_ub_em);
%         end
        obj.UpdateIteration;
    end

fititer = Iteration(basis);

fititer.ActivateMets({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P', 'mI', 'Glu', 'Gln', 'NAAG', 'GABA', 'NAA_as', 'Lac', ...
    'Scyllo','Tau','Glyc','GSH', 'Asp', 'Leu'});

fititer.ConnectMets({'name'}, {'conc', 'em', 'df2'});
fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 't1'});

% use the same em dependent on cre for all small metabolites
fititer.ConnectMets({'Gln', 'NAAG', 'GABA', 'NAA_as', 'Lac', 'Scyllo','Tau','Glyc','GSH', 'Asp'}, {'em'});
fititer.ConnectMets({'Gln', 'NAAG', 'GABA', 'NAA_as', 'Lac', 'Scyllo','Tau','Glyc','GSH', 'Asp'}, {'df2'});

% keep concetration values for Cr & Cr391 the same
% fititer.ConnectMets({'Cr', 'Cr_CH2'}, {'conc'});
% fititer.ConnectMets({'NAA_ac', 'NAA_as'}, {'conc'});
fititer.ConnectMets({'Leu'}, {'em', 'em_g', 'gm'});

% fix parameters to certain values
fititer.FixMets({'all'}, {'t1'});
fititer.SetFixedValues({'all'}, {'t1'}, Inf);
fititer.FixMets({'all'}, {'df1'});
fititer.SetFixedValues({'all'}, {'df1'}, 0);
fititer.FixMets({'all'}, {'pc11'});
fititer.SetFixedValues({'all'}, {'pc11'}, 0);
fititer.FixMets({'all'}, {'em_g'}); % ^^^^
fititer.SetFixedValues({'all'}, {'em_g'}, 0); % ^^^^
fititer.FixMets({'Leu'}, {'em', 'gm', 'em_g'});
fititer.SetFixedValues({'Leu'}, {'em', 'gm', 'em_g'}, 0);

% set up typical bounds for the first iteration
fititer.SetLB({'all'}, {'conc'}, 0);
fititer.SetLB({'all'}, {'pc12'}, -5);

fititer.SetUB({'all'}, {'pc12'}, 5);

% add prior knowledge as linear inequality constraints
% fititer.AddLinPriors({'Cr', 'GABA'}, {'conc'}, [-0.25, 1]);	% GABA < 0.25*cre
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'GABA'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% GABA < 0.1*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'NAA_as'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% NAA_as < 0.1*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'Asp'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% Asp < 0.1*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'Scyllo'}, {'conc'}, [-0.06, -0.06, -3*0.06, 1]);	% Scyllo < 0.06*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'Tau'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% Tau < 0.1*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3

% define update function handles
fititer.UpdateLowerBounds    = @LB2;
fititer.UpdateUpperBounds    = @UB2;
fititer.UpdateStartingValues = @Starting2;

fititer.UpdateIteration;

% add the just configured iteration to the queue
fitsettings.AddIteration(fititer);

%% set up third fitting iteration ************************************************************************
% ********************************************************************************************************

    function obj = Starting3(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
        for par_cnt=1:length(mobj.Properties)
            current_par = mobj.Properties{par_cnt}.Name;
            mask = res.full_mask.(current_par).full(any(res.full_mask.(current_par).full,2), :);
            for fit_cnt=1:length(res.fitted_values.(current_par))
                fitted_value = res.fitted_values.(current_par)(fit_cnt);
                % the starting values are upated after the bounds so if some copied starting value
                % is outside the currently set bounds e.g. for df2 change that stating value to a value
                % within allowed values. This works just for df2 now. for more general case more conflicts
                % have to be considered
                if strcmp(current_par, 'df2')
                    if ((fitted_value>obj.upper_bounds.(current_par)(fit_cnt)) || ...
                            (fitted_value<obj.lower_bounds.(current_par)(fit_cnt)))
                        fitted_value = mean(unique([res.GetFittedValues({'Cr'}, {current_par}), ...
                            res.GetFittedValues({'Cr_CH2'}, {current_par}), ...
                            res.GetFittedValues({'NAA_ac'}, {current_par}), ...
                            res.GetFittedValues({'tCho_P'}, {current_par})]));
                    end
                end
                
                if strcmp(current_par, 'em')
                    fitted_value = mean(res.fitted_values.em);
                end
                obj.full_start_values.(current_par)(mask(fit_cnt,:)) = fitted_value;
            end
        end
        % overwrite df2 & df1 starting value for Leu with previously fitted
        obj.full_start_values.df2(obj.met_idx('Leu')) = res.GetFittedValues({'Leu'}, {'df2'});
        obj.SetStartingValues({'Leu'}, {'gm'}, 2);
        obj.UpdateIteration;
        obj = sanityCheckCorrection(obj);
    end

    function obj = LB3(obj, res)
        
        fitted_pc0  = res.GetFittedValues({'Cr'}, {'pc0'});
        fitted_gm   = res.GetFittedValues({'Cr'}, {'gm'});
%         fitted_em_g = res.GetFittedValues({'Cr'}, {'em_g'}); % ^^^^
        gm_lb   = fitted_gm - 2;
%         em_g_lb = fitted_em_g - 2; % ^^^^
        pc0_lb  = fitted_pc0 - 2.5;
        obj.SetLB({'all'}, {'pc0'}, pc0_lb);
        if gm_lb<0
            obj.SetLB({'all'}, {'gm'}, 0);
        else
            obj.SetLB({'all'}, {'gm'}, gm_lb);
        end
%         if em_g_lb<0 % ^^^^
%             obj.SetLB({'all'}, {'em_g'}, 0);
%         else % ^^^^
%             obj.SetLB({'all'}, {'em_g'}, em_g_lb);
%         end
        obj.SetLB({'Leu'}, {'df2'}, res.GetFittedValues({'Leu'}, {'df2'}) - 1.27);
        
        mask = res.full_mask.df2.full(any(res.full_mask.df2.full,2), :);
        for fit_cnt=1:length(res.fitted_values.df2)
            lb_df2 = res.fitted_values.df2(fit_cnt) - 1.27;
            obj.full_lower_bounds.df2(mask(fit_cnt,:)) = lb_df2;
        end
        
        %setup em lower bounds, set up first all values to 1 and then adjust
        %specific values based on res
        obj.SetLB({'all'}, {'em'}, 1);
%         mask = res.full_mask.em.full(any(res.full_mask.em.full,2), :);
%         for fit_cnt=1:length(res.fitted_values.em)
%             fitted_value = res.fitted_values.em(fit_cnt);
%             if fitted_value ~= 0
%                 lb_t2 = 1/(pi*fitted_value) + 40e-3;
%                 lb_em = 1/(pi*lb_t2);
%                 if lb_em < 0.5 %avoid non physical behaviour (T2 larger than 630 ms)
%                     lb_em = 0.5;
%                 end
%                 obj.full_lower_bounds.em(mask(fit_cnt,:)) = lb_em;
%             else
%                 obj.full_lower_bounds.em(mask(fit_cnt,:)) = 1;
%             end
%         end
        obj.SetLB({'Leu'}, {'gm'}, 0); 
        obj.UpdateIteration;
    end

    function obj = UB3(obj, res)
        
        fitted_pc0  = res.GetFittedValues({'Cr'}, {'pc0'});
        fitted_gm   = res.GetFittedValues({'Cr'}, {'gm'});
%         fitted_em_g = res.GetFittedValues({'Cr'}, {'em_g'}); % ^^^^
        gm_lb   = fitted_gm + 2;
%         em_g_lb = fitted_em_g + 2; % ^^^^
        pc0_lb  = fitted_pc0 + 2.5;
        obj.SetUB({'all'}, {'pc0'}, pc0_lb);
        obj.SetUB({'all'}, {'gm'}, gm_lb);
%         obj.SetUB({'all'}, {'em_g'}, em_g_lb); % ^^^^
        obj.SetUB({'Leu'}, {'df2'}, res.GetFittedValues({'Leu'}, {'df2'}) + 1.27);
        
        mask = res.full_mask.df2.full(any(res.full_mask.df2.full,2), :);
        for fit_cnt=1:length(res.fitted_values.df2)
            ub_df2 = res.fitted_values.df2(fit_cnt) + 1.27;
            obj.full_upper_bounds.df2(mask(fit_cnt,:)) = ub_df2;
        end
        
        %setup em upper bounds, set up first all values to 4 and then adjust
        %specific values based on res
        obj.SetUB({'all'}, {'em'}, 30);
%         mask = res.full_mask.em.full(any(res.full_mask.em.full,2), :);
%         for fit_cnt=1:length(res.fitted_values.em)
%             fitted_value = res.fitted_values.em(fit_cnt);
%             if fitted_value ~= 0
%                 ub_t2 = 1/(pi*fitted_value) - 40e-3;
%                 if ub_t2 < 10*1e-3 %set lower bound to T2 = 10 ms
%                     ub_t2 = 10*1e-3;
%                 end
%                 ub_em = 1/(pi*ub_t2);
%                 obj.full_upper_bounds.em(mask(fit_cnt,:)) = ub_em;
%             else
%                 obj.full_upper_bounds.em(mask(fit_cnt,:)) = 30;
%             end
%         end
%         
        obj.SetUB({'Leu'}, {'gm'}, 15);
        obj.UpdateIteration;
    end

fititer = Iteration(basis);

fititer.ActivateMets({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P', 'mI', 'Glu', 'Gln', 'NAAG', 'GABA', 'NAA_as', 'Lac', ...
    'Scyllo','Tau','Glyc','GSH', 'Asp', 'Leu'});

fititer.ConnectMets({'name'}, {'conc', 'em', 'df2'});
fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 'df1', 't1'});

% keep concetration values for Cr & Cr391 the same
% fititer.ConnectMets({'Cr', 'Cr_CH2'}, {'conc'});
% fititer.ConnectMets({'NAA_ac', 'NAA_as'}, {'conc'});
fititer.ConnectMets({'Leu'}, {'em', 'em_g', 'gm'}); 

% fix parameters to certain values
fititer.FixMets({'all'}, {'t1'});
fititer.SetFixedValues({'all'}, {'t1'}, Inf);
fititer.FixMets({'all'}, {'df1'});
fititer.SetFixedValues({'all'}, {'df1'}, 0);
fititer.FixMets({'all'}, {'pc11'});
fititer.SetFixedValues({'all'}, {'pc11'}, 0);
% fititer.FixMets({'Leu'}, {'em', 'gm', 'em_g'});
% fititer.SetFixedValues({'Leu'}, {'em', 'gm', 'em_g'}, 0);
fititer.FixMets({'Leu'}, {'em', 'em_g'}); 
fititer.SetFixedValues({'Leu'}, {'em', 'em_g'}, 0); 
fititer.FixMets({'all'}, {'em_g'}); % ^^^^
fititer.SetFixedValues({'all'}, {'em_g'}, 0); % ^^^^

% set up typical bounds for the first iteration
fititer.SetLB({'all'}, {'conc'}, 0);
fititer.SetLB({'all'}, {'pc12'}, -5);
fititer.SetUB({'all'}, {'pc12'}, 5);

% add prior knowledge as linear inequality constraints
%	fititer.AddLinPriors({'Cr', 'GABA'}, {'conc'}, [-0.25, 1]);	% GABA < 0.25*cre
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'GABA'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% GABA < 0.1*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'NAA_as'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% NAA_as < 0.1*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'Asp'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% Asp < 0.1*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'Scyllo'}, {'conc'}, [-0.06, -0.06, -3*0.06, 1]);	% Scyllo < 0.06*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3
%	fititer.AddLinPriors({'Cr', 'NAA_ac', 'tCho_P', 'Tau'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% Tau < 0.1*(cre + NAA_ac + 3*tCho_P) see LCMODEL manual Big3
%	fititer.AddLinPriors({'Asp', 'Gln'}, {'conc'}, [1, -1]);	% Asp < Gln

% define update function handles
fititer.UpdateLowerBounds    = @LB3;
fititer.UpdateUpperBounds    = @UB3;
fititer.UpdateStartingValues = @Starting3;

fititer.use_sp_baseline     = true;
fititer.spbase_knot_spacing = [0.5 0.5];
fititer.spbase_lambda       = 0e-1*[1 1];

fititer.UpdateIteration;

% add the just configured iteration to the queue
fitsettings.AddIteration(fititer);
%% set up forth fitting iteration ************************************************************************
% ********************************************************************************************************

    function obj = Starting4(obj, res)
        
        % first copy the previous fit values into the starting value fields
        % for the current iteration
        mobj = ?Parameters;
        for par_cnt=1:length(mobj.Properties)
            current_par = mobj.Properties{par_cnt}.Name;
            mask = res.full_mask.(current_par).full(any(res.full_mask.(current_par).full,2), :);
            for fit_cnt=1:length(res.fitted_values.(current_par))
                fitted_value = res.fitted_values.(current_par)(fit_cnt);
                obj.full_start_values.(current_par)(mask(fit_cnt,:)) = fitted_value;
            end
        end
        obj.SetStartingValues({'all'}, {'em_g', 'gm'}, 0);	%######
%         obj.SetStartingValues({'Leu'}, {'gm'}, 2);
        obj.SetStartingValues({'all'}, {'pc12'}, 0);
        obj.UpdateIteration;
        obj = sanityCheckCorrection(obj);
    end

    function obj = LB4(obj, res)
        
        % keep the lower bounds from the previous iteration
        obj.full_lower_bounds.pc0 = res.full_lower_bounds.pc0;
        obj.full_lower_bounds.df2 = res.full_lower_bounds.df2;
        obj.full_lower_bounds.em = res.full_lower_bounds.em;
%         obj.full_lower_bounds.em_g = res.full_lower_bounds.em_g; % ##
%         obj.full_lower_bounds.gm = res.full_lower_bounds.gm; % ##
        obj.SetLB({'all'}, {'gm'}, 0); % ##
        obj.SetLB({'all'}, {'em_g'}, 0);  % ##
        obj.SetLB({'Leu'}, {'gm'}, 0); %should always be true
        obj.SetLB({'all'}, {'pc12'}, -5);
        obj.UpdateIteration;
    end

    function obj = UB4(obj, res)
        
        % keep the upper bounds from the previous iteration
        obj.full_upper_bounds.pc0 = res.full_upper_bounds.pc0;
        obj.full_upper_bounds.df2 = res.full_upper_bounds.df2;
        obj.full_upper_bounds.em = res.full_upper_bounds.em;
%         obj.full_upper_bounds.em_g = res.full_upper_bounds.em_g;  % ##
%         obj.full_upper_bounds.gm = res.full_upper_bounds.gm;  % ##
        obj.SetUB({'all'}, {'gm'}, 1);  % ##
        obj.SetUB({'all'}, {'em_g'}, 1);  % ##
%         obj.SetUB({'Leu'}, {'gm'}, 15);
        obj.SetUB({'all'}, {'pc12'}, 5);
        obj.UpdateIteration;
    end

fititer = Iteration(basis);

fititer.ActivateMets({'NAA_ac', 'Cr', 'Cr_CH2', 'tCho_P', 'mI', 'Glu', 'Gln', 'NAAG', 'GABA', 'NAA_as', 'Lac', ...
    'Scyllo','Tau','Glyc','GSH', 'Asp', 'Leu'});

fititer.ConnectMets({'name'}, {'conc'});
fititer.ConnectMets({'global'}, {'em_g', 'gm', 'pc0', 'pc11', 'pc12', 't1'});

% 	% untangle some multiplets to allow for separate relaxation rates
% 	fititer.met_connections.em(3,4) = false;	% separate naa sub signals
% 	fititer.met_connections.em(4,3) = false;
% 	% separate choline multiplets from singlets so they will have different relaxations
% 	idx = sub2ind(size(fititer.met_connections.em), [5 6 8 5 6 8 7 7 7 9 9 9], [7 7 7 9 9 9 5 6 8 5 6 8]);
% 	fititer.met_connections.em(idx) = false;

% keep concetration values for Cre303 & Cr391 the same
% fititer.ConnectMets({'Cr', 'Cr_CH2'}, {'conc'});
% fititer.ConnectMets({'NAA_ac', 'NAA_as'}, {'conc'});
% fititer.ConnectMets({'Leu'}, {'em', 'em_g', 'gm'}); !!!! should not be need or true

% fix parameters to certain values
fititer.FixMets({'all'}, {'t1'});
fititer.SetFixedValues({'all'}, {'t1'}, Inf);
fititer.FixMets({'all'}, {'df1'});
fititer.SetFixedValues({'all'}, {'df1'}, 0);
fititer.FixMets({'all'}, {'pc11'});
fititer.SetFixedValues({'all'}, {'pc11'}, 0);
% fititer.FixMets({'Leu'}, {'em', 'gm', 'em_g'});
% fititer.SetFixedValues({'Leu'}, {'em', 'gm', 'em_g'}, 0);
fititer.FixMets({'Leu'}, {'em', 'em_g'});
fititer.SetFixedValues({'Leu'}, {'em', 'em_g'}, 0);

%TODO change!
% fititer.FixMets({'all'}, {'pc12'});
% fititer.SetFixedValues({'all'}, {'pc12'}, 0);
% fititer.FixMets({'all'}, {'pc0'});
% fititer.SetFixedValues({'all'}, {'pc0'}, 0);

% set up typical bounds for the first iteration
fititer.SetLB({'all'}, {'conc'}, 0);

% add prior knowledge as linear inequality constraints
%	fititer.AddLinPriors({'cre303', 'gaba'}, {'conc'}, [-0.25, 1]);	% gaba < 0.25*cre
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'gaba'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% gaba < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'glc'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% glc < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'asp'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% asp < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'scy'}, {'conc'}, [-0.06, -0.06, -3*0.06, 1]);	% scy < 0.06*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'cre303', 'naa', 'tcho', 'tau'}, {'conc'}, [-0.1, -0.1, -3*0.1, 1]);	% tau < 0.1*(cre + naa + 3*tcho) see LCMODEL manual Big3
%	fititer.AddLinPriors({'asp', 'gln'}, {'conc'}, [1, -1]);	% asp < gln

% define update function handles
%TODO change back? if we add spline lineshape
fititer.UpdateLowerBounds    = @LB4;
fititer.UpdateUpperBounds    = @UB4;
fititer.UpdateStartingValues = @Starting4;
% fititer.UpdateLowerBounds    = @LB3;
% fititer.UpdateUpperBounds    = @UB3;
% fititer.UpdateStartingValues = @Starting3;

% turn on nonparametric spline baseline part
fititer.use_sp_baseline     = true;
fititer.spbase_knot_spacing = [0.5 0.5];
fititer.spbase_lambda       = 0e-1*[1 1];

% also turn on spline line shape model
fititer.use_sp_lineshape = true;
fititer.full_apply_sp_lineshape(fititer.met_boolidx('Leu')) = false; % turn off deconv line shape for baseline

fititer.UpdateIteration;

% add the just configured iteration to the queue
fitsettings.AddIteration(fititer);
%% save protocol file
save(outputFile, 'fitsettings', 'data');


end
