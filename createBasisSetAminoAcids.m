function createBasisSetAminoAcids()

plotBasis = true;

%% add amino acids
pathName = 'AA basis path';
sampleCriteria = {''};
[filePath] = pathToDataFolder(pathName, sampleCriteria);

aminoAcidNames = {'ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY', 'HIS', ...
    'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL'};

useAmides = false;

fids = cell(1, length(aminoAcidNames));

for indexMetabolite = 1:length(aminoAcidNames)
    fileName = aminoAcidNames{indexMetabolite};
    if useAmides == false
        fileName = [fileName, '_visible'];
    end
    [fids{indexMetabolite}, bandwidth, scanFrequency, metaboliteName, singletPresent] = ...
        ImportLCModelBasis(filePath, fileName, plotBasis, '1H');
end

basis = basis_set;
basis.bw = [1, bandwidth];
basis.dt = 1 ./ basis.bw;
basis.f0 = scanFrequency*1e6;
basis.fid = fids;
basis.met = aminoAcidNames;
basis.pts = size(fids{1});

%% add singlets
metaboliteFileNames = {'Cr_singlet_tot_3.925', 'NAA_asp_DF'};
metaboliteNames =     {'Cr(CH2)',              'NAA'}; 

for indexMetabolite = 1:length(metaboliteNames)
    fileName = metaboliteFileNames{indexMetabolite};
    [fid(1,:), bandwidth, scanFrequency, metaboliteName, singletPresent] = ...
        ImportLCModelBasis(filePath, fileName, plotBasis, '1H');
    % add metabolite to basis set
    if ~all(size(fid)==basis.pts)
        error(['Wrong size of input FID: ' metaboliteName ' : ' num2str(size(fid)) ' vs. ' num2str(basis.pts)]);
    end
    
    basis.met = horzcat(basis.met, metaboliteNames{indexMetabolite});
    basis.fid = horzcat(basis.fid, fid);
end

save([filePath, 'basis_set.mat'], 'basis')