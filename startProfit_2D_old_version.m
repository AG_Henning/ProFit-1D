%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   PROFIT                                                                %
%                                                                         %
%   Run Profit to include proper path and files: just run startProfit.m   %
%   and select the batch file to process in .txt format                   % 
%   (possibly created with prepareProfitReadInFile.m)                     %
%                                                                         %
%   Patrik Wyss (pawyss@biomed.ee.ethz.ch)                                %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function startProfit(textFileToRun)
%STARTPROFIT retrieves from parameter or GUI user input the path on which
%the Profit fitting shall be executed. See more PROFIT_BATCH .
%Creates .txt files for the execution containing the input and output
%parameters and paths.

% add the path with all the profit code
[pathToProfit,~,~]=fileparts(pwd);
addpath(genpath([pathToProfit '/profit3']));    

%retrieve the file (either from function parameter or from file
%explorer.) which contains information about the input files (SDAT & SPAR),
%the output file (i.e. the matrix) and the corresponding settings file.
switch nargin 
    case 1
        status=0;
        nameOfOutputFile=textFileToRun;
    otherwise
        [nameOfOutputFile, status]=prepareProfitReadInFile;
        
end

if (~isempty(nameOfOutputFile)&&status==0)
    [pathname,filename_curr,ext] = fileparts(nameOfOutputFile);
        filenames=['/' filename_curr ext]; 
else
    [filenames, pathname] = uigetfile('*.txt',...               % Select files
                            ['Please select .txt file, where all' ...
                              'files to proceed are listed.'] ,...
                            pwd,'MultiSelect','on');
end   
    
%create a profit directory where to store the processed fitting results
if (exist([pathname, '/profit/'],'dir')==0)
        mkdir([pathname '/profit/']);
end

%handle single or multiple input files and call the profit batch process
if ischar(filenames)
    filename_act=filenames;
    disp(['Processing ' pathname filename_act])
    profit_batch([pathname filename_act])
else
    disp(['You selected multiple files:' filenames])
    for numOfFile=1:size(filenames,2)
        filename_act=filenames{numOfFile};
        disp(['Processing: ' pathname filename_act])
        profit_batch([pathname filename_act])
    end
end