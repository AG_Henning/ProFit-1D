function createBasisSetFromMrui_2D()

pathName = 'DF data path';
sampleCriteria = {'Basis_sets'};
[filePath] = pathToDataFolder(pathName, sampleCriteria);

metaboliteNames = {... 'Leu', ...
    'Asp', 'Cr',                   'Cr_CH2',               'GABA', 'Gln', 'Glu', ...
    'Glyc', 'GSH', 'Lac', 'mI', 'NAA_as', 'NAA_ac', 'NAAG', 'tCho_P',  'Scyllo', 'Tau'};
metaboliteFileNames = {... 'MMB_without_metabolite_TE24', ...
    'Asp', 'Cr_singlet_tot_3.028', 'Cr_singlet_tot_3.925', 'GABA', 'Gln', 'Glu',...
    'Glyc', 'GSH', 'Lac', 'mI', 'NAA_1',  'NAA_2',  'NAAG', 'tCho_PE', 'Scyllo', 'Tau'};    

filePathBasis = strcat(filePath,  'Basis_sets/final_T2_met_MM_paper/');
pathMM = 'sLASER_MM/';
pathMetabolites = 'sLASER_new_UF_7ppm/TE24/';

%%TODO change ############################################################
TEsteps = 15;
metaboliteNames = {'creatine', 'lactate', 'myo-inositol'};
metaboliteFileNames = {'creatine', 'lactate', 'myo-inositol'};  
filePathBasis = 'D:\Software\Spectro Data\TestDataProFit\Rubbish/';
suffixMet = '_0_0.txt';
pathMM = 'sLASER_MM/'; %not given yet
pathMetabolites = '';
%% #######################################################################

basisFileName = 'J-sLASER_basis';
fids = cell(1, length(metaboliteNames));

for indexMetabolite = 1:length(metaboliteNames)
    fileNameMet = metaboliteFileNames{indexMetabolite};
    if strcmp(metaboliteNames{indexMetabolite},'Leu')
        filePathMetabolite = strcat(filePathBasis, pathMM);
    else
        filePathMetabolite = strcat(filePathBasis, pathMetabolites);
    end
    for indexTE = 0:TEsteps
        [data, ~, samplingInterval, transmitterFrequency, ~, ~, ~] = ...
            importMrui(filePathMetabolite, [fileNameMet '-' num2str(indexTE) suffixMet]);
        if indexTE == 0
            fid = zeros(TEsteps+1, length(data));
            fid(indexTE+1,:) = data;
        else
            fid(indexTE+1,:) = data;
        end
    end
    scanFrequency = transmitterFrequency * 1e-6;
    bandwidth = 1/samplingInterval * 1e3;
    if strcmp(metaboliteNames{indexMetabolite},'NAA_ac')
        fid = fid * 3;
    end
    if strcmp(metaboliteNames{indexMetabolite},'Leu')
        fids{indexMetabolite}(:,:)= fid(:,1:4096); % stupid change to make all metabolites 4096 long!
    else
        fids{indexMetabolite}(:,:)= fid;
    end
end

basis = basis_set;
basis.bw = [1, bandwidth];
basis.dt = 1 ./ basis.bw;
basis.f0 = scanFrequency*1e6;
basis.fid = fids;
basis.met = metaboliteNames;
basis.pts = size(fids{1});

save([filePathBasis, basisFileName '.mat'], 'basis')